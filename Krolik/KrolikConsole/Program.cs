﻿using Krolik.Classifiers;
using Krolik.Infrastructure;
using Krolik.Mathematics;
using KrolikShell;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KrolikConsole {
    internal class Program {
        static void Main(string[] args) {
            //MakePointsOnPlane();
            //return;
            //GeneratePlanes(new[] {
            //    "DataForGeneration\\planes-dim4-try1-1.txt",
            //    "DataForGeneration\\planes-dim4-try1-2.txt",
            //}, "planes-dim4-try1-");
            //return;
            var inputFile = args.Length > 0
                ? args[0]
                : null;
            var outputFile = args.Length > 1
                ? args[1]
                : null;

            var initCmds = new Queue<string>();
            if (!string.IsNullOrWhiteSpace(inputFile)) {
                initCmds.Enqueue("load " + inputFile);
                initCmds.Enqueue("exit");
            } else {
                foreach (var initCmd in new string[] {
                    //"load Scripts\\cars1.txt",
                })
                    initCmds.Enqueue(initCmd);
            }

            IShell shell = new ShellBase();
            List<string> customOutput = string.IsNullOrWhiteSpace(outputFile)
                ? null
                : new List<string>();
            shell.Run(
                () => {
                    if (initCmds.Any()) {
                        var initCmd = initCmds.Dequeue();
                        Console.WriteLine(initCmd);
                        if (customOutput != null) {
                            customOutput.Add(initCmd);
                        }
                        return initCmd;
                    } else
                        return Console.ReadLine();
                },
                (msg) => {
                    if (customOutput != null) {
                        customOutput.Add(msg);
                    }
                    Console.Write(msg);
                }
            );
            if(customOutput != null) {
                using (var sw = new StreamWriter(outputFile, false)) {
                    foreach (var line in customOutput) {
                        sw.WriteLine(line);
                    }
                }
            }
            Console.ReadKey();
        }

        private static void PrepareData() {
            foreach (var cls in new[] {
                Tuple.Create("Data\\SinAndCos\\sinFft500.txt", (Cls)1),
                Tuple.Create("Data\\SinAndCos\\cosXFft500.txt", (Cls)2),
            }) {
                using (var sr = new StreamReader(cls.Item1))
                using (var sw = new StreamWriter(cls.Item1 + "-t")) {
                    string line;
                    while ((line = sr.ReadLine()) != null) {
                        line = line + " " + cls.Item2.ToString();
                        sw.WriteLine(line);
                    }
                }
            }
        }

        private class PlaneAndPoint {
            public IPlane Plane { get; set; }
            public decimal[] Point { get; set; }

            public PlaneAndPoint(IPlane plane, decimal[] point) {
                this.Plane = plane;
                this.Point = point;
            }
        }

        public static void MakePointsOnPlane() {
            var points = new[] {
                new decimal[] { 1, 0, 0, },
                new decimal[] { 0, 1, 0, },
                new decimal[] { 0, 0, 1, },
            };
            var precision = new CustomPrecisionProvider(1e-9m, 1m);
            var plane = LinearAlgebra.Get().MakePlane(points, precision);
            var madePoints = Enumerable.Range(0, 100)
                .Select(x => plane.Make(new decimal[] { 1, 0, 0, }, 10m))
                .ToList();
            var lines = madePoints
                .Select(p => string.Join(";", p.Select(c => c.ToString("f3"))))
                .ToList();
            File.WriteAllLines(
                "C:\\madePoints.csv",
                new[] { "x1;x2;x3" }.Union(lines)
            );
        }

        private static void GeneratePlanes(
            string[] fileNames,
            string filePrefix = "planes-",
            int dotsPerPlane = 10
        ) {
            var linal = LinearAlgebra.Get();
            var precision = new CustomPrecisionProvider(1e-3m, 1m);
            var dict = new Dictionary<Cls, List<PlaneAndPoint>>();
            var clsIndex = 0;
            foreach (var fileName in fileNames) {
                var cls = (Cls)(++clsIndex);
                var lines = File.ReadAllLines(fileName);
                var parts0 = lines[0].Split(' ')
                    .Select(x => x.Trim().ToDecimal().Value)
                    .ToArray();
                var dim = parts0.Length;
                var planesAndPoints = new List<PlaneAndPoint>();
                for (int i = 0; i < lines.Length; i += dim) {
                    var linesLocal = new List<decimal[]>();
                    for (int j = i; j < i + dim; j++) {
                        linesLocal.Add(lines[j].Split(' ')
                            .Select(x => x.Trim().ToDecimal().Value)
                            .ToArray()
                        );
                    }
                    planesAndPoints.Add(new PlaneAndPoint(
                        linal.MakePlane(linesLocal, precision),
                        parts0
                    ));
                }
                dict[cls] = planesAndPoints;
            }
            var data = new Dictionary<Cls, List<decimal[]>>();
            foreach (var pair in dict) {
                var dcms = new List<decimal[]>();
                foreach (var item in pair.Value) {
                    for (int i = 0; i < dotsPerPlane; i++) {
                        dcms.Add(item.Plane.Make(item.Point, 2));
                    }
                }
                data[pair.Key] = dcms;
            }
            PrintData(data, filePrefix);
        }

        private static void GeneratePlanes() {
            var linal = LinearAlgebra.Get();
            var precision = new CustomPrecisionProvider(1e-3m, 1m);
            var gr1 = new List<PlaneAndPoint> {
                new PlaneAndPoint(linal.MakePlane(new List<decimal[]> {
                    new decimal[] { 0, 0, (decimal)1.0, },
                    new decimal[] { 1, 0, (decimal)1.5, },
                    new decimal[] { 0, 1, (decimal)0.5, },
                }, precision), new decimal[] { 0, 0, (decimal)1.0, }),
                new PlaneAndPoint(linal.MakePlane(new List<decimal[]> {
                    new decimal[] { 0, 0, (decimal)1.0, },
                    new decimal[] { 1, 0, (decimal)1.5, },
                    new decimal[] { 0, 1, (decimal)2.0, },
                }, precision), new decimal[] { 0, 0, (decimal)1.0, }),
            };
            var gr2 = new List<PlaneAndPoint> {
                new PlaneAndPoint(linal.MakePlane(new List<decimal[]> {
                    new decimal[] { 0, 0, (decimal)5.0, },
                    new decimal[] { 1, 0, (decimal)4.5, },
                    new decimal[] { 0, 1, (decimal)4.0, },
                }, precision), new decimal[] { 0, 0, (decimal)5.0, }),
                new PlaneAndPoint(linal.MakePlane(new List<decimal[]> {
                    new decimal[] { 0, 0, (decimal)5.5, },
                    new decimal[] { 1, 0, (decimal)5.0, },
                    new decimal[] { 0, 1, (decimal)5.5, },
                }, precision), new decimal[] { 0, 0, (decimal)5.5, }),
            };
            
            Cls cls1 = (Cls)1, cls2 = (Cls)2;
            var data = new Dictionary<Cls, List<decimal[]>> {
                [(Cls)1] = new List<decimal[]>(),
                [(Cls)2] = new List<decimal[]>(),
            };
            foreach (var cls in new[] {
                Tuple.Create((Cls)1, gr1),
                Tuple.Create((Cls)2, gr2),
            }) {
                foreach (var item in cls.Item2) {
                    for (int i = 0; i < 10; i++) {
                        data[cls.Item1].Add(
                            item.Plane.Make(item.Point, 2)
                        );
                    }
                }
            }
            PrintData(data);
        }

        private static void PrintData(
            Dictionary<Cls, List<decimal[]>> data,
            string filePrefix = "planes-"
        ) {
            foreach (var datum in data) {
                var fl = new[] { datum.Key.ToString(), };
                var lines = datum.Value.Select(p => string.Join(" ", p.Select(c => c.ToString())));
                File.WriteAllLines(
                    filePrefix + datum.Key.ToString() + ".txt",
                    fl.Concat(lines)
                );
            }
        }

        private static void GenerateData() {
            var data = new Dictionary<Cls, List<decimal[]>>();
            var classes = new Dictionary<Cls, Func<decimal, decimal>> {
                { (Cls)1, x => (decimal)Math.Sin((double)x) },
                { (Cls)2, x => 1m + (decimal)Math.Sin((double)x + 1) },
            };
            var rand = new Random(DateTime.Now.Millisecond);
            var noise = (Func<decimal, decimal>)(x => x + (decimal)rand.NextGaussian(0, 1));
            foreach (var cls in classes) {
                data[cls.Key] = new List<decimal[]>();
            }
            decimal step = 2e-1m;
            int stepsCount = 30;
            for (int i = 0; i < stepsCount; i++) {
                var x = step * i;
                foreach (var cls in classes) {
                    var y = cls.Value(x);
                    data[cls.Key].Add(new[] { x, y, });
                }
            }
            using (var sw = new StreamWriter("trig-2-train.txt")) {
                for (int i = 0; i < stepsCount; i += 2) {
                    foreach (var d in data) {
                        sw.WriteLine(
                            string.Join(" ", d.Value[i].Select(x => x.ToString())) 
                            + " " + d.Key.Index
                        );
                    }
                }
            }
            using (var sw = new StreamWriter("trig-2-control.txt")) {
                for (int i = 1; i < stepsCount; i += 2) {
                    foreach (var d in data) {
                        sw.WriteLine(
                            string.Join(" ", d.Value[i].Select(x => x.ToString()))
                            + " " + d.Key.Index
                        );
                    }
                }
            }
        }

        private static void MakeIntegers() {
            foreach (var file in new[] {
                "Data\\PlaneCalculationFailure\\PlaneCase1.txt",
                "Data\\PlaneCalculationFailure\\PlaneCase2.txt",
                "Data\\PlaneCalculationFailure\\PlaneCase3.txt",
                "Data\\PlaneCalculationFailure\\PlaneCase4.txt",
            }) {
                var lines = new List<string[]>();
                using (var sr = new StreamReader(file)) { 
                    string line;
                    while((line = sr.ReadLine()) != null) {
                        lines.Add(line.Split(' ').Select(x => x.Trim()).ToArray());
                    }
                }
                var go = true;
                while (go) {
                    go = false;
                    var lines2 = new List<string[]>();
                    foreach (var line in lines) {
                        var line2 = new List<string>();
                        foreach (var number in line) {
                            string number2;
                            var indexOfComma = number.IndexOf(',');
                            if (indexOfComma >= 0) {
                                go = true;
                                number2 = number.Substring(0, indexOfComma) +
                                    number.Substring(indexOfComma + 1, 1) +
                                    "," +
                                    number.Substring(indexOfComma + 2);
                                if (number2.EndsWith(","))
                                    number2 = number2.Substring(0, number2.Length - 1);
                            } else {
                                number2 = number + "0";
                            }
                            line2.Add(number2);
                        }
                        lines2.Add(line2.ToArray());
                    }
                    lines = lines2;
                }
                using(var sw = new StreamWriter(file + "-int.txt")) {
                    foreach (var line in lines) {
                        sw.WriteLine(string.Join(" ", line));
                    }
                }
            }
        }

        //private static void FindMaximums(string src, string dst) {
        //    var files = Directory.GetFiles(src);
        //    var maximums = new Dictionary<string, List<decimal>>();
        //    foreach (var file in files) {
        //        string line;
        //        using (var sr = new StreamReader(file)) {
        //            var currentClass = sr.ReadLine();
        //            if (!maximums.ContainsKey(currentClass))
        //                maximums[currentClass] = new List<decimal>();
        //            while ((line = sr.ReadLine()) != null) {
        //                var parts = line.Split(' ');
        //                var max = parts
        //                    .Select(x => x.ToDouble())
        //                    .Where(x => x.HasValue)
        //                    .Select(x => x.Value)
        //                    .Max();
        //                maximums[currentClass].Add(max);
        //            }
        //        }
        //    }
        //    foreach (var cls in maximums) {
        //        using (var sw = new StreamWriter(dst + "\\" + cls.Key + ".txt")) {
        //            foreach (var v in cls.Value) {
        //                sw.WriteLine(v.ToString());
        //            }
        //        }
        //    }
        //}

        //private static void FindMaximums(string src, string dst, int maximumsCount) {
        //    var maximums = _findMaximums(src, dst, 2);
        //    foreach (var cls in maximums) {
        //        using (var sw = new StreamWriter(dst + "\\" + cls.Key + ".txt")) {
        //            foreach (var v in cls.Value) {
        //                sw.WriteLine(string.Join(" ", v.Select(x => x.Y.ToString())));
        //            }
        //        }
        //    }
        //}

        //private static void MakeLines(string src, string dst) {
        //    var maximums = _findMaximums(src, dst, 2);
        //    foreach (var cls in maximums) {
        //        using (var sw = new StreamWriter(dst + "\\" + cls.Key + ".txt")) {
        //            foreach (var v in cls.Value) {
        //                if (v.Count != 2)
        //                    throw new Exception("There must be 2 points.");
        //                if (v[1].X == v[0].X)
        //                    throw new Exception("X values must be different.");
        //                var k = (v[1].Y - v[0].Y)
        //                    / (v[1].X - v[0].X);
        //                var b = v[0].Y - k * v[0].X;
        //                sw.WriteLine(b.ToString() + " " + k.ToString());
        //            }
        //        }
        //    }
        //}

        //private static Dictionary<string, List<List<Point>>> _findMaximums(string src, string dst, int maximumsCount) {
        //    var files = Directory.GetFiles(src);
        //    var maximums = new Dictionary<string, List<List<Point>>>();
        //    foreach (var file in files) {
        //        string line;
        //        using (var sr = new StreamReader(file)) {
        //            var currentClass = sr.ReadLine();
        //            if (!maximums.ContainsKey(currentClass))
        //                maximums[currentClass] = new List<List<Point>>();
        //            var st = maximums[currentClass];
        //            while ((line = sr.ReadLine()) != null) {
        //                var local = new List<Point>();
        //                var parts = line.Split(' ');
        //                var points = parts
        //                    .Select(x => x.ToDouble())
        //                    .Where(x => x.HasValue)
        //                    .Select((x, i) => new Point(i, x.Value))
        //                    .ToArray();
        //                decimal lastMax = -1;
        //                for (int i = 0; i < maximumsCount; i++) {
        //                    Point? cur = null;
        //                    for (int j = 0; j < points.Length; j++) {
        //                        var y = points[j].Y;
        //                        var ok = true
        //                            && (j == 0 || y >= points[j - 1].Y)
        //                            && (j == points.Length - 1 || y >= points[j + 1].Y)
        //                            && (lastMax < 0 || y < lastMax)
        //                            && (!cur.HasValue || y > cur.Value.Y)
        //                        ;
        //                        if (!ok)
        //                            continue;
        //                        cur = points[j];
        //                    }
        //                    if (cur.HasValue) {
        //                        if(local.Any(x => x.Equals(cur.Value))) {
        //                            int asdf = 0;
        //                        }
        //                        local.Add(cur.Value);
        //                        lastMax = cur.Value.Y;
        //                    } else {
        //                        throw new Exception("Cannot find the next maximum.");
        //                    }
        //                }
        //                st.Add(local);
        //            }
        //        }
        //    }
        //    return maximums;
        //}

        //private struct Point {
        //    public decimal X, Y;

        //    public Point(decimal x, decimal y) {
        //        this.X = x;
        //        this.Y = y;
        //    }
        //}

        private static void Prepare(string srcFolder, string dstFolder, decimal smooth) {
            var files = Directory.GetFiles(srcFolder);
            foreach (var file in files) {
                var fn = Path.GetFileName(file);
                using (var sr = new StreamReader(file))
                using (var sw = new StreamWriter(dstFolder + "\\" + fn)) {
                    string line;
                    while((line = sr.ReadLine()) != null) {
                        if (string.IsNullOrWhiteSpace(line) || line.StartsWith("#"))
                            continue;
                        var parts = line.Split(' ')
                            .Select(x => x.Trim())
                            .Where(x => !string.IsNullOrWhiteSpace(x))
                            .Select(x => {
                                x = x.Replace('.', ',');
                                var d = decimal.Parse(x);
                                //var r = Math.Round(d / smooth);
                                return d.ToString();
                            });
                        var ln = string.Join(" ", parts);
                        sw.WriteLine(ln);
                    }
                }
            }
        }
    }
}
