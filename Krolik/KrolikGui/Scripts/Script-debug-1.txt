﻿set ds = new DataSource(mode: "simple", fileName: "Data\Debug\dbg-train.txt", continuousComponentsCount: 2, precision: 1e-12, divisor: 1e-1);
set gpn = new Gpn(founderType: "sequential-std", fuzzyPrecision: 1e-1);
gpn.Prepare(ds: ds, mode: "e", fileName: "Gpns\Script-debug-1.gpn", savePeriod: 1000);
set ds2 = new DataSource(mode: "simple", fileName: "Data\Debug\dbg-control.txt", continuousComponentsCount: 2, precision: 1e-12, divisor: 1e-1);
gpn.Classify(ds2);