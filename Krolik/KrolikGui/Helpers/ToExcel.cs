﻿using GemBox.Spreadsheet;
using Krolik.Infrastructure;
using System;
using System.Linq;

namespace KrolikGui.Helpers {
    internal class ToExcel {
        public void Convert(Point[] points, Plane[] planes) {
            // Set license key to use GemBox.Spreadsheet in a Free mode.
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            this._savePointsVsPlanes(points, planes);
            this._savePointsVsPoints(points);
        }

        private void _savePointsVsPoints(Point[] points) {
            // Create new empty Excel file.
            var workbook = new ExcelFile();

            // Create new worksheet and set cell A1 value to 'Hello world!'.
            var ws = workbook.Worksheets.Add("Krolik");
            int horizontalOffset = 0, verticalOffset = 0;
            //var firstPlane = planes.FirstOrDefault();
            var firstPoint = points.FirstOrDefault();
            int hfixed = 3, vfixed = 3;
            if (firstPoint != null) {
                horizontalOffset = hfixed + firstPoint.Components.Length;
            }
            //if (firstPlane != null) {
            //    verticalOffset = 1 + firstPlane.Components.Length;
            //}
            if (firstPoint != null) {
                verticalOffset = vfixed + firstPoint.Components.Length;
            }
            for (int i = 0; i < points.Length; i++) {
                var point = points[i];
                ws.Cells[verticalOffset + i, 0].Value = point.Name;
                ws.Cells[verticalOffset + i, 1].Value = point.TrueClass;
                ws.Cells[verticalOffset + i, 2].Value = point.SuggestedClass;
                for (int j = 0; j < point.Components.Length; j++) {
                    ws.Cells[verticalOffset + i, hfixed + j].Value = point.Components[j];
                }
            }
            //for (int i = 0; i < planes.Length; i++) {
            //    var plane = planes[i];
            //    ws.Cells[0, horizontalOffset + i].Value = plane.Name;
            //    for (int j = 0; j < plane.Components.Length; j++) {
            //        ws.Cells[1 + j, horizontalOffset + i].Value = plane.Components[j];
            //    }
            //}
            for (int i = 0; i < points.Length; i++) {
                var point = points[i];
                ws.Cells[0, horizontalOffset + i].Value = point.Name;
                ws.Cells[1, horizontalOffset + i].Value = point.TrueClass;
                ws.Cells[2, horizontalOffset + i].Value = point.SuggestedClass;
                for (int j = 0; j < point.Components.Length; j++) {
                    ws.Cells[vfixed + j, horizontalOffset + i].Value = point.Components[j];
                }
            }
            var distancesMatrix = new float[points.Length, points.Length];
            for (int i = 0; i < points.Length; i++) {
                for (int j = 0; j < points.Length; j++) {
                    var distance = _getDistance(points[j], points[i]);
                    distancesMatrix[i, j] = distance;
                    ws.Cells[verticalOffset + i, horizontalOffset + j].Value = distance;
                }
            }

            this._colorRespectively(
                points,
                //planes,
                ws,
                horizontalOffset,
                verticalOffset,
                distancesMatrix
            );

            SpreadsheetInfo.FreeLimitReached += (sender, e) => {
                ws.Cells[0, 0].Value = "The output is truncated.";
            };

            // Save to XLSX file.
            workbook.Save("Krolik.PointsVsPoints.xlsx");
        }

        private void _savePointsVsPlanes(Point[] points, Plane[] planes) {
            // Create new empty Excel file.
            var workbook = new ExcelFile();

            // Create new worksheet and set cell A1 value to 'Hello world!'.
            var ws = workbook.Worksheets.Add("Krolik");
            int horizontalOffset = 0, verticalOffset = 0;
            int hfixed = 3, vfixed = 2;
            var firstPlane = planes.FirstOrDefault();
            var firstPoint = points.FirstOrDefault();
            if (firstPoint != null) {
                horizontalOffset = hfixed + firstPoint.Components.Length;
            }
            if (firstPlane != null) {
                verticalOffset = vfixed + firstPlane.Components.Length;
            }
            for (int i = 0; i < points.Length; i++) {
                var point = points[i];
                ws.Cells[verticalOffset + i, 0].Value = point.Name;
                ws.Cells[verticalOffset + i, 1].Value = point.TrueClass;
                ws.Cells[verticalOffset + i, 2].Value = point.SuggestedClass;
                for (int j = 0; j < point.Components.Length; j++) {
                    ws.Cells[verticalOffset + i, hfixed + j].Value = point.Components[j];
                }
            }
            for (int i = 0; i < planes.Length; i++) {
                var plane = planes[i];
                ws.Cells[0, horizontalOffset + i].Value = plane.Name;
                ws.Cells[1, horizontalOffset + i].Value = plane.Class;
                for (int j = 0; j < plane.Components.Length; j++) {
                    ws.Cells[vfixed + j, horizontalOffset + i].Value = plane.Components[j];
                }
            }
            var distancesMatrix = new float[points.Length, planes.Length];
            for (int i = 0; i < points.Length; i++) {
                for (int j = 0; j < planes.Length; j++) {
                    var distance = _getDistance(planes[j], points[i]);
                    distancesMatrix[i, j] = distance;
                    ws.Cells[verticalOffset + i, horizontalOffset + j].Value = distance;
                }
            }

            this._colorRespectively(
                points,
                planes,
                ws,
                horizontalOffset,
                verticalOffset,
                distancesMatrix
            );

            SpreadsheetInfo.FreeLimitReached += (sender, e) => {
                ws.Cells[0, 0].Value = "The output is truncated.";
            };

            // Save to XLSX file.
            workbook.Save("Krolik.PointsVsPlanes.xlsx");
        }

        private void _colorAll(
            Point[] points, 
            Plane[] planes,
            ExcelWorksheet ws,
            int horizontalOffset,
            int verticalOffset,
            float[,] distancesMatrix
        ) {
            var maxDistance = Krolik.Infrastructure.Extensions.AsEnumerable(distancesMatrix)
                .Max();
            for (int i = 0; i < points.Length; i++) {
                for (int j = 0; j < planes.Length; j++) {
                    var distance = distancesMatrix[i, j];
                    var rel = distance / maxDistance;
                    var color = (int)(255f * rel);
                    ws.Cells[verticalOffset + i, horizontalOffset + j]
                        .Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(
                            color,
                            color,
                            255
                    ));
                }
            }
        }

        private void _colorRespectively(
            Point[] points,
            Plane[] planes,
            ExcelWorksheet ws,
            int horizontalOffset,
            int verticalOffset,
            float[,] distancesMatrix
        ) {
            for (int i = 0; i < points.Length; i++) {
                for (int j = 0; j < planes.Length; j++) {
                    var distance = distancesMatrix[i, j];
                    var maxDist0 = distancesMatrix.Slice(0, i).Max();
                    var maxDist1 = distancesMatrix.Slice(1, j).Max();
                    var rel0 = distance / maxDist0;
                    var rel1 = distance / maxDist1;
                    var color0 = (int)(255f * rel1);
                    var color1 = (int)(255f * rel0);
                    ws.Cells[verticalOffset + i, horizontalOffset + j]
                        .Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(
                            color0,
                            color0,
                            255
                    ));
                    ws.Cells[verticalOffset + i, horizontalOffset + j]
                        .Style.Borders.SetBorders(
                            MultipleBorders.Bottom,
                            SpreadsheetColor.FromArgb(
                                color1,
                                color1,
                                color1
                            ),
                            LineStyle.Thick
                    );
                }
            }
        }

        private void _colorRespectively(
            Point[] points,
            ExcelWorksheet ws,
            int horizontalOffset,
            int verticalOffset,
            float[,] distancesMatrix
        ) {
            for (int i = 0; i < points.Length; i++) {
                for (int j = 0; j < points.Length; j++) {
                    var distance = distancesMatrix[i, j];
                    var maxDist0 = distancesMatrix.Slice(0, i).Max();
                    var maxDist1 = distancesMatrix.Slice(1, j).Max();
                    var rel0 = distance / maxDist0;
                    var rel1 = distance / maxDist1;
                    var color0 = (int)(255f * rel1);
                    var color1 = (int)(255f * rel0);
                    ws.Cells[verticalOffset + i, horizontalOffset + j]
                        .Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(
                            color0,
                            color0,
                            255
                    ));
                    ws.Cells[verticalOffset + i, horizontalOffset + j]
                        .Style.Borders.SetBorders(
                            MultipleBorders.Bottom,
                            SpreadsheetColor.FromArgb(
                                color1,
                                color1,
                                color1
                            ),
                            LineStyle.Thick
                    );
                }
            }
        }

        private float _getDistance(Point a, Point b) {
            var sum = 0f;
            for (int i = 0; i < a.Components.Length; i++) {
                var diff = b.Components[i] - a.Components[i];
                sum += diff * diff;
            }
            var result = (float)Math.Sqrt(sum);
            return result;
        }

        private float _getDistance(Plane plane, Point point) {
            float numerator = plane.Components[plane.Components.Length - 1];
            float denominator = 0;
            for (int i = 0; i < plane.Components.Length - 1; i++) {
                numerator += point.Components[i] * plane.Components[i];
                denominator += plane.Components[i] * plane.Components[i];
            }
            var denom = (float)Math.Sqrt((float)denominator);
            if (denom == (float)0)
                return float.MaxValue;
            var result = Math.Abs(numerator) / denom;
            return result;
        }

        public class Point {
            public string Name { get; set; }
            public float[] Components { get; set; }
            public string TrueClass { get; set; }
            public string SuggestedClass { get; set; }

            public Point(string name, string trueClass, string suggestedClass, params float[] components) {
                this.Name = name;
                this.TrueClass = trueClass;
                this.SuggestedClass = suggestedClass;
                this.Components = components;
            }
        }

        public class Plane {
            public string Name { get; set; }
            public float[] Components { get; set; }
            public string Class { get; set; }

            public Plane(string name, string cls, params float[] components) {
                this.Name = name;
                this.Class = cls;
                this.Components = components;
            }
        }
    }
}
