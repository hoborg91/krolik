﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GpnVisualizer.Contexts {
    class DrawingContext : MonoGameHelpers.DrawingContext {
        public DrawingContext(
            SpriteBatch sb,
            GameWindow window,
            Texture2D dotTexture,
            SpriteFont defaultFont
        ) : base(
            sb,
            window,
            dotTexture,
            defaultFont
        ) {

        }
    }
}
