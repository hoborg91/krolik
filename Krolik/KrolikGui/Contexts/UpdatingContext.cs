﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GpnVisualizer.Contexts {
    class UpdatingContext : MonoGameHelpers.UpdatingContext {
        [Obsolete]
        public Vector2? PhysicalPosition { get; private set; }

        public void Refresh(
            GameTime time, 
            MouseState mouse, 
            KeyboardState keyboard,
            Vector2 physicalPosition
        ) {
            base.Refresh(time, mouse, keyboard);
            this.PhysicalPosition = physicalPosition;
        }
    }
}
