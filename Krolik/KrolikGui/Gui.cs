﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using Cil.Common;
using GpnVisualizer.Widgets;
using Krolik.Infrastructure;
using Krolik;
using System.Configuration;
using Krolik.Main;

namespace GpnVisualizer {
    public class Gui : Game {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private GraphVm _graph;

        private Contexts.DrawingContext _drawCtx;
        private Contexts.UpdatingContext _updCtx;

        private float _camX, _camY, _zoom = 1;

        private int? _screenWidth, _screenHeight;

        private Matrix
            _viewMatrix,
            _viewMatrixInverse;

        private VisualizerConsole _console;

        private bool _guiLogEnabled = false;

        private PlotRepresentation _plot = new PlotRepresentation();

        private TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();

        private static NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        private int _recentGraphHashCode;

        private async Task<string> _waitInput() {
            var result = await tcs.Task;
            tcs = new TaskCompletionSource<string>();
            return result;
        }

        private KrolikShell.IShell _shell;

        public Gui(string scriptFile = null) {
            if (scriptFile == null)
                scriptFile = "load " + ConfigurationManager.AppSettings["defaultKrolikScript"]; // Scripts\\Script-trig-2.txt"

            graphics = new GraphicsDeviceManager(this) {
                //IsFullScreen = true,
            };

            Content.RootDirectory = "Content";

            this._console = new VisualizerConsole(0, 0, 600, 200, x => {
                tcs.SetResult(x);
                if (x == "switch gui log") {
                    this._guiLogEnabled = !this._guiLogEnabled;
                }
            });

            Task.Run(() => {
                var scriptExecuted = false;
                this._shell = new KrolikShell.ShellBase();
                try {
                    this._shell.Run(() => {
                        if (!scriptExecuted) {
                            scriptExecuted = true;
                            return scriptFile;
                        }
                        return this._waitInput().Result;
                    }, x => {
                        KrolikLoggers.Get.Console.Debug(x);
                        this._console.WriteLine(x);
                    });
                    Exit();
                } catch (Exception ex) {
                    Logger.Error(ex);
                    this._console.WriteLine("An error occured.");
                    this._console.WriteLine(ex.Message);
                    this._console.Disable();
                } finally {
                    try {
                        this._shell.Dispose();
                    } catch(Exception ex) {
                        Logger.Error(ex);
                    }
                }
            });
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            this.IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            this._updCtx = new Contexts.UpdatingContext();

            this._drawCtx = new Contexts.DrawingContext(
                this.spriteBatch,
                this.Window,
                Content.Load<Texture2D>("WhiteDot"),
                Content.Load<SpriteFont>("HelloFont")
            );
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }

        private void _tryMakeFullScreen() {
            //if (this._screenWidth == null || this._screenHeight == null) {
            //    this._screenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width; // GraphicsDevice?.Viewport.Width;
            //    this._screenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height; // GraphicsDevice?.Viewport.Height;
            //    this.Window.Position = new Point(0, 0);
            //    this.Window.IsBorderless = true;
            //    graphics.PreferredBackBufferWidth = (int)this._screenWidth;
            //    graphics.PreferredBackBufferHeight = (int)this._screenHeight;
            //    graphics.ApplyChanges();
            //}
        }

        private class PlotRepresentation {
            public GpnPlot Plot { get; private set; }

            private int _lastVectorsCount = 0;
            private int? _lastGraphHashCode;

            public void Update(IGraphForVisualization graph, IList<IVectorForVisualization> vectors) {
                var vectorsCount = vectors.Count;
                var graphHash = graph?.GetHashCode();
                if (graph == null || !graph.Levels.Any() ||
                    this._lastVectorsCount == vectorsCount && this._lastGraphHashCode == graphHash
                )
                    return;
                this._lastVectorsCount = vectorsCount;
                this._lastGraphHashCode = graphHash;
                this.Plot = this._makeGpnPlotOf(graph, vectors);
            }

            private GpnPlot _makeGpnPlotOf(IGraphForVisualization graph, IEnumerable<IVectorForVisualization> vectors) {
                if (graph != null && vectors != null)
                    new KrolikGui.Helpers.ToExcel().Convert(
                        vectors
                            .Select(v => new KrolikGui.Helpers.ToExcel.Point(
                                "",
                                v.TrueClasses.JoinBy(","),
                                v.SuggestedClasses.JoinBy(","),
                                v.ContinuousComponents.Select(c => (float)c).ToArray())
                            )
                            .ToArray(),
                        graph.Levels.First()
                            .Select(v => new KrolikGui.Helpers.ToExcel.Plane(
                                "",
                                v.IsControlFor.JoinBy(","),
                                v.PlaneCoefficients.Select(c => (float)c).ToArray())
                            )
                            .ToArray()
                    );

                var colors = new[] {
                    Color.Red,
                    Color.Blue,
                    Color.Green,
                    Color.Purple,
                    Color.Orange,
                    Color.Cyan,
                }.ToCycle();
                var colorsDict = new Dictionary<string, Color>();

                var linesGroups = graph.Levels.First()
                    .GroupBy(x => string.Join(",", x.IsControlFor))
                    .Select(x => {
                        var lines = x
                            .Where(lin => lin.PlaneCoefficients != null)
                            .Select(lin => lin.PlaneCoefficients
                                .Select(coeff => (float)coeff).ToArray()
                            )
                            .Select(cs => new GpnPlotLine(cs[0], cs[1], cs[2]))
                            .ToList();
                        var color = colorsDict.GetValueOrDefaultAndAdd(x.Key, colors.GetCurrentAndMoveToNext());
                        return Tuple.Create(
                            (IList<GpnPlotLine>)lines,
                            (Color?)color
                        );
                    })
                    .ToList();

                if (vectors == null || !vectors.Any() || vectors.First().ContinuousComponents.Length != 2)
                    return null;

                var pointsGroups = vectors
                    .GroupBy(x => new {
                        x.IsRealData,
                        ClassesString = string.Join(",", x.TrueClasses),
                    })
                    .Select(x => {
                        IList<Point> points = x
                            .Select(x1 => new Point(
                                (int)x1.ContinuousComponents[0],
                                (int)x1.ContinuousComponents[1])
                            )
                            .ToList();
                        var color = colorsDict.GetValueOrDefaultAndAdd(x.Key.ClassesString, colors.GetCurrentAndMoveToNext());
                        return Tuple.Create(
                            points,
                            (Color?)color.Brighten(x.Key.IsRealData
                                ? 1.5f
                                : 0.7f
                            )
                        );
                    })
                    .ToList();

                return new GpnPlot(linesGroups, pointsGroups);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                try {
                    this._shell.Dispose();
                } catch(Exception ex) {
                    Logger.Error(ex);
                }
                this.Exit();
            }

            this._tryMakeFullScreen();
            this._updatePlot();
            
            var physicalPos = Vector2.Transform(
                this._updCtx.CurrentMousePosition.ToVector2(),
                this._viewMatrixInverse
            );
            this._updCtx.Refresh(
                gameTime,
                Mouse.GetState(),
                Keyboard.GetState(),
                physicalPos
            );
            if (this._graph != null) {
                this._graph.Update(this._updCtx);
            }
            if (this._plot.Plot != null) {
                this._plot.Plot.Update(this._updCtx);
                this._plot.Plot.UpdateLogic(this._graph?.GetHighlightedVertices());
            }
            this._console.Update(this._updCtx);

            this._viewMatrix = Matrix.Identity;

            if (this._updCtx.GetReleasedOnceKeys().Contains(Keys.OemTilde)) {
                this._console.SwitchVisibility();
            }

            this._viewMatrixInverse = Matrix.Invert(this._viewMatrix);

            if (this._updCtx.GetReleasedOnceKeys().Contains(Keys.C))
                Krolik.Classifiers.GpnClassifier.FoundingDebugger.Continue();
            else if (this._updCtx.GetReleasedOnceKeys().Contains(Keys.O))
                Krolik.Classifiers.GpnClassifier.FoundingDebugger.Step();
            else if (this._updCtx.GetReleasedOnceKeys().Contains(Keys.P))
                Krolik.Classifiers.GpnClassifier.FoundingDebugger.Pause();

            base.Update(gameTime);
        }

        private void _updatePlot() {
            return;
            var graph = this._shell.GetGraphForVisualization("gpn");
            var vectorsFromTrainingSet = this._shell.GetVectorsForVisualization("ds", false)
                ?? new List<IVectorForVisualization>();
            var vectorsToBeClassified = this._shell.GetVectorsForVisualization("ds2", true)
                ?? new List<IVectorForVisualization>();
            this._plot.Update(graph, vectorsFromTrainingSet
                .Union(vectorsToBeClassified)
                .ToList()
            );
            var graphHash = graph?.GetHashCode();
            if (graph != null && graphHash != this._recentGraphHashCode) {
                this._recentGraphHashCode = graphHash.Value;
                this._graph = this._makeGraphOf(graph);

                if (this._plot.Plot != null) {
                    this._plot.Plot.Log += x => {
                        if (this._guiLogEnabled)
                            this._console.WriteLine(x);
                    };
                    this._graph.Log += x => {
                        if (this._guiLogEnabled)
                            this._console.WriteLine(x);
                    };
                }
            }
        }

        private GraphVm _makeGraphOf(IGraphForVisualization graph) {
            var result = new GraphVm();
            int x = 0, y = 500,
                stepX = 50, stepY = 50,
                dimX = 20, dimY = 20;
            var verticesRef = new Dictionary<IVertexForVisualization, VertexVm>();
            foreach (var level in graph.Levels) {
                x = 0;
                foreach (var vertex in level) {
                    var v = new VertexVm(new Rectangle(x, y, dimX, dimY)) {
                        Caption = "Vertex",
                        Graph = result,
                        Properties = vertex.Info,
                    };
                    if (vertex.PlaneCoefficients != null && vertex.PlaneCoefficients.Length > 2)
                        v.ObjectProperties[VertexVm.VertexVmObjectProperty.GpnPlotLine] = new GpnPlotLine(
                            (float)vertex.PlaneCoefficients[0],
                            (float)vertex.PlaneCoefficients[1],
                            (float)vertex.PlaneCoefficients[2]
                        );
                    verticesRef[vertex] = v;
                    result.Vertices.Add(v);
                    x += stepX;
                }
                y -= stepY;
            }
            foreach (var level in graph.Levels) {
                foreach (var vertex in level) {
                    foreach (var upper in vertex.Upper) {
                        result.Edges.Add(new EdgeVm {
                            From = verticesRef[vertex],
                            To = verticesRef[upper],
                        });
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.White);

            var transformMatrix = this._viewMatrix;
            spriteBatch.Begin(
                transformMatrix: transformMatrix
            );
            if (this._graph != null)
                this._graph.Draw(this._drawCtx);
            if (this._plot.Plot != null)
                this._plot.Plot.Draw(this._drawCtx);
            spriteBatch.End();

            spriteBatch.Begin();
            try {
                this._console.Draw(this._drawCtx);
            } catch (Exception ex) {
                this._console.WriteLine("Error occured");
                Logger.Error(ex.Message);
                Logger.Error(ex.StackTrace);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void _drawGraphForVisualization(Contexts.DrawingContext _drawCtx, IGraphForVisualization graph) {
            var stepY = 50;
            var stepX = 50;
            var dimX = 20;
            var dimY = 20;
            var y = 400;
            foreach (var level in graph.Levels) {
                var x = 0;
                foreach (var vertex in level) {
                    this._drawVertexForVisualization(_drawCtx, x, y, dimX, dimY);
                    x += stepX;
                }
                y -= stepY;
            }
        }

        private void _drawVertexForVisualization(Contexts.DrawingContext drawCtx, int x, int y, int dimX, int dimY) {
            MonoGameHelpers.DrawingHelper.DrawRectangle(
                drawCtx.SpriteBatch,
                drawCtx.DotTexture,
                new Rectangle(x, y, dimX, dimY),
                Color.White,
                Color.Black,
                1
            );
        }
    }

    public static class ColorExtensions {
        private static int _brighten(int rgb255Component, float percentage) {
            var result = rgb255Component * percentage;
            if (result > 255)
                result = 255;
            return (int)result;
        }

        public static Color Brighten(this Color color, float percentage) {
            if (percentage < 0)
                throw new ArgumentException(nameof(percentage));
            Color result = new Color(
                _brighten(color.R, percentage),
                _brighten(color.G, percentage),
                _brighten(color.B, percentage),
                color.A
            );
            return result;
        }
    }
}
