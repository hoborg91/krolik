﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using MonoGameHelpers;

namespace GpnVisualizer.Widgets {
    class VertexDescriptionBox : Widget {
        public string Caption;
        public string Properties;
        public int Left;
        public int Horizont;

        public VertexDescriptionBox(string caption, IDictionary<string, string> properties, Rectangle vertexBox) {
            this.Caption = caption;
            this.Properties = properties == null
                ? ""
                : string.Join(
                    Environment.NewLine,
                    properties.Select(x => x.Key + ": " + x.Value));
            this.Left = vertexBox.Right + 8;
            this.Horizont = vertexBox.Center.Y;
        }

        protected override Rectangle MouseSensitiveArea {
            get {
                return Rectangle.Empty;
            }
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            var mProp = ctx.DefaultFont.MeasureString(this.Properties);
            var totalHeight = (int)Math.Ceiling(mProp.Y);
            var boxProp = new Rectangle(
                this.Left,
                this.Horizont - (int)Math.Ceiling(totalHeight / 2.0f),
                (int)Math.Ceiling(mProp.X),
                totalHeight
            );
            boxProp.Inflate(4, 4);
            DrawingHelper.DrawText(
                this.Properties,
                boxProp,
                ctx.DotTexture,
                ctx.DefaultFont,
                ctx.SpriteBatch,
                Color.DarkBlue,
                Color.LightYellow
            );
        }

        public override void Scale(int zoomDifference, Point anchor) {
            
        }

        public override bool ContainsExactly(Point point) {
            return false;
        }
    }
}
