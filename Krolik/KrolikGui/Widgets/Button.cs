﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using MonoGameHelpers;

namespace GpnVisualizer.Widgets {
    class Button : Widget {
        // TODO. Use new or inherit?
        public new Rectangle Box;
        public string Caption;
        public Action<UpdatingContext, Button> OnClick;
        public Dictionary<WidgetState, ButtonColors> Colors = new Dictionary<WidgetState, ButtonColors> {
            { WidgetState.Normal, new ButtonColors {
                Bg = Color.White,
                Line = Color.Blue,
                Text = Color.DarkBlue,
            } },
            { WidgetState.Hovered, new ButtonColors {
                Bg = Color.LightYellow,
                Line = Color.Blue,
                Text = Color.DarkBlue,
            } },
            { WidgetState.Pressed, new ButtonColors {
                Bg = Color.Yellow,
                Line = Color.Blue,
                Text = Color.DarkBlue,
            } },
        };

        public Button(Rectangle box, string caption, Action<UpdatingContext, Button> onClick = null) {
            this.Box = box;
            this.Caption = caption;
            this.OnClick = onClick;
            MakeUpdatable(ur => {
                if (true
                    && ur.Context.IsLeftButtonClickOnce
                    && this.OnClick != null
                    && this.MouseSensitiveArea.Contains(
                        ur.Context.PhysicalPosition?.ToPoint() ?? ur.Context.CurrentMousePosition
                    )
                ) {
                    this.OnClick(ur.Context, this);
                }
                return ur;
            });
        }

        protected override Rectangle MouseSensitiveArea {
            get {
                return this.Box;
            }
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            DrawingHelper.DrawRectangle(
                ctx.SpriteBatch,
                ctx.DotTexture,
                this.Box,
                this.Colors[this.State].Bg,
                this.Colors[this.State].Line
            );
            var captionBox = this.Box;
            captionBox.Inflate(2, 2);
            captionBox.X = captionBox.X + 4;
            captionBox.Y = captionBox.Y + 2;
            DrawingHelper.DrawText(
                this.Caption,
                captionBox,
                ctx.DotTexture,
                ctx.DefaultFont,
                ctx.SpriteBatch,
                this.Colors[this.State].Text
            );
        }

        public override void Scale(int zoomDifference, Point anchor) {
            
        }

        public override bool ContainsExactly(Point point) {
            return true;
        }

        public struct ButtonColors {
            public Color Bg, Line, Text;
        }
    }
}
