﻿using Cil.Common;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace GpnVisualizer.Widgets {
    internal class GpnPlot : WidgetGroup {
        private float _plotLimit = 100;

        public GpnPlot(
            IList<Tuple<IList<GpnPlotLine>, Color?>> linesGroups,
            IList<Tuple<IList<Point>, Color?>> pointsGroups
        ) {
            this._buildAuxiliaryLines();
            this._buildLines(linesGroups);
            this._buildPoints(pointsGroups);

            this.MakeHoverable();
            this.MakeDraggble();
            this.MakeScalable(
                Microsoft.Xna.Framework.Input.Keys.E, 
                Microsoft.Xna.Framework.Input.Keys.Q
            );

            this.Id = nameof(GpnPlot) + Rand.Next(1000);
        }

        private void _buildPoints(IList<Tuple<IList<Point>, Color?>> pointsGroups) {
            if (pointsGroups == null)
                return;
            foreach (var pointsGroup in pointsGroups) {
                var color = pointsGroup.Item2 ?? Color.Black;
                foreach (var point in pointsGroup.Item1) {
                    var pointWidget = new GpnPlotPointWidget(
                        point, 
                        color,
                        this.ZoomAcc
                    );
                    this.Content.Add(pointWidget);
                }
            }
        }

        private void _buildLines(
            IList<Tuple<IList<GpnPlotLine>, Color?>> linesGroups,
            float scaleFactor = 1
        ) {
            if (linesGroups == null)
                return;
            foreach (var linesGroup in linesGroups) {
                var color = linesGroup.Item2 ?? Color.Black;
                foreach (var line in linesGroup.Item1) {
                    if (line.A == 0 && line.B == 0)
                        throw new NotImplementedException();
                    GpnPlotLineWidget lineWidget;
                    if (line.A == 0) {
                        var y = -line.C / line.B;
                        lineWidget = new GpnPlotLineWidget(
                            new Vector2(0, y) * scaleFactor,
                            new Vector2(this._plotLimit, y) * scaleFactor,
                            color,
                            this.ZoomAcc,
                            line
                        );
                    } else if (line.B == 0) {
                        var x = -line.A / line.B;
                        lineWidget = new GpnPlotLineWidget(
                            new Vector2(x, 0) * scaleFactor,
                            new Vector2(x, this._plotLimit) * scaleFactor,
                            color,
                            this.ZoomAcc,
                            line
                        );
                    } else if (Math.Abs(line.A) > Math.Abs(line.B)) {
                        var a = new Vector2(-line.C / line.A, 0) * scaleFactor;
                        var b = new Vector2((-line.C - line.B * this._plotLimit) / line.A,
                            this._plotLimit) * scaleFactor;
                        lineWidget = new GpnPlotLineWidget(a, b, color, this.ZoomAcc, line);
                    } else {
                        var a = new Vector2(0, -line.C / line.B) * scaleFactor;
                        var b = new Vector2(this._plotLimit,
                            (-line.C - line.A * this._plotLimit) / line.B) * scaleFactor;
                        lineWidget = new GpnPlotLineWidget(a, b, color, this.ZoomAcc, line);
                    }
                    this.Content.Add(lineWidget);
                }
            }
        }

        private void _buildAuxiliaryLines() {
            var auxiliaryLinesNum = 10;
            var auxiliaryLinesStep = 10f;
            for (int i = 0; i < auxiliaryLinesNum; i++) {
                var progress = i * auxiliaryLinesStep;
                this.Content.Add(new GpnPlotLineWidget(
                    new Vector2(0, progress),
                    new Vector2(this._plotLimit, progress),
                    Color.LightGray,
                    this.ZoomAcc
                ));
                this.Content.Add(new GpnPlotLineWidget(
                    new Vector2(progress, 0),
                    new Vector2(progress, this._plotLimit),
                    Color.LightGray,
                    this.ZoomAcc
                ));
                if (i % 2 != 0)
                    this.Content.Add(new GpnPlotLabelWidget(
                        (progress).ToString("f0"),
                        new Point(0, (int)progress),
                        this.ZoomAcc,
                        GpnPlotLabelWidget.LabelPosition.ForY)
                    );
                if (i % 3 == 0)
                    this.Content.Add(new GpnPlotLabelWidget(
                        (progress).ToString("f0"),
                        new Point((int)progress, 0),
                        this.ZoomAcc,
                        GpnPlotLabelWidget.LabelPosition.ForX)
                    );
            }
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            base.Draw(ctx);
        }

        internal void UpdateLogic(IList<VertexVm> highlightedVertices) {
            if (highlightedVertices == null)
                return;
            var linesFromVertices = new HashSet<GpnPlotLine>();
            foreach (var vertex in highlightedVertices) {
                var lineUncasted = vertex.ObjectProperties
                    .GetValueOrDefault(VertexVm.VertexVmObjectProperty.GpnPlotLine);
                if (lineUncasted == null || !(lineUncasted is GpnPlotLine))
                    continue;
                var line = (GpnPlotLine)lineUncasted;
                linesFromVertices.Add(line);
            }
            foreach (var widget in this.Content) {
                if (!(widget is GpnPlotLineWidget))
                    continue;
                var lineWidget = (GpnPlotLineWidget)widget;
                lineWidget.HighlightAccordingTo(linesFromVertices);
            }
        }

        public override bool ContainsExactly(Point point) {
            var msa = this.MouseSensitiveArea;
            return msa.Contains(point);
        }

        class GpnPlotLineWidget : Widget {
            private Color _color;
            public GpnPlotLine? Line { get; private set; }
            private bool _isHighlighted;

            private readonly Vector2 _A0, _L0;

            private Vector2 L {
                get {
                    var result = this._L0;
                    result *= (100f + this.ZoomAcc.TotalValue) / 100f;
                    return result;
                }
            }

            private Vector2 A {
                get {
                    var result = this._A0 + this.AccumulatedDisplacement;
                    result = this.ZoomAcc.Avalanche(result);
                    return result;
                }
            }

            private Vector2 B {
                get {
                    return this.A + this.L;
                }
            }

            public GpnPlotLineWidget(
                Vector2 a, 
                Vector2 b, 
                Color color, 
                ZoomAccumulator zoomAcc,
                GpnPlotLine? line = null
            ) {
                this._A0 = new Vector2(a.X, -a.Y) + new Vector2(100, 100);
                this._L0 = b - a;
                this._L0 = new Vector2(this._L0.X, -this._L0.Y);
                this._color = color;
                this.Line = line;
                this.ZoomAcc = zoomAcc;
            }

            protected override Rectangle MouseSensitiveArea {
                get {
                    var result = new Rectangle(
                        (int)Math.Min(this.A.X, this.B.X),
                        (int)Math.Min(this.A.Y, this.B.Y),
                        (int)Math.Ceiling(this.L.X),
                        (int)Math.Ceiling(this.L.Y)
                    );
                    return result;
                }
            }
            
            public override void Draw(Contexts.DrawingContext ctx) {
                var width = this.ZoomAcc.TotalValue > 1
                    ? (int)Math.Ceiling(Math.Sqrt(this.ZoomAcc.TotalValue * 0.01f))
                    : 1;
                if(this._isHighlighted) {
                    width = (int)Math.Ceiling(width * 1.5f);
                }
                MonoGameHelpers.DrawingHelper.DrawLine(
                    this.A,
                    this.B,
                    ctx.DotTexture,
                    ctx.SpriteBatch,
                    this._color,
                    width
                );
            }

            internal bool CorrespondsTo(GpnPlotLine line) {
                return this.Line == line;
            }

            internal void HighlightAccordingTo(ISet<GpnPlotLine> linesFromVertices) {
                this._isHighlighted = this.Line.HasValue &&
                    linesFromVertices.Contains(this.Line.Value);
            }

            public override void Scale(int zoomDifference, Point anchor) {
                
            }
            
            public override bool ContainsExactly(Point point) {
                return false;
            }
        }

        class GpnPlotPointWidget : Widget {
            private readonly Vector2 _point;
            private Color _color;
            private bool _isHovered = false;

            private static int SensitiveRadius = 2;

            private Vector2 PointWithDisplacement {
                get {
                    var result = this._point + this.AccumulatedDisplacement;
                    result = this.ZoomAcc.Avalanche(result);
                    return result;
                }
            }

            public GpnPlotPointWidget(Point point, Color color, ZoomAccumulator zoomAcc) {
                this._point = new Vector2(point.X, -point.Y)
                    + new Vector2(100, 100);
                this._color = color;
                this.ZoomAcc = zoomAcc;
                MakeUpdatable(ur => {
                    this._isHovered = this.MouseSensitiveArea.Contains(
                        ur.Context.PhysicalPosition?.ToPoint() ?? 
                        ur.Context.CurrentMousePosition);
                    return ur;
                });
            }

            protected override Rectangle MouseSensitiveArea {
                get {
                    var sensitiveRadius = this.ZoomAcc.TotalValue > 0
                        ? (int)Math.Ceiling(Math.Sqrt((float)this.ZoomAcc.TotalValue * SensitiveRadius * 0.01f))
                        : SensitiveRadius;
                    return new Rectangle(
                        (int)this.PointWithDisplacement.X - sensitiveRadius,
                        (int)this.PointWithDisplacement.Y - sensitiveRadius,
                        sensitiveRadius * 2 + 1,
                        sensitiveRadius * 2 + 1
                    );
                }
            }

            public override void Draw(Contexts.DrawingContext ctx) {
                var r = new Rectangle(
                    new Point((int)this.PointWithDisplacement.X - 1, (int)this.PointWithDisplacement.Y - 1),
                    new Point(3, 3)
                );
                var border = this.ZoomAcc.TotalValue > 1
                    ? (int)Math.Ceiling(Math.Sqrt(this.ZoomAcc.TotalValue * 0.01f))
                    : 1;
                if (this.ZoomAcc.TotalValue > 1) {
                    var infl = (int)Math.Ceiling(Math.Sqrt(this.ZoomAcc.TotalValue * 0.01f));
                    r.Inflate(infl, infl);
                }
                if (this._isHovered) {
                    r.Inflate(
                        (int)Math.Ceiling(r.Width / 2f),
                        (int)Math.Ceiling(r.Height / 2f)
                    );
                }
                MonoGameHelpers.DrawingHelper.DrawRectangle(
                    ctx.SpriteBatch,
                    ctx.DotTexture,
                    r,
                    Color.White,
                    this._color,
                    border
                );
            }

            public override void Scale(int zoomDifference, Point anchor) {
                
            }

            public override bool ContainsExactly(Point point) {
                return false;
            }
        }

        class GpnPlotLabelWidget : Widget {
            private Temporary<Rectangle> _r;

            private string _text;

            private readonly Vector2 _anchor;

            private Vector2 DisplacedAnchor {
                get {
                    var result = this._anchor + this.AccumulatedDisplacement;;
                    result = this.ZoomAcc.Avalanche(result);
                    return result;
                }
            }

            private LabelPosition _positionType;

            private Point _lastStringMeasurement = Point.Zero;

            public GpnPlotLabelWidget(
                string text,
                Point anchor,
                ZoomAccumulator zoomAcc,
                LabelPosition position = LabelPosition.Exact
            ) {
                this._text = text;
                this._anchor = new Vector2(
                    anchor.X,
                    -anchor.Y
                ) + new Vector2(100, 100);
                this._positionType = position;
                this.ZoomAcc = zoomAcc;
                this._r = new Temporary<Rectangle>(() => {
                    return new Rectangle(
                        this.DisplacedAnchor.ToPoint(),
                        this._lastStringMeasurement
                    );
                });
            }

            protected override Rectangle MouseSensitiveArea {
                get {
                    return this._r.GetValue();
                }
            }

            public override void Draw(Contexts.DrawingContext ctx) {
                this._r.RefreshIfNecessary(() => {
                    var measure = ctx.DefaultFont.MeasureString(this._text);
                    this._lastStringMeasurement = measure.ToPoint();
                    var composer = LabelComposer.Get(this._positionType);
                    var shifted = composer.Shift(
                        this.DisplacedAnchor.ToPoint(),
                        measure
                    );
                    return new Rectangle(
                        shifted.X,
                        shifted.Y,
                        (int)measure.X,
                        (int)measure.Y
                    );
                });
                var labelPosition = this._r.GetValue()
                    .Location
                    .ToVector2();
                MonoGameHelpers.DrawingHelper.DrawText(
                    this._text,
                    labelPosition,
                    ctx.DotTexture,
                    ctx.DefaultFont,
                    ctx.SpriteBatch,
                    Color.Black
                );
            }

            protected override void MoveInternal(Point displacement) {
                this._r.Reset();
            }

            public override void Scale(int zoomDifference, Point anchor) {
                this._r.Reset();
            }

            public override bool ContainsExactly(Point point) {
                return false;
            }

            private class Temporary<T> {
                private Func<T> _initial;

                private bool _needRefresh = true;

                private T _value;

                public Temporary(Func<T> initial) {
                    this._initial = initial;
                    this._value = this._initial();
                }

                internal T GetValue() {
                    return this._value;
                }

                internal void Reset() {
                    this._value = this._initial();
                    this._needRefresh = true;
                }

                internal void RefreshIfNecessary(Func<T> refresher) {
                    if (!this._needRefresh)
                        return;
                    this._value = refresher();
                    this._needRefresh = false;
                }
            }

            public enum LabelPosition {
                Exact,
                ForX,
                ForY,
            }

            private static class LabelComposer {
                public static ILabelComposer Get(LabelPosition position) {
                    switch (position) {
                        case LabelPosition.Exact:
                            return LabelComposer.Exact;
                        case LabelPosition.ForX:
                            return LabelComposer.ForX;
                        case LabelPosition.ForY:
                            return LabelComposer.ForY;
                        default:
                            throw new Exception("This type of LabelPosition is not supported.");
                    }
                }

                private static LabelComposerExact _exact;
                private static LabelComposerForX _forX;
                private static LabelComposerForY _forY;

                private static LabelComposerExact Exact {
                    get {
                        if (_exact == null) {
                            _exact = new LabelComposerExact();
                        }
                        return _exact;
                    }
                }

                private static LabelComposerForX ForX {
                    get {
                        if (_forX == null) {
                            _forX = new LabelComposerForX();
                        }
                        return _forX;
                    }
                }

                private static LabelComposerForY ForY {
                    get {
                        if (_forY == null) {
                            _forY = new LabelComposerForY();
                        }
                        return _forY;
                    }
                }
            }

            /// <summary>
            /// Shifts the given anchor according to the
            /// label's purpose (is it for X axis, for Y
            /// axis, or other).
            /// </summary>
            private interface ILabelComposer {
                Point Shift(Point anchor, Vector2 labelSize);
            }

            private class LabelComposerExact : ILabelComposer {
                public Point Shift(Point anchor, Vector2 labelSize) {
                    return anchor;
                }
            }

            private class LabelComposerForX : ILabelComposer {
                public Point Shift(Point anchor, Vector2 labelSize) {
                    return new Point(
                        anchor.X - (int)(labelSize.X / 2),
                        anchor.Y
                    );
                }
            }

            private class LabelComposerForY : ILabelComposer {
                public Point Shift(Point anchor, Vector2 labelSize) {
                    return new Point(
                        anchor.X - (int)labelSize.X,
                        anchor.Y - (int)(labelSize.Y / 2)
                    );
                }
            }
        }
    }

    /// <summary>
    /// Represents a line of type Ax + By + C = 0.
    /// </summary>
    struct GpnPlotLine {
        public float A, B, C;

        public GpnPlotLine(float a, float b, float c) {
            this.A = a;
            this.B = b;
            this.C = c;
        }

        public override int GetHashCode() {
            return (int)(this.A + this.B + this.C);
        }

        public override bool Equals(object obj) {
            if (!(obj is GpnPlotLine))
                return false;
            var line = (GpnPlotLine)obj;
            return true
                && this.A == line.A
                && this.B == line.B
                && this.C == line.C;
        }

        public static bool operator ==(GpnPlotLine a, GpnPlotLine b) {
            return a.Equals(b);
        }

        public static bool operator !=(GpnPlotLine a, GpnPlotLine b) {
            return !(a == b);
        }
    }
}
