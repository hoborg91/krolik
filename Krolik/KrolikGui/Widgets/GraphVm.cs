﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using MonoGameHelpers;
using System;
using Cil.Common;

namespace GpnVisualizer.Widgets {
    class GraphVm : Widget {
        public IList<VertexVm> Vertices;
        public IList<EdgeVm> Edges;

        public bool SomethingHighlighted { get; set; }

        public GraphVm() {
            this.Vertices = new List<VertexVm>();
            this.Edges = new List<EdgeVm>();

            this.MakeHoverable();
            this.MakeDraggble();
            this.MakeUpdatable(ur => {
                // ? Children update ? TODO
                foreach (var vvm in this.Vertices) {
                    vvm.Update(ur.Context);
                }
                foreach (var evm in this.Edges) {
                    evm.Update(ur.Context);
                }
                return ur;
            });

            this.Id = nameof(GraphVm) + Rand.Next(1000);
        }

        protected override Rectangle MouseSensitiveArea {
            get {
                // TODO. Optimize (hide lists of vertices and
                // edges to control access to them). Duplicate
                // code is in WidgetGroup class.
                int
                    minX = this.Vertices.Min(x => x.Box.Left),
                    minY = this.Vertices.Min(x => x.Box.Top),
                    maxX = this.Vertices.Max(x => x.Box.Right),
                    maxY = this.Vertices.Max(x => x.Box.Bottom);
                return new Rectangle(
                    minX,
                    minY,
                    maxX - minX,
                    maxY - minY
                );
            }
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            if(new[] { WidgetState.Hovered, WidgetState.Pressed, }.Contains(this.State))
                MonoGameHelpers.DrawingHelper.DrawRectangle(
                    ctx.SpriteBatch,
                    ctx.DotTexture,
                    this.MouseSensitiveArea,
                    null,
                    Color.Yellow,
                    3
                );
            foreach (var evm in this.Edges) {
                evm.Draw(ctx);
            }
            foreach (var vvm in this.Vertices) {
                vvm.Draw(ctx);
            }
            foreach (var vvm in this.Vertices) {
                if (vvm.Description != null) {
                    vvm.Description.Draw(ctx);
                }
            }
        }

        public IList<VertexVm> GetHighlightedVertices() {
            var result = this.Vertices
                .Where(x => x.Highlighted)
                .ToList();
            return result;
        }

        public void HighlightFor(VertexVm vertexVm, bool to, bool enable) {
            var toHighlight = new Queue<VertexVm>();
            var visited = new List<VertexVm>();
            toHighlight.Enqueue(vertexVm);
            while (toHighlight.Any()) {
                var current = toHighlight.Dequeue();
                visited.Add(current);
                current.Highlight(enable);
                var edges = this.Edges
                    .Where(e => to
                        ? e.To == current
                        : e.From == current
                    )
                    .ToList();
                foreach (var edge in edges) {
                    var v = to
                        ? edge.From
                        : edge.To;
                    toHighlight.Enqueue(v);
                    edge.Highlight(enable);
                }
            }
        }

        protected override void MoveInternal(Point displacement) {
            foreach (var v in this.Vertices) {
                v.Move(displacement);
            }
            foreach (var e in this.Edges) {
                e.Move(displacement);
            }
        }

        public override void Scale(int zoomDifference, Point anchor) {
            foreach (var v in this.Vertices) {
                v.Scale(zoomDifference, anchor);
            }
            foreach (var e in this.Edges) {
                e.Scale(zoomDifference, anchor);
            }
        }

        public override bool ContainsExactly(Point point) {
            return false
                || this.Vertices.Any(x => x.ContainsExactly(point))
                || this.Edges.Any(x => x.ContainsExactly(point))
            ;
        }
    }

    class VertexVm : Widget {
        private Rectangle _box;
        public string Caption = "Vertex";
        public IDictionary<string, string> Properties = new Dictionary<string, string>();
        public GraphVm Graph;

        public Widget Description;
        public Widget Controls;
        public bool Highlighted { get; private set; }

        public IDictionary<VertexVmObjectProperty, object> ObjectProperties = new Dictionary<VertexVmObjectProperty, object>();

        private Rectangle DisplacedBox {
            get {
                return new Rectangle(
                    this._box.Location + this.AccumulatedDisplacement.ToPoint(),
                    this._box.Size
                );
            }
        }
        
        protected override Rectangle MouseSensitiveArea {
            get {
                return this.DisplacedBox;
            }
        }

        public VertexVm(Rectangle box) {
            this._box = box;

            this.MakeHoverable();
            this.MakeUpdatable(ur => {
                if (this.MouseSensitiveArea.Contains(
                    ur.Context.PhysicalPosition?.ToPoint() ?? ur.Context.CurrentMousePosition)
                ) {
                    if (ur.Context.IsLeftButtonClickOnce) {
                        if (this.Description == null) {
                            this.Description = new VertexDescriptionBox(
                                this.Caption,
                                this.Properties,
                                this.DisplacedBox
                            );
                        } else {
                            this.Description = null;
                        }
                    }
                }
                var region = this.DisplacedBox;
                region.Inflate(24, 24);
                if (region.Contains(
                    ur.Context.PhysicalPosition?.ToPoint() ?? ur.Context.CurrentMousePosition)
                ) {
                    if (this.Controls == null)
                        this.ShowControls();
                } else if (this.Controls != null) {
                    this.Controls = null;
                }

                if (this.Description != null) {
                    this.Description.Update(ur.Context);
                }
                if (this.Controls != null) {
                    this.Controls.Update(ur.Context);
                }
                return ur;
            });
        }

        private void ShowControls() {
            var graph = this.Graph;
            this.Controls = new WidgetGroup(
                new Button(
                    new Rectangle(
                        this.DisplacedBox.Center.X - 16,
                        this.DisplacedBox.Bottom + 2,
                        16,
                        16
                    ),
                    "v",
                    (upd1, b1) => {
                        graph.HighlightFor(
                            this, 
                            true,
                            !graph.SomethingHighlighted
                        );
                        graph.SomethingHighlighted =
                            !graph.SomethingHighlighted;
                    }
                ),
                new Button(
                    new Rectangle(
                        this.DisplacedBox.Center.X,
                        this.DisplacedBox.Bottom + 2,
                        16,
                        16
                    ),
                    "^",
                    (upd1, b1) => {
                        graph.HighlightFor(
                            this, 
                            false,
                            !graph.SomethingHighlighted
                        );
                        graph.SomethingHighlighted =
                            !graph.SomethingHighlighted;
                    }
                )
            );
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            DrawingHelper.DrawRectangle(
                ctx.SpriteBatch,
                ctx.DotTexture,
                this.DisplacedBox,
                Color.White,
                this.Highlighted
                    ? Color.Red
                    : Color.CornflowerBlue,
                this.State == WidgetState.Normal
                    ? 1
                    : 3
            );

            var captionBox = new Rectangle(
                this.DisplacedBox.Left + 5,
                this.DisplacedBox.Top + 5,
                this.DisplacedBox.Width - 10,
                this.DisplacedBox.Height - 10
            );
            DrawingHelper.DrawText(
                this.Caption ?? "",
                captionBox,
                ctx.DotTexture,
                ctx.DefaultFont,
                ctx.SpriteBatch,
                Color.DarkBlue
            );
            
            if(this.Controls != null) {
                this.Controls.Draw(ctx);
            }
        }

        public void Highlight(bool v) {
            this.Highlighted = v;
        }

        public override void Scale(int zoomDifference, Point anchor) {
            
        }

        public override bool ContainsExactly(Point point) {
            return true;
        }

        public enum VertexVmObjectProperty {
            GpnPlotLine,
        }
    }

    class EdgeVm : Widget {
        public VertexVm From, To;
        public bool Highlighted { get; private set; }

        protected override Rectangle MouseSensitiveArea {
            get {
                return new Rectangle(
                    Math.Min(From.Box.Center.X, To.Box.Center.X),
                    Math.Min(From.Box.Center.Y, To.Box.Center.Y),
                    Math.Abs(From.Box.Center.X - To.Box.Center.X),
                    Math.Abs(From.Box.Center.Y - To.Box.Center.Y)
                );
            }
        }

        public override bool ContainsExactly(Point point) {
            return false;
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            DrawingHelper.DrawLine(
                this.From.Box.Center.ToVector2(),
                this.To.Box.Center.ToVector2(),
                ctx.DotTexture,
                ctx.SpriteBatch,
                this.Highlighted
                    ? Color.Red
                    : Color.Blue
            );
        }

        public void Highlight(bool v) {
            this.Highlighted = v;
        }

        public override void Scale(int zoomDifference, Point anchor) {

        }
    }
}
