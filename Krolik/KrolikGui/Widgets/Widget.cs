﻿using Cil.Common;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GpnVisualizer.Widgets {
    interface IBox {
        Rectangle Box { get; }
    }

    interface IWidget {
        void Update(Contexts.UpdatingContext ctx);
        void Draw(Contexts.DrawingContext ctx);
        void Move(Point displacement);
        bool ContainsExactly(Point point);
    }

    interface IWithLogging<T> {
        event Action<T> Log;
    }

    abstract class Widget : IWidget, IBox, IWithLogging<string> {
        protected WidgetState State = WidgetState.Normal;
        protected abstract Rectangle MouseSensitiveArea { get; }
        protected virtual string Id { get; set; } = Rand.Next(1000).ToString();

        protected Vector2 AccumulatedDisplacement = Vector2.Zero;

        protected ZoomAccumulator ZoomAcc = new ZoomAccumulator();

        public event Action<string> Log;

        public Rectangle Box {
            get {
                return this.MouseSensitiveArea;
            }
        }

        #region Implement IWidget

        public void Update(Contexts.UpdatingContext ctx) {
            var updatingResult = new UpdatingResult(ctx);
            foreach (var updatingAction in this._updatingPipeline) {
                updatingResult = updatingAction(updatingResult);
            }
        }

        public abstract void Draw(Contexts.DrawingContext ctx);

        /// <summary>
        /// Updates the discplacement of the widget and then
        /// call the MoveInternal method.
        /// </summary>
        /// <param name="displacement"></param>
        public void Move(Point displacement) {
            var displVector = displacement.ToVector2();
            displVector /= ((100f + this.ZoomAcc.TotalValue) / 100f);
            this.AccumulatedDisplacement += displVector;
            this.MoveInternal(displacement);
        }

        /// <summary>
        /// This method is being called after updating the 
        /// dicplacement of the widget in the Move method.
        /// </summary>
        protected virtual void MoveInternal(Point displacement) {

        }

        public abstract bool ContainsExactly(Point point);

        #endregion Implement IWidget

        private List<Func<UpdatingResult, UpdatingResult>> _updatingPipeline = new List<Func<UpdatingResult, UpdatingResult>>();

        protected Widget MakeUpdatable(Func<UpdatingResult, UpdatingResult> updatingAction) {
            if (updatingAction == null)
                throw new ArgumentNullException(nameof(updatingAction));
            this._updatingPipeline.Add(updatingAction);
            return this;
        }

        protected Widget MakeHoverable() {
            Func<UpdatingResult, UpdatingResult> update = ur => {
                if (this.MouseSensitiveArea.Contains(
                    ur.Context.PhysicalPosition?.ToPoint() ?? ur.Context.CurrentMousePosition)
                ) {
                    this.State = WidgetState.Hovered;
                    if (ur.Context.IsLeftButtonPressed)
                        this.State = WidgetState.Pressed;
                } else {
                    this.State = WidgetState.Normal;
                }
                return ur;
            };
            this._updatingPipeline.Add(update);
            return this;
        }

        protected Widget MakeDraggble() {
            this._updatingPipeline.Add(ur => {
                if (ur.Context.IsLeftButtonPressed) {
                    if(this.Contains(ur.Context.CurrentMousePosition))
                        this.Move(ur.Context.CurrentMousePosition
                            - ur.Context.RecentMousePosition);
                }
                return ur;
            });
            return this;
        }

        protected Widget MakeScalable(
            Keys zoomIn, 
            Keys zoomOut, 
            bool enableMouseScrollZoom = true
        ) {
            this._updatingPipeline.Add(ur => {
                if (this.Contains(ur.Context.CurrentMousePosition)) {
                    var zoom = 0;
                    if (enableMouseScrollZoom && ur.Context.Scrolled != 0)
                        zoom = (int)Math.Round((float)ur.Context.Scrolled / 4, MidpointRounding.AwayFromZero);
                    else if (ur.Context.CurrentKeyboardState.IsKeyDown(zoomIn))
                        zoom = 1;
                    else if (ur.Context.CurrentKeyboardState.IsKeyDown(zoomOut))
                        zoom = -1;
                    if (zoom != 0) {
                        this.ZoomAcc.Add(
                            ur.Context.CurrentMousePosition,
                            zoom
                        );
                        this.Scale(zoom, ur.Context.CurrentMousePosition);
                    }
                }
                return ur;
            });
            return this;
        }
        
        // TODO. Generalize code in the implementations of this method.
        public abstract void Scale(int zoomDifference, Point anchor);
        
        protected bool Contains(Point point) {
            var msa = this.MouseSensitiveArea;
            if (!msa.Contains(point))
                return false;
            var log = this.Log == null
                ? x => { }
                : this.Log;
            log($"Widget {this.Id}. MouseSensitiveArea ({msa}) contains {point}.");

            var containsExactly = this.ContainsExactly(point);
            log($"Widget {this.Id}. Contains {point} exaclty.");
            return containsExactly;
        }

        protected class UpdatingResult {
            public Contexts.UpdatingContext Context { get; private set; }

            public UpdatingResult(Contexts.UpdatingContext ctx) {
                this.Context = ctx;
            }
        }

        public enum WidgetState {
            Normal,
            Hovered,
            Pressed,
        }

        protected class ZoomAccumulator {
            private List<ZoomDifference> AccumulatedZoom;

            private int? _totalValue = null;

            internal int TotalValue {
                get {
                    if(this._totalValue == null) {
                        this._totalValue = this.AccumulatedZoom.Sum(x => x.Value);
                    }
                    return this._totalValue.Value;
                }
            }
            
            public ZoomAccumulator() {
                this._resetAccumulatedZoom();
            }

            internal void Add(Point anchor, int zoom) {
                if (zoom == 0)
                    return;
                if(zoom < 0 && this.TotalValue + zoom <= -100) {
                    zoom = -99 - this.TotalValue;
                }
                var last = this.AccumulatedZoom.Last();
                if (last.Anchor == anchor) {
                    this.AccumulatedZoom[this.AccumulatedZoom.Count - 1] =
                        new ZoomDifference(last.Anchor, last.Value + zoom);
                } else {
                    this.AccumulatedZoom.Add(new ZoomDifference(anchor, zoom));
                }
                this._totalValue = null;
            }

            internal Vector2 Avalanche(Vector2 start) {
                var result = start;
                var sum = 100;
                for (int i = 1; i < this.AccumulatedZoom.Count; i++) {
                    var diff = this.AccumulatedZoom[i];
                    var sumNext = sum + diff.Value;
                    var coeff = (float)sumNext / sum;
                    var x = diff.Anchor.ToVector2();
                    result = coeff * (result - x) + x;
                    sum = sumNext;
                }
                return result;
            }

            private void _invariant() {
                if (this.TotalValue <= -100)
                    throw new Exception("The ZoomAccumulator instance is in inconsistent state.");
            }

            private void _resetAccumulatedZoom() {
                this.AccumulatedZoom = new List<ZoomDifference> {
                    new ZoomDifference(Point.Zero, 0),
                };
            }

            private struct ZoomDifference {
                public Point Anchor;
                public int Value;

                public ZoomDifference(Point anchor, int value) {
                    this.Anchor = anchor;
                    this.Value = value;
                }

                public override string ToString() {
                    return this.Anchor.ToString() + " : " + 
                        this.Value;
                }
            }
        }
    }

    class WidgetGroup : Widget {
        private List<Widget> _content = new List<Widget>();
        public IList<Widget> Content {
            get {
                return this._content;
            }
            set {
                if (value == null)
                    throw new ArgumentNullException("Content");
                this._content = value.ToList();
            }
        }

        public WidgetGroup(params Widget[] children) {
            this._content.AddRange(children);
            this.MakeUpdatable(ur => {
                foreach (var childWidget in this.Content) {
                    if (childWidget == null)
                        continue;
                    childWidget.Update(ur.Context);
                }
                return ur;
            });
        }

        protected override Rectangle MouseSensitiveArea {
            get {
                // TODO. Optimize (hide Content). Duplicate
                // code is in GraphVm class.
                int
                    minX = this.Content.Min(x => x.Box.Left),
                    minY = this.Content.Min(x => x.Box.Top),
                    maxX = this.Content.Max(x => x.Box.Right),
                    maxY = this.Content.Max(x => x.Box.Bottom);
                return new Rectangle(
                    minX,
                    minY,
                    maxX - minX,
                    maxY - minY
                );
            }
        }

        public override bool ContainsExactly(Point point) {
            return this.Content.Any(x => x != null 
                && x.ContainsExactly(point));
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            foreach (var w in this.Content) {
                if(w != null)
                    w.Draw(ctx);
            }
        }

        protected override void MoveInternal(Point displacement) {
            foreach (var w in this.Content) {
                if (w != null)
                    w.Move(displacement);
            }
        }

        public override void Scale(int zoomDifference, Point anchor) {
            foreach (var w in this.Content) {
                if (w != null)
                    w.Scale(zoomDifference, anchor);
            }
        }
    }
}
