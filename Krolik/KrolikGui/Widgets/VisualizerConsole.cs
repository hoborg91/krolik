﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using MonoGameHelpers;
using Microsoft.Xna.Framework.Input;

namespace GpnVisualizer.Widgets {
    class VisualizerConsole : Widget {
        private int _x, _y, _w, _h;

        private string _printedText = string.Empty;

        private List<string> _history = new List<string>();

        private Action<string> _onEnter;

        private object _chest = new object();

        private bool _isEnabled = true;

        public bool IsActive { get; private set; } = true;

        private bool _isVisible = true;

        protected override Rectangle MouseSensitiveArea {
            get {
                return new Rectangle(this._x, this._y, this._w, this._h);
            }
        }

        public VisualizerConsole(int x, int y, int width, int height, Action<string> onEnter = null) {
            this._x = x;
            this._y = y;
            this._w = width;
            this._h = height;
            this._onEnter = onEnter ?? (i => { });
            //this.SwitchVisibility();
            MakeUpdatable(ur => {
                if (!this._isEnabled)
                    return ur;
                if (ur.Context.IsLeftButtonClickOnce)
                    this.IsActive = this.MouseSensitiveArea.Contains(ur.Context.CurrentMousePosition);
                if (!this.IsActive)
                    return ur;
                var keys = ur.Context.GetReleasedOnceKeys().ToList();
                lock (this._chest) {
                    foreach (var key in keys) {
                        if (Keys.Back == key) {
                            if (this._printedText.Length > 0)
                                this._printedText = this._printedText.Substring(0, this._printedText.Length - 1);
                        } else if (Keys.Up == key) {
                            if (this._history.Any())
                                this._printedText = this._history.Last();
                        } else
                            this._printedText += _textRepresentation(key, ur.Context.CurrentKeyboardState.IsKeyDown(Keys.LeftShift));
                    }
                    if (keys.Contains(Keys.Enter)) {
                        this._history.Add(this._printedText);
                        this._onEnter(this._printedText);
                        this._printedText = string.Empty;
                    }
                }
                return ur;
            });
        }

        internal void Disable() {
            this._isEnabled = false;
            this.IsActive = false;
        }

        private static Keys[] CommonKeys = new[] {
            Keys.Q, Keys.W, Keys.E, Keys.R, Keys.T, Keys.Y, Keys.U, Keys.I, Keys.O, Keys.P,
            Keys.A, Keys.S, Keys.D, Keys.F, Keys.G, Keys.H, Keys.J, Keys.K, Keys.L,
            Keys.Z, Keys.X, Keys.C, Keys.V, Keys.B, Keys.N, Keys.M,
        };

        private static IDictionary<Keys, Tuple<string, string>> SpecialKeys = new Dictionary<Keys, Tuple<string, string>> {
            { Keys.D1, Tuple.Create("1", "!") },
            { Keys.D2, Tuple.Create("2", "@") },
            { Keys.D3, Tuple.Create("3", "#") },
            { Keys.D4, Tuple.Create("4", "$") },
            { Keys.D5, Tuple.Create("5", "%") },
            { Keys.D6, Tuple.Create("6", "^") },
            { Keys.D7, Tuple.Create("7", "&") },
            { Keys.D8, Tuple.Create("8", "*") },
            { Keys.D9, Tuple.Create("9", "(") },
            { Keys.D0, Tuple.Create("0", ")") },
            { Keys.Space, Tuple.Create(" ", " ") },
            { Keys.OemMinus, Tuple.Create("-", "_") },
            { Keys.OemPlus, Tuple.Create("=", "+") },
            { Keys.OemComma, Tuple.Create(",", "<") },
            { Keys.OemPeriod, Tuple.Create(".", ">") },
            { Keys.OemSemicolon, Tuple.Create(";", ":") },
            { Keys.OemQuotes, Tuple.Create("'", "\"") },
        };

        private static string _textRepresentation(Keys key, bool shift) {
            foreach (var commonKey in CommonKeys) {
                if (commonKey != key)
                    continue;
                var r = key.ToString();
                if (shift)
                    r = r.ToUpper();
                else
                    r = r.ToLower();
                return r;
            }
            if (SpecialKeys.ContainsKey(key))
                return shift
                    ? SpecialKeys[key].Item2
                    : SpecialKeys[key].Item1;
            return string.Empty;
        }

        public override void Draw(Contexts.DrawingContext ctx) {
            if (!this._isVisible) {
                MonoGameHelpers.DrawingHelper.DrawText(
                "Press [~] to switch the console.",
                new Vector2(this._x, this._y),
                ctx.DotTexture,
                ctx.DefaultFont,
                ctx.SpriteBatch,
                Color.Black
            );
                return;
            }
            var showRecords = 5;
            var text = string.Empty;
            lock (this._chest) {
                text = string.Join(
                    Environment.NewLine,
                    this._history.Skip(this._history.Count > showRecords
                        ? this._history.Count - showRecords
                        : 0
                    )
                );
                text += Environment.NewLine + ">" + this._printedText;
            }
            MonoGameHelpers.DrawingHelper.DrawText(
                text,
                new Vector2(this._x, this._y),
                ctx.DotTexture,
                ctx.DefaultFont,
                ctx.SpriteBatch,
                Color.Black,
                this._isEnabled
                    ? (this.IsActive
                        ? Color.GreenYellow
                        : new Color(Color.GreenYellow, 0.5f)
                      )
                    : Color.LightGray
            );
        }

        public void WriteLine(string text) {
            if (!this._isEnabled)
                return;
            lock (this._chest)
                this._history.Add(text);
        }

        internal void SwitchVisibility() {
            this._isVisible = !this._isVisible;
            this._isEnabled = this._isVisible;
            this.IsActive = this._isEnabled;
        }

        public override void Scale(int zoomDifference, Point anchor) {
            throw new NotImplementedException();
        }

        public override bool ContainsExactly(Point point) {
            return true;
        }
    }
}
