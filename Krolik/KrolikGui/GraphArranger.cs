﻿using GpnVisualizer.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GpnVisualizer {
    class GraphArranger {
        public void Hello() {
            var g1 = GraphTools.MakeFromXml(@"
<graph>
<vertices>
<v id='1' />
<v id='2' />
<v id='3' />
<v id='4' />
<v id='5' />
<v id='6' />
</vertices>
<edges>
<e f='1' t='2' />
</edges>
</graph>
            ");

            var vertices = Enumerable.Range(0, 8)
                .Select(x => (IVertex)new Vertex(x.ToString(), 50, 20))
                .ToList();
            var v = vertices;
            var edges = new List<IEdge> {
                new Edge(v[0], v[1]),
                new Edge(v[0], v[2]),
                new Edge(v[1], v[3]),
                new Edge(v[2], v[3]),
                new Edge(v[3], v[4]),
                new Edge(v[4], v[5]),
                new Edge(v[4], v[6]),
                new Edge(v[5], v[7]),
                new Edge(v[6], v[7]),
            };
            var graph = new Graph {
                Vertices = vertices,
                Edges = edges,
            };
            var a = new A(graph);
        }

        interface IBridge {
            IList<IEdge> Edges { get; }
        }

        class Island : IIsland {
            public IList<IVertex> Vertices { get; set; }
            public IList<IEdge> InnerEdges { get; set; }
        }

        class Bridge : IBridge {
            public IList<IEdge> Edges { get; set; }
            public IA From { get; set; }
            public IA To { get; set; }
        }

        interface ITree<TLeaf> {
            IList<ITree<TLeaf>> Children { get; }
            TLeaf Leaf { get; }
        }

        class A : IA {
            public A(IGraph graph) {
                var parts = Devide(graph);
                this.InnerAs = parts.InnerAs.ToList();
                this.Bridges = parts.Bridges.ToList();
            }

            protected A() {

            }

            private static IA Devide(IGraph graph) {
                var result = new Dictionary<IEdge, IA>();
                if(!graph.Edges.Any()) {
                    if (graph.Vertices.Count != 1)
                        throw new NotImplementedException();
                    return graph.Vertices.Single();
                }
                foreach (var edge in graph.Edges) {
                    var structs = new[] {
                        edge.From,
                        edge.To,
                    }
                        .Select(v => new {
                            A = new List<IVertex> { v, },
                            Frontier = new Queue<IVertex>(new[] { v, }),
                        })
                        .ToList();
                    while (structs.Any(x => x.Frontier.Any())) {
                        foreach (var s in structs) {
                            if (!s.Frontier.Any())
                                continue;
                            var vertex = s.Frontier.Dequeue();
                            foreach (var neighbour in GetNeighboursOf(vertex, graph)) {
                                if (structs.Any(x => x.A.Contains(neighbour)))
                                    continue;
                                s.A.Add(neighbour);
                                s.Frontier.Enqueue(vertex);
                            }
                        }
                    }
                    var bridge = MakeBridgeBetween(
                        structs[0].A,
                        structs[1].A,
                        graph.Edges
                    );
                    var innersAs = structs
                        .Select(s => {
                            var innerEdges = GetOnlyEdgesBetweeen(s.A, graph.Edges);
                            var innerGraph = new Graph {
                                Edges = innerEdges,
                                Vertices = s.A,
                            };
                            var innerA = new A(innerGraph);
                            return (IA)innerA;
                        })
                        .ToList();
                    var a = new A {
                        InnerAs = innersAs,
                        Bridges = bridge,
                    };
                    result[edge] = a;
                }
                return result
                    .OrderBy(x => x.Value.Bridges.Count()) // TODO. Undesired enumeration?
                    .First()
                    .Value;
            }

            private static IList<IEdge> MakeBridgeBetween(List<IVertex> a1, List<IVertex> a2, IList<IEdge> edges) {
                return edges
                    .Where(x => 
                        new[] {
                            new { ep1 = x.From, ep2 = x.To, },
                            new { ep1 = x.To, ep2 = x.From, },
                        }.Any(eps => true
                            && a1.Contains(eps.ep1)
                            && a2.Contains(eps.ep2)
                        )
                    )
                    .ToList();
            }

            private static IList<IEdge> GetOnlyEdgesBetweeen(List<IVertex> vertices, IList<IEdge> edges) {
                return edges
                    .Where(x => 
                        vertices.Contains(x.From) && vertices.Contains(x.To))
                    .ToList();
            }

            private static IList<IVertex> GetNeighboursOf(IVertex vertex, IGraph graph) {
                var result = new List<IVertex>();
                result.AddRange(graph.Edges
                    .Where(x => x.From == vertex)
                    .Select(x => x.To));
                result.AddRange(graph.Edges
                    .Where(x => x.To == vertex)
                    .Select(x => x.From));
                return result
                    .Distinct()
                    .Where(x => graph.Vertices.Contains(x))
                    .ToList();
            }

            public IList<IEdge> Bridges { get; set; }

            public IList<IA> InnerAs { get; set; }

            IEnumerable<IA> IA.InnerAs {
                get {
                    return this.InnerAs;
                }
            }

            IEnumerable<IEdge> IA.Bridges {
                get {
                    return this.Bridges;
                }
            }
        }
    }

    interface IA {
        IEnumerable<IA> InnerAs { get; }
        IEnumerable<IEdge> Bridges { get; }
    }

    interface IGraph {
        IList<IVertex> Vertices { get; }
        IList<IEdge> Edges { get; }
    }

    interface IVertex : IA { }

    interface IEdge {
        IVertex From { get; }
        IVertex To { get; }
    }

    class Graph : IGraph {
        public IList<IEdge> Edges { get; set; }
        public IList<IVertex> Vertices { get; set; }

        public override string ToString() {
            return string.Join(",", this.Vertices.Select(x => x.ToString()));
        }
    }

    class Vertex : IVertex, IIsland, IBox {
        private string _id;

        public Vertex(string id = null, int width = 0, int height = 0) {
            this._id = id;
            this.Box = new Microsoft.Xna.Framework.Rectangle(
                0,
                0,
                width,
                height
            );
        }

        public string Id { get { return this._id; } }

        #region Implementation of IA

        public IEnumerable<IEdge> Bridges {
            get {
                return new List<IEdge>();
            }
        }

        public IEnumerable<IA> InnerAs {
            get {
                return new List<IA>();
            }
        }

        #endregion Implementation of IA

        #region Implementation of IIsland

        public IList<IEdge> InnerEdges {
            get {
                return new List<IEdge>();
            }
        }

        public IList<IVertex> Vertices {
            get {
                return new List<IVertex> { this, };
            }
        }

        #endregion Implementation of IIsland

        #region Implementation of IBox

        public Microsoft.Xna.Framework.Rectangle Box { get; set; }

        #endregion Implementation of IBox

        public override string ToString() {
            return this._id ?? "Vertex";
        }
    }

    class Edge : IEdge {
        public IVertex From { get; set; }
        public IVertex To { get; set; }

        public Edge(IVertex from, IVertex to) {
            this.From = from;
            this.To = to;
        }

        public override string ToString() {
            return
                this.From.ToString() +
                "-" +
                this.To.ToString();
        }
    }

    interface IIsland {
        IList<IVertex> Vertices { get; }
        IList<IEdge> InnerEdges { get; }
    }
}
