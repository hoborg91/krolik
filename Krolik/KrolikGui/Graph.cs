﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace GpnVisualizer {
    class GraphTools {
        public static IGraph MakeFromXml(string xmlString) {
            var xml = XElement.Parse(xmlString);
            var verticesXml = xml.Element("vertices");
            var edgesXml = xml.Element("edges");
            var vertices = new List<Vertex>();
            var edges = new List<IEdge>();
            foreach (var vertexXml in verticesXml.Elements("v")) {
                var id = vertexXml.Attribute("id").Value;
                if (vertices.Any(x => x.Id == id))
                    throw new Exception("Vertes '" + id + "' already exists.");
                vertices.Add(new Vertex(id));
            }
            foreach (var edgeXml in edgesXml.Elements("e")) {
                var fId = edgeXml.Attribute("f").Value;
                var tId = edgeXml.Attribute("t").Value;
                var f = vertices.SingleOrDefault(x => x.Id == fId);
                var t = vertices.SingleOrDefault(x => x.Id == tId);
                if (f == null)
                    throw new Exception("Vertex '" + fId + "' does not exist.");
                if (t == null)
                    throw new Exception("Vertex '" + tId + "' does not exist.");
                if (edges.Any(x => x.From == f && x.To == t))
                    throw new Exception("Edge from '" + fId + "' to '" + tId + "' already exits.");
                edges.Add(new Edge(f, t));
            }
            var result = new Graph {
                Vertices = vertices.Select(x => (IVertex)x).ToList(),
                Edges = edges,
            };
            return result;
        }
    }
}
