﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KrolikTranslator {
    public interface IExpressionsTranslator<TExpression> where TExpression : IKrolikExpression {
        TExpression Translate(string text);
    }

    public class TranslatorException : Exception {
        public TranslatorException(string message = "Incorrect input.") : base(
            message
        ) {

        }
    }

    public class ExpressionsTranslator<TExpr> : IExpressionsTranslator<TExpr>
        where TExpr : IKrolikExpression
    {
        KrolikExpressionGrammar _grammar = new KrolikExpressionGrammar();

        IDictionary<string, Func<ExpressionsTranslator<TExpr>, ParseTreeNode, TExpr>> _expressionsBuilder;

        public ExpressionsTranslator(IKrolikExpressionsFactory<TExpr> factory) {
            this._expressionsBuilder = new Dictionary<string, Func<ExpressionsTranslator<TExpr>, ParseTreeNode, TExpr>>() {
                [this._grammar.NtNamesExpr.Id] = (translator, node) => {
                    return factory.MakeVariable(node.Token.Text);
                },
                [this._grammar.NtNamesExpr.MethodCall] = (translator, node) => {
                    if(node.ChildNodes.Count != 6)
                        throw new TranslatorException("Incorrect input.");
                    var args = _buildParametersList(node.ChildNodes[4].ChildNodes);
                    return factory.MakeMethodCall(
                        node.ChildNodes[0].Token.Text,
                        node.ChildNodes[2].Token.Text,
                        args.OfType<IKrolikPositionParameter<TExpr>>().Select(x => x.Value).ToArray(),
                        args.OfType<IKrolikNamedParameter<TExpr>>().ToDictionary(x => x.Name, x => x.Value));
                },
                [this._grammar.NtNamesExpr.Instantiation] = (translator, node) => {
                    if (node.ChildNodes.Count != 5)
                        throw new TranslatorException("Incorrect input.");
                    var args = _buildParametersList(node.ChildNodes[3].ChildNodes);
                    return factory.MakeInstantiation(
                        node.ChildNodes[1].Token.Text,
                        args.OfType<IKrolikPositionParameter<TExpr>>().Select(x => x.Value).ToArray(),
                        args.OfType<IKrolikNamedParameter<TExpr>>().ToDictionary(x => x.Name, x => x.Value));
                },
                [this._grammar.NtNamesExpr.Literal] = (translator, node) => {
                    if(new string[] { "true", "false", }.Contains(node.Token?.Text)) {
                        return factory.MakeLiteral(node.Token.Text);
                    }
                    return factory.MakeLiteral(node.ChildNodes[0].Token.Text);
                },
            };
        }

        public TExpr Translate(string text) {
            var parser = new Parser(this._grammar);
            var tree = parser.Parse(text);
            if (tree == null || tree.Root == null) {
                throw new TranslatorException();
            }
            var result = Build(tree.Root);
            return result;
        }

        internal TExpr Build(ParseTreeNode node) {
            var n = node.ChildNodes[0];
            if(n.Term == null)
                throw new TranslatorException();
            if(!this._expressionsBuilder.ContainsKey(n.Term.Name))
                throw new TranslatorException();
            return this._expressionsBuilder[n.Term.Name](this, n);
        }

        private List<IKrolikMethodParameter<TExpr>> _buildParametersList(ParseTreeNodeList nodes) {
            return nodes
                .Select((paramNode, i) => {
                    if (!new[] { 1, 3 }.Contains(paramNode.ChildNodes.Count))
                        throw new TranslatorException();
                    if (paramNode.ChildNodes.Count == 1)
                        return (IKrolikMethodParameter<TExpr>)new PositionParameter<TExpr>(i, this.Build(paramNode.ChildNodes[0]));
                    var name = paramNode.ChildNodes[0].Token.Text;
                    var value = this.Build(paramNode.ChildNodes[2]);
                    return new NamedParameter<TExpr>(name, value);
                })
                .ToList();
        }

        private class PositionParameter<TExpr> : IKrolikPositionParameter<TExpr> where TExpr : IKrolikExpression {
            public TExpr Value { get; private set; }
            public int ZeroBasedIndex { get; private set; }

            public PositionParameter(int zeroBasedIndex, TExpr value) {
                this.Value = value;
                this.ZeroBasedIndex = zeroBasedIndex;
            }
        }

        private class NamedParameter<TExpr> : IKrolikNamedParameter<TExpr> where TExpr : IKrolikExpression {
            public string Name { get; private set; }
            public TExpr Value { get; private set; }

            public NamedParameter(string name, TExpr value) {
                this.Name = name;
                this.Value = value;
            }
        }
    }
}
