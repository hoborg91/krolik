﻿using Irony.Parsing;

namespace KrolikTranslator {
    class KrolikExpressionGrammar : Grammar {
        public class NtNamesForExprCls {
            public readonly string
                Id = "id",
                Expr = "expr",
                MethodParameter = "param",
                List = "list",
                MethodCall = "methodCall",
                Instantiation = "instantiation",
                Literal = "literal";
        }

        public readonly NtNamesForExprCls NtNamesExpr = new NtNamesForExprCls();

        protected NonTerminal Expression;
        protected IdentifierTerminal Identifier;

        public KrolikExpressionGrammar() {
            this.Identifier = new IdentifierTerminal(this.NtNamesExpr.Id);

            var expression = new NonTerminal(this.NtNamesExpr.Expr);
            var methodParameter = new NonTerminal(this.NtNamesExpr.MethodParameter);
            var parmaetersList = new NonTerminal(this.NtNamesExpr.List);
            var methodCall = new NonTerminal(this.NtNamesExpr.MethodCall);
            var instantiation = new NonTerminal(this.NtNamesExpr.Instantiation);
            var literal = new NonTerminal(this.NtNamesExpr.Literal);

            instantiation.Rule = "new" + this.Identifier + "(" + parmaetersList + ")";
            methodCall.Rule = this.Identifier + "." + this.Identifier + "(" + parmaetersList + ")";
            literal.Rule = new StringLiteral("stringLiteral", "\"") 
                | new NumberLiteral("numberLiteral", NumberOptions.AllowSign)
                | "true" | "false";
            methodParameter.Rule = expression | this.Identifier + ":" + expression;
            expression.Rule = this.Identifier | methodCall | instantiation | literal;
            parmaetersList.Rule = this.MakeStarRule(parmaetersList, (BnfExpression)",", methodParameter);
            this.Root = expression;
            this.Expression = expression;
        }
    }
}
