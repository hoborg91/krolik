﻿using System.Collections.Generic;

namespace KrolikTranslator {
    public interface IKrolikExpression {

    }

    // TODO. Remove? It is not being used directly. Only via factory. Look Shift+F12.
    public interface IKrolikVariable : IKrolikExpression {
        string VariableName { get; set; }
    }

    // TODO. Remove? It is not being used directly. Only via factory. Look Shift+F12.
    //public interface IKrolikMethodCall : IKrolikExpression {
    //    string InstanceName { get; set; }
    //    string MethodName { get; set; }
    //    IKrolikExpression[] PositionParameters { get; }
    //    Dictionary<string, IKrolikExpression> NamedParameters { get; }
    //}

    // TODO. Remove? It is not being used directly. Only via factory. Look Shift+F12.
    //public interface IKrolikInstantiation : IKrolikExpression {
    //    string ClassName { get; set; }
    //    IKrolikExpression[] PositionParameters { get; }
    //}

    public interface IKrolikLiteral : IKrolikExpression {

    }

    public interface IKrolikMethodParameter<TExpr> where TExpr : IKrolikExpression {
        TExpr Value { get; }
    }

    public interface IKrolikPositionParameter<TExpr> : IKrolikMethodParameter<TExpr> where TExpr : IKrolikExpression {
        int ZeroBasedIndex { get; }
    }

    public interface IKrolikNamedParameter<TExpr> : IKrolikMethodParameter<TExpr> where TExpr : IKrolikExpression {
        string Name { get; }
    }

    public interface IKrolikExpressionsFactory<TExpression>
        where TExpression : IKrolikExpression
    {
        TExpression MakeVariable(string variableName);
        TExpression MakeMethodCall(string instance, string method, TExpression[] positionParameters, Dictionary<string, TExpression> namedParameters);
        TExpression MakeInstantiation(string cls, TExpression[] positionParameters, Dictionary<string, TExpression> namedParameters);
        TExpression MakeLiteral(string textRepresentation);
    }
}
