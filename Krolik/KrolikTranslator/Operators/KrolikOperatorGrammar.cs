﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrolikTranslator.Operators {
    class KrolikOperatorsGrammar : KrolikExpressionGrammar {
        public class NtNamesForOperCls {
            public readonly string
                Oper = "Operator",
                OperBody = "OperatorBody",
                OpersList = "OpersList",
                AssignFirst = "AssignFirst",
                AssignLater = "AssignLater",
                Call = "Call";
        }

        public readonly NtNamesForOperCls NtNamesOper = new NtNamesForOperCls();

        public KrolikOperatorsGrammar() {
            var opersList = new NonTerminal(this.NtNamesOper.OpersList);
            var oper = new NonTerminal(this.NtNamesOper.Oper);
            var operBody = new NonTerminal(this.NtNamesOper.OperBody);

            var assignFirst = new NonTerminal(this.NtNamesOper.AssignFirst);
            var assignLater = new NonTerminal(this.NtNamesOper.AssignLater);
            var call = new NonTerminal(this.NtNamesOper.Call);

            opersList.Rule = this.MakeStarRule(opersList, oper);
            oper.Rule = operBody + ";";
            operBody.Rule = assignFirst | assignLater | call;

            assignFirst.Rule = "set" + assignLater;
            assignLater.Rule = this.Identifier + "=" + this.Expression;
            call.Rule = this.Expression;

            this.Root = opersList;
        }
    }
}
