﻿using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrolikTranslator.Operators {
    public class OperatorsTranslator<TExpr, TOper>
        where TExpr : IKrolikExpression
        where TOper : IKrolikOperator
    {
        IOperatorsFactory<TOper, TExpr> _factory;
        ExpressionsTranslator<TExpr> _expressionsTranslator;
        KrolikOperatorsGrammar _grammar = new KrolikOperatorsGrammar();

        IDictionary<string, Func<OperatorsTranslator<TExpr, TOper>, ParseTreeNode, TOper>> _builder;

        public OperatorsTranslator(IKrolikExpressionsFactory<TExpr> expressionsFactory, IOperatorsFactory<TOper, TExpr> operatorsFactory) {
            this._expressionsTranslator = new ExpressionsTranslator<TExpr>(expressionsFactory);
            this._factory = operatorsFactory;

            this._builder = new Dictionary<string, Func<OperatorsTranslator<TExpr, TOper>, ParseTreeNode, TOper>> {
                { this._grammar.NtNamesOper.AssignFirst, (t, node) => {
                    var children = node.ChildNodes;
                    if(children == null || children.Count != 2)
                        throw new TranslatorException();
                    var assignLater = children[1];
                    var assign = t._buildAssignment(assignLater);
                    return t._factory.MakeAssignment(assign.Item1, assign.Item2, true);
                } },
                { this._grammar.NtNamesOper.AssignLater, (t, node) => {
                    var assign = t._buildAssignment(node);
                    return t._factory.MakeAssignment(assign.Item1, assign.Item2, false);
                } },
                { this._grammar.NtNamesOper.Call, (t, node) => {
                    if(node.ChildNodes == null || node.ChildNodes.Count != 1)
                        throw new TranslatorException();
                    var expr = t._expressionsTranslator.Build(node.ChildNodes[0]);
                    return t._factory.MakeMethodCall(expr);
                } },
            };
        }

        public TOper[] Translate(string text) {
            var parser = new Parser(this._grammar);
            var tree = parser.Parse(text);
            if (tree == null || tree.Root == null)
                throw new TranslatorException("Incorrect input.");
            var result = this._buildOperatorsList(tree.Root);
            return result.ToArray();
        }

        private List<TOper> _buildOperatorsList(ParseTreeNode node) {
            if(node.Term.Name == this._grammar.NtNamesOper.OpersList) {
                var children = node.ChildNodes;
                var result = children
                    .Select(x1 => this._buildOperator(x1))
                    .ToList();
                return result;
            } else
                throw new TranslatorException();
        }

        private TOper _buildOperator(ParseTreeNode node) {
            if (node.Term.Name == this._grammar.NtNamesOper.Oper) {
                if (node.ChildNodes == null || node.ChildNodes.Count != 2)
                    throw new TranslatorException();
                var operBody = node.ChildNodes[0];
                if (false
                    || operBody.Term.Name != this._grammar.NtNamesOper.OperBody
                    || operBody.ChildNodes == null 
                    || operBody.ChildNodes.Count != 1
                )
                    throw new TranslatorException();
                var oper = operBody.ChildNodes[0];
                if (this._builder.ContainsKey(oper.Term.Name))
                    return this._builder[oper.Term.Name](this, oper);
                throw new TranslatorException();
            } else
                throw new TranslatorException();
        }

        private Tuple<string, TExpr> _buildAssignment(ParseTreeNode node) {
            var children = node.ChildNodes;
            if (children == null || children.Count != 3)
                throw new TranslatorException();
            var varName = children[0].Token.Text;
            var expr = this._expressionsTranslator.Build(children[2]);
            return new Tuple<string, TExpr>(varName, expr);
        }
    }
}
