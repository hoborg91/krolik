﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrolikTranslator.Operators {
    public interface IKrolikOperator {
        
    }

    public interface IOperatorsFactory<TOperator, TExpression>
        where TOperator : IKrolikOperator
        where TExpression : IKrolikExpression
    {
        TOperator MakeAssignment(string variableName, TExpression expression, bool first);
        TOperator MakeMethodCall(TExpression methodCall);
    }
}
