﻿using Cil.Common;
using Krolik.Main;
using System;
using System.Collections.Generic;

namespace KrolikShell {
    public interface IShell : IDisposable {
        void Run(Func<string> commandsReader, Action<string> logger);
        IGraphForVisualization GetGraphForVisualization(string variableName);
        // TODO. The parameter 'isRealData' should be assigned by the
        // called side and so it should be removed from the method
        // signature. Consider refactoring.
        IEnumerable<IVectorForVisualization> GetVectorsForVisualization(string variableName, bool isRealData);
    }

    public class ShellBase : IShell {
        IShellState _currentState;

        public void Run(Func<string> commandsReader, Action<string> logger) {
            var readWrite = new CustomReadWrite<string>(commandsReader, logger);
            this.Run(readWrite);
        }

        public void Run(IReadWrite<string> readWrite) {
            using (this._currentState = new InitKrolik(readWrite)) {
                while (this._currentState != null) {
                    this._currentState = this._currentState.Cmd();
                }
            }
        }

        public IGraphForVisualization GetGraphForVisualization(string variableName) {
            return this._currentState?.GetGraphForVisualization(variableName);
        }

        public IEnumerable<IVectorForVisualization> GetVectorsForVisualization(
            string variableName,
            bool isRealData
        ) {
            return this._currentState?.GetVectorsForVisualization(
                variableName,
                isRealData
            );
        }

        #region Implementation of IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            this._currentState.Dispose();
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable
    }
}
