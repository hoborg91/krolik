﻿using Cil.Common;
using Krolik.Exceptions;
using Krolik.Infrastructure;
using Krolik.TypeSystem;
using KrolikTranslator;
using KrolikTranslator.Operators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KrolikShell {
    internal class InterpretKrolik : KrolikStateBase {
        abstract class KrolikExpression : IKrolikExpression {
            protected IReadWrite<string> ReadWrite { get; set; }

            public KrolikExpression(IReadWrite<string> readWrite) {
                this.ReadWrite = readWrite;
            }

            internal abstract IObject Calc(IEnvironment env);
        }

        class KrolikLiteral : KrolikExpression {
            private string _textRepresentation;

            private static Dictionary<string, bool> StringToBoolMapper = new Dictionary<string, bool> {
                ["true"] = true,
                ["false"] = false,
            };

            public KrolikLiteral(
                IReadWrite<string> readWrite, 
                string textRepresentation
            ) : base(readWrite) {
                if (string.IsNullOrWhiteSpace(textRepresentation))
                    throw new TypesMismatchException("The literal must have a non-empty text represntation.");
                this._textRepresentation = textRepresentation;
            }

            internal override IObject Calc(IEnvironment env) {
                if (true
                    && this._textRepresentation.Length > 1
                    && this._textRepresentation[0] == '"'
                    && this._textRepresentation[this._textRepresentation.Length - 1] == '"'
                ) {
                    return env.MakeObject<string>(
                        null,
                        this._textRepresentation.Substring(1, this._textRepresentation.Length - 2)
                    );
                } else if (StringToBoolMapper.ContainsKey(this._textRepresentation)) {
                    return env.MakeObject<bool>(
                        null, 
                        StringToBoolMapper[this._textRepresentation]
                    );
                }
                var dec = this._textRepresentation.ToDecimal();
                if (dec == null)
                    throw new Exception("The given literal was expected to be a number.");
                return env.MakeObject<decimal>(
                    null, 
                    dec.Value
                );
            }
        }

        class KrolikVariable : KrolikExpression, IKrolikVariable {
            public string VariableName { get; set; }

            public KrolikVariable(IReadWrite<string> readWrite, string name) : base(
                readWrite
            ) {
                this.VariableName = name;
            }

            public override string ToString() {
                return this.VariableName;
            }

            internal override IObject Calc(IEnvironment env) {
                if (env[this.VariableName] == null)
                    throw new NoSuchVariableException($"Variable '{this.VariableName}' does not exist.");
                return env[this.VariableName];
            }

            public class NoSuchVariableException : Exception {
                public NoSuchVariableException(string message)
                    : base(message)
                {

                }
            }
        }

        class KrolikMethodCall : KrolikExpression {//, IKrolikMethodCall {
            public string InstanceName { get; set; }
            public string MethodName { get; set; }
            public KrolikExpression[] PositionParameters { get; private set; }
            public Dictionary<string, KrolikExpression> NamedParameters { get; private set; }

            public KrolikMethodCall(IReadWrite<string> readWrite, string instance, string method, KrolikExpression[] pos, Dictionary<string, KrolikExpression> nam) : base(
                readWrite
            ) {
                this.InstanceName = instance;
                this.MethodName = method;
                this.PositionParameters = pos;
                this.NamedParameters = nam;
            }

            public override string ToString() {
                return this.InstanceName + "." + this.MethodName + "("
                    + string.Join(", ", this.PositionParameters.Select(x1 => x1.ToString()))
                    + string.Join(", ", this.NamedParameters.Select(x1 => x1.ToString()))
                    + ")";
            }

            internal override IObject Calc(IEnvironment env) {
                var pos = this.PositionParameters
                    .Select(x => x.Calc(env))
                    .ToArray();
                var nam = this.NamedParameters
                    .ToDictionary(x => x.Key, x => x.Value.Calc(env));
                return new KrolikVariable(this.ReadWrite, this.InstanceName)
                    .Calc(env)
                    .Call(this.MethodName, this.ReadWrite, pos, nam);
            }
        }

        class KrolikInstantiation : KrolikExpression {//, IKrolikInstantiation {
            public class NoSuchTypeException : Exception {
                public NoSuchTypeException(string typeName) : base(
                    $"There is no type \"{typeName}\"."
                ) {

                }
            }

            public string ClassName { get; set; }
            public KrolikExpression[] PositionParameters { get; set; }
            public Dictionary<string, KrolikExpression> NamedParameters { get; set; }

            public KrolikInstantiation(IReadWrite<string> readWrite, string cls, KrolikExpression[] pos, Dictionary<string, KrolikExpression> nam) : base(
                readWrite
            ) {
                this.ClassName = cls;
                this.PositionParameters = pos;
                this.NamedParameters = nam;
            }

            public override string ToString() {
                return "new " + this.ClassName + "("
                    + string.Join(", ", this.PositionParameters.Select(x1 => x1.ToString()))
                    + ")";
            }

            internal override IObject Calc(IEnvironment env) {
                var type = env.Types.ContainsKey(this.ClassName)
                    ? env.Types[this.ClassName]
                    : null;
                if (type == null)
                    throw new NoSuchTypeException(this.ClassName);
                var pos = this.PositionParameters
                    .Select(x => x.Calc(env))
                    .ToArray();
                var nam = this.NamedParameters
                    .ToDictionary(x => x.Key, x => x.Value.Calc(env));
                var ctx = env.MakeContext(null, this.ReadWrite, pos, nam);
                var obj = type.Constructor.PrepareContext(ctx, type).Call();
                return obj;
            }
        }

        class KrolikExpressionsFactory : IKrolikExpressionsFactory<KrolikExpression> {
            IReadWrite<string> _readWrite;

            public KrolikExpressionsFactory(IReadWrite<string> readWrite) {
                this._readWrite = readWrite;
            }

            public KrolikExpression MakeInstantiation(string cls, KrolikExpression[] pos, Dictionary<string, KrolikExpression> nam) {
                return new KrolikInstantiation(this._readWrite, cls, pos, nam);
            }

            public KrolikExpression MakeMethodCall(string instance, string method, KrolikExpression[] pos, Dictionary<string, KrolikExpression> nam) {
                return new KrolikMethodCall(this._readWrite, instance, method, pos, nam);
            }

            public KrolikExpression MakeVariable(string variableName) {
                return new KrolikVariable(this._readWrite, variableName);
            }

            public KrolikExpression MakeLiteral(string textRepresentation) {
                return new KrolikLiteral(this._readWrite, textRepresentation);
            }
        }

        abstract class KrolikOperator : IKrolikOperator {
            internal abstract void Run(IEnvironment env);
        }

        class Assignment : KrolikOperator {
            string _varName;
            KrolikExpression _expression;
            bool _declaration = false;

            public Assignment(string varName, KrolikExpression expression, bool declaration = false) {
                this._varName = varName;
                this._expression = expression;
                this._declaration = declaration;
            }

            internal override void Run(IEnvironment env) {
                if (this._declaration && env[this._varName] != null)
                    throw new AssignmentException($"Variable \"{this._varName}\" has been already declared.");
                else if (!this._declaration && env[this._varName] == null)
                    throw new AssignmentException($"Unknown variable \"{this._varName}\"");
                var val = this._expression.Calc(env);
                env[this._varName] = val;
            }

            public class AssignmentException : Exception {
                public AssignmentException(string message)
                    : base(message)
                {

                }
            }
        }

        class MethodCall : KrolikOperator {
            KrolikExpression _expression;

            public MethodCall(KrolikExpression expression) {
                this._expression = expression;
            }

            internal override void Run(IEnvironment env) {
                this._expression.Calc(env);
            }
        }

        class KrolikOperatorsFactory : IOperatorsFactory<KrolikOperator, KrolikExpression> {
            public KrolikOperator MakeAssignment(string variableName, KrolikExpression expression, bool first) {
                return new Assignment(variableName, expression, first);
            }

            public KrolikOperator MakeMethodCall(KrolikExpression methodCall) {
                return new MethodCall(methodCall);
            }
        }

        KrolikOperator[] _operators = new KrolikOperator[0];

        public InterpretKrolik(string cmd, KrolikStateBase state) : base(state) {
            var expressionsFactory = new KrolikExpressionsFactory(this.ReadWrite);
            var operatorsFactory = new KrolikOperatorsFactory();
            var translator = new OperatorsTranslator<KrolikExpression, KrolikOperator>(expressionsFactory, operatorsFactory);
            try {
                this._operators = translator.Translate(cmd);
            } catch(TranslatorException ex) {
                this.ReadWrite.Write(ex.Message);
            }
        }

        private static void _stdExceptionHandler(Exception ex, IReadWrite<string> userInterface) {
            userInterface.Write(ex.Message);
        }

        protected override KrolikStateBase Run() {
            try {
                foreach (var oper in this._operators) {
                    oper.Run(this.Env);
                }
            } catch(KrolikInstantiation.NoSuchTypeException ex) {
                _stdExceptionHandler(ex, this.ReadWrite);
            } catch(Assignment.AssignmentException ex) {
                _stdExceptionHandler(ex, this.ReadWrite);
            } catch(KrolikVariable.NoSuchVariableException ex) {
                _stdExceptionHandler(ex, this.ReadWrite);
            } catch(NoSuchMethodException ex) {
                _stdExceptionHandler(ex, this.ReadWrite);
            } catch(AlgorithmicException ex) {
                _stdExceptionHandler(ex, this.ReadWrite);
            } catch(EnvironmentException ex) {
                _stdExceptionHandler(ex, this.ReadWrite);
            }
            var result = new InitKrolik(this);
            return result;
        }
    }
}
