﻿using System;
using System.Collections.Generic;
using System.Linq;
using Krolik;
using Krolik.Infrastructure;
using Cil.Common;
using Krolik.Exceptions;
using Krolik.TypeSystem;
using Krolik.Main;

namespace KrolikShell {
    internal interface IShellState : IDisposable {
        /// <summary>
        /// Implements base command shell cycle. E. g., shows
        /// command prompt, reads the input and acts
        /// correspondingly to the input.
        /// </summary>
        /// <returns></returns>
        IShellState Cmd();
        IReadWrite<string> ReadWrite { get; set; }

        IGraphForVisualization GetGraphForVisualization(string variableName);
        IEnumerable<IVectorForVisualization> GetVectorsForVisualization(
            string variableName,
            bool isRealData
        );
    }

    internal abstract class KrolikStateBase : IShellState {
        public IReadWrite<string> ReadWrite { get; set; }
        protected readonly IEnvironment Env;

        public KrolikStateBase(IReadWrite<string> readWrite) {
            this.ReadWrite = readWrite;
            this.Env = new KrolikEnvironmentActivator().CreateEnvironment();
        }

        public KrolikStateBase(KrolikStateBase state) {
            this.ReadWrite = state.ReadWrite;
            this.Env = state.Env;
        }

        public IGraphForVisualization GetGraphForVisualization(string variableName) {
            // TODO. Bad code ('as' operator). Refactor.
            return (this.Env as IKrolikEnvironment)?.GetGraphForVisualization(variableName);
        }

        public IEnumerable<IVectorForVisualization> GetVectorsForVisualization(
            string variableName,
            bool isRealData
        ) {
            // TODO. Bad code ('as' operator). Refactor.
            return (this.Env as IKrolikEnvironment)?.GetVectorsForVisualization(
                variableName,
                isRealData
            );
        }

        /// <summary>
        /// Shows command prompt, reads the input and implements
        /// the input command.
        /// </summary>
        public IShellState Cmd() {
            this.ReadWrite.Write(Environment.NewLine + ">");
            var commandLine = this.ReadWrite.Read();
            if (commandLine == null)
                commandLine = "";
            var parts = commandLine.Split(' ');
            var head = parts[0];
            var parameters = parts.Skip(1).ToArray();
            switch (head) {
                case "exit":
                    this.Dispose();
                    return null;
                case "strictness":
                    return new SwitchSoftKrolik(this, parameters).Run();
                case "log-classification":
                    return new SwitchLogClassificationKrolik(this, parameters).Run();
                case "load":
                    return new LoadKrolik(this, parameters).Run();
                case "help":
                    return new HelpKrolik(this, parameters).Run();
                default:
                    return new InterpretKrolik(commandLine, this).Run();
            }
        }

        #region Implementation of IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            this.Env.Dispose();
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable

        protected abstract KrolikStateBase Run();
    }

    internal class SwitchSoftKrolik : KrolikStateBase {
        private readonly bool _strict;

        public SwitchSoftKrolik(KrolikStateBase state, string[] parameters) : base(state) {
            if (parameters == null || parameters.Length != 1
                || !new[] { "soft", "strict", }.Contains(parameters[0]))
                throw new ArgumentException("parameters");
            _strict = parameters[0] == "strict";
        }

        protected override KrolikStateBase Run() {
            KrolikSettings.Current.Soft = !this._strict;
            this.ReadWrite.Write(KrolikSettings.Current.Soft
                ? "Current GPN type: soft."
                : "Current GPN type: strict.");
            return new InitKrolik(this);
        }
    }

    internal class SwitchLogClassificationKrolik : KrolikStateBase {
        private readonly bool _log;

        public SwitchLogClassificationKrolik(KrolikStateBase state, string[] parameters) : base(state) {
            if (parameters == null || parameters.Length != 1
                || !new[] { "true", "1", "yes", "y", "false", "0", "no", "n", }.Contains(parameters[0].ToLower()))
                throw new ArgumentException("parameters");
            _log = new[] { "true", "1", "yes", "y" }.Contains(parameters[0].ToLower());
        }

        protected override KrolikStateBase Run() {
            KrolikSettings.Current.EnableClassificationLogging = !this._log;
            this.ReadWrite.Write(KrolikSettings.Current.EnableClassificationLogging
                ? "Log classification (is turned on)."
                : "Do not log classification (is turned off).");
            return new InitKrolik(this);
        }
    }

    internal class HelpKrolik : KrolikStateBase {
        private readonly string _helpType, _helpMethod;

        public HelpKrolik(KrolikStateBase state, string[] parameters) : base(state) {
            var param0 = parameters.Length > 0 
                ? parameters[0] 
                : null;
            if (param0.IsNullOrWhiteSpace())
                return;
            const string dot = ".";
            var indexOfComma = param0.LastIndexOf(dot);
            if (indexOfComma < 0) {
                this._helpType = param0;
                return;
            }
            this._helpType = param0.Substring(0, indexOfComma);
            this._helpMethod = param0.Substring(indexOfComma + dot.Length);
        }

        protected override KrolikStateBase Run() {
            var reference = "";
            if (this._helpType.IsNullOrWhiteSpace())
                reference = string.Join(Environment.NewLine, 
                    "Supported types: " + string.Join(", ", this.Env.Types.Select(x => x.Key)) + ".",
                    "Write \"set myVar = new T(arguments);\" to create an instance myVar of type T.",
                    "Write \"help T\" to obtain the refernce for the specified type T.",
                    "Write \"help T.M\" to obtain the refernce for the specified method M of the type T.",
                    "Write \"myVar.MyMethod();\" to call MyMethod of the myVar.",
                    "Write \"exit\" to exit the \"Krolik\" command interpreter.");
            else {
                reference = _getReferenceMaker(
                    this._helpType,
                    this._helpMethod,
                    this.Env
                ).GetReference();
            }
            this.ReadWrite.Write(reference + Environment.NewLine);
            return new InitKrolik(this);
        }

        private static IRefMaker _getReferenceMaker(
            string typeName,
            string methodName,
            IEnvironment environment
        ) {
            var type = environment.Types.ContainsKey(typeName)
                    ? environment.Types[typeName]
                    : null;
            if (type == null)
                return new UnknownTypeRefMaker(typeName);
            if (methodName.IsNullOrWhiteSpace()) {
                return new TypeRefMaker(type);
            }
            var method = type.GetMethodsReference()
                .SingleOrDefault(m => m.Name == methodName);
            if (method == null)
                return new UnknownMethodRefMaker(typeName, methodName);
            return new MethodRefMaker(type, method);
        }

        private interface IRefMaker {
            string GetReference();
        }

        private class UnknownTypeRefMaker : IRefMaker {
            private readonly string _helpType;

            public UnknownTypeRefMaker(string type) {
                this._helpType = type;
            }

            public string GetReference() => "The type \"" + this._helpType + "\" does not exist.";
        }

        private class UnknownMethodRefMaker : IRefMaker {
            private readonly string _helpType, _helpMethod;

            public UnknownMethodRefMaker(string type, string method) {
                this._helpType = type;
                this._helpMethod = method;
            }

            public string GetReference() => $"The type {this._helpType} " +
                $"does not contain method {this._helpMethod}.";
        }

        private class TypeRefMaker : IRefMaker {
            private readonly IType _type;

            public TypeRefMaker(IType type) {
                this._type = type ??
                    throw new ArgumentNullException(nameof(type));
            }

            public string GetReference() {
                var typeRef = new System.Text.StringBuilder();
                typeRef.AppendLine(_type.Name + " (type)");
                if (!_type.Description.IsNullOrWhiteSpace())
                    typeRef.AppendLine(_type.Description);
                typeRef.AppendLine();
                var methodsReference = _type.GetMethodsReference();
                if (methodsReference.Any()) {
                    typeRef.AppendLine("This type defines the following methods.");
                    typeRef.AppendLine();
                    foreach (var method in methodsReference) {
                        typeRef.AppendLine("(method) " + method.To.Name + " " + method.Name + "(" +
                            string.Join(", ", method.From.Select(x => x.Name)) +
                            ")");
                        if (!method.Description.IsNullOrWhiteSpace())
                            typeRef.AppendLine(method.Description);
                        foreach (var arg in method.From) {
                            if (!arg.Description.IsNullOrWhiteSpace())
                                typeRef.AppendLine("- " +
                                    (arg.Type == null ? "primitive type" : arg.Type.Name) + " " +
                                    arg.Name + ". " + arg.Description);
                        }
                        typeRef.AppendLine();
                    }
                } else {
                    typeRef.AppendLine("This type defines no methods.");
                    typeRef.AppendLine();
                }
                return typeRef.ToString();
            }
        }

        private class MethodRefMaker : IRefMaker {
            private readonly IType _type;
            private readonly IMethod _method;

            public MethodRefMaker(IType type, IMethod method) {
                this._type = type ??
                    throw new ArgumentNullException(nameof(type));
                this._method = method ??
                    throw new ArgumentNullException(nameof(method));
            }

            public string GetReference() {
                var methodRef = new System.Text.StringBuilder();
                methodRef.AppendLine(this._type.Name + "." + this._method.Name + " (method)");
                if (!this._method.Description.IsNullOrWhiteSpace())
                    methodRef.AppendLine(this._method.Description);
                methodRef.AppendLine();
                methodRef.AppendLine("Signature: " +
                    this._method.To.Name + " " + this._method.Name + "(" +
                    string.Join(", ", this._method.From.Select(a => (a.Type == null ? "" : a.Type.Name + " ") + a.Name)) +
                    ")."
                );
                foreach (var arg in this._method.From) {
                    methodRef.AppendLine("(argument) " + arg.Name);
                    if (arg.Description.IsNullOrWhiteSpace())
                        continue;
                    methodRef.AppendLine(arg.Description);
                }
                return methodRef.ToString();
            }
        }
    }

    internal class LoadKrolik : KrolikStateBase {
        string _fileName;

        public LoadKrolik(KrolikStateBase state, params string[] parameters) : base(
            state
        ) {
            if (parameters == null || parameters.Length == 0)
                throw new Exception("File name is necessary.");
            this._fileName = SlashMaster.CorrectPath(parameters[0]);
        }

        protected override KrolikStateBase Run() {
            string script = null;
            try {
                using (var sr = new System.IO.StreamReader(this._fileName)) {
                    script = sr.ReadToEnd();
                }
                var cmds = new Queue<string>();
                foreach (var cmd in script.Split('\n')
                    .Select(x1 => x1.Trim())
                ) {
                    cmds.Enqueue(cmd);
                }
                IShellState state = new InitKrolik(this) {
                    ReadWrite = new CustomReadWrite<string>(
                        () => {
                            string cmd;
                            try {
                                cmd = cmds.Dequeue();
                            } catch(InvalidOperationException) {
                                throw new IncompleteScriptException();
                            }
                            this.ReadWrite.Write(cmd + Environment.NewLine);
                            return cmd;
                        },
                        (msg) => {
                            this.ReadWrite.Write(msg);
                        }),
                };
                while (cmds.Any()) {
                    state = state.Cmd();
                }
            } catch(System.IO.FileNotFoundException ex) {
                this.ReadWrite.Write(ex.Message);
            } catch(System.IO.DirectoryNotFoundException ex) {
                this.ReadWrite.Write(ex.Message);
            } catch(IncompleteScriptException) {
                this.ReadWrite.Write("The given script is incomplete.");
            } catch(UnauthorizedAccessException) {
                this.ReadWrite.Write("The operating system deines access to the specified file. " +
                    "Check the file name and the priveledges for this file.");
            }
            var result = new InitKrolik(this);
            return result;
        }
    }

    class IncompleteScriptException : EnvironmentException {

    }

    internal class InitKrolik : KrolikStateBase {
        public InitKrolik(IReadWrite<string> readWrite) : base(
            readWrite
        ) {

        }

        public InitKrolik(KrolikStateBase state) : base(state) {

        }

        protected override KrolikStateBase Run() {
            return this;
        }
    }
}
