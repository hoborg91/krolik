﻿using Cil.Common;
using Krolik.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.Classifiers {
    // TODO. It seems that it is better for this type to be a
    // class or struct (DTO), not interface.
    internal interface IFuzzyClassificationResult {
        IDictionary<Cls, ClassificationDecision> Classes { get; }
    }

    internal class ClassificationDecision {
        public Certainty FinalCertainty { get; set; }
        public List<string> DecisionHistroy { get; private set; } = new List<string>();

        private ClassificationDecision(Certainty finalCertainty, List<string> decisionHistory) {
            this.FinalCertainty = finalCertainty;
            this.DecisionHistroy.AddRange(decisionHistory);
        }

        public ClassificationDecision(Certainty initialCertainty, string comment = null) {
            this.FinalCertainty = initialCertainty;
            this.DecisionHistroy.Add(comment.IsNullOrWhiteSpace()
                ? $"Initial certainty is {initialCertainty}."
                : comment
            );
        }

        public ClassificationDecision Modify(Certainty certainty, string comment) {
            if (comment.IsNullOrWhiteSpace())
                throw new ArgumentException("Empty comment is not allowed at modification.");
            var result = new ClassificationDecision(
                certainty,
                DecisionHistroy.Union(new []{ comment, }).ToList()
            );
            return result;
        }

        public ClassificationDecision Modify(string comment) {
            if (comment.IsNullOrWhiteSpace())
                throw new ArgumentException("Empty comment is not allowed at modification.");
            var result = new ClassificationDecision(
                this.FinalCertainty,
                DecisionHistroy.Union(new[] { comment, }).ToList()
            );
            return result;
        }
    }

    internal class FuzzyClassificationResult : IFuzzyClassificationResult {
        public IDictionary<Cls, ClassificationDecision> Classes { get; private set; }

        public FuzzyClassificationResult(IDictionary<Cls, Certainty> classes) {
            this.Classes = classes.ToDictionary(
                x => x.Key,
                x => new ClassificationDecision(x.Value)
            );
        }

        public FuzzyClassificationResult(IDictionary<Cls, ClassificationDecision> decisions) {
            this.Classes = decisions;
        }

        public FuzzyClassificationResult(List<Cls> result) {
            this.Classes = result.ToDictionary(
                r => r,
                r => new ClassificationDecision(1, "Non-fuzzy classification.")
            );
        }
    }

    //internal static class ClassificationIterpreters {
    //    private static IClassificationInterpretersFactory Factory;

    //    public static IClassificationInterpretersFactory Get {
    //        get {
    //            if (Factory == null) {
    //                Factory = new ClassificationInterpretersFactory();
    //            }
    //            return Factory;
    //        }
    //    }

        internal interface IFuzzyClassificationInterpreter {
            IFuzzyClassificationResult Filter(IFuzzyClassificationResult results);
            IFuzzyClassificationInterpreter FollowBy(IFuzzyClassificationInterpreter interpreter);
        }

        //// TODO. The interface has only one implementation.
        //// Remove the interface?
        //internal interface IClassificationInterpretersFactory {
        //    IFuzzyClassificationInterpreter AbsoluteTrashold(decimal trashold);
        //    IFuzzyClassificationInterpreter RelativeTrashold(decimal trashold);
        //    IFuzzyClassificationInterpreter Relative(decimal ratio);
        //}

        internal class ClassificationInterpretersFactory
            //: IClassificationInterpretersFactory
        {
            public IFuzzyClassificationInterpreter AbsoluteTrashold(decimal trashold) {
                return new AbsoluteTrasholdInterpreter(trashold);
            }

            public IFuzzyClassificationInterpreter RelativeTrashold(decimal trashold) {
                return new RelativeInterpreter(trashold);
            }

            public IFuzzyClassificationInterpreter Relative(decimal ratio) {
                return new RelativeInterpreter(ratio);
            }

            private abstract class InterpretFuzzyClassification : IFuzzyClassificationInterpreter {
                public IFuzzyClassificationInterpreter FollowBy(IFuzzyClassificationInterpreter interpreter) {
                    var result = new ChainedInterpreter(this, interpreter);
                    return result;
                }

                public abstract IFuzzyClassificationResult Filter(IFuzzyClassificationResult results);
            }

            private class ChainedInterpreter : InterpretFuzzyClassification {
                private IFuzzyClassificationInterpreter _a, _b;

                public ChainedInterpreter(IFuzzyClassificationInterpreter a, IFuzzyClassificationInterpreter b) {
                    this._a = a;
                    this._b = b;
                }

                public override IFuzzyClassificationResult Filter(IFuzzyClassificationResult results) {
                    return this._b.Filter(this._a.Filter(results));
                }
            }

            private class AbsoluteTrasholdInterpreter : InterpretFuzzyClassification {
                public decimal Trashold { get; private set; }

                public AbsoluteTrasholdInterpreter(decimal trashold) {
                    if (trashold <= 0 || trashold > 1)
                        throw new ArgumentException($"The '{nameof(trashold)}' parameter must be in (0; 1].");
                    this.Trashold = trashold;
                }

                /// <summary>
                /// Returns classes with the certainty of the given 
                /// trashold value or more.
                /// </summary>
                public override IFuzzyClassificationResult Filter(IFuzzyClassificationResult input) {
                    var output = new Dictionary<Cls, ClassificationDecision>();
                    foreach (var cls in input.Classes) {
                        var cert = cls.Value.FinalCertainty;
                        ClassificationDecision val;
                        if (cert >= this.Trashold) {
                            val = cls.Value.Modify($"[ATr] Cert. {cert} >= trashold {this.Trashold}. The decision's cert. is not changed.");
                        } else {
                            val = cls.Value.Modify(0, $"[ATr] Cert. {cert} < trashold {this.Trashold}. The decision's cert. is set to 0.");
                        }
                        output[cls.Key] = val;
                    }
                    return new FuzzyClassificationResult(output);
                }
            }

            private class RelativeTrasholdInterpreter : InterpretFuzzyClassification {
                public decimal Trashold { get; private set; }

                public RelativeTrasholdInterpreter(decimal trashold) {
                    if (trashold <= 0 || trashold > 1)
                        throw new ArgumentException($"The '{nameof(trashold)}' parameter must be in (0; 1].");
                    this.Trashold = trashold;
                }

                /// <summary>
                /// Returns classes with the normalized certainty of the 
                /// given trashold value or more. Here normalization mean
                /// that the class with the greatest certainty gets 1 and
                /// other classes fit according to it.
                /// </summary>
                public override IFuzzyClassificationResult Filter(IFuzzyClassificationResult input) {
                    var output = new Dictionary<Cls, ClassificationDecision>();
                    var trashold = input.Classes.Max(x => x.Value.FinalCertainty) * this.Trashold;
                    foreach (var cls in input.Classes) {
                        var cert = cls.Value.FinalCertainty;
                        ClassificationDecision val;
                        if (cert >= trashold) {
                            val = cls.Value.Modify($"[RTr] Cert. {cert} >= trashold {trashold}. The decision's cert. is not changed.");
                        } else {
                            val = cls.Value.Modify(0, $"[RTr] Cert. {cert} < trashold {trashold}. The decision's cert. is set to 0.");
                        }
                        output[cls.Key] = val;
                    }
                    return new FuzzyClassificationResult(output);
                }
            }

            private class RelativeInterpreter : InterpretFuzzyClassification {
                public decimal Ratio { get; private set; }

                public RelativeInterpreter(decimal ratio) {
                    if (ratio <= 1)
                        throw new ArgumentException($"The '{nameof(ratio)}' parameter must be in [1; +Inf].");
                    this.Ratio = ratio;
                }

                /// <summary>
                /// Returns classes with the certainty greater than the
                /// certainties of other classes by the given ratio.
                /// </summary>
                public override IFuzzyClassificationResult Filter(IFuzzyClassificationResult input) {
                    var output = new Dictionary<Cls, ClassificationDecision>();
                    foreach (var cls in input.Classes) {
                        var cert = cls.Value.FinalCertainty;
                        var others = input.Classes.Where(x => x.Key != cls.Key).ToList();
                        var maxOther = others.Any()
                            ? others.Max(x => x.Value.FinalCertainty)
                            : 0;
                        var trashold = maxOther * this.Ratio;
                        if (trashold > 1)
                            trashold = 1;
                        ClassificationDecision val;
                        // TODO. Strange semantics. Check.
                        if (cert >= trashold) {
                            val = cls.Value.Modify($"[Rel] Cert. {cert} >= max. other cert. {maxOther} * ratio {this.Ratio} ~= {trashold}. The decision's cert. is not changed.");
                        } else {
                            val = cls.Value.Modify(0, $"[Rel] Cert. {cert} < max. other cert. {maxOther} * ratio {this.Ratio} ~= {trashold}. The decision's cert. is set to 0.");
                        }
                        output[cls.Key] = val;
                    }
                    return new FuzzyClassificationResult(output);
                }
            }
        }
    //}
}
