﻿using System;
using System.Collections.Generic;

namespace Krolik.Classifiers {
    internal interface IProvider<T> : IDisposable {
        /// <summary>
        /// Returns the next instance (if exhausted starts from the beginning and so on).
        /// </summary>
        T Next();

        /// <summary>
        /// Goes to the first provided instance.
        /// </summary>
        void Reset();

        /// <summary>
        /// Splits the contained collection in two parts. Bound
        /// instances in the output providers MAY be the same.
        /// </summary>
        Tuple<IProvider<T>, IProvider<T>> Split(float part, float startAt = 0);

        /// <summary>
        /// Returns all contained objects.
        /// </summary>
        IList<T> GetAll();
    }

    internal interface ISamplable<T> {
        IProvider<T> GetProbabilitySample(float probability);
    }
}
