﻿using Krolik.Mathematics;

namespace Krolik.Classifiers {
    // TODO. There exists only one implementation.
    // Remove the interface?
    internal interface IGpnClassifierFactory {
        Krolik.Classifiers.GpnClassifier.GpnClassifier Make(
            string founderType,
            string classificationActivationStrategy,
            IFuzzySet fuzzySet
        );
    }
}
