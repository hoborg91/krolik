﻿using Cil.Common;
using System;

namespace Krolik.Classifiers {
    [System.Runtime.Serialization.DataContract]
    internal struct Cls : IComparable<Cls> {
        [System.Runtime.Serialization.DataMember]
        public byte Index { get; set; }

        [System.Runtime.Serialization.DataMember]
        public string Name { get; set; }

        public static explicit operator Cls(byte index) {
            return new Cls { Index = index, };
        }

        public static explicit operator byte(Cls cls) {
            return cls.Index;
        }

        public override string ToString() {
            return this.Name.IsNullOrWhiteSpace()
                ? "." + this.Index.ToString()
                : this.Name;
        }

        internal Cls Next() {
            if (this.Index == byte.MaxValue)
                throw new Exception("The set of all possible classes is exhausted.");
            return (Cls)(this.Index + 1);
        }

        public override int GetHashCode() {
            return this.Index.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (!(obj is Cls))
                return false;
            var cls = (Cls)obj;
            return this.Index == cls.Index;
        }

        public int CompareTo(Cls other) {
            return this.Index.CompareTo(other.Index);
        }

        public static bool operator ==(Cls a, Cls b) {
            return a.Equals(b);
        }

        public static bool operator !=(Cls a, Cls b) {
            return !(a == b);
        }
    }
}
