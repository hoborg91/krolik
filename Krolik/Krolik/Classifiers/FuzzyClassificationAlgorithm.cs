﻿using Krolik.Exceptions;
using Krolik.Gpns;
using Krolik.Infrastructure;
using Krolik.Mathematics;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Krolik.Classifiers {
    internal abstract class FuzzyClassificationAlgorithm {
        protected readonly IFuzzyLogic FuzzyLogic;

        protected FuzzyClassificationAlgorithm(IFuzzyLogic fuzzyLogic) {
            if (fuzzyLogic == null)
                throw new ArgumentNullException(nameof(fuzzyLogic));
            this.FuzzyLogic = fuzzyLogic;
        }

        public abstract FuzzyClassificationResult Classify(
            IGpn network,
            IHybridVector vector
        );

        protected IList<IList<IVertex>> _splitInPyramids(IList<IVertex> joinedPyramid) {
            var result = new List<List<IVertex>>();
            foreach (var vertex in joinedPyramid) {
                var company = vertex.GetSuperset().Union(new[] { vertex, });
                if (vertex is IConceptor)
                    company = company.Union((vertex as IConceptor).GetSubset());
                var companyList = company.Intersect(joinedPyramid).ToList();
                var already = false;
                foreach (var r in result) {
                    if (companyList.Any(c => r.Contains(c))) {
                        already = true;
                        r.Add(vertex);
                        break;
                    }
                }
                if (!already)
                    result.Add(new List<IVertex> { vertex, });
            }
            return result
                .Select(x => (IList<IVertex>)x)
                .ToList();
        }
    }

    internal class FuzzyClassificationAlgorithmFactory {
        private readonly IFuzzyLogic _fuzzyLogic;

        public FuzzyClassificationAlgorithmFactory(IFuzzyLogic fuzzyLogic) {
            if (fuzzyLogic == null)
                throw new ArgumentNullException(nameof(fuzzyLogic));
            this._fuzzyLogic = fuzzyLogic;
        }

        public FuzzyClassificationAlgorithm TopSuspiciousDirtyMinusOthers() {
            return new TopSuspiciousDirtyMinusOthersAlgorithm(this._fuzzyLogic);
        }

        public FuzzyClassificationAlgorithm TopClearRelative() {
            return new TopClearRelativeAlgorithm(this._fuzzyLogic);
        }

        public FuzzyClassificationAlgorithm AccumulativeSubset() {
            return new AccumulativeSubsetAlgorithm(this._fuzzyLogic);
        }

        public FuzzyClassificationAlgorithm AccumulativeSuperset() {
            return new AccumulativeSupersetAlgorithm(this._fuzzyLogic);
        }

        public FuzzyClassificationAlgorithm TopClearRelativeWithEnforcement() {
            return new TopClearRelativeWithEnforcementAlgorithm(this._fuzzyLogic);
        }

        private class TopSuspiciousDirtyMinusOthersAlgorithm : FuzzyClassificationAlgorithm {
            public TopSuspiciousDirtyMinusOthersAlgorithm(
                IFuzzyLogic fuzzyLogic
            ) : base(fuzzyLogic) {

            }

            public override FuzzyClassificationResult Classify(
                IGpn network,
                IHybridVector vector
            ) {
                var result = new Dictionary<Cls, Certainty>();
                var joinedPyramid = network.GetVertices()
                    .Where(x => x.Active > 0)
                    .ToList();//(this.Network as IFuzzyActivatable).ActivateFuzzy(vector, precision);
                if (joinedPyramid.Count == 0)
                    return new FuzzyClassificationResult(result);
                var isolatedPyramids = _splitInPyramids(joinedPyramid);
                if (isolatedPyramids.Count == 0)
                    throw new InconsistentStateException("There must be at least one isolated pyramid.");

                var suspicious = joinedPyramid
                    .SelectMany(x1 => x1.IsControlFor())
                    .Distinct();
                foreach (var cls in suspicious) {
                    var isolatedConfidences = new List<Certainty>();
                    foreach (var isolatedPyramid in isolatedPyramids) {
                        var suspiciousControls = isolatedPyramid
                            .Where(x => x.IsControlFor(cls))
                            .ToList();
                        if (!suspiciousControls.Any()) {
                            isolatedConfidences.Add(0);
                            continue;
                        }
                        var topSuspiciousControls = suspiciousControls
                            .Where(x => !x.GetSuperset().Any(super => super.IsControlFor(cls)))
                            .ToList();
                        var topWithOthers = topSuspiciousControls
                            .Select(top => new {
                                Suspicious = top,
                                Others = top.GetSuperset()
                                    .Where(x => x.IsControlFor().Any())
                                    .Select(x => x)
                                    .ToList(),
                            })
                            .ToList();
                        var suspConf = this.FuzzyLogic.Or(topWithOthers.Select(x => x.Suspicious.Active).ToArray());
                        var otherConf = this.FuzzyLogic.Or(
                            topWithOthers
                                .SelectMany(x => x.Others)
                                .Select(x => x.Active)
                                .ToArray()
                        );
                        var resultConf = this.FuzzyLogic.And(
                            suspConf,
                            this.FuzzyLogic.Not(otherConf)
                        );
                        isolatedConfidences.Add(resultConf);
                    }

                    KrolikLoggers.Get.Classification.Debug(
                        $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}) as {cls}: " +
                        string.Join(" or ", isolatedConfidences.Select(x => (x).ToString()))
                    );

                    var joinedConfidence = this.FuzzyLogic.Or(isolatedConfidences.ToArray());
                    if (joinedConfidence > 0)
                        result[cls] = joinedConfidence;
                }

                KrolikLoggers.Get.Classification.Debug(
                    $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}): " +
                    string.Join(", ", result.Select(x => $"[{x.Key}]={x.Value.ToString()}"))
                );

                return new FuzzyClassificationResult(result);
            }
        }

        private class TopClearRelativeAlgorithm : FuzzyClassificationAlgorithm {
            public TopClearRelativeAlgorithm(
                IFuzzyLogic fuzzyLogic
            ) : base(fuzzyLogic) {

            }

            public override FuzzyClassificationResult Classify(
                IGpn network,
                IHybridVector vector
            ) {
                var result = new Dictionary<Cls, Certainty>();
                var joinedPyramid = network.GetVertices()
                    .Where(x => x.Active > 0)
                    .ToList();//(this.Network as IFuzzyActivatable).ActivateFuzzy(vector, precision);
                if (joinedPyramid.Count == 0)
                    return new FuzzyClassificationResult(result);
                var isolatedPyramids = _splitInPyramids(joinedPyramid);
                if (isolatedPyramids.Count == 0)
                    throw new InconsistentStateException("There must be at least one isolated pyramid.");

                var suspicious = joinedPyramid
                    .SelectMany(x1 => x1.IsControlFor())
                    .Distinct();
                foreach (var cls in suspicious) {
                    var isolatedConfidences = new List<Certainty>();
                    foreach (var isolatedPyramid in isolatedPyramids) {
                        var suspiciousControls = isolatedPyramid
                            .Where(x => x.IsControlFor(cls))
                            .ToList();
                        if (!suspiciousControls.Any()) {
                            isolatedConfidences.Add(0);
                            continue;
                        }
                        var topControls = isolatedPyramid
                            .Where(v => true
                                && v.IsControlFor().Any()
                                && !v.GetSuperset().Any(super => super.IsControlFor().Any()))
                            .GroupByCollections(x => x.IsControlFor().ToList())
                            .ToDictionary(
                                g => g.Key.ToList(),
                                g => g.Select(v => v.Active));
                        var suspConf = this.FuzzyLogic.Or(topControls
                            .Where(x => x.Key.Contains(cls) && x.Key.Count == 1)
                            .SelectMany(x => x.Value)
                            .ToArray());
                        var otherConf = this.FuzzyLogic.Or(topControls
                            .Where(x => !x.Key.Contains(cls))
                            .SelectMany(x => x.Value)
                            .ToArray()
                        );
                        var resultConf = this.FuzzyLogic.And(
                            suspConf,
                            this.FuzzyLogic.Not(otherConf)
                        );
                        isolatedConfidences.Add(resultConf);
                    }

                    KrolikLoggers.Get.Classification.Debug(
                        $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}) as {cls}: " +
                        string.Join(" or ", isolatedConfidences.Select(x => (x).ToString()))
                    );

                    var joinedConfidence = this.FuzzyLogic.Or(isolatedConfidences.ToArray());
                    if (joinedConfidence > 0)
                        result[cls] = joinedConfidence;
                }

                KrolikLoggers.Get.Classification.Debug(
                    $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}): " +
                    string.Join(", ", result.Select(x => $"[{x.Key}]={x.Value.ToString()}"))
                );

                return new FuzzyClassificationResult(result);
            }
        }

        private class TopClearRelativeWithEnforcementAlgorithm : FuzzyClassificationAlgorithm {
            public TopClearRelativeWithEnforcementAlgorithm(
                IFuzzyLogic fuzzyLogic
            ) : base(fuzzyLogic) {

            }

            public override FuzzyClassificationResult Classify(
                IGpn network,
                IHybridVector vector
            ) {
                var result = new Dictionary<Cls, Certainty>();
                var joinedPyramid = network.GetVertices()
                    .Where(x => x.Active > 0)
                    .ToList();//(this.Network as IFuzzyActivatable).ActivateFuzzy(vector, precision);
                if (joinedPyramid.Count == 0)
                    return new FuzzyClassificationResult(result);
                var isolatedPyramids = _splitInPyramids(joinedPyramid);
                if (isolatedPyramids.Count == 0)
                    throw new InconsistentStateException("There must be at least one isolated pyramid.");

                var suspicious = joinedPyramid
                    .SelectMany(x1 => x1.IsControlFor())
                    .Distinct();
                foreach (var cls in suspicious) {
                    var isolatedConfidences = new List<Certainty>();
                    foreach (var isolatedPyramid in isolatedPyramids) {
                        var suspiciousControls = isolatedPyramid
                            .Where(x => x.IsControlFor(cls))
                            .ToList();
                        if (!suspiciousControls.Any()) {
                            isolatedConfidences.Add(0);
                            continue;
                        }
                        var topControls = isolatedPyramid
                            .Where(v => true
                                && v.IsControlFor().Any()
                                && !v.GetSuperset().Any(super => super.IsControlFor().Any()))
                            .GroupByCollections(x => x.IsControlFor().ToList())
                            .ToDictionary(
                                g => g.Key.ToList(),
                                g => g.Select(v => v.Active));
                        var suspConf = this._or(topControls
                            .Where(x => x.Key.Contains(cls) && x.Key.Count == 1)
                            .SelectMany(x => x.Value)
                            .ToArray());
                        var otherConf = this._or(topControls
                            .Where(x => !x.Key.Contains(cls))
                            .SelectMany(x => x.Value)
                            .ToArray()
                        );
                        var resultConf = this.FuzzyLogic.And(
                            suspConf,
                            this.FuzzyLogic.Not(otherConf)
                        );
                        isolatedConfidences.Add(resultConf);
                    }

                    KrolikLoggers.Get.Classification.Debug(
                        $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}) as {cls}: " +
                        string.Join(" or ", isolatedConfidences.Select(x => (x).ToString()))
                    );

                    var joinedConfidence = this._or(isolatedConfidences.ToArray());
                    if (joinedConfidence > 0)
                        result[cls] = joinedConfidence;
                }

                KrolikLoggers.Get.Classification.Debug(
                    $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}): " +
                    string.Join(", ", result.Select(x => $"[{x.Key}]={x.Value.ToString()}"))
                );

                return new FuzzyClassificationResult(result);
            }

            private Certainty _or(Certainty[] input) {
                var result = Certainty.None;
                for (int i = 0; i < input.Length; i++) {
                    var c = input[i];
                    result = this.FuzzyLogic.Or(
                        result,
                        c,
                        this.FuzzyLogic.Not(this.FuzzyLogic.And(
                            result,
                            c)
                        )
                    );
                }
                return result;
            }
        }

        private abstract class AccumulativeAlgorithmBase : FuzzyClassificationAlgorithm {
            public AccumulativeAlgorithmBase(
                IFuzzyLogic fuzzyLogic
            ) : base(fuzzyLogic) {

            }

            public override FuzzyClassificationResult Classify(
                IGpn network,
                IHybridVector vector
            ) {
                var result = new Dictionary<Cls, Certainty>();
                var joinedPyramid = network.GetVertices()
                    .Where(x => x.Active > 0)
                    .ToList();//(this.Network as IFuzzyActivatable).ActivateFuzzy(vector, precision);
                if (joinedPyramid.Count == 0)
                    return new FuzzyClassificationResult(result);
                var isolatedPyramids = _splitInPyramids(joinedPyramid);
                if (isolatedPyramids.Count == 0)
                    throw new InconsistentStateException("There must be at least one isolated pyramid.");

                var suspicious = joinedPyramid
                    .SelectMany(x1 => x1.IsControlFor())
                    .Distinct();
                foreach (var cls in suspicious) {
                    var isolatedConfidences = new List<Certainty>();
                    foreach (var isolatedPyramid in isolatedPyramids) {
                        var suspiciousControls = isolatedPyramid
                            .Where(x => x.IsControlFor(cls))
                            .ToList();
                        if (!suspiciousControls.Any()) {
                            isolatedConfidences.Add(0);
                            continue;
                        }
                        var topControls = isolatedPyramid
                            .Where(v => true
                                && v.IsControlFor().Any()
                                && !v.GetSuperset().Any(super => super.IsControlFor().Any()))
                            .GroupByCollections(x => x.IsControlFor().ToList())
                            .ToDictionary(
                                g => g.Key.ToList(),
                                g => g.Select(v => v));
                        var suspConf = this.FuzzyLogic.Or(topControls
                            .Where(x => x.Key.Contains(cls) && x.Key.Count == 1)
                            .SelectMany(x => x.Value)
                            .Select(x => this._accumulate(x, x.IsControlFor().Single()))
                            .ToArray());
                        var otherConf = this.FuzzyLogic.Or(topControls
                            .Where(x => !x.Key.Contains(cls))
                            .SelectMany(x => x.Value)
                            .Select(x => this._accumulate(x, x.IsControlFor().Single()))
                            .ToArray()
                        );
                        var resultConf = this.FuzzyLogic.And(
                            suspConf,
                            this.FuzzyLogic.Not(otherConf)
                        );
                        isolatedConfidences.Add(resultConf);
                    }

                    KrolikLoggers.Get.Classification.Debug(
                        $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}) as {cls}: " +
                        string.Join(" or ", isolatedConfidences.Select(x => (x).ToString()))
                    );

                    var joinedConfidence = this.FuzzyLogic.Or(isolatedConfidences.ToArray());
                    if (joinedConfidence > 0)
                        result[cls] = joinedConfidence;
                }

                KrolikLoggers.Get.Classification.Debug(
                    $"Try classify {vector} (true cls: {string.Join(",", vector.Classes)}): " +
                    string.Join(", ", result.Select(x => $"[{x.Key}]={x.Value.ToString()}"))
                );

                return new FuzzyClassificationResult(result);
            }

            protected abstract Certainty _accumulate(
                IVertex value,
                Cls cls
            );
        }

        private class AccumulativeSubsetAlgorithm : AccumulativeAlgorithmBase {
            public AccumulativeSubsetAlgorithm(
                IFuzzyLogic fuzzyLogic
            ) : base(fuzzyLogic) {

            }
            
            protected override Certainty _accumulate(
                IVertex value, 
                Cls cls
            ) {
                var currentClss = value.IsControlFor().ToList();
                var conceptor = value as IConceptor;
                var subResults = new Certainty[0];
                if (conceptor != null) {
                    subResults = conceptor.GetZeroSubset()
                        .Select(x => this._accumulate(x, cls))
                        .ToArray();
                }
                var subResult = this.FuzzyLogic.Or(subResults);
                if (currentClss.Count == 1) {
                    if (currentClss.Single() == cls) {
                        var r = this.FuzzyLogic.Or(value.Active, subResult);
                        return r;
                    } else {
                        var r = this.FuzzyLogic.And(
                            subResult,
                            this.FuzzyLogic.Not(value.Active)
                        );
                        return r;
                    }
                } else if (currentClss.Any()) {
                    throw new NotImplementedException();
                } else
                    return subResult;
            }
        }

        private class AccumulativeSupersetAlgorithm : AccumulativeAlgorithmBase {
            public AccumulativeSupersetAlgorithm(
                IFuzzyLogic fuzzyLogic
            ) : base(fuzzyLogic) {

            }

            protected override Certainty _accumulate(
                IVertex value,
                Cls cls
            ) {
                var currentClss = value.IsControlFor().ToList();
                //var conceptor = value as IConceptor;
                var subResults = new Certainty[0];
                //if (conceptor != null) {
                    subResults = value.GetZeroSuperset()
                        .Select(x => this._accumulate(x, cls))
                        .ToArray();
                //}
                var subResult = this.FuzzyLogic.Or(subResults);
                if (currentClss.Count == 1) {
                    if (currentClss.Single() == cls) {
                        var r = this.FuzzyLogic.Or(value.Active, subResult);
                        return r;
                    } else {
                        throw new InconsistentStateException();
                    }
                } else if (currentClss.Any()) {
                    throw new NotImplementedException();
                } else
                    return subResult;
            }
        }
    }
}
