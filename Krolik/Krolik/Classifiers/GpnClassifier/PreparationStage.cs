﻿using Krolik.Gpns;
using Krolik.VectorsProviders;
using System;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(Founding))]
    [System.Runtime.Serialization.KnownType(typeof(FoundationChecking))]
    [System.Runtime.Serialization.KnownType(typeof(Building))]
    [System.Runtime.Serialization.KnownType(typeof(MsFilling))]
    [System.Runtime.Serialization.KnownType(typeof(Teaching))]
    internal abstract class PreparationStage {
        [System.Runtime.Serialization.DataMember]
        public PreparationStage NextStage { get; set; }

        [System.Runtime.Serialization.DataMember]
        private byte _enterState = 0;

        protected bool IsJustEntered => this._enterState == 1;

        internal Tuple<PreparationStage, bool> Prepare(IGpn network, IVectorsProvider trainingSet) {
            if (this._enterState == 0)
                this._enterState = 1;
            else if (this._enterState == 1)
                this._enterState = 2;

            if (this.InternalPrepare(network, trainingSet))
                return Tuple.Create(this, true);
            if (this.NextStage == null)
                return Tuple.Create(this, false);
            return Tuple.Create(this.NextStage, true);
        }

        internal abstract bool InternalPrepare(IGpn network, IVectorsProvider trainingSet);

        internal abstract string GetProgress();
    }
}
