﻿using System.Collections.Generic;
using System.Linq;

namespace Krolik.Classifiers.GpnClassifier {
    /// <summary>
    /// Incapsulates rules which are used to determine if
    /// the vertices group can be classified as a given class.
    /// </summary>
    internal static class ClassificationStrategy {
        /// <summary>
        /// Returns true, iff the superset of the given vertex (intersected with the
        /// given pyramid) contains at least one vertex containing the given class
        /// (maybe among other classes).
        /// </summary>
        public static bool Classify(IVertex vertex, IList<IVertex> pyramid, Cls vectorCls) {
            return vertex.GetSuperset().Intersect(pyramid)
                .Any(x2 => true
                    //&& x2.IsControlFor().Count() == 1 
                    && x2.IsControlFor().Contains(vectorCls)
                );
        }

        //public static bool ClassifyFuzzy(IFuzzyActiveVertex vertex, IList<IVertex> pyramid, Cls vectorCls) {
        //    return vertex.GetSuperset().Intersect(pyramid)
        //        .Any(x2 => true
        //            //&& x2.IsControlFor().Count() == 1 
        //            && x2.IsControlFor().Contains(vectorCls)
        //            && (x2 as IFuzzyActiveVertex).FuzzyActive > vertex.FuzzyActive
        //        );
        //}
    }
}
