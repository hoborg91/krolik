﻿using Krolik.Mathematics;
using Krolik.VectorsProviders;
using System.Collections.Generic;

namespace Krolik.Classifiers.GpnClassifier {
    internal interface IClassifier {
        void Reset();

        /// <summary>
        /// Returns false if the preparation is completed.
        /// Otherwise, true.
        /// </summary>
        bool Prepare(IVectorsProvider trainingSet);

        IFuzzyClassificationResult Classify(IHybridVector vector, IPrecisionProvider precision);
    }
}
