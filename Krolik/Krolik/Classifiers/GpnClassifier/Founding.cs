﻿using Krolik.Gpns;
using Krolik.VectorsProviders;
using System.Threading;
using System.Configuration;

namespace Krolik.Classifiers.GpnClassifier {
    public static class FoundingDebugger {
        private static byte _continueState = 2;

        public static void Step() {
            _continueState = 1;
        }

        public static void Continue() {
            _continueState = 2;
        }

        public static void Pause() {
            _continueState = 0;
        }

        internal static void Wait() {
            //Thread.Sleep(10);
            while (_continueState == 0) {
                Thread.Sleep(10);
            }
            if (_continueState == 1)
                _continueState = 0;
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class Founding : PreparationStage {
        [System.Runtime.Serialization.DataMember]
        private IGpnFounder _founder;

        public Founding(IGpnFounder founder) {
            this._founder = founder;
        }

        internal override bool InternalPrepare(IGpn network, IVectorsProvider trainingSet) {
            if (true
                && this.IsJustEntered 
                && ConfigurationManager.AppSettings["debugFoundingStage"] != null 
                && bool.Parse(ConfigurationManager.AppSettings["debugFoundingStage"])
            ) {
                FoundingDebugger.Pause();
            }
            FoundingDebugger.Wait();
            return this._founder.Found(network, trainingSet);
        }

        internal override string GetProgress() {
            return "Founding" + this._founder.GetProgress();
        }
    }
}
