﻿using Krolik.Exceptions;
using Krolik.Gpns;
using Krolik.Infrastructure;
using Krolik.VectorsProviders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(ClassicActivationStrategy))]
    [System.Runtime.Serialization.KnownType(typeof(FuzzyAndActivationStrategy))]
    internal class Teaching : PreparationStage {
        [System.Runtime.Serialization.DataMember]
        private GpnTeacher _teacher = new GpnTeacher();

        [System.Runtime.Serialization.DataMember]
        private readonly IActivationStrategy _activationStrategy;

        public Teaching(IActivationStrategy activationStrategy) {
            this._activationStrategy = activationStrategy ?? throw new ArgumentNullException(nameof(activationStrategy));
        }

        internal override bool InternalPrepare(IGpn network, IVectorsProvider trainingSet) {
            return this._teacher.Teach(network, trainingSet, this._activationStrategy);
        }

        internal override string GetProgress() {
            return "Teaching";
        }

        // TODO. Refactor. It seems that the GpnTeacher class is not
        // necessary. Its logic can be injected directly in the Teaching
        // class.
        [System.Runtime.Serialization.DataContract]
        class GpnTeacher {
            private IHybridVector _lastSignificantVector = null;

            public bool Teach(
                IGpn gpn, 
                IVectorsProvider trainingSet, 
                IActivationStrategy activationStrategy
            ) {
                Dbg.Enter("Teach");
                if (gpn == null)
                    throw new ArgumentNullException(nameof(gpn));

                //var conceptors = gpn.GetVertices() // for debug only
                //    .OfType<IConceptor>()
                //    .Select(x => new {
                //        Conceptor = x,
                //        ZeroSubsetCount = x.GetZeroSubset().Count(),
                //    })
                //    .ToList();

                if (trainingSet is MultipleFilesVectorsProvider)
                    (trainingSet as MultipleFilesVectorsProvider).EnableLogging(true);
                var vector = trainingSet.Next();
                gpn.Deactivate();

                if (true
                    && this._lastSignificantVector != null
                    && this._lastSignificantVector.Equals(vector)
                ) {
                    if (trainingSet is MultipleFilesVectorsProvider)
                        (trainingSet as MultipleFilesVectorsProvider).EnableLogging(false);
                    Dbg.Leave("Teach");
                    return false;
                }
                gpn.Activate(vector, activationStrategy);

                var vectorCls = vector.Classes.Single();
                KrolikLoggers.Get.Preparation.Debug($"{nameof(Teach)}({vector})");
                var pyramid = gpn.GetVertices(x1 => x1.IsActive()).ToList();
                if (!pyramid.Any()) {
                    //var dbg0 = gpn.GetVertices()
                    //    .OfType<IReceptor>()
                    //    .Where(x => x.CorrespondsTo(vector))
                    //    .ToList();
                    //var p = 0.1;
                    //LinearAlgebra.Get().Precision = p;
                    //var dbg1 = gpn.GetVertices()
                    //    .OfType<IReceptor>()
                    //    .Where(x => x.CorrespondsTo(vector))
                    //    .ToList();
                    throw new InconsistentNetworkException("It seems that the building stage has not been completed.");
                }
                var go = true;
                do {
                    //if (KrolikSettings.Current.EnableDetailedLogging)
                    //    KrolikSettings.Current.Log("Teaching");
                    go = false
                        || this._teachB1(pyramid, vectorCls)
                        || this._teachB2(pyramid, vectorCls)
                    ;
                    if (go)
                        this._lastSignificantVector = vector;
                } while (go);
                if (this._lastSignificantVector == null)
                    this._lastSignificantVector = vector;
                Dbg.Leave("Teach");
                return true;
            }

            private bool _teachB1(List<IVertex> pyramid, Cls vectorCls) {
                Dbg.Enter("_teachB1");
                //KrolikLoggers.Get.Preparation.Debug($"{nameof(_teachB1)}({pyramid.Count}, {vectorCls})");
                var shouldBeControl = ShouldBeControl(pyramid, vectorCls);
                var result = shouldBeControl.MarkAsControlFor(vectorCls);
                Dbg.Leave("_teachB1");
                return result;
            }

            private bool _teachB2(List<IVertex> pyramid, Cls vectorCls) {
                Dbg.Enter("_teachB2");
                //KrolikLoggers.Get.Preparation.Debug($"{nameof(_teachB2)}({pyramid.Count}, {vectorCls})");
                var result = false;
                Dbg.Enter("(find controls)");
                var otherControls = pyramid
                    .Where(x1 => x1.IsControlFor().Any(x2 => !x2.Equals(vectorCls)))
                    .ToList();
                otherControls = otherControls
                    .Where(x1 => !ClassificationStrategy.Classify(x1, pyramid, vectorCls))
                    .ToList();
                Dbg.Leave("(find controls)");
                foreach (var other in otherControls) {
                    //if (KrolikSettings.Current.EnableDetailedLogging)
                    //    KrolikSettings.Current.Log("Teaching: B2");
                    //result = true;
                    Dbg.Enter("(get superset)");
                    var superset = other.GetSuperset()
                        //.Union(new[] { other, })
                        .Intersect(pyramid)
                        .ToList();
                    Dbg.Leave("(get superset)");
                    if (superset.Count == 0) {
                        throw new NotSupportedDataException("The given data possibly contains controversial examples.");
                    }
                    Dbg.Enter("(find proper)");
                    var shouldBeControl = ShouldBeControl(superset, vectorCls);
                    Dbg.Leave("(find proper)");
                    if (shouldBeControl.IsControlFor().Contains(vectorCls)) {
                        //if (shouldBeControl2.IsControlFor().Count() != 1)
                        //    throw new NotSupportedDataException("The given data possibly contains controversial examples.");
                        continue;
                    }
                    result = true;
                    Dbg.Enter("(mark as control)");
                    shouldBeControl.MarkAsControlFor(vectorCls);
                    Dbg.Leave("(mark as control)");
                }
                Dbg.Leave("_teachB2");
                return result;
            }

            private static IVertex ShouldBeControl(IEnumerable<IVertex> vertices, Cls cls) {
                var result = vertices.OrderByDescending(x1 => x1.M(cls))
                    .ThenByDescending(x1 => x1.K())
                    .First();
                return result;
            }
        }
    }
}
