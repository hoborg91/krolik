﻿using Krolik.Gpns;
using Krolik.Infrastructure;
using Krolik.Mathematics;
using Krolik.VectorsProviders;
using System;
using System.Linq;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(ClassicActivationStrategy))]
    [System.Runtime.Serialization.KnownType(typeof(FuzzyAndActivationStrategy))]
    internal class Building : PreparationStage {
        [System.Runtime.Serialization.DataMember]
        private GpnBuilder _builder = new GpnBuilder();

        [System.Runtime.Serialization.DataMember]
        private readonly IActivationStrategy _activationStrategy;

        public Building(IActivationStrategy activationStrategy) {
            this._activationStrategy = activationStrategy ?? throw new ArgumentNullException(nameof(activationStrategy));
        }

        internal override bool InternalPrepare(IGpn network, IVectorsProvider trainingSet) {
            return this._builder.Build(network, trainingSet, this._activationStrategy);
        }

        internal override string GetProgress() => "Building (" + 
            "last significant vector " + this._builder.LastSignificantVector.Vector + ", " +
            this._builder.LastSignificantVector.SkippedVectorsCount + " vectors skipped)";

        [System.Runtime.Serialization.DataContract]
        private class GpnBuilder {
            private VectorInfo _lastSignificantVector = null;

            public VectorInfo LastSignificantVector {
                get {
                    if (this._lastSignificantVector == null) {
                        this._lastSignificantVector = new VectorInfo();
                    }
                    return this._lastSignificantVector;
                }
            }

            public int SkippedVectorsCount { get; private set; } = 0;

            public bool Build(IGpn gpn, IVectorsProvider trainingSet, IActivationStrategy activationStrategy) {
                if (gpn == null)
                    throw new ArgumentNullException(nameof(gpn));
                var vector = trainingSet.Next();
                this.LastSignificantVector.Increment();
                KrolikLoggers.Get.Preparation.Debug("Build. Vector: " + vector.ToString());
                gpn.Deactivate();
                if (true
                    && this.LastSignificantVector.Vector != null
                    && this.LastSignificantVector.Vector.Equals(vector)
                )
                    return false;
                gpn.Activate(vector, activationStrategy);
                var go = false;
                do {
                    go = false
                        || this._buildA1(gpn, vector)
                        || this._buildA2(gpn, vector)
                    ;
                } while (go);
                if (this.LastSignificantVector.Vector == null)
                    this.LastSignificantVector.SetVector(vector);
                return true;
            }

            /// <summary>
            /// Inserts vertices between two layers.
            /// </summary>
            private bool _buildA1(IGpn gpn, IHybridVector vector) {
                var result = false;
                var vertexToCorrect = gpn
                    .GetVertices()
                    .OfType<IConceptor>()
                    .Select(x1 => new {
                        Head = x1,
                        ActiveZeroSubset = x1.GetZeroSubset()
                            .Where(x2 => x2.IsActive())
                            .ToList(),
                    })
                    .Where(x1 => true
                        && !x1.Head.IsActive()
                        && x1.ActiveZeroSubset.Count() >= 2
                    )
                    .FirstOrDefault();
                if (vertexToCorrect != null) {
                    this.LastSignificantVector.SetVector(vector);
                    var lower = vertexToCorrect.ActiveZeroSubset;
                    var higher = vertexToCorrect.Head;
                    var toAdd = gpn.InsertBetween(lower, higher);
                    toAdd.Active = Certainty.Absolute;
                    result = true;
                    KrolikLoggers.Get.Preparation.Debug("BuildA1. " +
                        "vector=" + vector.ToString() + " " +
                        "lower={" + string.Join(", ", lower.Select(x => x.ToString())) + "} " +
                        "higher=" + higher.ToString()
                    );
                }

                return result;
            }

            /// <summary>
            /// Inserts vertices above the activated ones.
            /// </summary>
            private bool _buildA2(IGpn gpn, IHybridVector vector) {
                var result = false;
                var g = gpn.G().ToList();
                if (g.Count > 1) {
                    if (g.Count > 2) {
                        var dists = g
                            .Select(vx => vx as Gpn.VerticesStructure.ContinuousReceptor)
                            .Where(x => x != null)
                            .Select(x => new {
                                Vertex = x,
                                Distance = x.ForDebug_Distance,
                                Mu = x.ForDebug_Mu,
                                Sum = x.PlaneComponents[x.PlaneComponents.Length - 1] + x.PlaneComponents
                                    .Take(x.PlaneComponents.Length - 1)
                                    .Select((c, i) => c * vector.ContinuumComponents[i])
                                    .Sum(),
                            })
                            .ToList();
                        var hello = 1;
                    }
                    this.LastSignificantVector.SetVector(vector);
                    var toAdd = gpn.InsertBetween(g, null);
                    toAdd.Active = Certainty.Absolute;
                    result = true;
                    var gStrings = g.Select(x => x.ToString()).ToList();
                    KrolikLoggers.Get.Preparation.Debug("BuildA2. " +
                        "vector=" + vector.ToString() + " " +
                        "g={" + string.Join(", ", gStrings) + "}"
                    );
                }
                return result;
            }

            public class VectorInfo {
                private IHybridVector _vector = null;

                public long SkippedVectorsCount { get; private set; } = 0;

                public IHybridVector Vector {
                    get {
                        return this._vector;
                    }
                    private set {
                        this.SkippedVectorsCount = 0;
                        this._vector = value;
                    }
                }

                public void SetVector(IHybridVector vector) {
                    this.Vector = vector;
                }

                public void Increment() {
                    this.SkippedVectorsCount++;
                }
            }
        }
    }
}
