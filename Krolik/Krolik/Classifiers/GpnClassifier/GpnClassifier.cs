﻿using Krolik.Exceptions;
using Krolik.Gpns;
using Krolik.Infrastructure;
using Krolik.Mathematics;
using Krolik.VectorsProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(Gpn))]
    [System.Runtime.Serialization.KnownType(typeof(CartesianGpnFounder))]
    [System.Runtime.Serialization.KnownType(typeof(SequentialGpnFounder))]
    [System.Runtime.Serialization.KnownType(typeof(ClassicActivationStrategy))]
    [System.Runtime.Serialization.KnownType(typeof(FuzzyAndActivationStrategy))]
    internal class GpnClassifier 
        : IClassifier
        , IDisposable 
    {
        [System.Runtime.Serialization.DataMember]
        public IGpn Network { get; protected set; }

        [System.Runtime.Serialization.DataMember]
        private PreparationStage _currentStage;

        [System.Runtime.Serialization.DataMember]
        private readonly IActivationStrategy _activationStrategyForPreparation;

        [System.Runtime.Serialization.DataMember]
        private readonly IActivationStrategy _activationStrategyForClassification;

        public GpnClassifier(
            IGpnFounder founder, 
            IFuzzySet fuzzySet,
            IActivationStrategy activationStrategyForPreparation,
            IActivationStrategy activationStrategyForClassification
        ) {
            this._activationStrategyForPreparation = activationStrategyForPreparation ?? throw new ArgumentNullException(nameof(activationStrategyForPreparation));
            this._activationStrategyForClassification = activationStrategyForClassification ?? throw new ArgumentNullException(nameof(activationStrategyForClassification));

            this.Network = new Gpn(fuzzySet);
            this._currentStage = _makeStagesChain(
                new Founding(founder),
                new FoundationChecking(activationStrategyForPreparation),
                new Building(activationStrategyForPreparation),
                new MsFilling(activationStrategyForPreparation),
                new Teaching(activationStrategyForPreparation)
            );
        }

        public GpnClassifier(
            IGpn network,
            PreparationStage currentStage,
            IActivationStrategy activationStrategyForPreparation,
            IActivationStrategy activationStrategyForClassification
        ) {
            this.Network = network ??
                throw new ArgumentNullException(nameof(network));
            this._currentStage = currentStage ??
                throw new ArgumentNullException(nameof(currentStage));
            this._activationStrategyForPreparation = activationStrategyForPreparation ?? 
                throw new ArgumentNullException(nameof(activationStrategyForPreparation));
            this._activationStrategyForClassification = activationStrategyForClassification ?? 
                throw new ArgumentNullException(nameof(activationStrategyForClassification));
        }

        private PreparationStage _makeStagesChain(params PreparationStage[] stages) {
            if (stages.Length == 0)
                throw new ArgumentException($"'{nameof(stages)}' parameter must not be empty.");
            for (int i = 0; i < stages.Length - 1; i++) {
                if (stages[i] == null)
                    throw new ArgumentException($"'{nameof(stages)}' parameter must not contain nulls.");
                stages[i].NextStage = stages[i + 1];
            }
            return stages[0];
        }

        #region Implementation of IClassifier

        public void Reset() {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns false if the preparation is completed.
        /// Otherwise, true.
        /// </summary>
        public bool Prepare(IVectorsProvider trainingSet) {
            Dbg.Enter("Prepare");
            if (trainingSet == null)
                throw new ArgumentNullException(nameof(trainingSet));
            
            var result = false;
            
            var p = this._currentStage.Prepare(this.Network, trainingSet);
            this._currentStage = p.Item1;
            result = p.Item2;
            Dbg.Leave("Prepare");
            return result;
        }

        public IFuzzyClassificationResult Classify(IHybridVector vector, IPrecisionProvider precision) {
            this._checkNetworkAndVector(vector);
            var result = new List<Cls>();
            this.Network.Deactivate();
            this.Network.Activate(vector, this._activationStrategyForClassification);
            
            // Only for debugging purposes.
            //var actives = this.Network.GetVertices(x => x.IsActive)
            //    .GroupBy(x => x.K())
            //    .Select(x => new {
            //        K = x.Key,
            //        Actives = x.ToList(),
            //    })
            //    .ToList();
            //var distances = this.Network.GetVertices()
            //    .OfType<Gpn.VerticesStructure.ContinuousReceptor>()
            //    .Select(x => new {
            //        Plane = x._plane,
            //        Distance = x._plane.DistanceTo(vector.ContinuumComponents),
            //    })
            //    .OrderBy(x => x.Distance)
            //    .ToList();

            var pyramid = this.Network.GetVertices(x1 => x1.IsActive())
                .ToList();
            if(KrolikSettings.Current.Soft)
                pyramid = _complete(pyramid);
            var suspicious = pyramid
                .SelectMany(x1 => x1.IsControlFor())
                .Distinct();
            foreach (var cls in suspicious) {
                var ok = true;
                foreach (var other in pyramid
                    .Where(x1 => x1.IsControlFor()
                        .Any(x2 => !x2.Equals(cls))
                    )
                ) {
                    if (ClassificationStrategy.Classify(other, pyramid, cls))
                        continue;
                    ok = false;
                    break;
                }
                if (ok)
                    result.Add(cls);
            }
            return new FuzzyClassificationResult(result);
        }

        public IFuzzyClassificationResult ClassifyFuzzy(
            IHybridVector vector, 
            IPrecisionProvider precision,
            FuzzyClassificationAlgorithm algorithm
        ) {
            KrolikLoggers.Get.Classification.Debug($"{nameof(ClassifyFuzzy)}({vector}, {precision})");
            this._checkNetworkAndVector(vector);

            this.Network.Deactivate();
            this.Network.Activate(vector, this._activationStrategyForClassification);

            return algorithm.Classify(this.Network, vector);
        }

        public string GetProgress() {
            var result = new StringBuilder(this._currentStage.GetProgress());
            var vertices = this.Network.GetVertices();
            var counts = vertices
                .GroupBy(x => x.K())
                .Select(x => new {
                    K = x.Key,
                    Count = x.Count(),
                })
                .Where(x => x.Count > 0)
                .OrderBy(x => x.K)
                .ToList();
            result.Append(". Network: ");
            if (counts.Count > 0) {
                result.Append(string.Join(", ", counts
                    .Select(x => "[" + x.K + "] = " + x.Count)));
                result.Append(".");
            }
            return result.ToString();
        }

        private void _checkNetworkAndVector(IHybridVector vector) {
            if (this.Network == null)
                throw new ClassifierNotReadyException();
            if (vector == null)
                throw new ArgumentNullException(nameof(vector));
        }

        private List<IVertex> _complete(List<IVertex> pyramid) {
            var result = new List<IVertex>();
            result.AddRange(pyramid);
            var go = true;
            while (go) {
                var toAdd = this.Network.GetVertices()
                    .OfType<IConceptor>()
                    .Where(x => true
                        && !result.Contains(x)
                        && result.Any(x1 => x.GetZeroSubset().Contains(x1))
                    )
                    .ToList();
                if (toAdd.Count == 0)
                    go = false;
                result.AddRange(toAdd);
            }
            return result;
        }

        #endregion Implementation of IClassifier

        #region Implementation of IDisposable

        [System.Runtime.Serialization.DataMember]
        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable

        public GpnClassifierForSerialization GetSerializationView() {
            return new GpnClassifierForSerialization(
                this.Network,
                this._currentStage,
                this._activationStrategyForPreparation,
                this._activationStrategyForClassification
            );
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class GpnClassifierForSerialization {
        [System.Runtime.Serialization.DataMember]
        public GpnForSerialization Network { get; set; }

        [System.Runtime.Serialization.DataMember]
        public PreparationStage CurrentStage { get; set; }

        [System.Runtime.Serialization.DataMember]
        public IActivationStrategy ActivationStrategyForPreparation;

        [System.Runtime.Serialization.DataMember]
        public IActivationStrategy ActivationStrategyForClassification;

        public GpnClassifierForSerialization(
            IGpn network,
            PreparationStage currentStage,
            IActivationStrategy forPreparation,
            IActivationStrategy forClassification
        ) {
            this.Network = network.GetViewForSerialization();
            this.CurrentStage = currentStage;
            this.ActivationStrategyForPreparation = forPreparation;
            this.ActivationStrategyForClassification = forClassification;
        }

        public GpnClassifier ToClassifier(GpnFactory gpnFactory) {
            var classifier = new GpnClassifier(
                this.Network.ToGpn(gpnFactory),
                this.CurrentStage,
                this.ActivationStrategyForPreparation,
                this.ActivationStrategyForClassification
            );
            return classifier;
        }
    }

    internal interface IGpnClassifiersSerializer {
        void SerializeToStream(Stream stream, GpnClassifier classifier);

        GpnClassifier DeserializeFromStream(Stream stream);
    }

    internal class StandardGpnClassifiersSerializer : IGpnClassifiersSerializer {
        public void SerializeToStream(Stream stream, GpnClassifier classifier) {
            Dbg.Enter("SerializeToStream");
            var formatter = new System.Runtime.Serialization.DataContractSerializer(typeof(GpnClassifier), new[] {
                typeof(CustomPrecisionProvider),
            });
            KrolikLoggers.Get.Common.Debug("formatter.WriteObject...");
            formatter.WriteObject(stream, classifier);
            Dbg.Leave("SerializeToStream");
        }

        public GpnClassifier DeserializeFromStream(Stream stream) {
            var formatter = new System.Runtime.Serialization.DataContractSerializer(typeof(GpnClassifier));
            var result = formatter.ReadObject(stream) as GpnClassifier;
            return result;
        }
    }

    internal class CustomGpnClassifiersSerializer : IGpnClassifiersSerializer {
        private readonly GpnFactory _gpnFactory;

        private Lazy<System.Runtime.Serialization.DataContractSerializer> _serializer =
            new Lazy<System.Runtime.Serialization.DataContractSerializer>(() => {
                return new System.Runtime.Serialization.DataContractSerializer(typeof(GpnClassifierForSerialization), new[] {
                    typeof(ClassicActivationStrategy),
                    typeof(FuzzyAndActivationStrategy),
                    typeof(LeftDiscreteFuzzySet),
                    typeof(LeftInverseFuzzySet),
                    typeof(LeftTriangleFuzzySet),
                    typeof(SequentialGpnFounder),
                    typeof(CartesianGpnFounder),
                    typeof(LinearAlgebra.Plane),
                    typeof(LinearAlgebra.MultiConcentratedPlane),
                    typeof(LinearAlgebra.ConcentratedPlane),
                });
            });

        public CustomGpnClassifiersSerializer(
            GpnFactory gpnFactory
        ) {
            this._gpnFactory = gpnFactory ??
                throw new ArgumentNullException(nameof(gpnFactory));
        }

        public void SerializeToStream(Stream stream, GpnClassifier classifier) {
            Dbg.Enter("SerializeToStream");
            var serializationView = classifier.GetSerializationView();
            var formatter = this._serializer.Value;
            KrolikLoggers.Get.Common.Debug("formatter.WriteObject...");
            formatter.WriteObject(stream, serializationView);
            Dbg.Leave("SerializeToStream");
        }

        public GpnClassifier DeserializeFromStream(Stream stream) {
            var formatter = this._serializer.Value;
            var result = formatter.ReadObject(stream) as GpnClassifierForSerialization;
            return result.ToClassifier(this._gpnFactory);
        }
    }
}