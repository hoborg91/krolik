﻿using Krolik.Gpns;
using Krolik.VectorsProviders;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    internal class Finish : PreparationStage {
        internal override bool InternalPrepare(IGpn network, IVectorsProvider trainingSet) {
            return false;
        }

        internal override string GetProgress() => "Finish";
    }
}
