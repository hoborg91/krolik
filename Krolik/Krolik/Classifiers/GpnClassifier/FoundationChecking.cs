﻿using Krolik.Exceptions;
using Krolik.Gpns;
using Krolik.VectorsProviders;
using System;
using System.Linq;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(ClassicActivationStrategy))]
    [System.Runtime.Serialization.KnownType(typeof(FuzzyAndActivationStrategy))]
    internal class FoundationChecking : PreparationStage {
        [System.Runtime.Serialization.DataMember]
        private readonly IActivationStrategy _activationStrategy;

        public FoundationChecking(IActivationStrategy activationStrategy) {
            this._activationStrategy = activationStrategy ??
                throw new ArgumentNullException(nameof(activationStrategy));
        }

        internal override bool InternalPrepare(
            IGpn network, 
            IVectorsProvider trainingSet
        ) {
            var receptors = network.GetVertices()
                .OfType<Gpn.VerticesStructure.ContinuousReceptor>()
                .ToList();
            //var points = receptors
            //    .SelectMany(x => (x._plane as LinearAlgebra.Plane).BasedOnPoints)
            //    .Distinct(EnumerableComparer.For<decimal>())
            //    .ToList();
            //var doubles = points
            //    .Select(x => new {
            //        Points = x,
            //        Receptors = receptors
            //            .Where(cs => (cs._plane as LinearAlgebra.Plane).BasedOnPoints
            //                .Contains(x, EnumerableComparer.For<decimal>())
            //            )
            //            .ToList(),
            //    })
            //    .ToList();

            var vector = trainingSet.Next();
            var startAt = vector.Index;
            do {
                network.Deactivate();
                network.Activate(vector, this._activationStrategy);
                var active = network.GetVertices(x => x.IsActive()).ToList();
                if (active.Count == 0)
                    throw new InconsistentNetworkException("FoundationChecking failed");
                vector = trainingSet.Next();
            } while (startAt != vector.Index);
            return false;
        }

        internal override string GetProgress() => "Foundation checking";
    }
}
