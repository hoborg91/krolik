﻿using Krolik.Gpns;
using Krolik.VectorsProviders;
using System;
using System.Linq;

namespace Krolik.Classifiers.GpnClassifier {
    [System.Runtime.Serialization.DataContract]
    internal class MsFilling : PreparationStage {
        private readonly IActivationStrategy _activationStrategy;

        public MsFilling(IActivationStrategy activationStrategy) {
            this._activationStrategy = activationStrategy ?? 
                throw new ArgumentNullException(nameof(activationStrategy));
        }

        internal override bool InternalPrepare(
            IGpn gpn, 
            IVectorsProvider trainingSet
        ) {
            trainingSet.Reset();
            var vr = trainingSet.Next();
            var index = vr.Index;
            do {
                gpn.Deactivate();
                gpn.Activate(vr, this._activationStrategy);
                var pyr = gpn.GetVertices(x1 => x1.IsActive()).ToList();
                foreach (var vx in pyr) {
                    foreach (var cls in vr.Classes) {
                        vx.M(cls, vx.M(cls) + 1);
                    }
                }
                vr = trainingSet.Next();
            } while (index != vr.Index);
            trainingSet.Reset();
            return false;
        }

        internal override string GetProgress() => "MsFilling";
    }
}
