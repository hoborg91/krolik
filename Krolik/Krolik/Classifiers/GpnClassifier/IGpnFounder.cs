﻿using Cil.Common;
using Krolik.Exceptions;
using Krolik.Gpns;
using Krolik.Mathematics;
using Krolik.VectorsProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Krolik.Classifiers.GpnClassifier {
    /// <summary>
    /// Constructs receptors for the given network using
    /// the vectors from the given vectors provider.
    /// </summary>
    internal interface IGpnFounder {
        /// <summary>
        /// Constructs receptors for the given network using
        /// the vectors from the given vectors provider.
        /// </summary>
        bool Found(IGpn gpn, IVectorsProvider trainingSet);
        string GetProgress();
    }

    internal class GpnFoundersFactory {
        private readonly ILinearAlgebra _linal;
        private readonly ICalculatorProvider _calculatorProvider;
        private readonly Func<StandardSequentialGpnFounderStrategy> _stdSequentialStrategyCommonFactory;
        private readonly Func<MultiConcentratedPlanesSequentialGpnFounderStrategy> _mcpSequentialStrategyCommonFactory;

        private readonly Dictionary<string, Func<ISequentialGpnFounderStrategy>> _seqSubstrategies;

        public GpnFoundersFactory(
            ILinearAlgebra linal,
            ICalculatorProvider calculatorProvider,
            Func<StandardSequentialGpnFounderStrategy> stdSequentialStrategyCommonFactory,
            Func<MultiConcentratedPlanesSequentialGpnFounderStrategy> mcpSequentialStrategyCommonFactory
        ) {
            KrolikException.CheckArgumentNotNull(linal, nameof(linal));
            KrolikException.CheckArgumentNotNull(calculatorProvider, nameof(calculatorProvider));
            KrolikException.CheckArgumentNotNull(stdSequentialStrategyCommonFactory, nameof(stdSequentialStrategyCommonFactory));
            KrolikException.CheckArgumentNotNull(mcpSequentialStrategyCommonFactory, nameof(mcpSequentialStrategyCommonFactory));

            this._linal = linal;
            this._calculatorProvider = calculatorProvider;
            this._stdSequentialStrategyCommonFactory = stdSequentialStrategyCommonFactory;
            this._mcpSequentialStrategyCommonFactory = mcpSequentialStrategyCommonFactory;

            this._seqSubstrategies = new Dictionary<string, Func<ISequentialGpnFounderStrategy>> {
                ["sequential-std"] = () => this._stdSequentialStrategyCommonFactory(),
                ["sequential-multi-concentrated"] = () => this._mcpSequentialStrategyCommonFactory(),
            };
        }

        public IGpnFounder Get(string gpnFounderType) {
            try {
                return gpnFounderType.IsNullOrWhiteSpace() || gpnFounderType == "cartesian"
                    ? this._getCartesian()
                    : this._getSequential(gpnFounderType);
            } catch(ArgumentException ex) {
                throw new WrongEnumValueException(ex);
            }
        }

        private IGpnFounder _getCartesian() {
            return new CartesianGpnFounder(this._linal);
        }

        private IGpnFounder _getSequential(string gpnFounderType) {
            if (!this._seqSubstrategies.ContainsKey(gpnFounderType))
                throw new ArgumentException($"The given {nameof(gpnFounderType)} parameter must be in range (" +
                    this._seqSubstrategies.Keys.Select(x => "\"" + x + "\"").JoinBy(", ") + "). " +
                    $"The given value is \"{gpnFounderType}\".");
            var sequentialFounderSubstrategy = this._seqSubstrategies[gpnFounderType]();
            return new SequentialGpnFounder(
                sequentialFounderSubstrategy,
                this._calculatorProvider,
                this._linal
            );
        }
    }

    /// <summary>
    /// Constructs receptors for the given network using
    /// the vectors from the given vectors provider.
    /// All receptors are "orthogonal".
    /// </summary>
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
    internal class CartesianGpnFounder : IGpnFounder {
        [System.Runtime.Serialization.DataMember]
        private Idx? _startedAt = null;

        [System.Runtime.Serialization.DataMember]
        private readonly ILinearAlgebra _linal;

        public CartesianGpnFounder(ILinearAlgebra linal) {
            KrolikException.CheckArgumentNotNull(linal, nameof(linal));
            this._linal = linal;
        }

        /// <summary>
        /// Constructs receptors for the given network using
        /// the vectors from the given vectors provider.
        /// All receptors are "orthogonal".
        /// </summary>
        public bool Found(
            IGpn gpn, 
            IVectorsProvider trainingSet
        ) {
            KrolikException.CheckArgumentNotNull(gpn, nameof(gpn));
            KrolikException.CheckArgumentNotNull(trainingSet, nameof(trainingSet));
            
            var vector = trainingSet.Next();
            if (this._startedAt.HasValue) {
                if (vector.Index == _startedAt.Value)
                    return false;
            } else
                this._startedAt = vector.Index;
            for (int i = 0; i < vector.DiscreteComponents.Length; i++) {
                gpn.AddReceptor(i, vector.DiscreteComponents[i]);
            }
            for (int i = 0; i < vector.ContinuumComponents.Length; i++) {
                var plane = this._linal.MakePlane(vector.ContinuumComponents.Length, i, vector.ContinuumComponents[i]);
                gpn.AddReceptor(plane);
            }
            return true;
        }

        public string GetProgress() {
            return string.Empty;
        }
    }

    internal interface ISequentialGpnFounderStrategy {
        void AddReceptors(IList<decimal[]> points, IGpn gpn, IPrecisionProvider precision);
    }

    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
    internal class StandardSequentialGpnFounderStrategy : ISequentialGpnFounderStrategy {
        [System.Runtime.Serialization.DataMember]
        private readonly ILinearAlgebra _linal;

        public StandardSequentialGpnFounderStrategy(ILinearAlgebra linal) {
            KrolikException.CheckArgumentNotNull(linal, nameof(linal));
            this._linal = linal;
        }

        public void AddReceptors(IList<decimal[]> points, IGpn gpn, IPrecisionProvider precision) {
            var plane = this._linal.MakePlane(
                    points,
                    precision);
            gpn.AddReceptor(plane);
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class ConcentratedPlanesSequentialGpnFounderStrategy : ISequentialGpnFounderStrategy {
        [System.Runtime.Serialization.DataMember]
        private readonly ConcentratedPlanesFactory _factory;

        public ConcentratedPlanesSequentialGpnFounderStrategy(
            ConcentratedPlanesFactory factory
        ) {
            KrolikException.CheckArgumentNotNull(factory, nameof(factory));
            this._factory = factory;
        }

        public void AddReceptors(IList<decimal[]> points, IGpn gpn, IPrecisionProvider precision) {
            var planes = this._factory.Make(points, precision);
            foreach (var plane in planes) {
                gpn.AddReceptor(plane);
            }
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class MultiConcentratedPlanesSequentialGpnFounderStrategy : ISequentialGpnFounderStrategy {
        [System.Runtime.Serialization.DataMember]
        private readonly MultiConcentratedPlanesFactory _factory;

        public MultiConcentratedPlanesSequentialGpnFounderStrategy(
            MultiConcentratedPlanesFactory factory
        ) {
            KrolikException.CheckArgumentNotNull(factory, nameof(factory));
            this._factory = factory;
        }

        public void AddReceptors(IList<decimal[]> points, IGpn gpn, IPrecisionProvider precision) {
            var plane = this._factory.Make(points, precision);
            gpn.AddReceptor(plane);
        }
    }

    /// <summary>
    /// Constructs receptors for the given network using
    /// the vectors from the given vectors provider.
    /// Receptors for continuous values represent sequences
    /// of the vectors from the given vectors provider.
    /// </summary>
    // TODO. The class is made public for testing. Consider
    // refactoring or InternalsVisibleToAttribute.
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
    [System.Runtime.Serialization.KnownType(typeof(CalculatorProvider))]
    [System.Runtime.Serialization.KnownType(typeof(StandardSequentialGpnFounderStrategy))]
    [System.Runtime.Serialization.KnownType(typeof(ConcentratedPlanesSequentialGpnFounderStrategy))]
    [System.Runtime.Serialization.KnownType(typeof(MultiConcentratedPlanesSequentialGpnFounderStrategy))]
    internal class SequentialGpnFounder : IGpnFounder {
        [System.Runtime.Serialization.DataMember]
        private readonly Dictionary<Cls, PointsStorage> _accumulator =
            new Dictionary<Cls, PointsStorage>();

        [System.Runtime.Serialization.DataMember]
        private Idx? _startAtForDiscrete = null;

        [System.Runtime.Serialization.DataMember]
        private readonly ISequentialGpnFounderStrategy _strategy;

        [System.Runtime.Serialization.DataMember]
        private readonly ICalculatorProvider _calculatorProvider;

        [System.Runtime.Serialization.DataMember]
        private readonly ILinearAlgebra _linal;

        public SequentialGpnFounder(
            ISequentialGpnFounderStrategy strategy,
            ICalculatorProvider calculatorProvider,
            ILinearAlgebra linal
        ) {
            KrolikException.CheckArgumentNotNull(strategy, nameof(strategy));
            KrolikException.CheckArgumentNotNull(calculatorProvider, nameof(calculatorProvider));
            KrolikException.CheckArgumentNotNull(linal, nameof(linal));

            this._strategy = strategy;
            this._calculatorProvider = calculatorProvider;
            this._linal = linal;
        }

        /// <summary>
        /// Constructs receptors for the given network using
        /// the vectors from the given vectors provider.
        /// Receptors for continuous values represent sequences
        /// of the vectors from the given vectors provider.
        /// </summary>
        public bool Found(IGpn gpn, IVectorsProvider trainingSet) {
            KrolikException.CheckArgumentNotNull(gpn, nameof(gpn));
            KrolikException.CheckArgumentNotNull(trainingSet, nameof(trainingSet));

            var vector = trainingSet.Next();
            var clss = vector.Classes.ToList();
            if (clss.Count != 1)
                throw new NotImplementedException("All vectors must have exactly 1 class.");
            var cls = clss.Single();

            // Add descrete components.
            for (int i = 0; i < vector.DiscreteComponents.Length; i++) {
                gpn.AddReceptor(i, vector.DiscreteComponents[i]);
            }

            // Add continuous components.
            if (!this._accumulator.ContainsKey(cls))
                this._accumulator[cls] = new PointsStorage(
                    this._calculatorProvider,
                    this._linal);
            this._accumulator[cls].Add(vector, trainingSet.PrecisionProvider);
            foreach (var vectors in this._accumulator) {
                if (!vectors.Value.IsComplete)
                    continue;
                var points = vectors.Value.FlushPoints();
                this._strategy.AddReceptors(points, gpn, trainingSet.PrecisionProvider);
                Infrastructure.KrolikLoggers.Get.Preparation.Debug(
                    "Foundation (seq). Cls " + vectors.Key.ToString() + "." + Environment.NewLine +
                    "Given points: " + string.Join(", ", points.Select(p => "(" + string.Join("; ", p.Select(x => x.ToString("f3"))) + ")")) + "." + Environment.NewLine +
                    ""//"Plane: " + plane.ToString()
                );
            }

            // Break condition.
            var discreteIsReady = false;
            if (this._startAtForDiscrete.HasValue) {
                if (this._startAtForDiscrete.Value == vector.Index)
                    discreteIsReady = true;
            } else {
                this._startAtForDiscrete = vector.Index;
            }
            var allContinuousAreReady = true;
            foreach (var accumulator in this._accumulator) {
                if (!accumulator.Value.IsReady) {
                    allContinuousAreReady = false;
                    break;
                }
                this._addReceptors(
                    gpn, 
                    accumulator.Value, 
                    trainingSet.PrecisionProvider
                );
            }
            return !(discreteIsReady && allContinuousAreReady);
        }

        public string GetProgress() {
            var result = new StringBuilder("(");
            if(this._accumulator.Any()) {
                result.Append(string.Join(", ", this._accumulator
                    .OrderBy(x => x.Key)
                    .Select(x => "[" + x.Key + "] = {" + x.Value.GetProgress() + "}")));
            } else {
                result.Append("nothing");
            }
            result.Append(")");
            return result.ToString();
        }

        private void _addReceptor(IGpn gpn, PointsStorage accumulator, IPrecisionProvider precision) {
            if (!accumulator.IsComplete)
                throw new ArgumentException("The given accumulator is not full.");
            var points = accumulator.FlushPoints();
            var plane = this._linal.MakePlane(points, precision);
            gpn.AddReceptor(plane);
        }

        private void _addReceptors(IGpn gpn, PointsStorage accumulator, IPrecisionProvider precision) {
            while (accumulator.Debt.Any()) {
                var points = accumulator.PayDebt(precision);
                var plane = this._linal.MakePlane(points, precision);
                gpn.AddReceptor(plane);
            }
        }

        /// <summary>
        /// Contains points retrieved from the vectors provider.
        /// </summary>
        [System.Runtime.Serialization.DataContract]
        [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
        [System.Runtime.Serialization.KnownType(typeof(CalculatorProvider))]
        [System.Runtime.Serialization.KnownType(typeof(HybridVectorsFactory.HybridVector))]
        private class PointsStorage {
            [System.Runtime.Serialization.DataMember]
            private List<decimal[]> _points = new List<decimal[]>();

            [System.Runtime.Serialization.DataMember]
            private int _dimension = 0;

            [System.Runtime.Serialization.DataMember]
            private Idx? _startAt = null;

            [System.Runtime.Serialization.DataMember]
            public bool IsReady { get; private set; }

            [System.Runtime.Serialization.DataMember]
            private bool _cycle = false;

            [System.Runtime.Serialization.DataMember]
            private static readonly List<string> _log = new List<string>();
            
            [System.Runtime.Serialization.DataMember]
            private readonly ICalculatorProvider _calculators;

            [System.Runtime.Serialization.DataMember]
            private readonly HashSet<IHybridVector> _debt = new HashSet<IHybridVector>();

            [System.Runtime.Serialization.DataMember]
            private readonly ILinearAlgebra _linal;

            public PointsStorage(
                ICalculatorProvider calculators,
                ILinearAlgebra linal
            ) {
                KrolikException.CheckArgumentNotNull(calculators, nameof(calculators));
                KrolikException.CheckArgumentNotNull(linal, nameof(linal));

                this._calculators = calculators;
                this._linal = linal;
            }

            public IEnumerable<IHybridVector> Debt {
                get {
                    return this._debt;
                }
            }

            public void Add(IHybridVector vector, IPrecisionProvider precision) {
                if (this._startAt.HasValue) {
                    if (this._startAt.Value == vector.Index) {
                        if (this._points.Count == 0) {
                            this.IsReady = true;
                            return;
                        } else {
                            if (this._cycle) {
                                _completeArtificially(precision);
                            } else {
                                this._cycle = true;
                            }
                        }
                    }
                } else {
                    this._startAt = vector.Index;
                }
                var point = vector.ContinuumComponents;
                if (this._dimension == 0) {
                    if (point.Length == 0) {
                        this.IsReady = true;
                        return;
                    }
                    this._dimension = point.Length;
                }
                if (!this._addWithCheck(point, precision)) {
                    this._debt.Add(vector);
                }
            }

            private void _checkRank(IPrecisionProvider precision) {
                if (this._dimension == 0 || this._points.Count == 0)
                    return;
                var m = this._linal.MakePlaneMatrixOf(this._points);
                var rank = this._calculators
                    .GetMatrixRankCalculator()
                    .CalculateRank(m.Matrix, m.Rows, m.Columns, precision);
                if (rank < this._points.Count) {
                    throw new Exception("Inconsisntent accumulator state.");
                }
            }

            private bool _addWithCheck(decimal[] point, IPrecisionProvider precision) {
                foreach (var existedPoint in this._points) {
                    var contains = true;
                    for (int i = 0; i < point.Length; i++) {
                        if (precision.IsZero(point[i] - existedPoint[i]))
                            continue;
                        contains = false;
                        break;
                    }
                    if (contains)
                        return false;
                }
                this._points.Add(point);
                return true;
            }

            private void _completeArtificially(IPrecisionProvider precision) {
                _log.Add("_completeArtificially starts. _dimension = " + this._dimension + ".");
                var toRemove = new List<IHybridVector>();
                foreach (var debtVector in this._debt) {
                    if (this._points.Count == this._dimension)
                        break;
                    if (this._addWithCheck(debtVector.ContinuumComponents, precision))
                        toRemove.Add(debtVector);
                }
                foreach (var vectorToRemove in toRemove) {
                    this._debt.Remove(vectorToRemove);
                }
                for (int i = 0; i < this._dimension && this._points.Count < this._dimension; i++) {
                    var v = new decimal[this._dimension];
                    v[i] = (decimal)1;
                    this._addWithCheck(v, precision);
                }
                _log.Add("_completeArtificially ends. _dimension = " + this._dimension + ".");
                if (this._points.Count != this._dimension)
                    throw new Exception("Inconsistent state. The accumulator must be full at this point.");
            }

            public bool IsComplete {
                get {
                    return this._points.Count == this._dimension
                        && this._dimension > 0;
                }
            }

            internal IList<decimal[]> FlushPoints() {
                if (this._cycle)
                    this.IsReady = true;
                return this._flushPoints();
            }

            internal IList<decimal[]> PayDebt(IPrecisionProvider precision) {
                if (!this.IsReady)
                    throw new Exception("This call is allowed only when the accumulator is ready.");
                this._completeArtificially(precision);
                return this._flushPoints();
            }

            private IList<decimal[]> _flushPoints() {
                var result = this._points;
                this._points = new List<decimal[]>();
                return result;
            }

            internal string GetProgress() {
                return this._points.Count
                    + (this._debt.Any()
                        ? " debt " + this._debt.Count
                        : string.Empty)
                    + (this.IsComplete
                        ? " complete"
                        : string.Empty)
                    + (this.IsReady
                        ? " ready"
                        : string.Empty)
                ;
            }
        }
    }
}
