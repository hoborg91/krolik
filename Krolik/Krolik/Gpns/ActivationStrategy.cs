﻿using Krolik.Infrastructure;
using Krolik.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.Gpns {
    internal interface IActivationStrategy {
        void Activate(IList<IVertex> allVertices, IHybridVector vector);
    }

    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(CustomPrecisionProvider))]
    [System.Runtime.Serialization.KnownType(typeof(LeftDiscreteFuzzySet))]
    [System.Runtime.Serialization.KnownType(typeof(LeftInverseFuzzySet))]
    [System.Runtime.Serialization.KnownType(typeof(LeftTriangleFuzzySet))]
    internal class ClassicActivationStrategy : IActivationStrategy {
        [System.Runtime.Serialization.DataMember]
        private readonly IPrecisionProvider _precision;

        [System.Runtime.Serialization.DataMember]
        private readonly IFuzzySet _fuzziness;

        public ClassicActivationStrategy(IPrecisionProvider precision) {
            if (precision == null)
                throw new ArgumentNullException(nameof(precision));
            this._precision = precision;
            this._fuzziness = new LeftDiscreteFuzzySet(precision.Tolerance);
        }

        public void Activate(IList<IVertex> allVertices, IHybridVector vector) {
            KrolikLoggers.Get.Classification.Debug($"Activate GPN {this} with vector {vector}.");
            var receptors = allVertices.OfType<IReceptor>();
            foreach (var receptor in receptors) {
                if (receptor.CorrespondsTo(vector, this._precision, this._fuzziness) == 1)
                    receptor.Active = Certainty.Absolute;
            }

            foreach (var level in allVertices
                .GroupBy(v => v.K())
                .OrderBy(x => x.Key)
                .Where(x => x.Key > 1)
            ) {
                foreach (IConceptor conceptor in level) {
                    var zeroSubset = conceptor.GetZeroSubset()
                        .ToList();
                    if (zeroSubset.All(x => x.IsActive()))
                        conceptor.Active = Certainty.Absolute;
                }
            }
        }
    }

    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(CustomPrecisionProvider))]
    [System.Runtime.Serialization.KnownType(typeof(LeftDiscreteFuzzySet))]
    [System.Runtime.Serialization.KnownType(typeof(LeftInverseFuzzySet))]
    [System.Runtime.Serialization.KnownType(typeof(LeftTriangleFuzzySet))]
    internal class FuzzyAndActivationStrategy : IActivationStrategy {
        [System.Runtime.Serialization.DataMember]
        private readonly IPrecisionProvider _precision;

        [System.Runtime.Serialization.DataMember]
        private readonly IFuzzySet _fuzziness;

        public FuzzyAndActivationStrategy(
            IPrecisionProvider precision,
            IFuzzySet fuzziness
        ) {
            if (precision == null)
                throw new ArgumentNullException(nameof(precision));
            if (fuzziness == null)
                throw new ArgumentNullException(nameof(fuzziness));
            this._precision = precision;
            this._fuzziness = fuzziness;
        }

        public void Activate(IList<IVertex> allVertices, IHybridVector vector) {
            KrolikLoggers.Get.Classification.Debug($"Activate GPN {this} with vector {vector}.");
            var receptors = allVertices.OfType<IReceptor>();
            foreach (var receptor in receptors) {
                receptor.Active = receptor.CorrespondsTo(vector, this._precision, this._fuzziness);
            }

            foreach (var level in allVertices
                .GroupBy(v => v.K())
                .OrderBy(x => x.Key)
                .Where(x => x.Key > 1)
            ) {
                foreach (IConceptor conceptor in level) {
                    var zeroSubset = conceptor.GetZeroSubset()
                        .ToList();
                    conceptor.Active = Krolik.Mathematics.Fuzzy.Logic.And(
                        zeroSubset.Select(x => x.Active).ToArray()
                    );
                }
            }
        }
    }
}
