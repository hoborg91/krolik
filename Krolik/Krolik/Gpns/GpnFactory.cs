﻿using Krolik.Mathematics;

namespace Krolik.Gpns {
    internal class GpnFactory {
        public IGpn Make(decimal fuzzyPrecision) {
            return new Gpn(new Krolik.Mathematics.LeftTriangleFuzzySet(fuzzyPrecision));
        }

        internal IGpn Make(IFuzzySet fuzziness, Gpn.VerticesStructure verticesStructure) {
            return new Gpn(fuzziness, verticesStructure);
        }
    }
}