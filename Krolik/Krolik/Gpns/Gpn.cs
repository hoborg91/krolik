﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Krolik.Classifiers;
using Krolik.Mathematics;
using System.Text;
using Krolik.Infrastructure;
using Krolik.Exceptions;
using Cil.Common;

namespace Krolik.Gpns {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(LeftTriangleFuzzySet))]
    [System.Runtime.Serialization.KnownType(typeof(LeftInverseFuzzySet))]
    [System.Runtime.Serialization.KnownType(typeof(LeftDiscreteFuzzySet))]
    internal class Gpn : IGpn {
        [System.Runtime.Serialization.DataMember]
        private VerticesStructure _vertices = new VerticesStructure();

        [System.Runtime.Serialization.DataMember]
        private readonly IFuzzySet _fuzziness;

        public Gpn(IFuzzySet fuzziness) {
            KrolikException.CheckArgumentNotNull(fuzziness, nameof(fuzziness));
            this._fuzziness = fuzziness;
        }

        public Gpn(IFuzzySet fuzziness, VerticesStructure verticesStructure) : this(fuzziness) {
            this._fuzziness = fuzziness ??
                throw new ArgumentNullException(nameof(fuzziness));
            this._vertices = verticesStructure ??
                throw new ArgumentNullException(nameof(verticesStructure));
        }

        public GpnForSerialization GetViewForSerialization() {
            return new GpnForSerialization(
                this._vertices.GetViewForSerialization(),
                this._fuzziness
            );
        }

        public IList<IVertex> Activate(IHybridVector vector, IActivationStrategy strategy) {
            if (vector == null)
                throw new ArgumentNullException(nameof(vector));
            if (strategy == null)
                throw new ArgumentNullException(nameof(strategy));
            lock (this._verticesChest) {
                var allVertices = this._vertices.All().ToList();
                strategy.Activate(allVertices, vector);
                return this._vertices.All()
                    .Where(x => x.IsActive())
                    .ToList();
            }
        }

        [System.Runtime.Serialization.DataMember]
        private object _verticesChest = new object();

        public bool AddReceptor(int featureIndex, DiscreteValue featureValue) {
            lock (this._verticesChest) {
                return this._vertices.AddReceptor(featureIndex, featureValue);
            }
        }

        public bool AddReceptor(IPlane plane) {
            lock (this._verticesChest) {
                return this._vertices.AddReceptor(plane);
            }
        }

        public void Deactivate(Expression<Func<IVertex, bool>> filter = null) {
            lock (this._verticesChest) {
                var vertices = this._vertices.All();
                if (filter != null)
                    vertices = vertices.Where(filter);
                foreach (var vertex in vertices) {
                    vertex.Active = Certainty.None;
                }
            }
        }

        public IEnumerable<IVertex> G() {
            lock (this._verticesChest) {
                return this._vertices.All()
                    .Where(x1 => true
                        && x1.IsActive()
                        && !x1.GetZeroSuperset().Any(x2 => x2.IsActive())
                    );
            }
        }

        public IEnumerable<IVertex> GetVertices(Expression<Func<IVertex, bool>> filter = null) {
            lock (this._verticesChest) {
                var result = this._vertices.All();
                if (filter != null)
                    result = result.Where(filter);
                return result;
            }
        }

        public IEnumerable<TVertex> Projection<TVertex>(
            Func<IVertex, TVertex> map, 
            Action<TVertex, TVertex> addSecondVertexToUpperOfFirstVertex,
            Action<TVertex, TVertex> addSecondVertexToLowerOfFirstVertex,
            Expression<Func<IVertex, bool>> filter = null
        ) {
            KrolikException.CheckArgumentNotNull(map, nameof(map));
            KrolikException.CheckArgumentNotNull(addSecondVertexToUpperOfFirstVertex, nameof(addSecondVertexToUpperOfFirstVertex));
            KrolikException.CheckArgumentNotNull(addSecondVertexToLowerOfFirstVertex, nameof(addSecondVertexToLowerOfFirstVertex));

            lock (this._verticesChest) {
                var result = this._vertices.All();
                if (filter != null)
                    result = result.Where(filter);
                var vertices = result
                    .Select(x => new {
                        Vertex = x,
                        Upper = x.GetZeroSuperset().ToList(),
                        Lower = x is IConceptor
                            ? (x as IConceptor).GetZeroSubset().ToList()
                            : new List<IVertex>()
                    })
                    .ToList();
                var verticesRef = vertices
                    .ToDictionary(x => x.Vertex, x => map(x.Vertex));
                foreach (var vertex in vertices) {
                    foreach (var sup in vertex.Upper) {
                        addSecondVertexToUpperOfFirstVertex(verticesRef[vertex.Vertex], verticesRef[sup]);
                    }
                    foreach (var sub in vertex.Lower) {
                        addSecondVertexToLowerOfFirstVertex(verticesRef[vertex.Vertex], verticesRef[sub]);
                    }
                }
                return verticesRef
                    .Values
                    .ToList();
            }
        }

        public IVertex InsertBetween(IEnumerable<IVertex> lower, IConceptor upper) {
            lock (this._verticesChest) {
                var toAdd = this._vertices.InsertBetween(lower, upper);
                return toAdd;
            }
        }

        /// <summary>
        /// Thing that incapsulates logic of vertices manipulation
        /// in purpose to support network in consistent state.
        /// </summary>
        [System.Runtime.Serialization.DataContract]
        [System.Runtime.Serialization.KnownType(typeof(Conceptor))]
        [System.Runtime.Serialization.KnownType(typeof(DiscreteReceptor))]
        [System.Runtime.Serialization.KnownType(typeof(ContinuousReceptor))]
        [System.Runtime.Serialization.KnownType(typeof(VectorSignature))]
        public class VerticesStructure {
            /// <summary>
            /// Contains receptors (level 1) for discrete values.
            /// </summary>
            [System.Runtime.Serialization.DataMember]
            private Dictionary<DiscreteValue, DiscreteReceptor>[] _discrReceptors = new Dictionary<DiscreteValue, DiscreteReceptor>[0];

            [System.Runtime.Serialization.DataMember]
            private List<ContinuousReceptor> _contReceptors = new List<ContinuousReceptor>();

            /// <summary>
            /// Contains conceptors (levels 2 and higher).
            /// </summary>
            [System.Runtime.Serialization.DataMember]
            private Dictionary<int, List<Conceptor>> _levels = new Dictionary<int, List<Conceptor>>();

            ///// <summary>
            ///// Contains all vertices (levels 1 and higher). Duplication
            ///// possibly will help with performance optimization.
            ///// </summary>
            //[System.Runtime.Serialization.DataMember]
            //private IList<IVertex> _vertices = new List<IVertex>();

            private List<IVertex> _verticesCache = null;

            public VerticesStructure() {

            }

            public VerticesStructure(
                List<VerticesForSerialization.DiscreteReceptorForSerialization> discrRcps, 
                List<VerticesForSerialization.ContinuousReceptorForSerialization> contRcps, 
                List<VerticesForSerialization.ConceptorForSerialization> conceptors, 
                Dictionary<long, HashSet<long>> supersets, 
                Dictionary<long, HashSet<long>> subsets
            ) {
                var verticesRef = new Dictionary<long, IVertex>();
                var discrReceptors = new Dictionary<int, Dictionary<DiscreteValue, DiscreteReceptor>>();
                foreach (var discr in discrRcps) {
                    if (!discrReceptors.ContainsKey(discr.FeatureIndex))
                        discrReceptors[discr.FeatureIndex] = new Dictionary<DiscreteValue, DiscreteReceptor>();
                    var dict = discrReceptors[discr.FeatureIndex];
                    var vertex = new DiscreteReceptor(
                        discr.FeatureIndex,
                        discr.FeatureValue
                    ) {
                        MsField = discr.MsField,
                        Active = discr.Active,
                    };
                    if (discr.IsControlFor != null)
                        foreach (var controlCls in discr.IsControlFor) {
                            vertex.MarkAsControlFor(controlCls);
                        }
                    dict[discr.FeatureValue] = vertex;
                    verticesRef[discr.Id] = vertex;
                }
                this._discrReceptors = new Dictionary<DiscreteValue, DiscreteReceptor>[
                    discrReceptors.Any()
                        ? discrReceptors.Select(x => x.Key).Max() + 1
                        : 0
                ];
                for (int i = 0; i < this._discrReceptors.Length; i++) {
                    this._discrReceptors[i] = new Dictionary<DiscreteValue, DiscreteReceptor>();
                    if (!discrReceptors.ContainsKey(i))
                        continue;
                    this._discrReceptors[i] = discrReceptors[i];
                }

                foreach (var cont in contRcps) {
                    var vertex = new ContinuousReceptor(
                        cont.Plane
                    ) {
                        MsField = cont.MsField,
                        Active = cont.Active,
                    };
                    if (cont.IsControlFor != null)
                        foreach (var controlCls in cont.IsControlFor) {
                            vertex.MarkAsControlFor(controlCls);
                        }
                    this._contReceptors.Add(vertex);
                    verticesRef[cont.Id] = vertex;
                }

                foreach (var cncp in conceptors) {
                    if (!this._levels.ContainsKey(cncp.LevelKey))
                        this._levels[cncp.LevelKey] = new List<Conceptor>();
                    var vertex = new Conceptor {
                        MsField = cncp.MsField,
                        Active = cncp.Active,
                    };
                    if (cncp.IsControlFor != null)
                        foreach (var controlCls in cncp.IsControlFor) {
                            vertex.MarkAsControlFor(controlCls);
                        }
                    this._levels[cncp.LevelKey].Add(vertex);
                    verticesRef[cncp.Id] = vertex;
                }

                foreach (var super in supersets) {
                    verticesRef[super.Key].AddToZeroSuperset(super.Value
                        .Select(s => (IConceptor)verticesRef[s])
                        .ToArray()
                    );
                }

                foreach (var sub in subsets) {
                    ((IConceptor)verticesRef[sub.Key]).AddToZeroSubset(sub.Value
                        .Select(s => verticesRef[s])
                        .ToArray()
                    );
                }
            }

            public IQueryable<IVertex> All() {
                if (this._verticesCache == null) {
                    this._verticesCache = new List<IVertex>();
                    this._verticesCache.AddRange(this._discrReceptors
                        .SelectMany(x => x.Values));
                    this._verticesCache.AddRange(this._contReceptors);
                    this._verticesCache.AddRange(this._levels
                        .SelectMany(x => x.Value));
                }
                return this._verticesCache.AsQueryable();
            }

            public Dictionary<int, IEnumerable<IVertex>> Levels(bool evenEmpty = false) {
                var result = new Dictionary<int, IEnumerable<IVertex>>();
                result[1] = this.Receptors();
                foreach (var level in this._levels) {
                    result[level.Key] = level.Value;
                }
                if (evenEmpty) {
                    var max = result.Keys.Max();
                    for (int i = 2; i < max; i++) {
                        if (!result.ContainsKey(i))
                            result[i] = new List<IVertex>();
                    }
                }

                this._invariant();

                return result;
            }

            public IEnumerable<IReceptor> Receptors() {
                var receptors = new List<IReceptor>();
                foreach (var rcps in this._discrReceptors) {
                    foreach (var rcps2 in rcps) {
                        receptors.Add(rcps2.Value);
                    }
                }
                receptors.AddRange(this._contReceptors);

                this._invariant();

                return receptors;
            }

            /// <summary>
            /// Returns true, if the given feature and value combination were absent.
            /// </summary>
            public bool AddReceptor(int featureIndex, DiscreteValue featureValue) {
                var result = false;
                var currentDiscrRcpsCount = this._discrReceptors.Length;
                if (featureIndex >= currentDiscrRcpsCount) {
                    var rcps = new Dictionary<DiscreteValue, DiscreteReceptor>[featureIndex + 1];
                    this._discrReceptors.CopyTo(rcps, 0);
                    for (int i = this._discrReceptors.Length; i < rcps.Length; i++) {
                        rcps[i] = new Dictionary<DiscreteValue, DiscreteReceptor>();
                    }
                    this._discrReceptors = rcps;
                }
                if (!this._discrReceptors[featureIndex].ContainsKey(featureValue)) {
                    result = true;
                    var receptor = new DiscreteReceptor(featureIndex, featureValue);
                    this._discrReceptors[featureIndex][featureValue] = receptor;
                    this._verticesCache = null;
                }

                this._invariant();

                return result;
            }

            /// <summary>
            /// Returns true, if the given plane was not presented.
            /// </summary>
            public bool AddReceptor(IPlane plane) {
                var already = this._contReceptors
                    .Where(x => x.Equals(plane))
                    .ToList();
                if (already.Count > 0)
                    return false;
                var receptor = new ContinuousReceptor(plane);
                this._contReceptors.Add(receptor);
                //this._vertices.Add(receptor);
                this._verticesCache = null;

                this._invariant();

                return true;
            }

            public IConceptor InsertBetween(IEnumerable<IVertex> lower, IConceptor upper) {
                if (lower == null)
                    throw new ArgumentNullException("lower");
                var lowerArray = lower.ToArray();
                if (!lowerArray.Any())
                    throw new ArgumentException("The argument \"lower\" must not be empty.");
                var lowerReceptors = lowerArray
                    .SelectMany(x => x.GetUnderlyingReceptors())
                    .Distinct()
                    .ToList();

                DiscreteReceptor.CheckCollision(lowerReceptors.OfType<DiscreteReceptor>().ToList());

                var k = lowerReceptors.Count();
                if (k <= 0)
                    throw new InconsistentNetworkException();
                var toAdd = new Conceptor {
                    ZeroSubset = lower.ToList(),
                };
                foreach (var lr in lowerArray) {
                    lr.AddToZeroSuperset(toAdd);
                }
                if (upper != null) {
                    foreach (var lr in lowerArray) {
                        lr.RemoveVromZeroSuperset(upper);
                    }
                    upper.RemoveVromZeroSubset(lowerArray);
                    toAdd.ZeroSuperset.Add(upper);
                    upper.AddToZeroSubset(toAdd);
                }
                if (!this._levels.ContainsKey(k))
                    this._levels[k] = new List<Conceptor>();
                this._levels[k].Add(toAdd);
                //this._vertices.Add(toAdd);
                this._verticesCache = null;

                this._invariant();

                return toAdd;
            }

            //private List<Tuple<IReceptor, IReceptor>> _inspectedReceptors =
            //    new List<Tuple<IReceptor, IReceptor>>();

            private void _invariant() {
                return;
            }

            //private void _invariant() {
            //    return;
            //    foreach (var vertex in this.All().ToList()) {
            //        var conceptor = vertex as IConceptor;
            //        if (conceptor == null)
            //            continue;
            //        var rcps = conceptor.GetUnderlyingReceptors()
            //            .OfType<ContinuousReceptor>()
            //            .ToList();
            //        if (!rcps.Any())
            //            continue;
            //        var dim = rcps[0].PlaneComponents.Length;
            //        if (dim != 3)
            //            continue;
            //        var intersections = new List<Tuple<decimal[], IReceptor, IReceptor>>();
            //        foreach (var r1 in rcps) {
            //            foreach (var r2 in rcps) {
            //                var t1 = Tuple.Create(r1 as IReceptor, r2 as IReceptor);
            //                var t2 = Tuple.Create(r2 as IReceptor, r1 as IReceptor);
            //                if (this._inspectedReceptors.ContainsAny(t1, t2))
            //                    continue;
            //                this._inspectedReceptors.Add(t1);
            //                var p1 = r1._plane as IOpenPlane;
            //                var p2 = r2._plane as IOpenPlane;
            //                if (p1 == null || p2 == null)
            //                    throw new Exception("Not an open plane.");
            //                var i = p1.Intersection(p2);
            //                if (i != null)
            //                    intersections.Add(Tuple.Create(i, r1 as IReceptor, r2 as IReceptor));
            //            }
            //        }
            //        foreach (var i1 in intersections) {
            //            foreach (var i2 in intersections) {
            //                var distance = LinearAlgebra.Get().Distance(
            //                    i1.Item1, 
            //                    i2.Item1
            //                );
            //                if (distance > (decimal)0.1) {
            //                    KrolikLoggers.Get.Common.Error("InconsistentState: " +
            //                        "receptor " + i1.Item2.ToString() + " " +
            //                        "receptor " + i1.Item3.ToString() + " " +
            //                        "intersection " + i1.Item1.Select(x => x.ToString()).JoinBy(" ") + " " +
            //                        "receptor " + i2.Item2.ToString() + " " +
            //                        "receptor " + i2.Item3.ToString() + " " +
            //                        "intersection " + i2.Item1.Select(x => x.ToString()).JoinBy(" ") + " " +
            //                        "distance " + distance.ToString() + ".");
            //                    //throw new InconsistentStateException();
            //                }
            //            }
            //        }
            //    }
            //    return;
            //    foreach (var vertex in this.All().ToList()) {
            //        foreach (var super in vertex.GetZeroSuperset()) {
            //            if (super.K() <= vertex.K())
            //                throw new InconsistentStateException();
            //        }
            //        var con = vertex as IConceptor;
            //        if (con == null)
            //            continue;
            //        foreach (var sub in con.GetZeroSubset()) {
            //            if (sub.K() >= con.K())
            //                throw new InconsistentStateException();
            //        }
            //    }
            //}

            public VerticesForSerialization GetViewForSerialization() {
                var idsSequence = new Int64SequenceGenerator(0);
                var verticesRef = new Dictionary<IVertex, long>();

                var discrRcps = new List<VerticesForSerialization.DiscreteReceptorForSerialization>();
                for (int i = 0; i < this._discrReceptors.Length; i++) {
                    foreach (var rcpPair in this._discrReceptors[i]) {
                        verticesRef[rcpPair.Value] = idsSequence.GetNext();
                        discrRcps[i] = new VerticesForSerialization.DiscreteReceptorForSerialization(
                            i,
                            rcpPair.Key,
                            verticesRef[rcpPair.Value],
                            rcpPair.Value.MsField,
                            rcpPair.Value.Active,
                            rcpPair.Value.IsControlFor().ToList()
                        );
                    }
                }

                var contRcps = new List<VerticesForSerialization.ContinuousReceptorForSerialization>();
                foreach (var contRcp in this._contReceptors) {
                    verticesRef[contRcp] = idsSequence.GetNext();
                    contRcps.Add(contRcp.GetViewForSerialization(
                        verticesRef[contRcp]
                    ));
                }

                var conceptors = new List<VerticesForSerialization.ConceptorForSerialization>();
                foreach (var level in this._levels) {
                    foreach (var cncp in level.Value) {
                        verticesRef[cncp] = idsSequence.GetNext();
                        conceptors.Add(cncp.GetViewForSerialization(
                            level.Key,
                            verticesRef[cncp]
                        ));
                    }
                }

                var supersets = new Dictionary<long, HashSet<long>>();
                foreach (var vertex in this.All()) {
                    supersets[verticesRef[vertex]] = new HashSet<long>(
                        vertex
                            .GetSuperset()
                            .Select(super => verticesRef[super])
                    );
                }

                var subsets = new Dictionary<long, HashSet<long>>();
                foreach (var vertex in this.All()
                    .OfType<IConceptor>()
                ) {
                    subsets[verticesRef[vertex]] = new HashSet<long>(
                        vertex
                            .GetSubset()
                            .Select(super => verticesRef[super])
                    );
                }

                return new VerticesForSerialization(
                    discrRcps,
                    contRcps,
                    conceptors,
                    supersets,
                    subsets
                );
            }

            [System.Runtime.Serialization.DataContract(IsReference = true)]
            [System.Runtime.Serialization.KnownType(typeof(VectorSignature))]
            public abstract class Vertex : IVertex {
                [System.Runtime.Serialization.DataMember]
                public Certainty Active { get; set; }

                //[System.Runtime.Serialization.DataMember]
                //public decimal FuzzyActive { get; set; }

                [System.Runtime.Serialization.DataMember]
                public List<IVertex> ZeroSuperset = new List<IVertex>();

                [System.Runtime.Serialization.DataMember]
                public Dictionary<Cls, int> MsField = new Dictionary<Cls, int>();

                [System.Runtime.Serialization.DataMember]
                private List<Cls> _isControlFor = new List<Cls>();

                [System.Runtime.Serialization.DataMember]
                private IVectorSignature _sgn;

                public IEnumerable<IVertex> GetZeroSuperset() {
                    return this.ZeroSuperset;
                }

                public IDictionary<Cls, int> Ms() {
                    return this.MsField;
                }

                public void RemoveVromZeroSuperset(params IConceptor[] vertices) {
                    if (vertices == null)
                        return;
                    foreach (var vertex in vertices) {
                        this.ZeroSuperset.Remove(vertex);
                    }
                    this._supersetCache.Clear(this);
                }

                public void AddToZeroSuperset(params IConceptor[] vertices) {
                    if (vertices == null)
                        return;
                    foreach (var vertex in vertices) {
                        if (this.ZeroSuperset.Contains(vertex))
                            continue;
                        this.ZeroSuperset.Add(vertex);
                    }
                    this._supersetCache.Clear(this);
                }

                public abstract int K();

                public int M(Cls vectorCls) {
                    return this.MsField.ContainsKey(vectorCls)
                        ? this.MsField[vectorCls]
                        : 0;
                }

                public bool MarkAsControlFor(Cls vectorCls) {
                    var already = this._isControlFor.Contains(vectorCls);
                    if (!already) {
                        KrolikLoggers.Get.Preparation.Debug($"({this.ToString()}).MarkAsControlFor({vectorCls})");
                        this._isControlFor.Add(vectorCls);
                    }
                    return !already;
                }

                public IEnumerable<Cls> IsControlFor() {
                    return this._isControlFor;
                }

                [System.Runtime.Serialization.DataContract]
                [System.Runtime.Serialization.KnownType(typeof(Conceptor))]
                [System.Runtime.Serialization.KnownType(typeof(ContinuousReceptor))]
                [System.Runtime.Serialization.KnownType(typeof(DiscreteReceptor))]
                private class VerticesCache {
                    [System.Runtime.Serialization.DataMember]
                    private List<IVertex> _vertices;

                    [System.Runtime.Serialization.DataMember]
                    private readonly object _chest = new object();

                    public List<IVertex> Get(Func<IEnumerable<IVertex>> fill, IVertex mustNotBeIncluded) {
                        lock (this._chest) {
                            if (this._vertices == null)
                                this._vertices = fill().ToList();
                            this._check(mustNotBeIncluded);
                            return new List<IVertex>(this._vertices);
                        }
                    }

                    public void Clear(IVertex mustNotBeIncluded) {
                        lock (this._chest) {
                            this._vertices = null;
                            this._check(mustNotBeIncluded);
                        }
                    }

                    private void _check(IVertex mustNotBeIncluded) {
                        if (this._vertices == null)
                            return;
                        if (this._vertices.Contains(mustNotBeIncluded))
                            throw new InconsistentStateException(
                                "The vertex itself cannot be included in the list of its superset vertices.");
                    }
                }

                [System.Runtime.Serialization.DataMember]
                private VerticesCache _supersetCache = new VerticesCache();

                public IEnumerable<IVertex> GetSuperset() {
                    return this._supersetCache.Get(() => {
                        var result = new List<IVertex>();
                        result.AddRange(this.GetZeroSuperset());
                        foreach (var sup in this.GetZeroSuperset()) {
                            result.AddRange(sup.GetSuperset());
                        }
                        return result.Distinct();
                    }, this);
                }

                public void M(Cls vectorCls, int value) {
                    if (!this.MsField.ContainsKey(vectorCls))
                        this.MsField[vectorCls] = 0;
                    this.MsField[vectorCls] = value;
                }

                public abstract IEnumerable<IReceptor> GetUnderlyingReceptors();
            }

            // TODO. Make this class private.
            [System.Runtime.Serialization.DataContract]
            public abstract class Receptor : Vertex, IReceptor {
                public abstract Certainty CorrespondsTo(IHybridVector vector, IPrecisionProvider precision, IFuzzySet fuzziness);

                public override int K() {
                    return 1;
                }

                public override IEnumerable<IReceptor> GetUnderlyingReceptors() {
                    return new List<Receptor> { this, };
                }
            }

            [System.Runtime.Serialization.DataContract]
            private class DiscreteReceptor : Receptor {
                [System.Runtime.Serialization.DataMember]
                private readonly int _featureIndex;

                [System.Runtime.Serialization.DataMember]
                private readonly DiscreteValue _featureValue;

                public DiscreteReceptor(int featureIndex, DiscreteValue featureValue) {
                    this._featureIndex = featureIndex;
                    this._featureValue = featureValue;
                }

                public override Certainty CorrespondsTo(
                    IHybridVector vector,
                    IPrecisionProvider precision,
                    IFuzzySet fuzziness
                ) {
                    if (this._featureIndex >= vector.DiscreteComponents.Length)
                        throw new Exception("The given vector does not correspond to the network.");
                    return vector.DiscreteComponents[this._featureIndex] == this._featureValue
                        ? Certainty.Absolute
                        : Certainty.None;
                }

                public override string ToString() {
                    return "[" + this._featureIndex + "]=" + this._featureValue;
                }

                internal static void CheckCollision(IList<DiscreteReceptor> receptors) {
                    foreach (var receptor in receptors) {
                        if (receptors.Count(x => x._featureIndex == receptor._featureIndex) != 1)
                            throw new Exception("Discrete receptors collision is detected.");
                    }
                }
            }

            [System.Runtime.Serialization.DataContract]
            [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra.Plane))]
            [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra.ConcentratedPlane))]
            [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra.MultiConcentratedPlane))]
            public class ContinuousReceptor : Receptor, IReceptorWithPlaneComponents {//, IFuzzyReceptor {
                [System.Runtime.Serialization.DataMember]
                public readonly IPlane _plane;

                public ContinuousReceptor(IPlane plane) {
                    KrolikException.CheckArgumentNotNull(plane, "plane");
                    this._plane = plane;
                }

                [Obsolete("Dirty code. Avoid using this property.")]
                public decimal[] PlaneComponents {
                    get {
                        return (this._plane as IWithComponents)?.Components;
                    }
                }

                public bool Equals(IPlane plane) {
                    return this._plane.Equals(plane);
                }

                public override Certainty CorrespondsTo(
                    IHybridVector vector,
                    IPrecisionProvider precision,
                    IFuzzySet fuzziness
                ) {
                    var components = vector.ContinuumComponents;

                    if (true
                        && components[0].EqualsWithPrecision(30, 0.1m)
                        && components[1].EqualsWithPrecision(12.432m, 0.1m)
                    ) {
                        var hello = 0;
                    }

                    var distance = this._plane.DistanceTo(components);
                    var result = fuzziness.Mu(distance);
                    //var result = precision.IsZero(distance);

                    this.ForDebug_Distance = distance;
                    this.ForDebug_Mu = result;

                    return result;
                }

                public decimal ForDebug_Distance;
                public Certainty ForDebug_Mu;

                //public decimal FuzzyCorrespondsTo(
                //    IHybridVector vector,
                //    IPrecisionProvider precision,
                //    IFuzzySet fuzziness
                //) {
                //    var components = vector.ContinuumComponents;
                //    var distance = this._plane.DistanceTo(components);
                //    var mu = fuzziness.Mu(distance);
                //    KrolikLoggers.Get.Classification.Debug(
                //        $"plane {this._plane} distance {distance.ToString("f3")} mu {mu.ToString("f3")}"
                //    );
                //    return mu;
                //}

                public override int GetHashCode() {
                    return this._plane.GetHashCode();
                }

                public override bool Equals(object obj) {
                    var rcp = obj as ContinuousReceptor;
                    if ((object)rcp == null)
                        return false;
                    return this._plane == rcp._plane;
                }

                public override string ToString() {
                    var result = new StringBuilder();
                    result.Append($"plane {this._plane.ToString()} " + (this.IsActive() ? "A" : "") +
                        $" fa={this.Active.ToString()}" +
                        (this.IsControlFor().Any() ? " ctrl=" + string.Join(",", this.IsControlFor()) : ""));
                    return result.ToString();
                }

                internal VerticesForSerialization.ContinuousReceptorForSerialization GetViewForSerialization(
                    long id
                ) {
                    return new VerticesForSerialization.ContinuousReceptorForSerialization(
                        id,
                        this._plane,
                        this.MsField,
                        this.Active,
                        this.IsControlFor().ToList()
                    );
                }
            }

            [System.Runtime.Serialization.DataContract(IsReference = true)]
            private class Conceptor : Vertex, IConceptor {
                [System.Runtime.Serialization.DataMember]
                public List<IVertex> ZeroSubset = new List<IVertex>();

                public void AddToZeroSubset(params IVertex[] vertices) {
                    if (vertices == null)
                        return;
                    foreach (var vertex in vertices) {
                        if (this.ZeroSubset.Contains(vertex))
                            continue;
                        this.ZeroSubset.Add(vertex);
                    }
                    this._k = 0;
                }

                public IEnumerable<IVertex> GetZeroSubset() {
                    return this.ZeroSubset;
                }

                public void RemoveVromZeroSubset(params IVertex[] vertices) {
                    if (vertices == null)
                        return;
                    foreach (var vertex in vertices) {
                        this.ZeroSubset.Remove(vertex);
                    }
                    this._k = 0;
                }

                [System.Runtime.Serialization.DataMember]
                private int _k = 0;

                public override int K() {
                    if (this._k == 0) {
                        this._k = this.GetUnderlyingReceptors().Distinct().Count();
                    }
                    return this._k;
                }

                public override IEnumerable<IReceptor> GetUnderlyingReceptors() {
                    var result = new List<IReceptor>();
                    foreach (var sub in this.ZeroSubset) {
                        result.AddRange(sub.GetUnderlyingReceptors());
                    }
                    return result;
                }

                public override string ToString() {
                    return this.ZeroSubset.Count +
                        $"->(lvl={this._k} {(this.IsActive() ? "A" : "")} " +
                        $"fa={this.Active.ToString()}" +
                        (this.IsControlFor().Any() ? " ctrl=" + string.Join(",", this.IsControlFor()) : "") + ")->" +
                        this.ZeroSuperset.Count;
                }

                internal VerticesForSerialization.ConceptorForSerialization GetViewForSerialization(
                    int levelKey,
                    long id
                ) {
                    return new VerticesForSerialization.ConceptorForSerialization(
                        levelKey,
                        id,
                        this.MsField,
                        this.Active,
                        this._k,
                        this.IsControlFor().ToList()
                    );
                }
            }
        }
    }
}