﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Krolik.Mathematics;
using Krolik.Classifiers;

namespace Krolik.Gpns {
    /// <summary>
    /// Each instance of the interface represents a growing
    /// pyramidal network - a graph with special properties.
    /// </summary>
    internal interface IGpn {
        /// <summary>
        /// Returns true, if the given feature value and index were absent.
        /// </summary>
        bool AddReceptor(int featureIndex, DiscreteValue featureValue);

        bool AddReceptor(IPlane plane);

        /// <summary>
        /// Deactivates all vertices, satisfying the given filter.
        /// </summary>
        /// <param name="filter">If null, deactivates all vertices.</param>
        void Deactivate(Expression<Func<IVertex, bool>> filter = null);
        
        // TODO. Remove 'soft' parameter.
        /// <summary>
        /// Activates the network according to the given vector.
        /// </summary>
        IList<IVertex> Activate(IHybridVector vector, IActivationStrategy strategy);
        
        /// <summary>
        /// Retrieves all vertices of the network.
        /// </summary>
        IEnumerable<IVertex> GetVertices(Expression<Func<IVertex, bool>> filter = null);

        IEnumerable<TVertex> Projection<TVertex>(
            Func<IVertex, TVertex> map,
            Action<TVertex, TVertex> addSecondVertexToUpperOfFirstVertex,
            Action<TVertex, TVertex> addSecondVertexToLowerOfFirstVertex,
            Expression<Func<IVertex, bool>> filter = null
        );
        
        /// <summary>
        /// Inserts a vertex and connect it with the given
        /// collection of vertices from bottom and the given
        /// conceptor from above (if not null).
        /// </summary>
        /// <param name="lower">This will be the zero subset of the new vertex.</param>
        /// <param name="upper">This will be included in zero superset of new vertex (if not null).</param>
        IVertex InsertBetween(IEnumerable<IVertex> lower, IConceptor upper);
        
        /// <summary>
        /// Retrieves all active vertices which have no active
        /// vertices in their supersets.
        /// </summary>
        IEnumerable<IVertex> G();

        GpnForSerialization GetViewForSerialization();
    }

    [System.Runtime.Serialization.DataContract]
    internal class GpnForSerialization {
        [System.Runtime.Serialization.DataMember]
        public VerticesForSerialization VerticesForSerialization { get; set; }

        [System.Runtime.Serialization.DataMember]
        public IFuzzySet Fuzziness { get; set; }

        public GpnForSerialization(
            VerticesForSerialization verticesForSerialization, 
            IFuzzySet fuzziness
        ) {
            this.VerticesForSerialization = verticesForSerialization;
            this.Fuzziness = fuzziness;
        }

        public IGpn ToGpn(GpnFactory gpnFactory) {
            var gpn = gpnFactory.Make(
                this.Fuzziness,
                this.VerticesForSerialization.ToVerticesStructure()
            );
            return gpn;
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class VerticesForSerialization {
        [System.Runtime.Serialization.DataMember]
        private List<DiscreteReceptorForSerialization> DiscrRcps { get; set; }

        [System.Runtime.Serialization.DataMember]
        private List<ContinuousReceptorForSerialization> ContRcps { get; set; }

        [System.Runtime.Serialization.DataMember]
        private List<ConceptorForSerialization> Conceptors { get; set; }

        [System.Runtime.Serialization.DataMember]
        private Dictionary<long, HashSet<long>> Supersets { get; set; }

        [System.Runtime.Serialization.DataMember]
        private Dictionary<long, HashSet<long>> Subsets { get; set; }

        public VerticesForSerialization(
            List<DiscreteReceptorForSerialization> discrRcps, 
            List<ContinuousReceptorForSerialization> contRcps, 
            List<ConceptorForSerialization> conceptors, 
            Dictionary<long, HashSet<long>> supersets, 
            Dictionary<long, HashSet<long>> subsets
        ) {
            this.DiscrRcps = discrRcps;
            this.ContRcps = contRcps;
            this.Conceptors = conceptors;
            this.Supersets = supersets;
            this.Subsets = subsets;
        }

        [System.Runtime.Serialization.DataContract]
        internal class DiscreteReceptorForSerialization {
            [System.Runtime.Serialization.DataMember]
            public int FeatureIndex { get; set; }

            [System.Runtime.Serialization.DataMember]
            public DiscreteValue FeatureValue { get; set; }

            [System.Runtime.Serialization.DataMember]
            public long Id { get; set; }

            [System.Runtime.Serialization.DataMember]
            public Dictionary<Cls, int> MsField { get; set; }

            [System.Runtime.Serialization.DataMember]
            public Certainty Active { get; set; }

            [System.Runtime.Serialization.DataMember]
            public IList<Cls> IsControlFor { get; set; }

            public DiscreteReceptorForSerialization(
                int featureIndex, 
                DiscreteValue featureValue,
                long id,
                Dictionary<Cls, int> msField,
                Certainty active,
                IList<Cls> isControlFor
            ) {
                this.FeatureIndex = featureIndex;
                this.FeatureValue = featureValue;
                this.Id = id;
                this.MsField = msField;
                this.Active = active;
                this.IsControlFor = isControlFor;
            }
        }

        [System.Runtime.Serialization.DataContract]
        internal class ContinuousReceptorForSerialization {
            [System.Runtime.Serialization.DataMember]
            public long Id { get; set; }

            [System.Runtime.Serialization.DataMember]
            public IPlane Plane { get; set; }

            [System.Runtime.Serialization.DataMember]
            public Dictionary<Cls, int> MsField { get; set; }

            [System.Runtime.Serialization.DataMember]
            public Certainty Active { get; set; }

            [System.Runtime.Serialization.DataMember]
            public IList<Cls> IsControlFor { get; set; }

            public ContinuousReceptorForSerialization(
                long id, 
                IPlane plane,
                Dictionary<Cls, int> msField,
                Certainty active,
                IList<Cls> isControlFor
            ) {
                this.Id = id;
                this.Plane = plane ??
                    throw new ArgumentNullException(nameof(plane));
                this.MsField = msField ??
                    throw new ArgumentNullException(nameof(msField));
                this.Active = active;
                this.IsControlFor = isControlFor;
            }
        }

        [System.Runtime.Serialization.DataContract]
        internal class ConceptorForSerialization {
            [System.Runtime.Serialization.DataMember]
            public int LevelKey { get; set; }

            [System.Runtime.Serialization.DataMember]
            public long Id { get; set; }

            [System.Runtime.Serialization.DataMember]
            public Dictionary<Cls, int> MsField { get; set; }

            [System.Runtime.Serialization.DataMember]
            public Certainty Active { get; set; }

            [System.Runtime.Serialization.DataMember]
            public int K { get; set; }

            [System.Runtime.Serialization.DataMember]
            public IList<Cls> IsControlFor { get; set; }

            public ConceptorForSerialization(
                int levelKey, 
                long id, 
                Dictionary<Cls, int> msField, 
                Certainty active, 
                int k,
                IList<Cls> isControlFor
            ) {
                this.LevelKey = levelKey;
                this.Id = id;
                this.MsField = msField ??
                    throw new ArgumentNullException(nameof(msField));
                this.Active = active;
                this.K = k;
                this.IsControlFor = isControlFor;
            }
        }

        internal Gpn.VerticesStructure ToVerticesStructure() {
            return new Gpn.VerticesStructure(
                this.DiscrRcps,
                this.ContRcps,
                this.Conceptors,
                this.Supersets,
                this.Subsets
            );
        }
    }
}