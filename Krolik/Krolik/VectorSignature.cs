﻿using Krolik.Classifiers;
using Krolik.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik {
    internal interface IVectorSignature {
        IComponentType[] Components { get; }

        /// <summary>
        /// Converts the given vector's components to the type
        /// described in the signature.
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="vector"></param>
        /// <param name="setSignature">If true, sets the vector's signature to this one.</param>
        /// <returns></returns>
        void Deserialize<TClass>(IHybridVector vector, bool setSignature = true);

        Cls DeserializeClass(string cls);
        string SerializeClass(Cls cls);
    }

    internal interface IComponentType {
        ///// <summary>
        ///// Returns a destination >= 0 between the given values,
        ///// or a value less than 0, if the values are not
        ///// comparable.
        ///// </summary>
        //decimal GetDestination(object a, object b);
        string Serialize(object value);
        void Deserialize(string component, out DiscreteValue? discreteValue, out decimal? continuousValue);
    }

    [System.Runtime.Serialization.DataContract]
    internal class VectorSignature : IVectorSignature {
        public IComponentType[] Components { get; private set; }

        private static IDictionary<string, Cls> ClsRef = new Dictionary<string, Cls>();

        public VectorSignature(IComponentType[] components) {
            if (components == null)
                throw new ArgumentNullException("components");
            this.Components = components;
        }

        public Cls DeserializeClass(string cls) {
            if(!ClsRef.ContainsKey(cls)) {
                var max = ClsRef.Values.OrderByDescending(x => x.Index).FirstOrDefault();
                var val = max.Next();
                val.Name = cls;
                ClsRef[cls] = val;
            }
            return ClsRef[cls];
        }

        public string SerializeClass(Cls cls) {
            var pairs = ClsRef.Where(x => x.Value == cls).ToList();
            if (pairs.Count == 0)
                throw new Exception("Unknown class.");
            return pairs.Single().Key;
        }

        public static IVectorSignature AllDiscrete(int componentsCount) {
            return new VectorSignature(Enumerable.Range(0, componentsCount)
                .Select(x => ComponentType.Discrete)
                .ToArray()
            );
        }

        public static IVectorSignature AllDecimal(int componentsCount) {
            return new VectorSignature(Enumerable.Range(0, componentsCount)
                .Select(x => ComponentType.ContinuumLike(
                    x2 => x2.ToString(),
                    x2 => {
                        try {
                            return (decimal)Infrastructure.Extensions.ToDecimal(x2);
                        } catch (InvalidCastException) {
                            throw new Exception("Cannot parse '" + x2 + "' as decimal.");
                        }
                    }
                ))
                .ToArray()
            );
        }

        public void Deserialize<TClass>(IHybridVector vector, bool setSignature = true) {
            throw new NotImplementedException();
            //var vectorLength = vector.Count();
            //if (vectorLength != this.Components.Length)
            //    throw new WrongSignatrueException();
            //for (int i = 0; i < vectorLength; i++) {
            //    if(vector[i] is string)
            //        vector[i] = this.Components[i].Deserialize((string)vector[i]);
            //}
            //if (setSignature)
            //    vector.Signature = this;
        }

        internal static void ClearCache() {
            ClsRef.Clear();
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class DiscreteComponentType : ComponentType {
        private IDictionary<string, DiscreteValue> _reference = new Dictionary<string, DiscreteValue>();

        public DiscreteComponentType(string name)
            : base(name)
        {

        }

        public override void Deserialize(string component, out DiscreteValue? discreteValue, out decimal? continuousValue) {
            if (!this._reference.ContainsKey(component)) {
                var allDiscrs = this._reference.Values;
                var max = allDiscrs.OrderByDescending(x => x.Value).FirstOrDefault();
                this._reference[component] = max.Next();
            }
            continuousValue = null;
            discreteValue = this._reference[component];
        }

        public override string Serialize(object value) {
            if (!(value is DiscreteValue))
                throw new Exception("The given value must be of discrete type.");
            var dv = (DiscreteValue)value;
            var pairs = this._reference.Where(x => x.Value == dv).ToList();
            if (pairs.Count == 0)
                throw new Exception("Unknown value.");
            return pairs.Single().Key;
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal class ContinuousComponentType : ComponentType {
        public ContinuousComponentType(string name)
            : base(name) 
        {

        }

        public override void Deserialize(string component, out DiscreteValue? discreteValue, out decimal? continuousValue) {
            discreteValue = null;
            var componentTtimmed = component;
            //var indexOfSeparator = component.IndexOf(',');
            //if (indexOfSeparator >= 0)
            //    componentTtimmed = componentTtimmed.Substring(0, indexOfSeparator);
            //else {
            //    indexOfSeparator = componentTtimmed.IndexOf('.');
            //    if(indexOfSeparator >= 0)
            //        componentTtimmed = componentTtimmed.Substring(0, indexOfSeparator);
            //}
            //var zeroChars = new[] { '0', '.', ',', };
            //if (componentTtimmed == "0" && component.Any(x => !zeroChars.Contains(x)))
            //    throw new Exception("Data lost.");
            continuousValue = componentTtimmed.ToDecimal().Value;// decimal.Parse(componentTtimmed);
        }

        public override string Serialize(object value) {
            if (!(value is decimal))
                throw new Exception("The given vallue must be of continuous type.");
            var cv = (decimal)value;
            return cv.ToString("f3");
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal abstract class ComponentType : IComponentType {
        //public Func<object, object, decimal> DestinationCalculator { get; private set; }
        //private Func<string, object> _deserializier;
        //private Func<object, string> _serializier;
        public string Name { get; private set; }

        public ComponentType(
            string name
            //Func<object, string> serializer,
            //Func<string, object> deserializer,
            //Func<object, object, decimal> destinationCalculator
        ) {
            this.Name = name;
            //this._serializier = serializer;
            //this._deserializier = deserializer;
            //this.DestinationCalculator = destinationCalculator;
        }

        //public decimal GetDestination(object a, object b) {
        //    return this.DestinationCalculator(a, b);
        //}

        public abstract string Serialize(object value);

        //public object Deserialize(string value) {
        //    return this._deserializier(value);
        //}

        public abstract void Deserialize(string component, out DiscreteValue? discreteValue, out decimal? continuousValue);

        private static ComponentType _discrete;
        public static IComponentType Discrete {
            get {
                if (_discrete == null) {
                    _discrete = new DiscreteComponentType(
                        "Discrete"
                        //x => x.ToString(),
                        //x => x,
                        //(a, b) => a == null && b == null || a != null && a.Equals(b)
                        //    ? 0
                        //    : -1m
                    );
                }
                return _discrete;
            }
        }

        public static IComponentType ContinuumLike(
            Func<object, string> serializer = null,
            Func<string, object> deserializer = null
        ) {
            if (serializer == null)
                serializer = x => x.ToString();
            if (deserializer == null)
                deserializer = x => {
                    try {
                        return Infrastructure.Extensions.ToDecimal(x);
                    } catch (InvalidCastException) {
                        throw new Exception("Cannot parse '" + x + "' as decimal.");
                    }
                };
            var result = new ContinuousComponentType(
                "ContinuumLike"
                //serializer,
                //deserializer,
                //(a, b) => Math.Abs((decimal)a - (decimal)b)
            );
            return result;
        }
    }
}
