﻿using NLog;
using System;

namespace Krolik.Infrastructure {
    public static class KrolikLoggers {
        private static IKrolikLoggersProvider Provider;

        public static IKrolikLoggersProvider Get {
            get {
                if (Provider == null) {
                    Provider = new KrolikLoggersProvider();
                }
                return Provider;
            }
        }

        private class KrolikLoggersProvider : IKrolikLoggersProvider {
            private Lazy<ILogger>
                _common = new Lazy<ILogger>(() => LogManager.GetLogger("Common")),
                _classification = new Lazy<ILogger>(() => LogManager.GetLogger("Classification")),
                _preparation = new Lazy<ILogger>(() => LogManager.GetLogger("Preparation")),
                _console = new Lazy<ILogger>(() => LogManager.GetLogger("Console"));
            
            public ILogger Common {
                get {
                    return this._common.Value;
                }
            }

            public ILogger Classification {
                get {
                    return this._classification.Value;
                }
            }

            public ILogger Preparation {
                get {
                    return this._preparation.Value;
                }
            }

            public ILogger Console {
                get {
                    return this._console.Value;
                }
            }
        }
    }

    public interface IKrolikLoggersProvider {
        ILogger Common { get; }
        ILogger Classification { get; }
        ILogger Preparation { get; }
        ILogger Console { get; }
    }
}
