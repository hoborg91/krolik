﻿using System.IO;
using System.Linq;

namespace Krolik.Infrastructure {
    public static class SlashMaster {
        /// <summary>
        /// Replaces folder separators in the given path
        /// according to the Path.DirectorySeparatorChar.
        /// </summary>
        public static string CorrectPath(this string path) {
            var result = path;
            foreach (var separator in new[] {
                '\\',
                '/',
            }) {
                if (!result.Contains(separator))
                    continue;
                if (Path.DirectorySeparatorChar == separator)
                    continue;
                var parts = path.Split(separator);
                result = string.Join("" + Path.DirectorySeparatorChar, parts);
            }
            return result;
        }
    }
}
