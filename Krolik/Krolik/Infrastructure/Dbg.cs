﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.Infrastructure {
    /// <summary>
    /// Can be used to debug call stack structure.
    /// </summary>
    internal static class Dbg {
        private static Stack<string> Location = new Stack<string>();

        private static NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static string[] Star = new[] { "*", };

        public static void Enter(string where) {
            Location.Push(where);
            //Logger.Debug(PrintLocation());
        }

        public static void Leave(string what) {
            var peek = Location.Peek();
            if (!Location.Any() || Location.Pop() != what)
                throw new Exception();
            //Logger.Debug(PrintLocation());
        }

        private static string PrintLocation() {
            return string.Join("->", Star.Concat(Location.Reverse()));
        }
    }
}
