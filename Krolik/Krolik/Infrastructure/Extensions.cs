﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Krolik.Infrastructure {
    public static class Extensions {
        /// <summary>
        /// Converts a given string to decimal.
        /// Considers different floating point delimeters.
        /// </summary>
        public static float? ToDouble(this string input) {
            return ToNumber<float>(input, s => {
                float result;
                if (float.TryParse(
                    s,
                    NumberStyles.Any,
                    CultureInfo.CurrentCulture,
                    out result)
                )
                    return result;
                return null;
            });
        }

        public static decimal? ToDecimal(this string input) {
            return ToNumber<decimal>(input, s => {
                decimal result;
                if (decimal.TryParse(
                    s,
                    NumberStyles.Any,
                    CultureInfo.CurrentCulture,
                    out result)
                )
                    return result;
                return null;
            });
        }

        private static T? ToNumber<T>(this string input, Func<string, T?> tryParse)
            where T : struct
        {
            T? result;
            foreach (var separator in new[] {
                null,
                ".",
                ",",
            }) {
                var inp = separator == null
                    ? input
                    : input.Replace(separator, CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                inp = inp.ToLower();
                result = tryParse(inp);
                if(result.HasValue)
                    return result;
            }
            return null;
        }

        public static bool EqualsWithPrecision(this decimal value, decimal other, decimal precision) {
            var diff = value - other;
            if (diff >= 0)
                return diff <= precision;
            return diff >= -precision;
        }

        public static IEnumerable<T> AsEnumerable<T>(this T[,] matrix, int dim0 = -1, int dim1 = -1) {
            if (matrix == null)
                throw new ArgumentNullException(nameof(matrix));
            if (dim0 < 0)
                dim0 = matrix.GetLength(0);
            if (dim1 < 0)
                dim1 = matrix.GetLength(1);
            for (int i = 0; i < dim0; i++) {
                for (int j = 0; j < dim1; j++) {
                    yield return matrix[i, j];
                }
            }
        }

        public static T[] Slice<T>(this T[,] matrix, int fixedDimension, int fixedIndex) {
            if (matrix == null)
                throw new ArgumentNullException(nameof(matrix));
            if (fixedDimension < 0 || fixedDimension > 1)
                throw new ArgumentException("Dimension must be in range (0; 1).");
            var len = matrix.GetLength(fixedDimension);
            if (fixedIndex < 0 || fixedIndex >= len)
                throw new ArgumentException("Index must be in range (0; corresponding length).");

            var result = new T[matrix.GetLength(1 - fixedDimension)];
            if (fixedDimension == 0)
                for (int i = 0; i < result.Length; i++) {
                    result[i] = matrix[fixedIndex, i];
                }
            else if (fixedDimension == 1)
                for (int i = 0; i < result.Length; i++) {
                    result[i] = matrix[i, fixedIndex];
                }
            else
                throw new NotImplementedException();
            return result;
        }

        public static IEnumerable<IGrouping<IEnumerable<TGrouping>, TElement>> GroupByCollections<TElement, TGrouping>(
            this IEnumerable<TElement> collection,
            Func<TElement, IEnumerable<TGrouping>> groupBy
        ) {
            var result = collection
                .Select(e => new {
                    Element = e,
                    GroupingCollection = groupBy(e),
                })
                .GroupBy(
                    x => x.GroupingCollection, 
                    x => x.Element,
                    Cil.Common.EnumerableComparer.For<TGrouping>()
                );
            return result;
        }
    }
}
