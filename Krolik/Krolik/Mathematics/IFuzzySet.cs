﻿using System;

namespace Krolik.Mathematics {
    internal interface IFuzzySet {
        /// <summary>
        /// Returns the value of the membership function represented
        /// by the IFuzziness object in the given point. The return
        /// value is in [0; 1].
        /// </summary>
        Certainty Mu(decimal point);
    }

    /// <summary>
    /// Represents a membership function of the following type.
    /// If x belongs to (-Inf; 0), then y = 0.
    /// If x belongs to [0; precision), then y = 1 - x / precision.
    /// If x belongs to [precison, +Inf), then y = 0.
    /// </summary>
    [System.Runtime.Serialization.DataContract]
    internal class LeftTriangleFuzzySet : IFuzzySet {
        [System.Runtime.Serialization.DataMember]
        public decimal Precision { get; private set; }

        /// <summary>
        /// Represents a membership function of the following type.
        /// If x belongs to (-Inf; 0), then y = 0.
        /// If x belongs to [0; precision), then y = 1 - x / precision.
        /// If x belongs to [precison, +Inf), then y = 0.
        /// </summary>
        public LeftTriangleFuzzySet(decimal precision) {
            if (precision < 0)
                throw new ArgumentException($"The '{nameof(precision)}' must be >= 0.");
            this.Precision = precision;
        }

        /// <summary>
        /// Returns the value of the membership function represented
        /// by the IFuzziness object in the given point. The return
        /// value is in [0; 1].
        /// </summary>
        public Certainty Mu(decimal value) {
            if (value < 0)
                throw new ArgumentException($"The '{nameof(value)}' must be >= 0.");
            if (value >= this.Precision)
                return 0;
            if (this.Precision == 0)
                return value == 0
                    ? Certainty.Absolute
                    : Certainty.None
                ;
            return (decimal)Certainty.Absolute - value / this.Precision;
        }
    }

    /// <summary>
    /// Represents a membership function of the following type.
    /// If x belongs to (-Inf; 0), then y = 0.
    /// If x belongs to [0; +Inf), then y = 1 / (sharpness * x + 1).
    /// </summary>
    [System.Runtime.Serialization.DataContract]
    internal class LeftInverseFuzzySet : IFuzzySet {
        [System.Runtime.Serialization.DataMember]
        public decimal Sharpness { get; private set; }

        /// <summary>
        /// Represents a membership function of the following type.
        /// If x belongs to (-Inf; 0), then y = 0.
        /// If x belongs to [0; +Inf), then y = 1 / (sharpness * x + 1).
        /// </summary>
        public LeftInverseFuzzySet(decimal sharpness) {
            if (sharpness <= 0)
                throw new ArgumentException($"The '{nameof(sharpness)}' must be > 0.");
            this.Sharpness = sharpness;
        }

        /// <summary>
        /// Returns the value of the membership function represented
        /// by the IFuzziness object in the given point. The return
        /// value is in [0; 1].
        /// </summary>
        public Certainty Mu(decimal point) {
            // TODO. Bad code. Refactor.
            if (point == decimal.MaxValue)
                return 0;
            var result = point < 0
                ? (decimal)Certainty.None
                : Certainty.Absolute / (this.Sharpness * point + 1);
            return result;
        }
    }

    /// <summary>
    /// Represents a membership function of the following type.
    /// If x belongs to (-Inf; 0), then y = 0.
    /// If x belongs to [0; value], then y = 1.
    /// If x belongs to (value; +Inf), then y = 0.
    /// </summary>
    [System.Runtime.Serialization.DataContract]
    internal class LeftDiscreteFuzzySet : IFuzzySet {
        [System.Runtime.Serialization.DataMember]
        private readonly decimal _value;

        /// <summary>
        /// Represents a membership function of the following type.
        /// If x belongs to (-Inf; 0), then y = 0.
        /// If x belongs to [0; value], then y = 1.
        /// If x belongs to (value; +Inf), then y = 0.
        /// </summary>
        public LeftDiscreteFuzzySet(decimal value) {
            if (value < 0)
                throw new ArgumentException(nameof(value) + " must be >= 0.");
            this._value = value;
        }

        /// <summary>
        /// Returns the value of the membership function represented
        /// by the IFuzziness object in the given point. The return
        /// value is in [0; 1].
        /// </summary>
        public Certainty Mu(decimal point) {
            return point <= this._value
                ? Certainty.Absolute
                : Certainty.None;
        }
    }
}
