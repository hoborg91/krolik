﻿using Krolik.Exceptions;
using System.Collections.Generic;

namespace Krolik.Mathematics {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
    internal class MultiConcentratedPlanesFactory {
        [System.Runtime.Serialization.DataMember]
        private ILinearAlgebra _linal;
        
        public MultiConcentratedPlanesFactory(ILinearAlgebra linal) {
            KrolikException.CheckArgumentNotNull(linal, nameof(linal));
            this._linal = linal;
        }

        internal LinearAlgebra.MultiConcentratedPlane Make(IList<decimal[]> points, IPrecisionProvider precision) {
            var truePlane = this._linal.MakePlane(points, precision) as IOpenPlane;
            decimal maxDistance = 0, minDistance = decimal.MaxValue;
            foreach (var a in points) {
                foreach (var b in points) {
                    var distance = this._linal.Distance(a, b);
                    if (distance > maxDistance)
                        maxDistance = distance;
                    if (distance < minDistance)
                        minDistance = distance;
                }
            }
            var result = new LinearAlgebra.MultiConcentratedPlane(
                truePlane,
                points,
                precision,
                new LeftTriangleFuzzySet(maxDistance),
                this._linal
            );
            return result;
        }
    }
}
