﻿using System;

namespace Krolik.Mathematics {
    internal interface IPrecisionProvider {
        decimal Divisor { get; }
        bool IsZero(decimal number);
        decimal Tolerance { get; }
    }

    // TODO. Replace usages of this class in tests with mocks.
    [System.Runtime.Serialization.DataContract]
    internal class CustomPrecisionProvider : IPrecisionProvider {
        [System.Runtime.Serialization.DataMember]
        private decimal _precision;

        [System.Runtime.Serialization.DataMember]
        public decimal Divisor { get; private set; }

        public CustomPrecisionProvider(decimal precision, decimal divisor) {
            if (precision < 0)
                throw new ArgumentException("The 'precision' must be >= 0.");
            this._precision = precision;
            this.Divisor = divisor;
        }

        public bool IsZero(decimal number) {
            return Math.Abs(number - 0) < this._precision;
        }

        public static IPrecisionProvider GetDefaultProvider() {
            var result = new CustomPrecisionProvider(1e-15m, 1e11m);
            return result;
        }

        public decimal Tolerance {
            get {
                return this._precision;
            }
        }
    }
}
