﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krolik.Mathematics {
    // TODO. There exists only one implementation.
    // Remove the interface?
    internal interface IFuzzySetFactory {
        IFuzzySet Make(string fuzzyType, decimal fuzzyParameter);
    }
}
