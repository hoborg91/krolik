﻿using System;
using System.Collections.Generic;

namespace Krolik.Mathematics {
    internal interface IPlane {
        bool Contains(decimal[] point, IPrecisionProvider precisionProvider);
        decimal DistanceTo(decimal[] point);
        decimal[] Make(decimal[] point, decimal maxDistance);
        decimal[] ProjectionOf(decimal[] point);
    }

    internal interface IWithComponents {
        decimal[] Components { get; }
        [Obsolete("For debug only")]
        IList<decimal[]> BasedOnPoints { get; }
        decimal[] Intersection(IWithComponents openPlane);
    }

    internal interface IOpenPlane : IPlane, IWithComponents {
        
    }
}
