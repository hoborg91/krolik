﻿using System;

namespace Krolik.Mathematics {
    [System.Runtime.Serialization.DataContract]
    internal struct Certainty : IComparable<Certainty>, IComparable {
        [System.Runtime.Serialization.DataMember]
        private readonly decimal _value;

        public Certainty(decimal value) {
            if (value < 0 || value > 1)
                throw new ArgumentException($"The given '{nameof(value)}' must be in [0; 1].");
            this._value = value;
        }

        public override string ToString() {
            return this._value.ToString("f3");
        }

        public int CompareTo(Certainty other) {
            return this._value.CompareTo(other._value);
        }

        public int CompareTo(object obj) {
            if (!(obj is Certainty))
                return -1;
            return this.CompareTo((Certainty)obj);
        }

        public static implicit operator Certainty(decimal value) {
            return new Certainty(value);
        }

        public static implicit operator decimal(Certainty certainty) {
            return certainty._value;
        }
        
        public static Certainty None => new Certainty();

        public static Certainty Absolute => new Certainty(1);
    }
}
