﻿using System;

namespace Krolik.Mathematics {
    internal static class ProbabilityDistribution {
        public static double NextGaussian(this Random random, double mean, double standardDeviation) {
            double u1 = 1.0 - random.NextDouble();
            double u2 = 1.0 - random.NextDouble();
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                Math.Sin(2.0 * Math.PI * u2);
            double randNormal = mean + standardDeviation * randStdNormal;
            return randNormal;
        }
    }
}
