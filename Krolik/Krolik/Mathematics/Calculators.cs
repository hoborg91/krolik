﻿using Krolik.Exceptions;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Krolik.Mathematics {
    internal interface IDeterminantCalculator {
        decimal CalculateDeterminant(decimal[] squareMatrix, int dimension, IPrecisionProvider precision);
    }

    internal interface IMatrixRankCalculator {
        /// <summary>
        /// Transforms the given matrix to the row echelon
        /// form and returns its rank.
        /// </summary>
        int CalculateRank(decimal[] A, int rows, int columns, IPrecisionProvider precision);
    }

    internal interface IGcdCalculator {
        decimal Gcd(IPrecisionProvider precision, params decimal[] numbers);
    }

    internal interface ICalculatorProvider {
        IDeterminantCalculator GetDeterminantCalculator();

        IMatrixRankCalculator GetMatrixRankCalculator();

        IGcdCalculator GetGcdCalculator();
    }

    [System.Runtime.Serialization.DataContract]
    internal class CalculatorProvider : ICalculatorProvider {
        private IDeterminantCalculator _determinantCalculator = null;
        private IMatrixRankCalculator _matrixRankCalculator = null;
        private IGcdCalculator _gcdCalculator = null;

        //private readonly ILinearAlgebra _linal;

        //private CalculatorProvider(ILinearAlgebra linal) {
        //    KrolikException.CheckArgumentNotNull(linal, nameof(linal));
        //    this._linal = linal;
        //}

        private static ICalculatorProvider _singleton;

        //public static ICalculatorProvider Get() {
        //    if(_singleton == null) {
        //        _singleton = new CalculatorProvider();
        //    }
        //    return _singleton;
        //}

        public IDeterminantCalculator GetDeterminantCalculator() {
            if (_determinantCalculator == null)
                // TODO. Refactor new GaussCalculator(this...).
                _determinantCalculator = new GaussCalculator(this);//, this._linal);
            return _determinantCalculator;
        }

        public IMatrixRankCalculator GetMatrixRankCalculator() {
            if (_matrixRankCalculator == null)
                // TODO. Refactor new GaussCalculator(this...).
                _matrixRankCalculator = new GaussCalculator(this);//, this._linal);
            return _matrixRankCalculator;
        }

        public IGcdCalculator GetGcdCalculator() {
            if (_gcdCalculator == null)
                _gcdCalculator = new PairsGcdCalculator();
            return _gcdCalculator;
        }

        private class MathNetDeterminantCalculator : IDeterminantCalculator {
            public decimal CalculateDeterminant(decimal[] matrix, int dimension, IPrecisionProvider precision) {
                var m = CreateMatrix.Dense(dimension, dimension, matrix);
                var result = m.Determinant();
                return result;
            }
        }

        public class GaussCalculator : IDeterminantCalculator, IMatrixRankCalculator {
            private readonly ICalculatorProvider _calculators;
            //private readonly ILinearAlgebra _linal;

            public GaussCalculator(
                ICalculatorProvider calculators//,
                //ILinearAlgebra linal
            ) {
                KrolikException.CheckArgumentNotNull(calculators, nameof(calculators));
                //KrolikException.CheckArgumentNotNull(linal, nameof(linal));

                this._calculators = calculators;
                //this._linal = linal;
            }

            private class MakeRowEchelonResult {
                public decimal Multiplier { get; set; }
                public int Rank { get; set; }
            }

            private static Type[] IntegerTypes = new[] {
                typeof(System.Numerics.BigInteger),
            };

            private MakeRowEchelonResult _makeRowEchelon(decimal[] A, int rows, int columns, IPrecisionProvider precision) {
                var logs = new List<string>();
                Action<string> log = (string x) => logs.Add(x);

                var trace = "0";
                Action<string> tr = x => trace += "-" + x;

                // TODO. "new" operator? No way.
                // Introduce PrecisionProvidersFactory or smth.
                var precisionForZero = new CustomPrecisionProvider(1e-15m, 1m);

                try {

                    int r = 0, c = 0;
                    decimal multiplier = 1;
                    while (r < rows && c < columns) {
                        log(Environment.NewLine);
                        log(string.Format("r = {0}, c = {1}, multiplier = {2}", r, c, multiplier));
                        log(this._formatMatrix(A, columns, rows));

                        if (precisionForZero.IsZero(this._getElem(A, columns, r, c))) {
                            tr("100");
                            var allZeros = true;
                            for (int i = r + 1; i < rows; i++) {
                                tr("101");
                                if (precisionForZero.IsZero(this._getElem(A, columns, i, c)))
                                    continue;
                                tr("102");
                                this._swapRows(A, columns, r, i);
                                tr("103");
                                multiplier *= -1;
                                allZeros = false;
                                break;
                            }
                            tr("104");
                            if (allZeros)
                                c++;
                        } else {
                            tr("200");
                            for (int i = r + 1; i < rows; i++) {
                                this._treatMatrix(A, columns, i, c, r, ref multiplier, precisionForZero);
                                tr("201");
                            }
                            //A.MakeZeros(columns, r, this.Precision / (decimal)1000);
                            c++;
                            r++;
                        }

                        var zeros = new List<int>();
                        for (int i = 0; i < rows; i++) {
                            if (precisionForZero.IsZero(Math.Abs(this._getElem(A, columns, i, i))))
                                zeros.Add(i);
                        }
                        if (zeros.Any()) {
                            log("Diagonal zeros are located at lines " + string.Join(", ", zeros) + ".");
                        } else {
                            log("There are no diagonal zeros.");
                        }
                    }

                    int rank = Math.Min(r, c);
                    logs.Add("rank = " + rank);
                    this._logIfMatrixIsStrange(logs, A, rows, columns);
                    
                    return new MakeRowEchelonResult {
                        Multiplier = multiplier,
                        Rank = rank,
                    };

                } catch (IndexOutOfRangeException ex) {
                    Console.WriteLine($"IndexOutOfRangeException, trace: {trace}. {ex.StackTrace}");
                    throw;
                }
            }

            private void _logIfMatrixIsStrange(List<string> logs, decimal[] A, int rows, int columns) {
                var needLog = false;
                for (int i = 0; i < rows && !needLog; i++) {
                    if (this._getElem(A, columns, i, i) == 0) {
                        needLog = true;
                        continue;
                    }
                    for (int j = 0; j < i; j++) {
                        if (this._getElem(A, columns, i, j) != 0)
                            needLog = true;
                    }
                }
                if (!needLog)
                    return;
                File.WriteAllLines("matix.txt", logs);
            }

            private string _formatMatrix(decimal[] A, int rows, int columns) {
                var result = new System.Text.StringBuilder();
                for (int i = 0; i < rows; i++) {
                    result.Append(i + "\t| ");
                    for (int j = 0; j < rows; j++) {
                        result.Append(this._getElem(A, columns, i, j) + " ");
                    }
                    result.AppendLine();
                }
                return result.ToString();
            }

            private bool _treatMatrix(decimal[] A, int columns, int i, int c, int r, ref decimal multiplier, IPrecisionProvider precision) {
                var integerTypes = IntegerTypes.Contains(multiplier.GetType());
                if (integerTypes) {
                    var current = this._getElem(A, columns, i, c);
                    var bas = this._getElem(A, columns, r, c);
                    var lcm = _leastCommonMultiple(bas, current, precision, this._calculators.GetGcdCalculator());
                    if (precision.IsZero(current))
                        return false;
                    var currentCoeff = lcm / current;
                    var baseCoeff = lcm / bas;
                    for (int j = 0; j < columns; j++) {
                        this._setElem(A, columns, r, j, this._getElem(A, columns, r, j) * baseCoeff);
                    }
                    multiplier *= baseCoeff;
                    multiplier *= currentCoeff;
                    this._setElem(A, columns, i, c, 0);
                    for (int j = c + 1; j < columns; j++) {
                        var a = this._getElem(A, columns, i, j) * currentCoeff;
                        this._setElem(A, columns, i, j, a - this._getElem(A, columns, r, j));
                    }
                    return true;
                } else {
                    var coeff = this._getElem(A, columns, i, c) / this._getElem(A, columns, r, c);
                    this._setElem(A, columns, i, c, 0);
                    for (int j = c + 1; j < columns; j++) {
                        var a = this._getElem(A, columns, i, j);
                        this._setElem(A, columns, i, j, a - this._getElem(A, columns, r, j) * coeff);
                    }
                    return true;
                }
            }

            private static decimal _leastCommonMultiple(decimal a, decimal b, IPrecisionProvider precision, IGcdCalculator gcdCalculator) {
                var gcd = gcdCalculator.Gcd(precision, a, b);
                var result = a * b / gcd;
                return result;
            }

            private void _swapRows<T>(T[] A, int columns, int row1, int row2) {
                for (int i = 0; i < columns; i++) {
                    var temp = this._getElem(A, columns, row1, i);
                    this._setElem(A, columns, row1, i, this._getElem(A, columns, row2, i));
                    this._setElem(A, columns, row2, i, temp);
                }
            }

            private T _getElem<T>(T[] A, int columns, int row, int column) {
                return A[row * columns + column];
            }

            private void _setElem<T>(T[] A, int columns, int row, int column, T value) {
                A[row * columns + column] = value;
            }

            public decimal CalculateDeterminant(decimal[] matrix, int dimension, IPrecisionProvider precision) {
                //var linAl = this._linal;
                var multipler = this
                    ._makeRowEchelon(matrix, dimension, dimension, precision)
                    .Multiplier;
                decimal product = 1;
                for (int i = 0; i < dimension; i++) {
                    product *= matrix[i * dimension + i];
                }
                if (IntegerTypes.Contains(multipler.GetType())) {
                    var gcd = _calculators.GetGcdCalculator().Gcd(precision, product, multipler);
                    if (!(gcd == multipler || gcd == -multipler))
                        throw new Exception("Inconsistent result.");
                }
                product /= multipler;
                return product;
            }

            public int CalculateRank(decimal[] A, int rows, int columns, IPrecisionProvider precision) {
                return this._makeRowEchelon(A, rows, columns, precision).Rank;
            }
        }
    }

    internal class StraightGcdCalculator : IGcdCalculator {
        public decimal Gcd(IPrecisionProvider precision, params decimal[] numbers) {
            if (numbers == null || numbers.Length < 2)
                throw new ArgumentException("There must be at least 2 given numbers.");
            decimal
                result = 1,
                test = 1,
                testDouble = 2;
            var positiveNumbers = new decimal[numbers.Length];
            var allZeros = true;
            for (int i = 0; i < numbers.Length; i++) {
                if (allZeros && numbers[i] != 0)
                    allZeros = false;
                positiveNumbers[i] = numbers[i] > 0
                    ? numbers[i]
                    : -numbers[i];
            }
            if (allZeros)
                return 1;
            while (true) {
                test++;
                testDouble += 2;
                var ok = true;
                for (int i = 0; i < numbers.Length; i++) {
                    if (precision.IsZero(numbers[i]))
                        continue;
                    if (testDouble > positiveNumbers[i]) {
                        test = positiveNumbers[i];
                        for (int j = 0; j < numbers.Length; j++) {
                            if (decimal.Remainder(numbers[j], test) != 0)
                                return result;
                        }
                        return test;
                    }
                    if (decimal.Remainder(numbers[i], test) != 0) {
                        ok = false;
                        break;
                    }
                }
                if (ok)
                    result = test;
            }
        }
    }

    internal class PairsGcdCalculator : IGcdCalculator {
        public decimal Gcd(IPrecisionProvider precision, params decimal[] numbers) {
            if (numbers == null || numbers.Length < 2)
                throw new ArgumentException("There must be at least 2 given numbers.");
            throw new NotImplementedException();
            //var result = decimal.GreatestCommonDivisor(numbers[0], numbers[1]);
            //for (int i = 2; i < numbers.Length; i++) {
            //    result = decimal.GreatestCommonDivisor(result, numbers[i]);
            //}
            //return result;
        }
    }
}
