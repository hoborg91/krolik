﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using System.IO;
using Krolik.Exceptions;
using Krolik.Infrastructure;

namespace Krolik.Mathematics {
    internal interface ILinearAlgebra {
        /// <summary>
        /// Creates a (<paramref name="totalDimensions"/> - 1)-dimensional hyperplane 
        /// in a <paramref name="totalDimensions"/>-dimensional space. The plane is
        /// perpendicular to the axis "<paramref name="dimension"/>" and intersects this
        /// axis at the point "<paramref name="value"/>".
        /// </summary>
        IPlane MakePlane(int totalDimensions, int dimension, decimal value);

        /// <summary>
        /// Creates an n-dimensional hyperplane.
        /// </summary>
        IPlane MakePlane(IList<decimal[]> points, IPrecisionProvider precision);

        PlaneMatrixResult<T> MakePlaneMatrixOf<T>(IList<T[]> points);
        decimal Distance(decimal[] a, decimal[] b);
    }

    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(CalculatorProvider))]
    internal class LinearAlgebra : ILinearAlgebra {
        private static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        [System.Runtime.Serialization.DataMember]
        private readonly ICalculatorProvider _calculatorProvider;

        private LinearAlgebra(ICalculatorProvider calculatorProvider) {
            this._calculatorProvider = calculatorProvider;
            //W3b.Sine.BigNum x = 100.0, y = 1234567;
            //x = x / y;
            //decimal d = x;
        }

        #region Singleton. Get method.

        private static object _singletonChest = new object();

        private static LinearAlgebra _singleton;

        private static LinearAlgebra _getSingleton() {
            if (_singleton == null) {
                lock (_singletonChest) {
                    if (_singleton == null)
                        // TODO. Unwind this dependency via DI-container.
                        _singleton = new LinearAlgebra(new CalculatorProvider());
                }
            }
            return _singleton;
        }

        public static ILinearAlgebra Get() {
            return _getSingleton();
        }

        #endregion Singleton. Get method.

        public void Test() {
            var m = MathNet.Numerics.LinearAlgebra.CreateMatrix.Dense(1, 1, new decimal[0]);
        }

        /// <summary>
        /// Creates a (<paramref name="totalDimensions"/> - 1)-dimensional hyperplane 
        /// in a <paramref name="totalDimensions"/>-dimensional space. The plane is
        /// perpendicular to the axis "<paramref name="dimension"/>" and intersects this
        /// axis at the point "<paramref name="value"/>".
        /// </summary>
        public IPlane MakePlane(int totalDimensions, int dimension, decimal value) {
            if (dimension >= totalDimensions)
                throw new ArgumentException("'dimension' is a zero-based index and must be less than 'totalDimensions'.");
            if (totalDimensions < 1 || dimension < 0)
                throw new ArgumentException("'dimension' must be >= 0 and 'totalDimensions' must be >= 1.");
            var components = new decimal[totalDimensions + 1];
            components[dimension] = (decimal)1;
            components[totalDimensions] = -value;
            return new Plane(components);
        }

        /// <summary>
        /// Creates an n-dimensional hyperplane.
        /// </summary>
        public IPlane MakePlane(IList<decimal[]> points, IPrecisionProvider precision) {
            #region Check arguments
            if (points == null)
                throw new ArgumentNullException(nameof(points));
            if (precision == null)
                throw new ArgumentNullException(nameof(precision));
            foreach (var point in points) {
                if (point == null)
                    throw new ArgumentException("All points must not be null.");
                if (point.Length != points.Count) {
                    var pointStr = string.Join("; ", point);
                    throw new ArgumentException("Point (" +
                        pointStr.Substring(0, new[] { pointStr.Length, 20, }.Min()) + ") has " +
                        point.Length + " components while the given array contains " +
                        points.Count + " points.");
                }
            }
            #endregion Check arguments

            var dim = points.Count;
            var diffs = new decimal[dim * dim];
            for (int p = 1; p < dim; p++) {
                for (int c = 0; c < dim; c++) {
                    var diff = points[p][c] - points[0][c];
                    diffs.Set(dim, p, c, diff);
                }
            }

            var planeComponents = new decimal[dim + 1];
            var negative = false;
            decimal sum = 0;
            for (int i = 0; i < dim; i++, negative = !negative) {
                var c = _get(diffs, i, dim, precision);
                var d = points[0][i] * c;
                if (negative)
                    c *= (decimal)(-1);
                else {
                    d *= (decimal)(-1);
                }
                planeComponents[i] = c;
                sum += d;
            }

            planeComponents[dim] = sum;
            //if (planeComponents.Any(x => decimal.IsNaN(x))) {
            //    throw new Exception("Inconsistent result.");
            //}
            //if (planeComponents.Length > 1) {
            var planeComponentsForDebug = new decimal[planeComponents.Length];
            planeComponents.CopyTo(planeComponentsForDebug, 0);
            if (new[] { typeof(System.Numerics.BigInteger), }.Contains(planeComponents[0].GetType())) {
                var norm = Gcd(planeComponents, precision);
                if (norm < 0)
                    throw new Exception("Inconsistent result.");
                if (planeComponents[0] < 0)
                    norm *= -1;
                if (norm != 1)
                    for (int i = 0; i < planeComponents.Length; i++) {
                        planeComponents[i] /= norm;
                    }
            } else {
                var norm = planeComponents[0];
                for (int i = 0; i < dim + 1; i++) {
                    if (true
                        && norm == 0
                        && planeComponents[i] != 0
                    //&& !decimal.IsInfinity(planeComponents[i])
                    )
                        norm = planeComponents[i];
                    if (norm == 0)
                        continue;
                    planeComponents[i] /= norm;
                    //if (precision.IsZero(Math.Abs(planeComponents[i])))
                    //    planeComponents[i] = 0;
                }
            }
            
            var result = new Plane(planeComponents);
            result.BasedOnPoints = points;
            
            foreach (var point in points) {
                if (!result.Contains(point, precision)) {
                    _savePlaneCase(
                        points, 
                        point, 
                        planeComponents, 
                        result.DistanceTo(point)
                    );
                    throw new Exception("Inconsistent result. " +
                        "It seems that the system's precision is not enough " +
                        "to deal with the given data.");
                }
            }
            return result;
        }

        public decimal Distance(decimal[] a, decimal[] b) {
            KrolikException.CheckArgumentNotNull(a, nameof(a));
            KrolikException.CheckArgumentNotNull(b, nameof(b));
            if (a.Length == 0 || b.Length == 0 || a.Length != b.Length)
                throw new ArgumentException("The given points have different or zero dimensions.");

            decimal result = 0;
            for (int i = 0; i < a.Length; i++) {
                result += (b[i] - a[i]) * (b[i] - a[i]);
            }
            result = (decimal)Math.Sqrt((double)result);
            return result;
        }

        /// <summary>
        /// Returns the greates common divisor of the given numbers.
        /// </summary>
        private decimal Gcd(decimal[] numbers, IPrecisionProvider precision) {
            return this._calculatorProvider.GetGcdCalculator().Gcd(precision, numbers);
        }

        private void _savePlaneCase(
            IList<decimal[]> points, 
            decimal[] failedPoint,
            decimal[] planeComponents,
            decimal distanceToTheFailedPoint,
            string fileName = "planeCase.txt"
        ) {
            using (var sw = new StreamWriter(fileName)) {
                sw.WriteLine(string.Join(" ", failedPoint));
                foreach (var point in points) {
                    sw.WriteLine(string.Join(" ", point));
                }
                sw.WriteLine("Plane components");
                sw.WriteLine(string.Join(" ", planeComponents));
                sw.WriteLine("Distance to the failed point");
                sw.Write(distanceToTheFailedPoint);
            }
        }

        private decimal _determinant(decimal[,] matrix) {
            var dim = matrix.GetLength(0);
            if (dim == 1)
                return matrix[0, 0];
            decimal result = 0;
            var row = 0;
            for (int i = 0; i < dim; i += 2) {
                var minor = _getMinor(matrix, row, i);
                var subDet = _makeSeqMatrix(minor).Determinant();
                //if(!decimal.IsInfinity(subDet)) {
                //    int debug = 0;
                //    //throw new Exception("Not inf.");
                //} else {
                //    subDet = _determinant(minor);
                //}
                var cur = matrix[row, i];
                var s = cur * subDet;
                result += s;
            }
            for (int i = 1; i < dim; i += 2) {
                var minor = _getMinor(matrix, row, i);
                var subDet = _makeSeqMatrix(minor).Determinant();
                //if (!decimal.IsInfinity(subDet)) {
                //    int debug = 0;
                //    //throw new Exception("Not inf.");
                //} else {
                //    subDet = _determinant(minor);
                //}
                var cur = matrix[row, i];
                var s = cur * subDet;
                result -= s;
            }
            //if (decimal.IsInfinity(result)) {
            //    throw new Exception("Det is inf.");
            //}
            return result;
        }

        private Matrix<decimal> _makeSeqMatrix(decimal[,] matrix) {
            var dim = matrix.GetLength(0);
            var seq = new decimal[dim * dim];
            var index = 0;
            for (int i = 0; i < dim; i++) {
                for (int j = 0; j < dim; j++) {
                    seq[index++] = matrix[i, j];
                }
            }
            var result = CreateMatrix.Dense(dim, dim, seq);
            return result;
        }

        private decimal[,] _getMinor(decimal[,] matrix, int i0, int i1) {
            var dim = matrix.GetLength(0);
            var queue = new Queue<decimal>();
            for (int i = 0; i < dim; i++) {
                if (i == i0)
                    continue;
                for (int j = 0; j < dim; j++) {
                    if (j == i1)
                        continue;
                    queue.Enqueue(matrix[i, j]);
                }
            }
            var result = new decimal[dim - 1, dim - 1];
            for (int i = 0; i < dim - 1; i++) {
                for (int j = 0; j < dim - 1; j++) {
                    result[i, j] = queue.Dequeue();
                }
            }
            return result;
        }

        /// <summary>
        /// Creates a new matrix from the given one excluding the
        /// first top row and the given column. Returns the
        /// determinant of the created matrix (i. e., returns the
        /// (0, index)-minor of the given matrix).
        /// </summary>
        private decimal _get(decimal[] matrix, int index, int dimension, IPrecisionProvider precision) {
            var m = new decimal[(dimension - 1) * (dimension - 1)];
            var mi = 0;
            for (int i = 1; i < dimension; i++) {
                for (int j = 0; j < dimension; j++) {
                    if (j == index)
                        continue;
                    m[mi] = matrix.Get(dimension, i, j);
                    mi++;
                }
            }

            //Logger.Debug("_get: index = " + index + ", dim = " + dimension + ".");
            var d = this._calculatorProvider
                .GetDeterminantCalculator()
                .CalculateDeterminant(m, dimension - 1, precision);
            //Logger.Debug("_get: determinant = " + d + ".");
            //if (decimal.IsInfinity(d)) {
            //    var m_ = new decimal[dimension - 1, dimension - 1];
            //    var idx = 0;
            //    for (int i = 0; i < dimension - 1; i++) {
            //        for (int j = 0; j < dimension - 1; j++) {
            //            m_[i, j] = m[idx];
            //            idx++;
            //        }
            //    }
            //    var d_ = _determinant(m_);
            //    throw new Exception("Inf");
            //}
            return d;
        }

        // TODO. The class is made public for testing. Consider
        // refactoring or InternalsVisibleToAttribute.
        [System.Runtime.Serialization.DataContract]
        public class Plane : IOpenPlane {
            [System.Runtime.Serialization.DataMember]
            private decimal[] _components;
            
            [System.Runtime.Serialization.DataMember]
            [Obsolete("For debug only")]
            public IList<decimal[]> BasedOnPoints { get; set; }

            public Plane(params decimal[] components) {
                KrolikException.CheckArgumentNotNull(components, nameof(components));
                if (components.Length < 2)
                    throw new ArgumentException("The given components must contain at least two elements.");
                if (components[0] < 0)
                    throw new ArgumentException("The given components are not normalized.");
                this._components = components;
            }

            public decimal[] Components {
                get {
                    return this._components;
                }
            }

            // The implementation of this method is not replaced with
            // call to the DistanceTo method because the last one fulfill
            // more calculations than necessary here.
            public bool Contains(decimal[] point, IPrecisionProvider precisionProvider) {
                KrolikException.CheckArgumentNotNull(point, nameof(point));
                KrolikException.CheckArgumentNotNull(precisionProvider, nameof(precisionProvider));
                if (point.Length + 1 != this._components.Length)
                    throw new ArgumentException("The dimension of the given point differs from the dimension of the plane.");

                var sum = this._components[this._components.Length - 1];
                for (int i = 0; i < point.Length; i++) {
                    sum += point[i] * this._components[i];
                }
                return precisionProvider.IsZero(sum);
            }

            public decimal DistanceTo(decimal[] point) {
                decimal numerator = this._components[this._components.Length - 1];
                decimal denominator = 0;
                for (int i = 0; i < this._components.Length - 1; i++) {
                    numerator += point[i] * this._components[i];
                    denominator += this._components[i] * this._components[i];
                }
                var denom = (decimal)Math.Sqrt((double)denominator);
                if (denom == (decimal)0)
                    return decimal.MaxValue;
                var result = Math.Abs(numerator) / denom;
                return result;
            }

            public decimal[] Make(decimal[] point, decimal maxDistance) {
                var maxDistanceOne = maxDistance / (decimal)Math.Sqrt(3);
                var result = new decimal[point.Length];
                for (int i = 0; i < result.Length; i++) {
                    result[i] = point[i] + 
                        (decimal)(Cil.Common.Rand.NextDouble() * 2 - 1) * 
                        maxDistanceOne;
                }
                var sum = this._components[this._components.Length - 1];
                var k = result.Length - 1;
                for (int i = 0; i < result.Length - 1; i++) {
                    if (i == k)
                        continue;
                    sum += result[i] * this._components[i];
                }
                result[k] = -sum / this._components[k];
                return result;
            }

            public decimal[] ProjectionOf(decimal[] point) {
                var r0 = new V(point);
                var n1 = new V(this.Components.Take(this.Components.Length - 1).ToArray());
                var d1 = this.Components[this.Components.Length - 1];
                var num = n1.Times(r0.Times(n1) + d1);
                var den = n1.Times(n1);
                var result = r0.Plus(num.Times(-1 / den));
                return result.Components;
            }

            public decimal[] Intersection(IWithComponents plane) {
                if (plane.Components.Length != 3)
                    throw new NotImplementedException();
                if (this.Components.Length != plane.Components.Length)
                    throw new Exception("Number of components must be equal.");
                if (this.Components[1] == 0) {
                    if (plane.Components[1] == 0)
                        return null;
                    var x0 = -this.Components[2] / this.Components[0];
                    var a0 = plane.Components[0] / plane.Components[1];
                    var b0 = plane.Components[2] / plane.Components[1];
                    var y0 = a0 * x0 + b0;
                    return new[] { x0, y0, };
                }
                if (plane.Components[1] == 0) {
                    var x0 = -plane.Components[2] / plane.Components[0];
                    var a0 = this.Components[0] / this.Components[1];
                    var b0 = this.Components[2] / this.Components[1];
                    var y0 = a0 * x0 + b0;
                    return new[] { x0, y0, };
                }
                var a1 = this.Components[0] / this.Components[1];
                var b1 = this.Components[2] / this.Components[1];
                var a2 = plane.Components[0] / plane.Components[1];
                var b2 = plane.Components[2] / plane.Components[1];
                if (a1 == a2)
                    return null;
                var x = (b2 - b1) / (a1 - a2);
                var y = a1 * x + b1;
                return new[] { x, y, };
            }

            public override int GetHashCode() {
                return this._components.GetHashCode();
            }

            public override bool Equals(object obj) {
                var plane = obj as Plane;
                if ((object)plane == null)
                    return false;
                if (this._components.Length != plane._components.Length)
                    return false;
                //var precision = LinearAlgebra.GetPrecisionProvider().Precision;
                for (int i = 0; i < this._components.Length; i++) {
                    if(this._components[i] != plane._components[i])
                    //if (Math.Abs(this._components[i] - plane._components[i]) > precision)
                        return false;
                }
                return true;
            }

            public static bool operator ==(Plane a, Plane b) {
                if ((object)a == null && (object)b == null)
                    return true;
                if ((object)a == null || (object)b == null)
                    return false;
                return a.Equals(b);
            }

            public static bool operator !=(Plane a, Plane b) {
                return !(a == b);
            }

            public override string ToString() {
                var result = string.Join(" ", this._components
                    .Select(x => x.ToString("f2")));
                if (result.Length > 110)
                    result = result.Substring(0, 100) + "...";
                return result;
            }

            private class V {
                public decimal[] Components;

                public V(decimal[] components) {
                    this.Components = components;
                }

                public decimal Times(V v) {
                    decimal result = 0;
                    for (int i = 0; i < this.Components.Length; i++) {
                        result += this.Components[i] * v.Components[i];
                    }
                    return result;
                }

                public V Times(decimal scalar) {
                    var result = new V(this.Components
                        .Select(x => x * scalar)
                        .ToArray()
                    );
                    return result;
                }

                public V Plus(V v) {
                    var result = new decimal[this.Components.Length];
                    for (int i = 0; i < result.Length; i++) {
                        result[i] = this.Components[i] + v.Components[i];
                    }
                    return new V(result);
                }
            }
        }

        // TODO. The class is made public for testing. Consider
        // refactoring or InternalsVisibleToAttribute.
        [System.Runtime.Serialization.DataContract]
        [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
        public class ConcentratedPlane : IOpenPlane {
            [System.Runtime.Serialization.DataMember]
            private readonly IOpenPlane _truePlane;

            [System.Runtime.Serialization.DataMember]
            private readonly decimal[] _centre;

            [System.Runtime.Serialization.DataMember]
            private readonly IFuzzySet _concentration;

            [System.Runtime.Serialization.DataMember]
            private readonly ILinearAlgebra _linal;

            public ConcentratedPlane(
                IOpenPlane truePlane,
                decimal[] centre,
                IPrecisionProvider precision,
                IFuzzySet concentration,
                ILinearAlgebra linal
            ) {
                KrolikException.CheckArgumentNotNull(truePlane, nameof(truePlane));
                KrolikException.CheckArgumentNotNull(centre, nameof(centre));
                KrolikException.CheckArgumentNotNull(precision, nameof(precision));
                KrolikException.CheckArgumentNotNull(concentration, nameof(concentration));
                KrolikException.CheckArgumentNotNull(linal, nameof(linal));
                if (!truePlane.Contains(centre, precision))
                    throw new ArgumentException("The given plane does not contain the given point.");
                
                this._truePlane = truePlane;
                this._centre = centre;
                this._concentration = concentration;
                this._linal = linal;
            }

            public bool Contains(decimal[] point, IPrecisionProvider precisionProvider) {
                var truePlaneContains = this._truePlane.Contains(point, precisionProvider);
                return true
                    && truePlaneContains
                    && precisionProvider.IsZero(this._concentration.Mu(this._linal.Distance(point, this._centre)) - 1)
                ;
            }

            public decimal DistanceTo(decimal[] point) {
                var distance = this._truePlane.DistanceTo(point);
                var projection = this._truePlane.ProjectionOf(point);
                var mu = (decimal)this._concentration.Mu(this._linal.Distance(projection, this._centre));
                var result = mu.EqualsWithPrecision(0m, 1e-9m)
                    ? decimal.MaxValue
                    : distance / mu;
                return result;
            }

            public decimal[] Make(decimal[] point, decimal maxDistance) {
                return this._truePlane.Make(point, maxDistance);
            }

            public decimal[] ProjectionOf(decimal[] point) {
                return this._truePlane.ProjectionOf(point);
            }

            public override string ToString() {
                return this._truePlane.ToString();
            }

            #region Imlementation of IOpenPlane

            public decimal[] Components => this._truePlane.Components;

            [Obsolete("For debug only")]
            public IList<decimal[]> BasedOnPoints {
                get {
                    return this._truePlane.BasedOnPoints;
                }
            }

            public decimal[] Intersection(IWithComponents plane) {
                return this._truePlane.Intersection(plane);
            }

            #endregion Imlementation of IOpenPlane
        }

        [System.Runtime.Serialization.DataContract]
        [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
        public class MultiConcentratedPlane : IOpenPlane {
            [System.Runtime.Serialization.DataMember]
            private readonly IOpenPlane _truePlane;

            [System.Runtime.Serialization.DataMember]
            private readonly IList<decimal[]> _poles;

            [System.Runtime.Serialization.DataMember]
            private readonly IFuzzySet _concentration;

            [System.Runtime.Serialization.DataMember]
            private readonly ILinearAlgebra _linal;

            public MultiConcentratedPlane(
                IOpenPlane truePlane,
                IList<decimal[]> poles,
                IPrecisionProvider precision,
                IFuzzySet concentration,
                ILinearAlgebra linal
            ) {
                KrolikException.CheckArgumentNotNull(truePlane, nameof(truePlane));
                KrolikException.CheckArgumentNotNull(poles, nameof(poles));
                KrolikException.CheckArgumentNotNull(precision, nameof(precision));
                KrolikException.CheckArgumentNotNull(concentration, nameof(concentration));
                KrolikException.CheckArgumentNotNull(linal, nameof(linal));
                foreach (var pole in poles) {
                    if (!truePlane.Contains(pole, precision))
                        throw new ArgumentException("The given plane does not contain the given point.");
                }

                this._truePlane = truePlane;
                this._poles = poles;
                this._concentration = concentration;
                this._linal = linal;
            }

            public bool Contains(decimal[] point, IPrecisionProvider precisionProvider) {
                var truePlaneContains = this._truePlane.Contains(point, precisionProvider);
                return true
                    && truePlaneContains
                    && this._poles.Any(pole =>
                        precisionProvider.IsZero(this._concentration.Mu(this._linal.Distance(point, pole)) - 1))
                ;
            }

            public decimal DistanceTo(decimal[] point) {
                var distance = this._truePlane.DistanceTo(point);
                var projection = this._truePlane.ProjectionOf(point);
                var minPlaneDist = this._poles
                    .Select(pole => this._linal.Distance(projection, pole))
                    .Min();
                var mu = (decimal)this._concentration.Mu(minPlaneDist);
                var result = mu.EqualsWithPrecision(0m, 1e-9m)
                    ? decimal.MaxValue
                    : distance / mu;
                return result;
            }

            public decimal[] Make(decimal[] point, decimal maxDistance) {
                return this._truePlane.Make(point, maxDistance);
            }

            public decimal[] ProjectionOf(decimal[] point) {
                return this._truePlane.ProjectionOf(point);
            }

            public override string ToString() {
                return this._truePlane.ToString();
            }

            #region Imlementation of IOpenPlane

            public decimal[] Components => this._truePlane.Components;

            [Obsolete("For debug only")]
            public IList<decimal[]> BasedOnPoints {
                get {
                    return this._truePlane.BasedOnPoints;
                }
            }

            public decimal[] Intersection(IWithComponents plane) {
                return this._truePlane.Intersection(plane);
            }

            #endregion Imlementation of IOpenPlane
        }

        public PlaneMatrixResult<T> MakePlaneMatrixOf<T>(IList<T[]> points) {
            if (points == null || points.Count == 0)
                return new PlaneMatrixResult<T>();
            var columns = points[0].Length;
            var matrix = new T[points.Count * columns];
            for (int i = 0; i < points.Count; i++) {
                for (int j = 0; j < points[i].Length; j++) {
                    matrix.Set(columns, i, j, points[i][j]);
                }
            }
            return new PlaneMatrixResult<T>(matrix, points.Count, points[0].Length);
        }
    }
}

