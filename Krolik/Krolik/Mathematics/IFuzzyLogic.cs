﻿using System;
using System.Linq;

namespace Krolik.Mathematics {
    internal interface IFuzzyLogic {
        Certainty Not(Certainty value);
        Certainty Or(params Certainty[] values);
        Certainty And(params Certainty[] values);
    }

    internal static class Fuzzy {
        private static IFuzzyLogic _fuzzyLogic;

        public static IFuzzyLogic Logic {
            get {
                if (_fuzzyLogic == null) {
                    _fuzzyLogic = new FuzzyLogic();
                }
                return _fuzzyLogic;
            }
        }

        private class FuzzyLogic : IFuzzyLogic {
            public Certainty Not(Certainty value) {
                if (value < 0 || value > 1)
                    throw new ArgumentException($"The {nameof(value)} must be in [0; 1].");
                return Certainty.Absolute - value;
            }

            public Certainty Or(params Certainty[] values) {
                if (values == null)
                    throw new ArgumentNullException(nameof(values));
                if (values.Length == 0)
                    return Certainty.None;
                return values.Max();
            }

            public Certainty And(params Certainty[] values) {
                if (values == null)
                    throw new ArgumentNullException(nameof(values));
                if (values.Length == 0)
                    return Certainty.Absolute;
                return values.Min();
            }
        }
    }
}
