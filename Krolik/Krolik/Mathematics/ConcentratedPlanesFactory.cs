﻿using Krolik.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.Mathematics {
    [System.Runtime.Serialization.DataContract]
    [System.Runtime.Serialization.KnownType(typeof(LinearAlgebra))]
    internal class ConcentratedPlanesFactory {
        [System.Runtime.Serialization.DataMember]
        private ILinearAlgebra _linal;
        
        public ConcentratedPlanesFactory(ILinearAlgebra linal) {
            KrolikException.CheckArgumentNotNull(linal, nameof(linal));
            this._linal = linal;
        }

        internal List<LinearAlgebra.ConcentratedPlane> Make(IList<decimal[]> points, IPrecisionProvider precision) {
            var truePlane = this._linal.MakePlane(points, precision) as IOpenPlane;
            decimal maxDistance = 0;
            foreach (var a in points) {
                foreach (var b in points) {
                    var distance = this._linal.Distance(a, b);
                    if (distance > maxDistance)
                        maxDistance = distance;
                }
            }
            var result = points
                .Select(p => new LinearAlgebra.ConcentratedPlane(
                    truePlane,
                    p,
                    precision,
                    new LeftTriangleFuzzySet(maxDistance),
                    this._linal
                ))
                .ToList();
            return result;
        }
    }
}
