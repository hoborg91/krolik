﻿using System;
using System.Collections.Generic;

namespace Krolik.Mathematics {
    internal struct PlaneMatrixResult<T> {
        public T[] Matrix;
        public int Rows;
        public int Columns;

        public PlaneMatrixResult(T[] matrix, int rows, int columns) : this() {
            this.Matrix = matrix;
            this.Rows = rows;
            this.Columns = columns;
        }
    }

    internal static class MatrixExtensions {
        public static List<string> GetRowsStrings<T>(this T[,] matrix) {
            var result = new List<string>();
            if (matrix == null)
                return result;
            for (int i = 0; i < matrix.GetLength(0); i++) {
                var row = new System.Text.StringBuilder();
                for (int j = 0; j < matrix.GetLength(1); j++) {
                    row.Append(matrix[i, j].ToString() + " ");
                }
                result.Add(row.ToString());
            }
            return result;
        }

        public static void MakeZeros(this decimal[,] matrix, decimal trashold) {
            throw new NotImplementedException("Do not call MakeZeros for BigInteger.");
        }

        public static void MakeZeros(this decimal[] matrix, int columns, int belowRow, decimal trashold) {
            throw new NotImplementedException("Do not call MakeZeros for BigInteger.");
        }

        public static T Get<T>(this T[] A, int columns, int row, int column) {
            return A[row * columns + column];
        }

        public static void Set<T>(this T[] A, int columns, int row, int column, T value) {
            A[row * columns + column] = value;
        }

    }
}
