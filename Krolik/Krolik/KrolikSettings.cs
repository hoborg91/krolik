﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Krolik {
    // TODO. Make internal?
    public class KrolikSettings {
        public bool EnableClassificationLogging { get; set; }
        
        /// <summary>
        /// Log preparation of the classifier.
        /// </summary>
        public bool EnableDetailedLogging {
            get {
                return this.Log != null;
            }
        }
        /// <summary>
        /// Is null iff EnableDetailedLogging is false.
        /// </summary>
        public Action<string> Log { get; set; }

        private static KrolikSettings _current;
        public static KrolikSettings Current {
            get {
                if (_current == null)
                    _current = new KrolikSettings();
                return _current;
            }
        }

        /// <summary>
        /// Log vectors read from the providers while classifier
        /// preparation and vectors classification.
        /// </summary>
        public bool LogVectors = false;

        /// <summary>
        /// Try use the soft classification methods.
        /// </summary>
        public bool Soft = false;

        private Dictionary<string, Stopwatch> timers =
            new Dictionary<string, Stopwatch>();

        public void StartTimer(string id) {
            timers[id] = new Stopwatch();
            timers[id].Start();
        }

        public long FinishTimer(string id) {
            if (!timers.ContainsKey(id))
                throw new Exception("The timer '" + id + "' is not set.");
            timers[id].Stop();
            return timers[id].ElapsedMilliseconds;
        }

        /// <summary>
        /// Log memory, CPU and drives usage.
        /// </summary>
        public bool LogPerformance = true;
    }

    /// <summary>
    /// Simple monitor for memory, CPU and drives usage.
    /// </summary>
    internal class Performance {
        PerformanceCounter cpuCounter;
        PerformanceCounter ramCounter;

        /// <summary>
        /// Simple monitor for memory, CPU and drives usage.
        /// </summary>
        public Performance() {
            cpuCounter = new PerformanceCounter {
                CategoryName = "Processor",
                CounterName = "% Processor Time",
                InstanceName = "_Total",
            };

            ramCounter = new PerformanceCounter {
                CategoryName = "Memory",
                CounterName = "Available MBytes",
                InstanceName = "_Total",
            };
        }

        public string GetCurrentCpuUsage() {
            try {
                return cpuCounter.NextValue() + "%";
            } catch(InvalidOperationException) {
                return "";
            }
        }

        public string GetAvailableRAM() {
            try {
                return ramCounter.NextValue() + "MB";
            } catch(InvalidOperationException) {
                return "";
            }
        }

        private long bytesInMb = 1024 * 1024;

        public string GetDrivesUsage() {
            var result = new StringBuilder();
            var drives = DriveInfo.GetDrives();
            foreach (var drive in drives) {
                result.Append(" " + drive.Name + " ");
                if (drive.IsReady)
                    result.Append(
                        Math.Floor(1.0 * drive.AvailableFreeSpace / bytesInMb).ToString("f3")
                        + "MB free"
                    );
                else
                    result.Append("not ready");
            }
            return result.ToString();
        }

        /// <summary>
        /// Returns a string of type "CPU-usage-in-% 
        /// available-RAM-in-MB 
        /// list-of-drives-with-free-space-in-MB".
        /// </summary>
        public string Brief() {
            return this.GetCurrentCpuUsage()
                + " "
                + this.GetAvailableRAM()
                + " "
                + this.GetDrivesUsage();
        }
    }
}
