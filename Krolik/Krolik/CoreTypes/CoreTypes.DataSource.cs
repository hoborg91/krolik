﻿using Krolik.Classifiers;
using Krolik.Exceptions;
using Krolik.Infrastructure;
using Krolik.TypeSystem;
using Krolik.VectorsProviders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.CoreTypes {
    internal partial class CoreTypes {
		private static KrolikGpnProjectOptions ReadDataSourceOptions(IPreparedContext ctx) {
            string modeRead = (ctx.Arguments["mode"] as MyObject<string>).Content;
            var loadModes = KrolikGpnProjectOptions.GetLoadModes();
            var allLoadModesAbbriviations = loadModes
                .SelectMany(x => x.Value)
                .ToList();
            if (!allLoadModesAbbriviations.Contains(modeRead)) {
                throw new TypesMismatchException("Unknown read mode for the data source.");
            }
            string path = (ctx.Arguments["fileName"] as MyObject<string>).Content;
            int? cci = ctx.Arguments.ContainsKey("classColumnIndex")
                ? (int?)(ctx.Arguments["classColumnIndex"] as MyObject<decimal>).Content
                : null;
            var mode = loadModes
                .Single(x => x.Value.Contains(modeRead))
                .Key;
            int? vectorLength = ctx.Arguments.ContainsKey("vectorLength")
                ? (int?)(ctx.Arguments["vectorLength"] as MyObject<decimal>).Content
                : null;
            string valuesSeparator = ctx.Arguments.ContainsKey("valuesSeparator")
                ? (ctx.Arguments["valuesSeparator"] as MyObject<string>).Content
                : " ";
            int 
                discreteComponentsCount = ctx.Arguments.ContainsKey("discreteComponentsCount")
                    ? (int)(ctx.Arguments["discreteComponentsCount"] as MyObject<decimal>).Content
                    : 0, 
                continuousComponentsCount = ctx.Arguments.ContainsKey("continuousComponentsCount")
                    ? (int)(ctx.Arguments["continuousComponentsCount"] as MyObject<decimal>).Content
                    : 0;
            decimal? 
                precision = ctx.Arguments.ContainsKey("precision")
                    ? (decimal?)(ctx.Arguments["precision"] as MyObject<decimal>).Content
                    : null, 
                divisor = ctx.Arguments.ContainsKey("divisor")
                    ? (decimal?)(ctx.Arguments["divisor"] as MyObject<decimal>).Content
                    : null;
            var signature = new VectorSignature(
                Enumerable.Range(0, discreteComponentsCount)
                    .Select(i => ComponentType.Discrete)
                    .Concat(
                Enumerable.Range(0, continuousComponentsCount)
                    .Select(i => ComponentType.ContinuumLike())
                ).ToArray()
            );
            var options = new KrolikGpnProjectOptions {
                Mode = mode,
                ClassColumnIndex = cci ?? -1,
                Path = SlashMaster.CorrectPath(path),
                VectorLength = vectorLength ?? -1,
                ValuesSeparator = valuesSeparator,
                Signature = signature,
                Precision = precision.Value,
                Divisor = divisor.Value,
            };
            return options;
        }

        private bool _dataSourceInited = false;
        private readonly IType _dataSource = new MyType("DataSource");
        public IType DataSource {
            get {
                if (!_dataSourceInited) {
                    _dataSource.Constructor = new MyMethod("ctor", new[] {
                        new Arg(
                            "mode", 
                            null, 
                            "Allowed values: " + string.Join(
                                ", ", 
                                KrolikGpnProjectOptions.GetLoadModes().SelectMany(lm => lm.Value).Select(x => $"'{x}'")) +
                            "."),
                        new Arg("fileName", null),
                        new Arg("classColumnIndex", null),
                        new Arg("vectorLength", null),
                        new Arg("valuesSeparator", null),
                        new Arg("discreteComponentsCount", null),
                        new Arg("continuousComponentsCount", null),
                        new Arg("precision", null),
                        new Arg("divisor", null),
                    }, _dataSource, (ctx) => {
                        var result = new MyObject<IVectorsProvider>(_dataSource);
                        var options = ReadDataSourceOptions(ctx);
                        var providers = new Dictionary<KrolikGpnProjectOptions.LoadMode, Func<KrolikGpnProjectOptions, IVectorsProvider>> {
                            { KrolikGpnProjectOptions.LoadMode.Simple, opts => new SimpleVectorsProvider(
                                opts.Path, 
                                opts.Signature,
                                new Mathematics.CustomPrecisionProvider(opts.Precision, opts.Divisor),
                                null, 
                                new[] { opts.ValuesSeparator, }, 
                                opts.ClassColumnIndex) },
                            { KrolikGpnProjectOptions.LoadMode.MultipleFiles, opts => new MultipleFilesVectorsProvider(
                                opts.Signature, 
                                opts.Path,
                                new Mathematics.CustomPrecisionProvider(opts.Precision, opts.Divisor),
                                new Cil.Design.FileSystemApi()) },
                        };
                        if (!providers.ContainsKey(options.Mode))
                            throw new NotImplementedException();
                        var provider = providers[options.Mode](options);
                        result.Content = provider;
                        return result;
                    });

                    _dataSource["GetSample"] = new MyMethod("GetSample", new[] {
                        //new Arg("ds", _dataSource),
                        new Arg("part", null),
                    }, _dataSource, (ctx) => {
                        var dataSourceArg = ctx.This as MyObject<IVectorsProvider>;
                        if (dataSourceArg == null)
                            throw new TypesMismatchException();
                        if (dataSourceArg.Content == null)
                            throw new Exception("The value of the data source is null.");
                        if (!ctx.Arguments.ContainsKey("part"))
                            throw new TypesMismatchException("The parameter 'part' in method DataSource.GetSample is mandatory.");
                        float
                            part = (float)(ctx.Arguments["part"] as MyObject<decimal>).Content;
                        // TODO. Consider refactoring to remove this cast. Search for similar notes.
                        var splitted = (IVectorsProvider)(dataSourceArg.Content as ISamplable<IHybridVector>).GetProbabilitySample(part);
                        var result = new MyObject<IVectorsProvider>(_dataSource, splitted);
                        return result;
                    });

                    _dataSource["Take"] = new MyMethod("Take", new[] {
                        new Arg("part", null),
                        new Arg("startAt", null),
                    }, _dataSource, (ctx) => {
                        var dataSourceArg = ctx.This as MyObject<IVectorsProvider>;
                        if (dataSourceArg == null)
                            throw new TypesMismatchException();
                        if (dataSourceArg.Content == null)
                            throw new Exception("The value of the data source is null.");
                        float
							part = (float)(ctx.Arguments["part"] as MyObject<decimal>).Content, 
							startAt = (float)(ctx.Arguments["startAt"] as MyObject<decimal>).Content;
                        // TODO. Consider refactoring to remove this cast. Search for similar notes.
                        var splitted = (IVectorsProvider)dataSourceArg.Content.Split(part, startAt).Item1;
                        var result = new MyObject<IVectorsProvider>(_dataSource, splitted);
                        return result;
                    });

                    _dataSource["Print"] = new MyMethod("Print", new[] { new Arg("ds", _dataSource), }, _dataSource, (ctx) => {
                        var dataSourceArg = ctx.This as MyObject<IVectorsProvider>;
                        if (dataSourceArg == null)
                            throw new TypesMismatchException();
                        var ds = dataSourceArg.Content;
                        if (ds == null)
                            throw new Exception("The value of the data source is null.");
                        ds.Reset();
                        var vector = ds.Next();
                        var index = vector.Index;
                        do {
                            ctx.CmdLine.Write(vector.ToString() + Environment.NewLine);
                            vector = ds.Next();
                        } while (vector.Index != index);
                        return dataSourceArg;
                    });

                    _dataSourceInited = true;
                }
                return _dataSource;
            }
        }
    }
}
