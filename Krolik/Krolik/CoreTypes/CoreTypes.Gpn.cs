﻿using System;
using System.Linq;
using Krolik.Classifiers;
using Krolik.Infrastructure;
using Krolik.VectorsProviders;
using System.Collections.Generic;
using System.IO;
using Krolik.Classifiers.GpnClassifier;
using Krolik.Exceptions;
using Krolik.TypeSystem;
using Cil.Common;

namespace Krolik.CoreTypes {
    internal static class ClassificationResultsHolder {
        public static Dictionary<IHybridVector, List<Cls>> ClassesByVectors =
            new Dictionary<IHybridVector, List<Cls>>();
    }

    internal partial class CoreTypes {
        /// <summary>
        /// This method formats the classification status as a string.
        /// </summary>
        private static string ClassificationLog(
            int total,
            int classified,
            int unknown,
            int wrong,
            bool brief = false
        ) {
            var quality = (decimal)classified / total;
            if(brief) {
                return ""
                    + classified.ToString()
                    + " / "
                    + total.ToString()
                    + " (u = " + unknown.ToString() + ")"
                ;
            }
            return ""
                + "Classified: " + classified.ToString() + "." + Environment.NewLine
                + "Total: " + total.ToString() + "." + Environment.NewLine
                + "Unknown: " + unknown.ToString() + "." + Environment.NewLine
                + "Wrong: " + wrong.ToString() + "." + Environment.NewLine
                + "Quality: " + quality.ToString("f3") + "." + Environment.NewLine
            ;
        }
        
        private bool _gpnInited = false;
        private readonly IType _gpn = new MyType("Gpn");
        public IType Gpn {
            get {
                if (!_gpnInited) {
                    _gpn.Constructor = new MyMethod("ctor", new[] {
                        new Arg("founderType", null, "Cartesian or sequential (s)."),
                        new Arg("fuzzyPrecision", null, "Parameter of the fuzzy set."),
                        new Arg("fuzzyType", null, "Left triangle or left inverse (simpleInverse) fuzzy set."),
                        new Arg("clsActivation", null, "Nodes activation strategy at classification: standard or fuzzy \"AND\" (fuzzyAnd)."),
                    }, _gpn, (ctx) => {
                        string founderType = (ctx.Arguments["founderType"] as MyObject<string>).Content;
                        decimal fuzzyPrecision = ctx.Arguments.ContainsKey("fuzzyPrecision")
                            ? (ctx.Arguments["fuzzyPrecision"] as MyObject<decimal>).Content
                            : 0;
                        var fuzzyType = ctx.Arguments.ContainsKey("fuzzyType")
                            ? (ctx.Arguments["fuzzyType"] as MyObject<string>).Content
                            : null;
                        var classificationActivationStrategy = ctx.Arguments.ContainsKey("clsActivation")
                            ? (ctx.Arguments["clsActivation"] as MyObject<string>).Content
                            : null;
                        
                        var fuzzySet = this._fuzzySetFactory.Make(fuzzyType, fuzzyPrecision);
                        var gpn = this._gpnClassifierFactory.Make(founderType, classificationActivationStrategy, fuzzySet);
                        
                        var result = new MyObject<GpnClassifier>(_gpn, gpn);
                        return result;
                    });

                    _gpn["Prepare"] = new MyMethod("Prepare", new[] {
                        new Arg("gpn", _gpn),
                        new Arg("ds", _dataSource),
                        new Arg("mode", null),
                        new Arg("fileName", null),
                        new Arg("savePeriod", null),
                        new Arg("reuseExisting", null),
                    }, _gpn, (ctx) => {
                        var gpnArg = ctx.This as MyObject<GpnClassifier>;
                        var dsArg = ctx.Arguments["ds"] as MyObject<IVectorsProvider>;
                        if (dsArg == null || dsArg.Content == null)
                            throw new TypesMismatchException();
                        long index = 0;
                        var ds = dsArg.Content as IVectorsProvider;
                        string mode = (ctx.Arguments["mode"] as MyObject<string>).Content;
                        string fileName = ctx.Arguments.ContainsKey("fileName")
                            ? (ctx.Arguments["fileName"] as MyObject<string>).Content
                            : null;
                        int savePeriod = mode == "e" && ctx.Arguments.ContainsKey("savePeriod")
                            ? (int)(ctx.Arguments["savePeriod"] as MyObject<decimal>).Content
                            : -1;
                        bool reuseExisting = mode == "e" && ctx.Arguments.ContainsKey("reuseExisting")
                            ? (bool)(ctx.Arguments["reuseExisting"] as MyObject<bool>).Content
                            : false;
                        float savePeriodIncrease = (float)0.97;
                        var performance = KrolikSettings.Current.LogPerformance
                            ? new Performance()
                            : null;
                        Action save = () => {
                            ctx.This.Call(
                                        "Save",
                                        ctx.CmdLine,
                                        null,
                                        new Dictionary<string, IObject> { ["fileName"] = new MyObject<string>(null, fileName), });
                        };
                        if(reuseExisting && File.Exists(fileName)) {
                            try {
                                using (var fs = new FileStream(fileName, FileMode.Open)) {
                                    gpnArg.Content = this._gpnClassifiersSerializer.DeserializeFromStream(fs);
                                }
                            } catch(Exception ex) {
                                ctx.CmdLine.Write($"Error occured while trying to open GPN at path \"{fileName}\".");
                                ctx.CmdLine.Write(ex.Message);
                                throw;
                            }
                        }
                        while (gpnArg.Content.Prepare(ds)) {
                            Dbg.Enter("(iteration " + index + ")");
                            if(savePeriod > 0) {
                                if(index > 0 && index % savePeriod == 0) {
                                    Dbg.Enter("(save)");
                                    save();
                                    if(savePeriodIncrease > 0) {
                                        savePeriod = (int)Math.Ceiling(savePeriod * savePeriodIncrease);
                                    }
                                    Dbg.Leave("(save)");
                                }
                            }
                            if(performance != null
                                && index % 100 == 0
                            ) {
                                Dbg.Enter("(performance)");
                                ctx.CmdLine.Write(
                                    gpnArg.Content.GetProgress()
                                    + Environment.NewLine
                                );
                                ctx.CmdLine.Write(
                                    performance.Brief()
                                    + Environment.NewLine
                                );
                                Dbg.Leave("(performance)");
                            }
                            if (index % 1000 == 0)
                                ctx.CmdLine.Write("Iteration #" 
                                    + index.ToString() + "." 
                                    + Environment.NewLine
                                );
                            Dbg.Leave("(iteration " + index + ")");
                            index++;
                        }
                        if (savePeriod > 0) {
                            Dbg.Enter("(save)");
                            save();
                            Dbg.Leave("(save)");
                        }
                        ctx.CmdLine.Write("Iteration #" 
                            + (index - 1).ToString() 
                            + ". Ready." 
                            + Environment.NewLine
                        );
                        return gpnArg;
                    });

                    _gpn["Classify"] = new MyMethod("Classify", new[] {
                        new Arg("ds", _dataSource),
                        new Arg("fuzzy", null),
                        new Arg("clsSettings", null),
                    }, _gpn, (ctx) => {
                        var gpnArg = ctx.This as MyObject<GpnClassifier>;
                        var dsArg = ctx.Arguments.ContainsKey("ds") ? ctx.Arguments["ds"] as MyObject<IVectorsProvider> : null;
                        if (gpnArg == null)
                            throw new TypesMismatchException("");
                        if (dsArg == null || dsArg.Content == null) {
                            throw new Exception("This functionality is not been supported yet.");
                        } else {
                            var ds = dsArg.Content as IVectorsProvider;
                            ds.Reset();
                            var vector = ds.Next();
                            var start = vector;
                            int classified = 0, total = 0, unknown = 0, wrong = 0;
                            int logPerdiod = 1000;
                            float logPeriodCoeff = (float)0.98;
                            var performance = KrolikSettings.Current.LogPerformance
                                ? new Performance()
                                : null;
                            //var fuzzyInt = ClassificationIterpreters.Get.AbsoluteTrashold((decimal)0.5);
                            var clsSettingsPath = ctx.Arguments.ContainsKey("clsSettings") && !(ctx.Arguments["clsSettings"] as MyObject<string>).Content.IsNullOrWhiteSpace()
                                ? (ctx.Arguments["clsSettings"] as MyObject<string>).Content
                                : null;
                            var clsSettings = this._classificationSettingsFactory.Make(clsSettingsPath);
                            var fuzzyInt = clsSettings.Interpreter;// ClassificationIterpreters.Get.Relative(2m);
                            var fuzzyAlgorithm = clsSettings.Algorithm;
                            var classify = ctx.Arguments.ContainsKey("fuzzy") && (ctx.Arguments["fuzzy"] as MyObject<bool>).Content
                                ? (Func<IHybridVector, Mathematics.IPrecisionProvider, IFuzzyClassificationResult>)((v, precision) => fuzzyInt.Filter(gpnArg.Content.ClassifyFuzzy(v, ds.PrecisionProvider, fuzzyAlgorithm)))
                                : (Func<IHybridVector, Mathematics.IPrecisionProvider, IFuzzyClassificationResult>)((v, precision) => gpnArg.Content.Classify(v, ds.PrecisionProvider))
                            ;
                            var classificationResults = new Dictionary<Cls, ClassificationResult>();
                            do {
                                var trueCls = vector.Classes.Single();
                                if (!classificationResults.ContainsKey(trueCls)) {
                                    classificationResults[trueCls] = new ClassificationResult();
                                }
                                classificationResults[trueCls].Total++;

                                total++;
                                var clsResult = classify(vector, ds.PrecisionProvider);
                                var clss = clsResult.Classes.Where(x => x.Value.FinalCertainty > 0).Select(x => x.Key).ToList();
                                ClassificationResultsHolder.ClassesByVectors[vector] = clss;
                                if (clss.Count == vector.Classes.Count()
                                    && vector.Classes.All(x1 => clss.Contains(x1))
                                    && clss.All(x1 => vector.Classes.Contains(x1))
                                ) {
                                    classified++;
                                    classificationResults[trueCls].Right++;
                                } else {
                                    if (clss.Any())
                                        wrong++;
                                    else
                                        unknown++;
                                    if (KrolikSettings.Current.EnableClassificationLogging) {
                                        var str = vector.ToString();
                                        ctx.CmdLine.Write(
                                            str.Substring(0, str.Length > 30 ? 30 : str.Length)
                                            + "... => "
                                            + (clss.Any()
                                                ? string.Join(", ", clss) + " (wrong)"
                                                : "unknown"
                                            )
                                            + Environment.NewLine
                                        );
                                    }
                                }
                                if (total % logPerdiod == 0) {
                                    var currentQuality = (decimal)classified / total;
                                    ctx.CmdLine.Write(ClassificationLog(total, classified, unknown, wrong, true)
                                        + Environment.NewLine);
                                    ctx.CmdLine.Write(performance.Brief() + Environment.NewLine);
                                    if(logPeriodCoeff > 0)
                                        logPerdiod = (int)Math.Ceiling(logPerdiod * logPeriodCoeff);
                                }
                                vector = ds.Next();
                            } while (!start.Equals(vector));
                            var quality = (decimal)classified / total;
                            ctx.CmdLine.Write(ClassificationLog(
                                total,
                                classified,
                                unknown,
                                wrong
                            ));
                            KrolikLoggers.Get.Classification.Debug("Classification complete." + Environment.NewLine +
                                string.Join(Environment.NewLine, classificationResults.Select(x => x.Key + ": " + x.Value.Right + " of " + x.Value.Total + "")));
                        }

                        return gpnArg;
                    });

                    _gpn["Save"] = new MyMethod("Save", new[] {
                        new Arg("fileName", null),
                    }, _gpn, (ctx) => {
                        Dbg.Enter("SaveMethod");
                        var gpnArg = ctx.This as MyObject<GpnClassifier>;
                        if (gpnArg == null)
                            throw new TypesMismatchException();
                        string fileName = (ctx.Arguments["fileName"] as MyObject<string>).Content;
                        fileName = SlashMaster.CorrectPath(fileName);

                        var attemptsToSerialize = 3;
                        try {
                            if (File.Exists(fileName)) {
                                var backupFileName = fileName + "-backup";
                                if (File.Exists(backupFileName))
                                    File.Delete(backupFileName);
                                File.Copy(fileName, backupFileName);
                            }
                        } catch(Exception ex) {
                            throw;
                        }
                        for (int i = 0; i < attemptsToSerialize; i++) {
                            try {
                                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create)) {
                                    KrolikLoggers.Get.Common.Debug("SerializeToStream start");
                                    this._gpnClassifiersSerializer.SerializeToStream(fs, gpnArg.Content);
                                    KrolikLoggers.Get.Common.Debug("SerializeToStream finish");
                                }
                                // Load the saved file to check it.
                                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open)) {
                                    KrolikLoggers.Get.Common.Debug("DeserializeFromStream start");
                                    gpnArg.Content = this._gpnClassifiersSerializer.DeserializeFromStream(fs);
                                    KrolikLoggers.Get.Common.Debug("DeserializeFromStream finish");
                                }
                                break;
                            } catch(IOException) {
                                if (i + 1 == attemptsToSerialize)
                                    throw;
                            }
                        }
                        
                        Dbg.Leave("SaveMethod");
                        return gpnArg;
                    });

                    _gpn["Load"] = new MyMethod("Load", new[] {
                        new Arg("fileName", null),
                    }, _gpn, (ctx) => {
                        var gpnArg = ctx.This as MyObject<GpnClassifier>;
                        if (gpnArg == null)
                            throw new TypesMismatchException();
                        string fileName = (ctx.Arguments["fileName"] as MyObject<string>).Content;
                        fileName = SlashMaster.CorrectPath(fileName);
                        using (var fs = System.IO.File.OpenRead(fileName)) {
                            gpnArg.Content = this._gpnClassifiersSerializer.DeserializeFromStream(fs);
                        }
                        return gpnArg;
                    });

                    _gpnInited = true;
                }
                return _gpn;
            }
        }

        private class ClassificationResult {
            public int Total { get; set; }
            public int Right { get; set; }
        }
    }
}
