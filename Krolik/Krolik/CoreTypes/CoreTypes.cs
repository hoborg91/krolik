﻿using Krolik.Classifiers;
using Krolik.Classifiers.GpnClassifier;
using Krolik.Exceptions;
using Krolik.Main;
using Krolik.Mathematics;
using Krolik.TypeSystem;
using System;
using System.Collections.Generic;

namespace Krolik.CoreTypes {
    internal partial class CoreTypes {
        public static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly IGpnClassifierFactory _gpnClassifierFactory;
        private readonly IFuzzySetFactory _fuzzySetFactory;
        private readonly ClassificationSettingsFactory _classificationSettingsFactory;
        private readonly IGpnClassifiersSerializer _gpnClassifiersSerializer;

        public CoreTypes(
            IGpnClassifierFactory gpnClassifierFactory,
            IFuzzySetFactory fuzzySetFactory,
            ClassificationSettingsFactory classificationSettingsFactory,
            IGpnClassifiersSerializer gpnClassifiersSerializer
        ) {
            KrolikException.CheckArgumentNotNull(gpnClassifierFactory, nameof(gpnClassifierFactory));
            KrolikException.CheckArgumentNotNull(fuzzySetFactory, nameof(fuzzySetFactory));
            KrolikException.CheckArgumentNotNull(classificationSettingsFactory, nameof(classificationSettingsFactory));
            KrolikException.CheckArgumentNotNull(gpnClassifiersSerializer, nameof(gpnClassifiersSerializer));

            this._gpnClassifierFactory = gpnClassifierFactory;
            this._fuzzySetFactory = fuzzySetFactory;
            this._classificationSettingsFactory = classificationSettingsFactory;
            this._gpnClassifiersSerializer = gpnClassifiersSerializer;
        }

        private readonly Dictionary<Type, IType> _anies = new Dictionary<Type, IType>();
        private IType Any<T>()
            where T : IDisposable
        {
            var t = typeof(T);
            if (!_anies.ContainsKey(t)) {
                var any = new MyType("Any<" + t.Name + ">");
                var ctor = new MyMethod("ctor", null, any, (args) => {
                    return new MyObject<T>(any);
                });
                any.Constructor = ctor;
                _anies[t] = any;
            }
            return _anies[t];
        }

        // TODO. Remove this method?
        public IObject MakeAny<T>(T content) 
            where T : IDisposable
        {
            var any = Any<T>();
            var result = new MyObject<T>(any);
            result.Content = content;
            return result;
        }
    }
}
