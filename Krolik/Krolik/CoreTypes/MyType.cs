﻿using Krolik.TypeSystem;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.CoreTypes {
    internal class MyType : IType {
        private Dictionary<string, IMethod> _methods = new Dictionary<string, IMethod>();

        public MyType(string name) {
            this.Name = name;
        }

        public IMethod this[string methodName] {
            get {
                return this._methods.ContainsKey(methodName)
                    ? this._methods[methodName]
                    : null;
            }

            set {
                this._methods[methodName] = value;
            }
        }

        public IMethod Constructor { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public IEnumerable<IMethod> GetMethodsReference() {
            var result = new List<IMethod>();
            result.Add(this.Constructor);
            result.AddRange(this._methods.Values
                .OrderBy(x => x.Name));
            return result;
        }
    }
}
