﻿using Krolik.Classifiers;
using Krolik.Exceptions;
using Krolik.Mathematics;
using System.Collections.Generic;
using System.Linq;

namespace Krolik {
    [System.Runtime.Serialization.DataContract]
    internal struct Idx {
        [System.Runtime.Serialization.DataMember]
        public long Value { get; set; }

        public static explicit operator Idx(long value) {
            return new Idx { Value = value, };
        }

        public static explicit operator long(Idx idx) {
            return idx.Value;
        }

        public override int GetHashCode() {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (!(obj is Idx))
                return false;
            var idx = (Idx)obj;
            return this.Value == idx.Value;
        }

        public static bool operator ==(Idx a, Idx b) {
            return a.Equals(b);
        }

        public static bool operator !=(Idx a, Idx b) {
            return !(a == b);
        }

        public override string ToString() {
            return this.Value.ToString();
        }
    }

    [System.Runtime.Serialization.DataContract]
    internal struct DiscreteValue {
        [System.Runtime.Serialization.DataMember]
        public int Value { get; set; }

        public static explicit operator DiscreteValue(int value) {
            return new DiscreteValue { Value = value, };
        }

        public static explicit operator int(DiscreteValue idx) {
            return idx.Value;
        }

        public override int GetHashCode() {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj) {
            if (!(obj is DiscreteValue))
                return false;
            var idx = (DiscreteValue)obj;
            return this.Value == idx.Value;
        }

        public static bool operator ==(DiscreteValue a, DiscreteValue b) {
            return a.Equals(b);
        }

        public static bool operator !=(DiscreteValue a, DiscreteValue b) {
            return !(a == b);
        }

        internal DiscreteValue Next() {
            if (this.Value == int.MaxValue)
                throw new NotSupportedDataException("The set of possible discrete values is exhausted.");
            return (DiscreteValue)(this.Value + 1);
        }

        public override string ToString() {
            return this.Value.ToString();
        }
    }

    internal interface IHybridVector {
        IEnumerable<Cls> Classes { get; }
        decimal[] ContinuumComponents { get; }
        DiscreteValue[] DiscreteComponents { get; }
        Idx Index { get; }

        bool EqualsComponents(IHybridVector vector);
    }

    internal class HybridVectorsFactory {
        protected HybridVectorsFactory() {

        }

        public bool EqualsComponents(IHybridVector x, IHybridVector y) {
            return _equalsComponents(x, y);
        }

        private static bool _equalsComponents(IHybridVector x, IHybridVector y) {
            if (x.ContinuumComponents.Length != y.ContinuumComponents.Length)
                return false;
            if (x.DiscreteComponents.Length != y.DiscreteComponents.Length)
                return false;
            for (int i = 0; i < x.DiscreteComponents.Length; i++) {
                if (!x.DiscreteComponents[i].Equals(y.DiscreteComponents[i]))
                    return false;
            }
            for (int i = 0; i < x.ContinuumComponents.Length; i++) {
                if (!x.ContinuumComponents[i].Equals(y.ContinuumComponents[i]))
                    return false;
            }
            return true;
        }

        [System.Runtime.Serialization.DataContract]
        [System.Runtime.Serialization.KnownType(typeof(VectorSignature))]
        public class HybridVector : IHybridVector {
            [System.Runtime.Serialization.DataMember]
            private IList<Cls> _classes = new List<Cls>();

            public IEnumerable<Cls> Classes {
                get {
                    return this._classes;
                }
            }

            [System.Runtime.Serialization.DataMember]
            private decimal[] _continuumComponents = new decimal[0];

            public decimal[] ContinuumComponents {
                get {
                    return this._continuumComponents;
                }
            }

            [System.Runtime.Serialization.DataMember]
            private DiscreteValue[] _discreteComponents = new DiscreteValue[0];

            public DiscreteValue[] DiscreteComponents {
                get {
                    return this._discreteComponents;
                }
            }

            [System.Runtime.Serialization.DataMember]
            public Idx Index { get; set; }

            [System.Runtime.Serialization.DataMember]
            private IVectorSignature _sgn;

            public HybridVector(IVectorSignature signature, DiscreteValue[] discreteComponents, decimal[] continuumComponents, params Cls[] classes) {
                this._discreteComponents = discreteComponents;
                this._continuumComponents = continuumComponents;
                this._classes = classes.ToList();
                this._sgn = signature;
            }

            public bool EqualsComponents(IHybridVector vector) {
                return _equalsComponents(this, vector);
            }

            public override int GetHashCode() {
                return this.Index.GetHashCode();
            }

            public override bool Equals(object obj) {
                var vector = obj as HybridVector;
                if ((object)vector == null)
                    return false;
                if (this.Index != vector.Index)
                    return false;
                var thisClasses = this.Classes.ToArray();
                var vectorClasses = vector.Classes.ToArray();
                if (thisClasses.Length != vectorClasses.Length)
                    return false;
                if (this.DiscreteComponents.Length != vector.DiscreteComponents.Length)
                    return false;
                if (this.ContinuumComponents.Length != vector.ContinuumComponents.Length)
                    return false;
                for (int i = 0; i < thisClasses.Length; i++) {
                    if (!thisClasses[i].Equals(vectorClasses[i]))
                        return false;
                }
                for (int i = 0; i < this.DiscreteComponents.Length; i++) {
                    if (!this.DiscreteComponents[i].Equals(vector.DiscreteComponents[i]))
                        return false;
                }
                for (int i = 0; i < this.ContinuumComponents.Length; i++) {
                    if (!this.ContinuumComponents[i].Equals(vector.ContinuumComponents[i]))
                        return false;
                }
                return true;
            }

            public override string ToString() {
                var result = new System.Text.StringBuilder();
                int takeDiscr = 3, takeCont = 5;
                int i = 0;
                var dcts = this._sgn.Components.OfType<DiscreteComponentType>().ToArray();
                var ccts = this._sgn.Components.OfType<ContinuousComponentType>().ToArray();
                foreach (var dv in this.DiscreteComponents.Take(takeDiscr)) {
                    result.Append(dcts[i++].Serialize(dv) + " ");
                }
                if (this.DiscreteComponents.Length > takeDiscr)
                    result.Append("... ");
                else
                    result.Append("");
                i = 0;
                foreach (var cv in this.ContinuumComponents.Take(takeCont)) {
                    result.Append(ccts[i++].Serialize(cv) + " ");
                }
                if (this.ContinuumComponents.Length > takeCont)
                    result.Append("... ");
                else
                    result.Append("");
                if (this._classes.Any())
                    result.Append(" cls{" + string.Join(",", this._classes.Distinct().Select(cls => cls.ToString())) + "}");
                else
                    result.Append(" no cls");
                return result.ToString();
            }
        }

        public IHybridVector Make(
            long index,
            string[] components,
            Cls cls,
            IVectorSignature signature,
            IPrecisionProvider precision
        ) {
            KrolikException.CheckArgumentNotNull(components, nameof(components));
            KrolikException.CheckArgumentNotNull(signature, nameof(signature));
            KrolikException.CheckArgumentNotNull(precision, nameof(precision));

            var discrete = new List<DiscreteValue>();
            var continuous = new List<decimal>();
            if (signature.Components.Length > components.Length)
                throw new VectorsProviderException("The signature does not match the given vector.");
            for (int i = 0; i < signature.Components.Length; i++) {
                var ct = signature.Components[i];
                DiscreteValue? dv;
                decimal? cv;
                ct.Deserialize(components[i], out dv, out cv);
                if (dv.HasValue)
                    discrete.Add(dv.Value);
                else if (cv.HasValue)
                    continuous.Add(cv.Value / precision.Divisor);
            }
            var result = new HybridVector(signature, discrete.ToArray(), continuous.ToArray(), cls) {
                Index = (Idx)index,
            };
            return result;
        }

        private static HybridVectorsFactory _defaultFactory;

        public static HybridVectorsFactory DefaultFactory {
            get {
                if(_defaultFactory == null) {
                    _defaultFactory = new HybridVectorsFactory();
                }
                return _defaultFactory;
            }
        }
    }
}
