﻿using Cil.Common;
using Krolik.Exceptions;
using Krolik.Infrastructure;
using System;
using System.Collections.Generic;

namespace Krolik.TypeSystem {
    internal class Arg : IArgument {
        public string Name { get; private set; }
        public IType Type { get; private set; }
        public string Description { get; private set; }

        public Arg(string name, IType type, string Description = null) {
            this.Name = name;
            this.Type = type;
            this.Description = Description;
        }
    }

    internal class MyObject<TContent> : IObject
        //where TContent : IDisposable
    {
        public string Id { get; set; }
        public IType Type { get; set; }

        public TContent Content { get; set; }

        public MyObject(IType type) {
            this.Type = type;
            this.Id = Guid.NewGuid().ToString();
        }

        public MyObject(IType type, TContent content) : this(
            type
        ) {
            this.Content = content;
        }

        public IObject Call(string methodName, IReadWrite<string> cmdLine, IObject[] pos, Dictionary<string, IObject> nam) {
            Dbg.Enter($"MyObject.Call({methodName})");
            var method = this.Type[methodName];
            if (method == null)
                throw new NoSuchMethodException(this.Type, methodName);
            
            var ctx = new CallContext(this, cmdLine, pos, nam);
            var prepCtx = method.PrepareContext(ctx, this.Type);
            var result = prepCtx.Call(cmdLine);
            Dbg.Leave("MyObject.Call(" + methodName + ")");
            return result;
            //return method.Algorithm(method.PrepareContext(ctx));
        }

        #region Implementation of IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            var iDisposable = this.Content as IDisposable;
            if(iDisposable != null)
                iDisposable.Dispose();
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable
    }

    internal class CallContext : ICallContext {
        /// <summary>
        /// Positional arguments.
        /// </summary>
        public IObject[] Pos { get; set; }

        /// <summary>
        /// Named arguments.
        /// </summary>
        public Dictionary<string, IObject> Nam { get; set; }

        public IReadWrite<string> CmdLine { get; set; }

        public IObject This { get; set; }

        public CallContext(IObject t, IReadWrite<string> cmdLine, IObject[] pos, Dictionary<string, IObject> nam) {
            this.This = t;
            this.CmdLine = cmdLine;
            this.Pos = pos ?? new IObject[0];
            this.Nam = nam ?? new Dictionary<string, IObject>();
        }
    }

    internal class PreparedContext : IPreparedContext {
        public IObject This { get; private set; }
        public Dictionary<string, IObject> Arguments { get; private set; }
        public IReadWrite<string> CmdLine { get; private set; }
        private IMethod _method;

        public PreparedContext(IObject instance, Dictionary<string, IObject> args, IReadWrite<string> cmdLine, IMethod method) {
            this.This = instance;
            this.Arguments = args ?? new Dictionary<string, IObject>();
            this.CmdLine = cmdLine;
            this._method = method;
        }

        public IObject Call(IReadWrite<string> cmdLine = null) {
            try {
                Dbg.Enter("PreparedContext.Call");
                if (cmdLine != null)
                    this.CmdLine = cmdLine;
                var result = this._method.Algorithm(this);
                Dbg.Leave("PreparedContext.Call");
                return result;
            } catch(ImplementationException ex) {
                throw new AlgorithmicException(ex);
            }
            //return this.This.Call(methodName, cmdLine ?? this.CmdLine, null, this.Arguments);
        }
    }
}
