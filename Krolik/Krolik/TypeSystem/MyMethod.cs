﻿using Krolik.Exceptions;
using Krolik.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.TypeSystem {
    internal class MyMethod : IMethod {
        public Func<IPreparedContext, IObject> Algorithm { get; set; }
        public IArgument[] From { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IType To { get; set; }

        public MyMethod(string name, IArgument[] from, IType to, Func<IPreparedContext, IObject> algorithm) {
            this.Name = name;
            this.From = from == null
                ? new IArgument[0]
                : from;
            this.To = to;
            this.Algorithm = algorithm;
        }

        public IPreparedContext PrepareContext(ICallContext ctx, IType type) {
            Dbg.Enter("PrepareContext");
            if (ctx.Nam != null && ctx.Nam.Keys.Any(x => !this.From.Select(x1 => x1.Name).Contains(x)))
                throw new NoSuchMethodException(type, this.Name, ctx.Nam.Keys.First(x => !this.From.Select(x1 => x1.Name).Contains(x)));
            var args = new Dictionary<string, IObject>();
            for (int i = 0; i < this.From.Length; i++) {
                var from = this.From[i];
                if(i < ctx.Pos.Length) {
                    args[from.Name] = ctx.Pos[i];
                } else if(ctx.Nam.ContainsKey(from.Name)) {
                    args[from.Name] = ctx.Nam[from.Name];
                }
            }
            Dbg.Leave("PrepareContext");
            return new PreparedContext(ctx.This, args, ctx.CmdLine, this);
        }
    }
}
