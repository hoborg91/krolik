﻿using Cil.Common;
using System;
using System.Collections.Generic;

namespace Krolik.TypeSystem {
    public interface IType {
        string Name { get; set; }
        string Description { get; set; }
        IMethod this[string methodName] { get; set; }
        IMethod Constructor { get; set; }
        IEnumerable<IMethod> GetMethodsReference();
    }

    public interface ICallContext {
        IObject This { get; set; }
        IObject[] Pos { get; set; }
        Dictionary<string, IObject> Nam { get; set; }
        IReadWrite<string> CmdLine { get; set; }
    }

    public interface IPreparedContext {
        IObject This { get; }
        Dictionary<string, IObject> Arguments { get; }
        IReadWrite<string> CmdLine { get; }
        IObject Call(IReadWrite<string> cmdLine = null);
    }

    public interface IArgument {
        string Name { get; }
        IType Type { get; }
        string Description { get; }
    }

    public interface IMethod {
        string Name { get; set; }
        string Description { get; set; }
        IArgument[] From { get; set; }
        IType To { get; set; }
        Func<IPreparedContext, IObject> Algorithm { get; set; }
        IPreparedContext PrepareContext(ICallContext ctx, IType type);
    }

    public interface IObject : IDisposable {
        string Id { get; set; }
        IType Type { get; set; }
        IObject Call(string methodName, IReadWrite<string> cmdLine, IObject[] pos, Dictionary<string, IObject> nam);
    }

    public interface IEnvironment : IDisposable {
        IObject this[string name] { get; set; }
        IDictionary<string, IType> Types { get; set; }
        [Obsolete("This method is a workaround for instantiating primitive " +
            "types not presented in the type system. Corresponding primitive " +
            "type should be created instead of using this method.")]
        IObject MakeObject<TContent>(IType type, TContent content);
        // TODO. Is this method necessary? Other ways?
        ICallContext MakeContext(IObject t, IReadWrite<string> cmdLine, IObject[] pos, Dictionary<string, IObject> nam);
    }
}
