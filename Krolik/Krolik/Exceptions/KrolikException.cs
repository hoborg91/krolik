﻿using System;

namespace Krolik.Exceptions {
    public abstract class KrolikException : Exception {
        public KrolikException() {

        }

        public KrolikException(string message) : base(
            message
        ) {
            
        }

        public static void CheckArgumentNotNull(object argument, string argumentName) {
            if (argument == null)
                throw new ArgumentNullException(argumentName);
        }
    }
}
