﻿namespace Krolik.Exceptions {
    /// <summary>
    /// Indicates error states in the algorithms and the
    /// functionality of classification tools.
    /// </summary>
    internal abstract class ImplementationException : KrolikException {
        public ImplementationException() {

        }

        public ImplementationException(string message) 
            : base(message)
        {

        }
    }

    internal class NotSupportedDataException : ImplementationException {
        public NotSupportedDataException(string message) 
            : base(message) 
        {

        }
    }

    internal class VectorsProviderException : ImplementationException {
        public VectorsProviderException(string message)
            : base(message) 
        {

        }
    }

    /// <summary>
    /// Indicates errors in algorithms or program structure.
    /// </summary>
    internal class InconsistentStateException : ImplementationException {
        public InconsistentStateException(string message = null)
            : base(message) {

        }
    }

    /// <summary>
    /// Indicates errors in the structure of network.
    /// </summary>
    internal class InconsistentNetworkException : InconsistentStateException {
        /// <summary>
        /// Indicates errors in the structure of network.
        /// </summary>
        public InconsistentNetworkException() {

        }

        /// <summary>
        /// Indicates errors in the structure of network.
        /// </summary>
        public InconsistentNetworkException(string message) : base(
            message
        ) {

        }
    }

    internal class TooFewDataForSamplingException : ImplementationException {
        public TooFewDataForSamplingException()
            : base("There is to few data for sampling. Try again or use sampling with greater part.") {

        }
    }

    internal class ClassifierNotReadyException : ImplementationException {

    }
}
