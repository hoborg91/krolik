﻿using Cil.Common;
using Krolik.TypeSystem;
using System;

namespace Krolik.Exceptions {
    /// <summary>
    /// Indicates error states in the using of language 
    /// and runtime environment.
    /// </summary>
    public abstract class EnvironmentException : KrolikException {
        public EnvironmentException() {

        }

        public EnvironmentException(string message)
            : base(message)
        {

        }
    }

    public class TypesMismatchException : EnvironmentException {
        public TypesMismatchException() {

        }

        public TypesMismatchException(string message) : base(
            message
        ) {

        }
    }

    public class NoSuchMethodException : EnvironmentException {
        public NoSuchMethodException(IType type, string methodName, string parameterName = null)
            : base($"The type '{type.Name}' does not contain method '{methodName}'{(parameterName.IsNullOrWhiteSpace() ? "." : " containing parameter '" + parameterName + "'.")}") {

        }
    }

    public class AlgorithmicException : EnvironmentException {
        private readonly ImplementationException _innerException;

        internal AlgorithmicException(ImplementationException innerException)
            : base(innerException.Message)
        {
            if (innerException == null)
                throw new ArgumentException(
                    $"An instance of the type {nameof(AlgorithmicException)} " +
                    $"must have a reference to the instance of the " +
                    $"{nameof(ImplementationException)}.");
        }
    }

    public class WrongConfigurationFileException : EnvironmentException {
        
    }

    public class WrongEnumValueException : EnvironmentException {
        public WrongEnumValueException(Exception ex) : base(ex.Message) {

        }
    }
}
