﻿using Krolik.Classifiers;
using System;
using System.Collections.Generic;

namespace Krolik.VectorsProviders {
    /// <summary>
    /// As a proxy adds to the common provider the ability 
    /// to record output values.
    /// </summary>
    /// <typeparam name="T">The type of the provided values.</typeparam>
    internal interface IProviderWithLogging<T> : IProvider<T> {
        /// <summary>
        /// Starts record hte output values to the buffer
        /// captioned by the given key. Breaks recording
        /// to other buffers (if any).
        /// </summary>
        /// <param name="key">Name of the buffer containing the output values.</param>
        /// <param name="save">If false, erases data from the buffer specified by key.</param>
        void BeginSession(string key, bool save = false);

        /// <summary>
        /// Breaks recording and returns accumulated values.
        /// </summary>
        /// <param name="key">The name of the buffer.</param>
        List<T> EndSession(string key);

        /// <summary>
        /// Recorded output values.
        /// </summary>
        Dictionary<string, List<T>> Sessions { get; }
    }

    internal abstract class ProviderWithLoggingBase<T>
        : IProviderWithLogging<T>
        , ISamplable<T>
    {
        protected IProvider<T> _provider;
        protected Dictionary<string, List<T>> _readValues = new Dictionary<string, List<T>>();
        protected string _currentSession = null;

        #region Implementation of IProviderWithLogging

        public Dictionary<string, List<T>> Sessions {
            get {
                return _readValues;
            }
        }

        public abstract void BeginSession(string key, bool save = false);

        public abstract List<T> EndSession(string key);

        #endregion Implementation of IProviderWithLogging

        #region Implementation of IProvider

        public abstract T Next();

        public void Reset() {
            this._provider.Reset();
        }

        public abstract Tuple<IProvider<T>, IProvider<T>> Split(float part, float startAt = 0);

        public IList<T> GetAll() {
            return this._provider.GetAll();
        }

        #endregion Implementation of IProvider

        #region Implementation of ISamplable

        public IProvider<T> GetProbabilitySample(float probability) {
            // TODO. This dynamic cast means that something
            // is wrong with the arhitecture.
            return (this._provider as ISamplable<T>).GetProbabilitySample(probability);
        }

        #endregion Implementation of ISamplable

        #region Implementation of IDisposable

        public void Dispose() {
            this._provider.Dispose();
        }

        #endregion Implementation of IDisposable
    }

    /// <summary>
    /// As a proxy adds to the common provider the ability 
    /// to record output values.
    /// </summary>
    /// <typeparam name="T">The type of the provided values.</typeparam>
    internal class ProviderWithLogging<T> 
        : ProviderWithLoggingBase<T>
    {
        /// <summary>
        /// Adds to the common provider the ability to 
        /// record output values.
        /// </summary>
        /// <param name="provider">The basic provider.</param>
        public ProviderWithLogging(IProvider<T> provider) {
            if (provider == null)
                throw new ArgumentNullException("provider");
            this._provider = provider;
        }

        #region Implementation of IProviderWithLogging

        public override void BeginSession(string key, bool save = false) {
            if (!(this._readValues.ContainsKey(key) && save)) {
                this._readValues[key] = new List<T>();
            }
            this._currentSession = key;
        }

        public override List<T> EndSession(string key) {
            this._currentSession = null;
            return this._readValues.ContainsKey(key)
                ? this._readValues[key]
                : new List<T>();
        }

        #endregion Implementation of IProviderWithLogging

        #region Implementation of IProvider

        public override T Next() {
            var result = this._provider.Next();
            if (this._currentSession != null) {
                if (!this._readValues[this._currentSession].Contains(result))
                    this._readValues[this._currentSession].Add(result);
            }
            return result;
        }

        public override Tuple<IProvider<T>, IProvider<T>> Split(float part, float startAt = 0) {
            return this._provider.Split(part, startAt);
        }

        #endregion Implementation of IProvider
    }

    /// <summary>
    /// An "empty" implementation of the provider with logging.
    /// Does not record anything, just translates calls to the 
    /// contained provider. When the recorded values are 
    /// requested, it is returning empty set.
    /// </summary>
    /// <typeparam name="T">The type of the provided values.</typeparam>
    internal class EmptyProviderWithLogging<T> 
        : ProviderWithLoggingBase<T>
    {
        /// <summary>
        /// An "empty" implementation of the provider with logging.
        /// Does not record anything, just translates calls to the 
        /// contained provider. When the recorded values are 
        /// requested, it is returning empty set.
        /// </summary>
        /// <param name="provider">The basic provider.</param>
        public EmptyProviderWithLogging(IProvider<T> provider) {
            if (provider == null)
                throw new ArgumentNullException("provider");
            this._provider = provider;
        }

        #region Implementation of IProviderWithLogging

        public override void BeginSession(string key, bool save = false) {
        }

        public override List<T> EndSession(string key) {
            return new List<T>();
        }

        #endregion Implementation of IProviderWithLogging

        #region Implementation of IProvider

        public override T Next() {
            var result = this._provider.Next();
            if (this._currentSession != null) {
                if (!this._readValues[this._currentSession].Contains(result))
                    this._readValues[this._currentSession].Add(result);
            }
            return result;
        }

        public override Tuple<IProvider<T>, IProvider<T>> Split(float part, float startAt = 0) {
            return this._provider.Split(part, startAt);
        }

        #endregion Implementation of IProvider
    }
}
