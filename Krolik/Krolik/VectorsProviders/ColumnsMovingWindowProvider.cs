﻿using Krolik.Classifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using Krolik.Mathematics;
using Krolik.Infrastructure;
using Cil.Design;
using Cil.Common;
using Krolik.Exceptions;

namespace Krolik.VectorsProviders {
    /// <summary>
    /// Treats a single file or a batch of files in the folder.
    /// Each file must contain one or more columns of values.
    /// The first value in each column determines the class of the
    /// vectors produced from that column. The provider creates a
    /// moving window which slides over the column. Current vector
    /// is a sequence of values currently located in the window.
    /// </summary>
    internal class ColumnsMovingWindowProvider
        : IVectorsProvider
        , ISamplable<IHybridVector> 
    {
        private readonly FileDescriptor[] _files;
        private int _currentFileIndex;
        private Queue<IHybridVector> _currentFileQueue;

        public ColumnsMovingWindowProvider(
            string path,
            IVectorSignature vectorSignature,
            IPrecisionProvider precision, 
            IFileSystemApi fileSystem
        ) {
            KrolikException.CheckArgumentNotNull(path, nameof(path));
            KrolikException.CheckArgumentNotNull(vectorSignature, nameof(vectorSignature));
            KrolikException.CheckArgumentNotNull(precision, nameof(precision));
            KrolikException.CheckArgumentNotNull(fileSystem, nameof(fileSystem));

            this.PrecisionProvider = precision;

            bool
                isDir = fileSystem.DirectoryExists(path),
                isFile = fileSystem.FileExists(path)
            ;
            if (isDir && isFile)
                throw new Exception($"It seems that there exist both file and directory with path \"{path}\".");
            else if (!(isDir || isFile))
                throw new Exception($"There is neither a directory nor a file with path \"{path}\".");
            var filePaths = isDir
                ? fileSystem.GetFiles(path)
                : new[] { path, };
            if (filePaths.Length == 0)
                throw new Exception($"It seems that the given directory \"{path}\" contains no appropriate files.");
            long nextFreeIndex = 0;
            this._files = filePaths
                .Select(x => new FileDescriptor(
                    x,
                    vectorSignature,
                    fileSystem,
                    () => {
                        var n = nextFreeIndex;
                        nextFreeIndex++;
                        return n;
                    },
                    this.PrecisionProvider)
                )
                .ToArray();
            this.Reset();
        }

        #region Implement IVectorsProvider

        public IPrecisionProvider PrecisionProvider { get; private set; }

        public IList<IHybridVector> GetAll() {
            if (this._isDisposed)
                throw new ObjectDisposedException(nameof(ColumnsMovingWindowProvider));
            return this._files
                .SelectMany(x => x.GetAll())
                .ToList();
        }

        public IHybridVector Next() {
            if (this._isDisposed)
                throw new ObjectDisposedException(nameof(ColumnsMovingWindowProvider));
            if (this._currentFileQueue == null)
                throw new InconsistentStateException("The provider is in inconsistent state.");
            if (this._currentFileQueue.Any())
                return this._currentFileQueue.Dequeue();
            this._currentFileIndex = (this._currentFileIndex + 1) % this._files.Length;
            foreach (var vector in this._files[this._currentFileIndex].GetAll()) {
                this._currentFileQueue.Enqueue(vector);
            }
            if (!this._currentFileQueue.Any())
                throw new NotSupportedDataException($"It seems that the given file \"{this._files[this._currentFileIndex].Path}\" does not contain any data.");
            return this._currentFileQueue.Dequeue();
        }

        public void Reset() {
            if (this._isDisposed)
                throw new ObjectDisposedException(nameof(ColumnsMovingWindowProvider));
            this._currentFileIndex = 0;
            this._currentFileQueue = new Queue<IHybridVector>();
            foreach (var vector in this._files[this._currentFileIndex].GetAll()) {
                this._currentFileQueue.Enqueue(vector);
            }
        }

        public Tuple<IProvider<IHybridVector>, IProvider<IHybridVector>> Split(float part, float startAt = 0) {
            if (this._isDisposed)
                throw new ObjectDisposedException(nameof(ColumnsMovingWindowProvider));
            if (part <= 0 || part >= 1)
                throw new ArgumentException($"The '{nameof(part)}' parameter must be in (0; 1).");
            if (startAt < 0 || startAt >= 1)
                throw new ArgumentException($"The '{nameof(startAt)}' parameter must be in [0; 1).");
            // TODO. Refactor this. Do not save vectors in memory.
            var dirtyCode = new InCodeVectorsProvider(
                this.PrecisionProvider,
                this.GetAll().ToArray()
            );
            var result = dirtyCode.Split(part, startAt);
            return result;
        }

        #endregion Implement IVectorsProvider

        #region Implement IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            this._isDisposed = true;
        }

        #endregion Implement IDisposable

        #region Implement ISamplable

        public IProvider<IHybridVector> GetProbabilitySample(float probability) {
            if (this._isDisposed)
                throw new ObjectDisposedException(nameof(ColumnsMovingWindowProvider));
            if (probability <= 0 || probability >= 1)
                throw new ArgumentException($"The '{nameof(probability)}' parameter must be in (0; 1).");
            var dirtyCode = new InCodeVectorsProvider(
                this.PrecisionProvider,
                this.GetAll().ToArray()
            );
            var result = dirtyCode.GetProbabilitySample(probability);
            return result;
        }

        #endregion Implement ISamplable

        private class FileDescriptor {
            internal string Path { get; private set; }
            private readonly IVectorSignature _vectorSignature;
            private readonly IFileSystemApi _fs;
            private readonly Func<long> _getNextFreeIndex;
            private readonly IPrecisionProvider _precision;

            private IDictionary<Cls, IList<IHybridVector>> _vectors;

            internal FileDescriptor(
                string path,
                IVectorSignature vectorSignature,
                IFileSystemApi fileSystem,
                Func<long> getNextFreeIndex,
                IPrecisionProvider precision
            ) {
                KrolikException.CheckArgumentNotNull(path, nameof(path));
                KrolikException.CheckArgumentNotNull(vectorSignature, nameof(vectorSignature));
                KrolikException.CheckArgumentNotNull(fileSystem, nameof(fileSystem));
                KrolikException.CheckArgumentNotNull(getNextFreeIndex, nameof(getNextFreeIndex));
                KrolikException.CheckArgumentNotNull(precision, nameof(precision));

                this.Path = path;
                this._vectorSignature = vectorSignature;
                this._fs = fileSystem;
                this._getNextFreeIndex = getNextFreeIndex;
                this._precision = precision;
            }

            internal IList<IHybridVector> GetAll() {
                return this._fillVectors();
            }

            private IList<IHybridVector> _fillVectors() {
                if(this._vectors == null) {
                    IBlockQueue<decimal>[] accumulators = null;
                    Cls[] clss = null;
                    using (var sr = this._fs.OpenForRead(this.Path)) {
                        string line;
                        int lineNaturalNumber = 0;
                        while ((line = sr.ReadLine()) != null) {
                            lineNaturalNumber++;
                            var parts = line.Split(' ')
                                .Select(x => x.Trim().ToDecimal().Value)
                                .ToArray();
                            if (accumulators == null)
                                accumulators = parts
                                    .Select(x => new BlockQueue<decimal>(this._vectorSignature.Components.Length, 1))
                                    .ToArray();
                            if (parts.Length == 0 || parts.Length != accumulators.Length)
                                throw new NotSupportedDataException($"The given file \"{this.Path}\" contains unsupported data at line {lineNaturalNumber}.");
                            if(lineNaturalNumber == 1) {
                                try {
                                    clss = parts
                                        .Select(x => (Cls)((int)x))
                                        .ToArray();
                                } catch(InvalidCastException) {
                                    throw new Exception($"The first line of the given file \"{this.Path}\" must contain classes indices.");
                                }
                                continue;
                            }
                            for (int i = 0; i < parts.Length; i++) {
                                accumulators[i].Push(parts[i]);
                            }
                        }
                    }
                    if (clss == null)
                        throw new Exception($"The given file \"{this.Path}\" contains data in not supported format. " +
                            "Possibly, it contains no data at all (which is not supported).");
                    this._vectors = new Dictionary<Cls, IList<IHybridVector>>();
                    for (int i = 0; i < clss.Length; i++) {
                        var cls = clss[i];
                        if (!this._vectors.ContainsKey(cls))
                            this._vectors[cls] = new List<IHybridVector>();
                        var acc = accumulators[i];
                        while (acc.Ready()) {
                            var vector = HybridVectorsFactory.DefaultFactory.Make(
                                this._getNextFreeIndex(),
                                acc.Pop().Select(x => x.ToString()).ToArray(),
                                cls,
                                this._vectorSignature,
                                this._precision
                            );
                            this._vectors[cls].Add(vector);
                        }
                    }
                }
                return this._vectors
                    .SelectMany(x => x.Value)
                    .ToList();
            }
        }
    }
}
