﻿using Krolik.Classifiers;
using Krolik.Mathematics;

namespace Krolik.VectorsProviders {
    internal interface IVectorsProvider : IProvider<IHybridVector> {
        IPrecisionProvider PrecisionProvider { get; }
    }
}