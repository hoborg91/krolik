﻿using Cil.Common;
using Cil.Design;
using Krolik.Classifiers;
using Krolik.Exceptions;
using Krolik.Mathematics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Krolik.VectorsProviders {
    /// <summary>
    /// Treats multiple files with class pointers located at the
    /// first lines of these files.
    /// </summary>
    internal class MultipleFilesVectorsProvider 
        : IVectorsProvider
        , ISamplable<IHybridVector>
    {
        private IList<FileDescriptor> _files = new List<FileDescriptor>();
        private int _current = 0;

        private IVectorSignature _sgn;

        private readonly string _sampleFilePattern;

        public IPrecisionProvider PrecisionProvider { get; private set; }

        public MultipleFilesVectorsProvider(
            IVectorSignature signature, 
            string directory, 
            IPrecisionProvider precision,
            IFileSystemApi fileSystem,
            string sampleFilePattern = "-sample-"
        ) {
            KrolikException.CheckArgumentNotNull(signature, nameof(signature));
            KrolikException.CheckArgumentNotNull(precision, nameof(precision));
            KrolikException.CheckArgumentNotNull(fileSystem, nameof(fileSystem));
            if (string.IsNullOrWhiteSpace(directory))
                throw new ArgumentException($"Parameter '{nameof(directory)}' must not be empty.");
            if (string.IsNullOrWhiteSpace(sampleFilePattern))
                throw new ArgumentException($"Parameter '{nameof(sampleFilePattern)}' must not be empty.");
            if (!fileSystem.DirectoryExists(directory))
                throw new Exception($"The given directory '{directory}' does not exist.");

            this._sgn = signature;
            this.PrecisionProvider = precision;
            this._sampleFilePattern = sampleFilePattern;
            var allFiles = fileSystem.GetFiles(directory)
                .Where(x => !x.Contains(sampleFilePattern))
                .ToArray();
            if (allFiles.Length == 0)
                throw new Exception($"The given directory '{directory}' contains nothing.");
            var accumulatedIndex = 0L;
            foreach (var file in allFiles) {
                var fd = new FileDescriptor(
                    this._sgn, 
                    file, 
                    accumulatedIndex,
                    fileSystem
                );
                accumulatedIndex += fd.TotalCount;
                this._files.Add(fd);
            }
        }

        private MultipleFilesVectorsProvider(
            List<FileDescriptor> files, 
            string sampleFilePattern,
            IPrecisionProvider precision
        ) {
            KrolikException.CheckArgumentNotNull(files, nameof(files));
            KrolikException.CheckArgumentNotNull(sampleFilePattern, nameof(sampleFilePattern));
            KrolikException.CheckArgumentNotNull(precision, nameof(precision));

            this._files = files;
            this._sampleFilePattern = sampleFilePattern;
            this.PrecisionProvider = precision;
        }

        #region Implementation of IProvider

        public IHybridVector Next() {
            var result = this._files[this._current].GetNextVector(this.PrecisionProvider);
            if (result == null) {
                this._files[this._current].Reset();
                this._current = (this._current + 1) % this._files.Count;
                result = this._files[this._current].GetNextVector(this.PrecisionProvider);
            }
            if (this._enableLogging) {
                Infrastructure.KrolikLoggers.Get.Preparation.Debug("Provider.Next() returns [" + result.Index + "] " + result.ToString());
            }
            return result;
        }

        public void Reset() {
            this._current = 0;
            foreach (var file in this._files)
                file.Reset();
        }

        public Tuple<IProvider<IHybridVector>, IProvider<IHybridVector>> Split(float part, float startAt = 0) {
            IProvider<IHybridVector>
                result1,
                result2;
            List<FileDescriptor>
                files1 = new List<FileDescriptor>(),
                files2 = new List<FileDescriptor>();
            foreach (var file in this._files) {
                var splittedFiles = file.Split(part, startAt);
                files1.AddRange(splittedFiles.Item1);
                files2.AddRange(splittedFiles.Item2);
            }
            result1 = new MultipleFilesVectorsProvider(
                files1, 
                this._sampleFilePattern,
                this.PrecisionProvider
            );
            result2 = new MultipleFilesVectorsProvider(
                files2, 
                this._sampleFilePattern,
                this.PrecisionProvider
            );
            return new Tuple<IProvider<IHybridVector>, IProvider<IHybridVector>>(
                result1,
                result2
            );
        }

        public IList<IHybridVector> GetAll() {
            return new List<IHybridVector>();
            var result = this._files
                .SelectMany(x => x.GetAll(this.PrecisionProvider))
                .ToList();
            return result;
        }

        #endregion Implementation of IProvider

        #region Implementation of ISamplable

        public IProvider<IHybridVector> GetProbabilitySample(float probability) {
            try {
                var files = this._files
                    .Select(f => f.GetProbabilitySample(probability, this._sampleFilePattern))
                    .ToList();
                var result = new MultipleFilesVectorsProvider(
                    files, 
                    this._sampleFilePattern,
                    this.PrecisionProvider
                );
                return result;
            } catch(NoDataFoundException) {
                throw new TooFewDataForSamplingException();
            }
        }

        #endregion Implementation of ISamplable

        #region Implementation of IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            foreach (var file in this._files) {
                file.Dispose();
            }
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable

        private class NoDataFoundException : KrolikException {
            public string FileName { get; private set; }

            public NoDataFoundException(string fileName)
                : base($"The file '{fileName}' does not contain any data.") {
                this.FileName = fileName;
            }
        }

        private class FileDescriptor : IDisposable {
            private readonly string _fileName;
            private int _fromIncluding, _toExcluding, _current;
            private Cls? _cls;
            public long TotalCount, FirstIndex;
            private bool _exhausted = false;
            //private Func<string, Cls> _clsMaker = null;

            private readonly Queue<IHybridVector> _cache = new Queue<IHybridVector>();

            private readonly int _cacheCapacity = 1000;

            private readonly IVectorSignature _sgn;

            private readonly IFileSystemApi _fileSystem;

            public FileDescriptor(
                IVectorSignature signature, 
                string fileName, 
                long accumulatedIndex,
                IFileSystemApi fileSystem
            ) {
                KrolikException.CheckArgumentNotNull(signature, nameof(signature));
                if (string.IsNullOrWhiteSpace(fileName))
                    throw new ArgumentNullException(nameof(fileName));
                KrolikException.CheckArgumentNotNull(fileSystem, nameof(fileSystem));
                //if (clsMaker == null)
                //    throw new ArgumentNullException("clsMaker");

                this._sgn = signature;
                //this._clsMaker = clsMaker;
                this._fileName = fileName;
                this._fileSystem = fileSystem;
                var linesCount = 1;

                var debugInfo = new List<string>();

                using (var sr = fileSystem.OpenForRead(fileName)) {
                    var line = sr.ReadLine();
                    if (string.IsNullOrWhiteSpace(line))
                        throw new VectorsProviderException("Empty lines are not supported.");
                    debugInfo.Add(line);
                    this._cls = this._sgn.DeserializeClass(line.Trim());
                    while ((line = sr.ReadLine()) != null) {
                        if (string.IsNullOrWhiteSpace(line)) {
                            File.WriteAllText(
                                "Trace1.txt", 
                                "\"" + this._fileName + "\"" +Environment.NewLine + debugInfo.JoinBy(Environment.NewLine)
                            );
                            throw new VectorsProviderException("Empty lines are not supported.");
                        }
                        debugInfo.Add(line);
                        linesCount++;
                    }
                }
                if (this._cls == null || linesCount <= 1)
                    throw new NoDataFoundException(fileName);
                this._fromIncluding = 1;
                this.Reset();
                this._toExcluding = linesCount;
                this.TotalCount = linesCount - 1;
                this.FirstIndex = accumulatedIndex;
            }

            public void Reset() {
                this._current = this._fromIncluding;
                this._exhausted = false;
                this._cache.Clear();
            }

            public IHybridVector GetNextVector(IPrecisionProvider precision) {
                while (!this._cache.Any()) {
                    if (this._exhausted)
                        return null;
                    using (var sr = this._fileSystem.OpenForRead(this._fileName)) {
                        var firstLine = sr.ReadLine();
                        var index = 0;
                        string line;
                        while ((line = sr.ReadLine()) != null) {
                            index++;
                            if (index < this._current)
                                continue;
                            if (index >= this._toExcluding) {
                                this._exhausted = true;
                                break;
                            }
                            var vector = this._parseLine(line, index, precision);
                            this._cache.Enqueue(vector);
                            if (this._cache.Count >= this._cacheCapacity)
                                break;
                        }
                        if (line == null) {
                            this._exhausted = true;
                            this._current = this._fromIncluding;
                        } else
                            this._current += this._cache.Count;
                    }
                }
                return this._cache.Dequeue();
            }

            private IHybridVector _parseLine(
                string line, 
                long relativeIndex,
                IPrecisionProvider precision
            ) {
                var parts = line.Split(' ')
                                .Select(x => x.Trim())
                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                .ToArray();
                var idx = this.FirstIndex + relativeIndex - this._fromIncluding;
                var result = HybridVectorsFactory.DefaultFactory.Make(
                    idx,
                    parts,
                    this._cls.Value,
                    this._sgn,
                    precision
                );
                return result;
            }

            public Tuple<IList<FileDescriptor>, List<FileDescriptor>> Split(float part, float startAt) {
                if (startAt < 0 || startAt > 1 || part < 0 || part > 1)
                    throw new Exception("Both startAt and part parameters must be in range [0; 1]. "
                        + "Given: part ~= " + part.ToString("f3") + "; startAt ~= " + startAt.ToString("f3") + ".");
                var count = this._toExcluding - this._fromIncluding;
                var result1 = new List<FileDescriptor>();
                var result2 = new List<FileDescriptor>();

                var firstBounds = new List<Tuple<float, float>>();
                var secondBounds = new List<Tuple<float, float>>();
                var final = startAt + part - 1;
                if (final > 0) {
                    firstBounds.Add(new Tuple<float, float>(0, final));
                    firstBounds.Add(new Tuple<float, float>(startAt, 1));
                    secondBounds.Add(new Tuple<float, float>(final, startAt));
                } else {
                    secondBounds.Add(new Tuple<float, float>(0, startAt));
                    secondBounds.Add(new Tuple<float, float>(part + startAt, 1));
                    firstBounds.Add(new Tuple<float, float>(startAt, part + startAt));
                }

                foreach (var bound in firstBounds) {
                    var file = new FileDescriptor(
                        this._sgn, 
                        this._fileName, 
                        this.FirstIndex,
                        this._fileSystem
                    ) {
                        _cls = this._cls,
                        _fromIncluding = this._fromIncluding + (int)Math.Floor(count * bound.Item1),
                        _toExcluding = this._fromIncluding + (int)Math.Ceiling(count * bound.Item2),
                        TotalCount = this.TotalCount,
                    };
                    file.Reset();
                    result1.Add(file);
                }
                foreach (var bound in secondBounds) {
                    var file = new FileDescriptor(
                        this._sgn, 
                        this._fileName, 
                        this.FirstIndex,
                        this._fileSystem
                    ) {
                        _cls = this._cls,
                        _fromIncluding = this._fromIncluding + (int)Math.Floor(count * bound.Item1),
                        _toExcluding = this._fromIncluding + (int)Math.Ceiling(count * bound.Item2),
                        TotalCount = this.TotalCount,
                    };
                    file.Reset();
                    result2.Add(file);
                }
                return new Tuple<IList<FileDescriptor>, List<FileDescriptor>>(
                    result1,
                    result2
                );
            }

            public FileDescriptor GetProbabilitySample(float probability, string sampleFilePattern) {
                if (probability < 0 || probability > 1)
                    throw new ArgumentException($"Parameter '{nameof(probability)}' must be >= 0 and <= 1.");
                if (string.IsNullOrWhiteSpace(sampleFilePattern))
                    throw new ArgumentException($"Parameter '{nameof(sampleFilePattern)}' must not be empty.");
                var directory = this._fileSystem.GetDirectoryName(this._fileName);
                var filesInDirectory = this._fileSystem.GetFiles(directory);
                var suffix = 0;
                var makeFileName = (Func<string>)(() => this._fileName + sampleFilePattern + suffix);
                while (filesInDirectory.Contains(makeFileName())) {
                    suffix++;
                    if (suffix == int.MaxValue)
                        throw new Exception("All possible file names are already used.");
                }
                var index = 0;
                var fileName = makeFileName();
                Console.WriteLine($"Sample file name: {fileName}.");
                using (var sw = this._fileSystem.OpenForWrite(fileName)) {
                    sw.WriteLine(this._sgn.SerializeClass(this._cls.Value));
                    using (var sr = this._fileSystem.OpenForRead(this._fileName)) {
                        string line;
                        while ((line = sr.ReadLine()) != null) {
                            var p = Convert.ToDouble(Cil.Common.Rand.NextDouble());
                            //Console.WriteLine("Random: " + p + ".");
                            if (this._fromIncluding <= index
                                && index <= this._toExcluding
                                && p <= probability
                            ) {
                                //Console.WriteLine("Write " + line);
                                sw.WriteLine(line);
                            }
                            index++;
                        }
                    }
                }
                if (index == 0)
                    throw new Exception("No samples have been selected. Try use greater probability.");
                //Console.WriteLine("Sample file name: " + fileName + ". End.");
                var result = new FileDescriptor(
                    this._sgn, 
                    fileName, 
                    this.FirstIndex,
                    this._fileSystem
                );
                result._isTempFile = true;
                return result;
            }

            internal IEnumerable<IHybridVector> GetAll(IPrecisionProvider precision) {
                var r = new List<IHybridVector>();
                using (var sr = this._fileSystem.OpenForRead(this._fileName)) {
                    var firstLine = sr.ReadLine();
                    var index = 0;
                    string line;
                    while ((line = sr.ReadLine()) != null) {
                        index++;
                        var vector = this._parseLine(line, index, precision);
                        r.Add(vector);
                    }
                }
                return r;
            }

            #region Implementation of IDisposable

            private bool _isDisposed = false;

            private bool _isTempFile = false;

            public void Dispose() {
                if (this._isDisposed)
                    return;
                if (this._isTempFile) {
                    this._fileSystem.DeleteFile(this._fileName);
                }
                this._isDisposed = true;
            }

            #endregion Implementation of IDisposable

            public override string ToString() {
                return this._fileName;
            }
        }

        private bool _enableLogging = false;

        internal void EnableLogging(bool enable) {
            this._enableLogging = enable;
        }
    }
}
