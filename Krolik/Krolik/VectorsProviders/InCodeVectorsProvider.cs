﻿using Cil.Common;
using Krolik.Classifiers;
using Krolik.Exceptions;
using Krolik.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.VectorsProviders {
    internal class InCodeVectorsProvider
        : IVectorsProvider
        , ISamplable<IHybridVector> 
    {
        private readonly ICycle<IHybridVector> _vectors;

        public IPrecisionProvider PrecisionProvider { get; private set; }

        public InCodeVectorsProvider(IPrecisionProvider precision, params IHybridVector[] vectors) {
            KrolikException.CheckArgumentNotNull(precision, nameof(precision));
            KrolikException.CheckArgumentNotNull(vectors, nameof(vectors));

            this._vectors = new Cycle<IHybridVector>(vectors);
            this.PrecisionProvider = precision;
        }

        #region Implementation of IProvider

        public IHybridVector Next() {
            var result = this._vectors.Current;
            this._vectors.Next();
            return result;
        }

        public void Reset() {
            this._vectors.SetTo(0);
        }

        public Tuple<IProvider<IHybridVector>, IProvider<IHybridVector>> Split(float part, float startAt = 0) {
            if (part < 0 || part > 1)
                throw new ArgumentException("The 'part' parameter must be >= 0 and <= 1.");
            if (startAt < 0 || startAt >= 1)
                throw new ArgumentException("The 'startAt' parameter must be >= 0 and < 1.");
            var a = new List<IHybridVector>();
            var b = new List<IHybridVector>();
            var vectors = this._vectors.ToList();
            var startFirstPartAt = (int)Math.Ceiling(vectors.Count * startAt);
            var finishFirstPartAt = (int)Math.Ceiling(vectors.Count * (startAt + part));
            while (finishFirstPartAt > vectors.Count)
                finishFirstPartAt -= vectors.Count;
            if(finishFirstPartAt > startFirstPartAt) {
                for (int i = 0; i < vectors.Count; i++) {
                    if (startFirstPartAt <= i && i <= finishFirstPartAt)
                        a.Add(vectors[i]);
                    if (i <= startFirstPartAt || finishFirstPartAt <= i)
                        b.Add(vectors[i]);
                }
            } else {
                for (int i = 0; i < vectors.Count; i++) {
                    if (finishFirstPartAt <= i && i <= startFirstPartAt)
                        b.Add(vectors[i]);
                    if (i <= finishFirstPartAt || startFirstPartAt <= i)
                        a.Add(vectors[i]);
                }
            }
            var result = Tuple.Create(
                (IProvider<IHybridVector>)new InCodeVectorsProvider(this.PrecisionProvider, a.ToArray()),
                (IProvider<IHybridVector>)new InCodeVectorsProvider(this.PrecisionProvider, b.ToArray()));
            return result;
        }

        public IList<IHybridVector> GetAll() {
            return this._vectors.ToList();
        }

        #endregion Implementation of IProvider

        #region Implementation of ISamplable

        public IProvider<IHybridVector> GetProbabilitySample(float probability) {
            var vectors = new List<IHybridVector>();
            foreach (var vector in this._vectors) {
                var random = Convert.ToDouble(Rand.NextDouble());
                if (random <= probability)
                    vectors.Add(vector);
            }
            var result = new InCodeVectorsProvider(this.PrecisionProvider, vectors.ToArray());
            return result;
        }

        #endregion Implementation of ISamplable

        #region Implementation of IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable
    }
}
