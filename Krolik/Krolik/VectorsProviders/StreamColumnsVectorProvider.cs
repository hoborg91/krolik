﻿using Cil;
using Krolik.Classifiers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Krolik.VectorsProviders {
    //// TODO. Remove this class?
    ///// <summary>
    ///// Treats a directory with files. Each file should contain one
    ///// or more columns. Each column represents one class which name
    ///// is written at the first line. A moving window reads the columns
    ///// and makes corresponding vectors.
    ///// </summary>
    //[Obsolete("The usefullness of this class is unproved. Currently it is not being developed.")]
    //public class StreamColumnsVectorsProvider<TClass> 
    //    : IProvider<IVector<TClass>> 
    //    , ISamplable<IVector<TClass>>
    //{
    //    protected class FileDescriptor {
    //        string _path;
    //        public TClass[] Classes { get; protected set; }
    //        /// <summary>
    //        /// Includes header.
    //        /// </summary>
    //        public int TotalRows = 1;
    //        /// <summary>
    //        /// Including.
    //        /// </summary>
    //        List<Tuple<int, int>> _segmentsToRead = new List<Tuple<int, int>>();
    //        int _current = 1;
    //        Queue<IVector<TClass>>[] _classes = new Queue<IVector<TClass>>[0];
    //        int _vectorLength;
    //        long StartIndex = 0;
    //        Func<string, TClass> _clsMaker = null;

    //        char _delimeter = ' ';

    //        public FileDescriptor(string path, int vectorLength, long startIndex, Func<string, TClass> clsMaker) {
    //            if (string.IsNullOrWhiteSpace(path))
    //                throw new ArgumentException("The \"path\" argument must not be empty.");
    //            if (vectorLength <= 0)
    //                throw new ArgumentException("The \"vectorLength\" argument must be > 0.");
    //            if (clsMaker == null)
    //                throw new ArgumentNullException("clsMaker");
    //            this._clsMaker = clsMaker;
    //            this._path = path;
    //            this._vectorLength = vectorLength;
    //            using (var sr = new StreamReader(this._path)) {
    //                var header = sr.ReadLine();
    //                this.Classes = header.Split(this._delimeter)
    //                    .Select(x1 => clsMaker(x1.Trim()))
    //                    .ToArray();
    //                this._classes = new Queue<IVector<TClass>>[this.Classes.Length];
    //                for (int i = 0; i < this._classes.Length; i++) {
    //                    this._classes[i] = new Queue<IVector<TClass>>();
    //                }
    //                string line;
    //                while ((line = sr.ReadLine()) != null) {
    //                    var partsCount = line.Split(this._delimeter).Count();
    //                    this.TotalRows++;
    //                    //for (int i = 0; i < partsCount; i++) {
    //                    //    this.TotalRows++;
    //                    //}
    //                }
    //            }
    //            if (this.TotalRows <= 1)
    //                throw new Exception("There is a column without rows.");
    //            this._segmentsToRead.Add(new Tuple<int, int>(1, this.TotalRows));
    //            this._current = this._segmentsToRead.First().Item1;
    //            this.StartIndex = startIndex;
    //        }

    //        protected FileDescriptor(string path, int vectorLength, long startIndex, List<Tuple<int, int>> segments, Func<string, TClass> clsMaker) 
    //            : this(path, vectorLength, startIndex, clsMaker
    //        ) {
    //            this._segmentsToRead = segments;
    //            this._current = this._segmentsToRead.First().Item1;
    //        }

    //        public void Reset() {
    //            this._current = this._segmentsToRead
    //                .First()
    //                .Item1;
    //        }

    //        /// <summary>
    //        /// 
    //        /// </summary>
    //        /// <param name="vectorLength"></param>
    //        /// <param name="vectorsCount">For each class.</param>
    //        /// <returns></returns>
    //        public IEnumerable<IVector<TClass>> GetVectors(int vectorsCount) {
    //            if (vectorsCount <= 0)
    //                throw new ArgumentException("\"vectorsCount\" argument must be > 0.");
    //            this._fillQueues(vectorsCount);
    //            var result = new List<IVector<TClass>>();
    //            foreach (var cls in this._classes) {
    //                for (int i = 0; i < vectorsCount && cls.Any(); i++) {
    //                    result.Add(cls.Dequeue());
    //                }
    //            }
    //            return result;
    //        }

    //        private void _fillQueues(int atLeast) {
    //            if (this._classes.First().Count >= atLeast)
    //                return;
    //            var startAt = this._current;
    //            var go = true;
    //            while (this._classes.First().Count < atLeast && go) {
    //                var cbqs = new BlockQueue<Tuple<long, string>>[this._classes.Length];
    //                for (int i = 0; i < cbqs.Length; i++) {
    //                    cbqs[i] = new BlockQueue<Tuple<long, string>>(this._vectorLength, 1);
    //                }
    //                var segmentsCycle = new Cycle<Tuple<int, int>>(this._segmentsToRead);
    //                var currentSegment = segmentsCycle
    //                    .SetTo(x1 => x1.Item1 <= this._current && this._current <= x1.Item2)
    //                    .Current;
    //                var lineIndex = 0;
    //                string line;
    //                using (var sr = new StreamReader(this._path)) {
    //                    while ((line = sr.ReadLine()) != null) {
    //                        if (lineIndex == this._current) {
    //                            this._enqueue(cbqs, line, lineIndex);
    //                            if (currentSegment.Item2 - lineIndex <= this._vectorLength) {
    //                                for (int i = lineIndex + 1; i < currentSegment.Item2; i++) {
    //                                    this._enqueue(cbqs, sr.ReadLine(), i);
    //                                }
    //                                currentSegment = segmentsCycle.Next().Current;
    //                                this._current = currentSegment.Item1;
    //                                go = this._current != startAt;
    //                                break;
    //                            } else {
    //                                this._current++;
    //                            }
    //                        }
    //                        lineIndex++;
    //                    }
    //                }
    //                for (int i = 0; i < this._classes.Length; i++) {
    //                    var bq = cbqs[i];
    //                    Tuple<long, string>[] vec;
    //                    while ((vec = bq.Pop(IBlockQueueIfNotReady.Null)) != null) {
    //                        var vector = new MyVector<TClass>(
    //                            vec[0].Item1,
    //                            vec
    //                                .Select((x1, j) => new {
    //                                    Index = j,
    //                                    Value = x1.Item2,
    //                                })
    //                                .OrderBy(x => x.Index)
    //                                .Select(x => x.Value)
    //                                .ToArray(),
    //                            this.Classes[i]
    //                        );
    //                        this._classes[i].Enqueue(vector);
    //                    }
    //                }
    //            }
    //        }

    //        private void _enqueue(BlockQueue<Tuple<long, string>>[] blockQueues, string line, int lineIndex) {
    //            if (blockQueues == null)
    //                throw new ArgumentNullException("blockQueues");
    //            if (string.IsNullOrWhiteSpace(line))
    //                throw new ArgumentException("The given argument \"line\" must not be empty.");
    //            var parts = line.Split(this._delimeter)
    //                .Select(x1 => x1.Trim())
    //                .ToArray();
    //            if (parts.Length != blockQueues.Length)
    //                throw new NotImplementedException();
    //            for (int i = 0; i < parts.Length; i++) {
    //                blockQueues[i].Push(new[] { new Tuple<long, string>(
    //                    lineIndex + i * this.TotalRows + this.StartIndex,
    //                    parts[i]
    //                ), });
    //            }
    //        }

    //        public Tuple<FileDescriptor, FileDescriptor> Split(double part, double startAt) {
    //            var resultSegments = new Tuple<List<Tuple<int, int>>, List<Tuple<int, int>>>(
    //                new List<Tuple<int, int>>(),
    //                new List<Tuple<int, int>>()
    //            );

    //            var firstBounds = new List<Tuple<double, double>>();
    //            var secondBounds = new List<Tuple<double, double>>();
    //            var final = startAt + part - 1;
    //            if (final > 0) {
    //                firstBounds.Add(new Tuple<double, double>(0, final));
    //                firstBounds.Add(new Tuple<double, double>(startAt, 1));
    //                secondBounds.Add(new Tuple<double, double>(final, startAt));
    //            } else {
    //                secondBounds.Add(new Tuple<double, double>(0, startAt));
    //                secondBounds.Add(new Tuple<double, double>(part + startAt, 1));
    //                firstBounds.Add(new Tuple<double, double>(startAt, part + startAt));
    //            }

    //            foreach (var segment in this._segmentsToRead) {
    //                var sgms = SplitSegment(segment, firstBounds, secondBounds);
    //                resultSegments.Item1.AddRange(sgms.Item1);
    //                resultSegments.Item2.AddRange(sgms.Item2);
    //            }

    //            var fd1 = new FileDescriptor(this._path, this._vectorLength, this.StartIndex, resultSegments.Item1, this._clsMaker);
    //            var fd2 = new FileDescriptor(this._path, this._vectorLength, this.StartIndex, resultSegments.Item2, this._clsMaker);
    //            return new Tuple<FileDescriptor, FileDescriptor>(fd1, fd2);
    //        }

    //        private static Tuple<IEnumerable<Tuple<int, int>>, IEnumerable<Tuple<int, int>>> SplitSegment(Tuple<int, int> segment, IEnumerable<Tuple<double, double>> firstBounds, IEnumerable<Tuple<double, double>> secondBounds) {
    //            var count = segment.Item2 - segment.Item1;
    //            var r1 = new List<Tuple<int, int>>();
    //            var r2 = new List<Tuple<int, int>>();
    //            foreach (var set in new[] {
    //                new { ResultSegments = r1, Bounds = firstBounds, },
    //                new { ResultSegments = r2, Bounds = secondBounds, },
    //            }) {
    //                foreach (var b in set.Bounds) {
    //                    var r = new Tuple<int, int>(
    //                        segment.Item1 + (int)Math.Floor(count * b.Item1),
    //                        segment.Item1 + (int)Math.Ceiling(count * b.Item2)
    //                    );
    //                    set.ResultSegments.Add(r);
    //                }
    //            }
    //            return new Tuple<IEnumerable<Tuple<int, int>>, IEnumerable<Tuple<int, int>>>(
    //                r1,
    //                r2
    //            );
    //        }
    //    }

    //    FileDescriptor[] _files = new FileDescriptor[0];
    //    int _vectorLength;
    //    Queue<IVector<TClass>> _queue = new Queue<IVector<TClass>>();
    //    Func<string, TClass> _clsMaker = null;

    //    public StreamColumnsVectorsProvider(string path, int vectorLength, Func<string, TClass> clsMaker) {
    //        if (string.IsNullOrWhiteSpace(path))
    //            throw new ArgumentException("The \"path\" argument must not be empty.");
    //        if (vectorLength <= 0)
    //            throw new ArgumentException("The \"vectorLength\" argument must be > 0.");
    //        if (clsMaker == null)
    //            throw new ArgumentNullException("clsMaker");
    //        this._clsMaker = clsMaker;
    //        this._vectorLength = vectorLength;
    //        var files = Directory.GetFiles(path);
    //        this._files = new FileDescriptor[files.Length];
    //        long startIndex = 0;
    //        for (int i = 0; i < files.Length; i++) {
    //            this._files[i] = new FileDescriptor(files[i], this._vectorLength, startIndex, this._clsMaker);
    //            startIndex += this._files[i].TotalRows * this._files[i].Classes.Length;
    //        }
    //    }

    //    protected StreamColumnsVectorsProvider(List<FileDescriptor> files, int vectorLength) {
    //        this._files = files.ToArray();
    //        this._vectorLength = vectorLength;
    //    }

    //    public IVector<TClass> Next() {
    //        if (!this._queue.Any())
    //            this._fillQueue();
    //        return this._queue.Dequeue();
    //    }

    //    private void _fillQueue() {
    //        var vectorsCount = 10;
    //        foreach (var file in this._files) {
    //            var vectors = file.GetVectors(vectorsCount);
    //            foreach (var vector in vectors) {
    //                this._queue.Enqueue(vector);
    //            }
    //        }
    //    }

    //    public void Reset() {
    //        this._queue.Clear();
    //        foreach (var file in this._files) {
    //            file.Reset();
    //        }
    //    }

    //    public Tuple<IProvider<IVector<TClass>>, IProvider<IVector<TClass>>> Split(double part, double startAt = 0) {
    //        var resultFiles = new Tuple<List<FileDescriptor>, List<FileDescriptor>>(
    //            new List<FileDescriptor>(),
    //            new List<FileDescriptor>()
    //        );
    //        foreach (var file in this._files) {
    //            var fs = file.Split(part, startAt);
    //            resultFiles.Item1.Add(fs.Item1);
    //            resultFiles.Item2.Add(fs.Item2);
    //        }
    //        var result = new Tuple<IProvider<IVector<TClass>>, IProvider<IVector<TClass>>>(
    //            new StreamColumnsVectorsProvider<TClass>(resultFiles.Item1, this._vectorLength),
    //            new StreamColumnsVectorsProvider<TClass>(resultFiles.Item2, this._vectorLength)
    //        );
    //        return result;
    //    }

    //    public IProvider<IVector<TClass>> GetProbabilitySample(double probability) {
    //        throw new NotImplementedException();
    //    }
    //}
}
