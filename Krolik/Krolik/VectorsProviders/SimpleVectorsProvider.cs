﻿using Krolik.Classifiers;
using Krolik.Exceptions;
using Krolik.Mathematics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Krolik.VectorsProviders {
    /// <summary>
    /// Opens one file with the given name and reads lines from this file.
    /// Each line corresponds to one vector. Each line may contain the index
    /// of the vectors' class.
    /// </summary>
    internal class SimpleVectorsProvider 
        : IVectorsProvider
        , ISamplable<IHybridVector> 
    {
        private Dictionary<Cls, IList<IHybridVector>> _classes = new Dictionary<Cls, IList<IHybridVector>>();
        private Cls _currentClass;
        private int _currentVectorIndex = -1;

        private IVectorSignature _sgn;

        public IPrecisionProvider PrecisionProvider { get; private set; }

        protected SimpleVectorsProvider(IPrecisionProvider precision, Dictionary<Cls, IList<IHybridVector>> classes) {
            KrolikException.CheckArgumentNotNull(precision, nameof(precision));
            KrolikException.CheckArgumentNotNull(classes, nameof(classes));
            if (classes.Count == 0)
                throw new ArgumentException($"The given argument \"{nameof(classes)}\" must contain at least 1 class.");
            this.PrecisionProvider = precision;
            this._classes = classes;
            this.Reset();
        }

        /// <summary>
        /// Opens one file with the given name and reads lines from this file.
        /// Each line corresponds to one vector. Each line may contain the index
        /// of the vectors' class.
        /// </summary>
        /// <param name="path">Path to the file with vectors.</param>
        /// <param name="vectorsSeparator"></param>
        /// <param name="valuesSeparator"></param>
        /// <param name="classColumnIndex">Zero-based index of the class pointer.</param>
        public SimpleVectorsProvider(string path, IVectorSignature signature, IPrecisionProvider precision, IEnumerable<string> vectorsSeparator = null, IEnumerable<string> valuesSeparator = null, int classColumnIndex = -1) {
            if (string.IsNullOrWhiteSpace(path))
                throw new Exception("The given path must not be null or empty.");
            KrolikException.CheckArgumentNotNull(precision, "precision");
            KrolikException.CheckArgumentNotNull(signature, "signature");
            if (vectorsSeparator == null)
                vectorsSeparator = new List<string> { "\n", };
            if (valuesSeparator == null || valuesSeparator.All(x => string.IsNullOrWhiteSpace(x)))
                valuesSeparator = new List<string> { " ", "\t", };
            if (!File.Exists(path))
                throw new NotSupportedDataException($"The file \"{path}\" does not exist.");
            this._sgn = signature;
            this.PrecisionProvider = precision;
            string content = null;
            using (var sr = new StreamReader(path)) {
                content = sr.ReadToEnd();
            }
            if (string.IsNullOrWhiteSpace(content))
                throw new NotSupportedDataException($"The file \"{path}\" is empty.");
            var linesSplitPattern = string.Join("|", vectorsSeparator);
            var valuesSplitPattern = string.Join("|", valuesSeparator);
            var vectorLength = -1;
            var lines = content.Split(vectorsSeparator.ToArray(), StringSplitOptions.RemoveEmptyEntries);// Regex.Split(content, linesSplitPattern);
            long index = 0;
            foreach (var line in lines) {
                var vals = line.Split(valuesSeparator.ToArray(), StringSplitOptions.RemoveEmptyEntries)// Regex.Split(line, valuesSplitPattern)
                    .Select(x1 => x1.Trim())
                    .ToArray();
                if (vals.Length <= 1)
                    throw new NotSupportedDataException($"All vectors from the file \"{path}\" must have exactly 1 class pointer and at least 1 value.");
                if (vectorLength < 0) {
                    vectorLength = vals.Length - 1;
                    if (classColumnIndex < 0)
                        classColumnIndex = vals.Length - 1;
                } else if (vectorLength != vals.Length - 1)
                    throw new NotSupportedDataException($"All vectors from the file \"{path}\" must have equal number of values.");
                var components = vals
                    .Take(classColumnIndex)
                    .Concat(vals.Skip(classColumnIndex + 1))
                    .ToArray();
                var clsPointer = vals[classColumnIndex];
                var clsPointerTyped = this._sgn.DeserializeClass(clsPointer);
                var vector = HybridVectorsFactory.DefaultFactory.Make(
                    index++, 
                    components,
                    clsPointerTyped,
                    this._sgn,
                    this.PrecisionProvider
                );
                if (this._classes.ContainsKey(clsPointerTyped))
                    this._classes[clsPointerTyped].Add(vector);
                else
                    this._classes[clsPointerTyped] = new List<IHybridVector> { vector, };
            }
            if (!this._classes.Any())
                throw new NotSupportedDataException($"The file \"{path}\" is empty.");
            this.Reset();
        }

        public IHybridVector Current() {
            return this._classes[this._currentClass][this._currentVectorIndex];
        }

        #region Implementation of IProvider

        public void Reset() {
            this._currentClass = this._classes.Keys.First();
            this._currentVectorIndex = 0;
        }

        public Tuple<IProvider<IHybridVector>, IProvider<IHybridVector>> Split(float part, float startAt = 0) {
            if (part <= 0 || part >= 1)
                throw new ArgumentException("\"part\" must be > 0 and < 1.");
            if (startAt < 0 || startAt > 1)
                throw new ArgumentException("\"startAt\" must be >= 0 and <= 1.");
            var first = new Dictionary<Cls, IList<IHybridVector>>();
            var second = new Dictionary<Cls, IList<IHybridVector>>();
            foreach (var key in this._classes.Keys) {
                first[key] = new List<IHybridVector>();
                second[key] = new List<IHybridVector>();
            }
            var firstBounds = new List<Tuple<float, float>>();
            var secondBounds = new List<Tuple<float, float>>();

            var firstFinal = startAt + part;
            if (firstFinal > 1) {
                firstFinal -= 1;
                firstBounds.Add(new Tuple<float, float>(0, firstFinal));
                firstBounds.Add(new Tuple<float, float>(startAt, 1));
                secondBounds.Add(new Tuple<float, float>(firstFinal, startAt));
            } else {
                secondBounds.Add(new Tuple<float, float>(0, startAt));
                secondBounds.Add(new Tuple<float, float>(firstFinal, 1));
                firstBounds.Add(new Tuple<float, float>(startAt, firstFinal));
            }

            foreach (var pair in this._classes) {
                foreach (var tp in new[] {
                    new { Dict = first, Bounds = firstBounds, },
                    new { Dict = second, Bounds = secondBounds, }
                }) {
                    foreach (var bound in tp.Bounds) {
                        foreach (var vector in pair.Value
                            .Skip((int)Math.Floor(bound.Item1 * pair.Value.Count))
                            .Take((int)Math.Ceiling((bound.Item2 - bound.Item1) * pair.Value.Count))
                        ) {
                            tp.Dict[pair.Key].Add(vector);
                        }
                    }
                }
            }
            var result = new Tuple<IProvider<IHybridVector>, IProvider<IHybridVector>>(
                new SimpleVectorsProvider(this.PrecisionProvider, first),
                new SimpleVectorsProvider(this.PrecisionProvider, second)
            );
            return result;
        }

        IHybridVector IProvider<IHybridVector>.Next() {
            var currentClassVectors = this._classes[this._currentClass];
            //var cycle = false;
            if (this._currentVectorIndex + 1 >= currentClassVectors.Count) {
                var keys = this._classes.Keys.ToArray();
                for (int i = 0; i < keys.Length; i++) {
                    if (keys[i].Equals(this._currentClass)) {
                        this._currentClass = keys[(i + 1) % keys.Length];
                        //if (i + 1 >= keys.Length)
                        //    cycle = true;
                        break;
                    }
                }
                this._currentVectorIndex = 0;
            } else {
                this._currentVectorIndex++;
            }
            var result = this._classes[this._currentClass][this._currentVectorIndex];
            return result;
        }

        public IList<IHybridVector> GetAll() {
            return this._classes
                .SelectMany(x => x.Value)
                .ToList();
        }

        #endregion Implementation of IProvider

        #region Implementation of ISamplable

        public IProvider<IHybridVector> GetProbabilitySample(float probability) {
            if (probability <= 0 || probability > 1)
                throw new ArgumentException(nameof(probability) + " must be in (0; 1].");
            var classes = new Dictionary<Cls, IList<IHybridVector>>();
            foreach (var cls in this._classes) {
                var vectors = new List<IHybridVector>();
                foreach (var vector in cls.Value) {
                    var r = Convert.ToDouble(Cil.Common.Rand.NextDouble());
                    if (r > probability)
                        continue;
                    vectors.Add(vector);
                }
                classes[cls.Key] = vectors;
            }
            var result = new SimpleVectorsProvider(this.PrecisionProvider, classes);
            return result;
        }

        #endregion Implementaion of ISamplable

        #region Implementation of IDisposable

        private bool _isDisposed = false;

        public void Dispose() {
            if (this._isDisposed)
                return;
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable
    }
}
