﻿using System.Collections.Generic;

namespace Krolik.Main {
    /// <summary>
    /// Contains functionality specific for GPNs and other supported
    /// classification algorithms.
    /// </summary>
    public interface IKrolikEnvironment {
        IGraphForVisualization GetGraphForVisualization(string variableName);
        IEnumerable<IVectorForVisualization> GetVectorsForVisualization(
            string variableName,
            bool isRealData
        );
    }

    public interface IGraphForVisualization {
        List<IEnumerable<IVertexForVisualization>> Levels { get; }
    }

    public interface IVectorForVisualization {
        decimal[] ContinuousComponents { get; }
        string[] DiscreteComponents { get; }
        string[] TrueClasses { get; }
        string[] SuggestedClasses { get; }

        /// <summary>
        /// True = this vector is from the set for classification.
        /// False = this vector is from the training set.
        /// </summary>
        bool IsRealData { get; }
    }

    public interface IVertexForVisualization {
        IDictionary<string, string> Info { get; }

        IEnumerable<IVertexForVisualization> Upper { get; }

        IEnumerable<IVertexForVisualization> Lower { get; }

        /// <summary>
        /// Coefficients of the plane corresponding to the vertex.
        /// The value is null for all vertices except the vertices
        /// determing continuous value located at the first level.
        /// </summary>
        decimal[] PlaneCoefficients { get; }

        IEnumerable<string> IsControlFor { get; }
    }
}
