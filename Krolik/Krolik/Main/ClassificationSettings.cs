﻿using Krolik.Classifiers;
using Krolik.Exceptions;
using Krolik.Infrastructure;
using System;
using System.Linq;
using System.Xml.Linq;

namespace Krolik.Main {
    internal class ClassificationSettingsFactory {
        private Cil.Design.IFileSystemApi _fileSystem;
        private ClassificationInterpretersFactory _interpretersFactory;
        private FuzzyClassificationAlgorithmFactory _algorithmFactory;

        public ClassificationSettingsFactory(
            Cil.Design.IFileSystemApi fileSystem,
            ClassificationInterpretersFactory interpretersFactory,
            FuzzyClassificationAlgorithmFactory algorithmFactory
        ) {
            if (fileSystem == null)
                throw new ArgumentNullException(nameof(fileSystem));
            if (interpretersFactory == null)
                throw new ArgumentNullException(nameof(interpretersFactory));
            if (algorithmFactory == null)
                throw new ArgumentNullException(nameof(algorithmFactory));

            this._fileSystem = fileSystem;
            this._interpretersFactory = interpretersFactory;
            this._algorithmFactory = algorithmFactory;
        }

        public ClassificationSettings Make(string pathToConfigurationFile = null) {
            if (pathToConfigurationFile == null)
                return this._makeDefault();
            var xml = System.Xml.Linq.XElement.Parse(this._fileSystem.ReadAllText(pathToConfigurationFile));
            var interpretersNode = xml.Element("classificationResultInterpreters");
            var interpreterNode = interpretersNode.Element("classificationResultInterpreter");
            var interpreter = this._makeInterpreter(interpreterNode);
            var algorithmsNode = xml.Element("fuzzyClassificationAlgorithms");
            var algorithmNode = algorithmsNode.Element("fuzzyClassificationAlgorithm");
            var algorithm = this._makeAlgorithm(algorithmNode);
            return new ClassificationSettings(interpreter, algorithm);
        }

        private FuzzyClassificationAlgorithm _makeAlgorithm(XElement algorithmNode) {
            var type = algorithmNode.Attribute("type").Value;
            FuzzyClassificationAlgorithm result;
            switch (type) {
                case "topSuspiciousDirtyMinusOthers":
                    result = this._algorithmFactory.TopSuspiciousDirtyMinusOthers();
                    break;
                case "topClearRelative":
                    result = this._algorithmFactory.TopClearRelative();
                    break;
                case "accumulativeSubset":
                    result = this._algorithmFactory.AccumulativeSubset();
                    break;
                case "accumulativeSuperset":
                    result = this._algorithmFactory.AccumulativeSuperset();
                    break;
                case "topClearRelativeWithEnforcement":
                    result = this._algorithmFactory.TopClearRelativeWithEnforcement();
                    break;
                default:
                    throw new WrongConfigurationFileException();
            }
            return result;
        }

        private IFuzzyClassificationInterpreter _makeInterpreter(XElement interpreterNode) {
            IFuzzyClassificationInterpreter result;
            var type = interpreterNode.Attribute("type").Value;
            switch (type) {
                case "chain":
                    var childrenInterpretersNodes = interpreterNode.Elements("classificationResultInterpreter");
                    var childrenInterpreters = childrenInterpretersNodes.Select(n => this._makeInterpreter(n));
                    result = childrenInterpreters.Aggregate((a, b) => a.FollowBy(b));
                    break;
                case "absoluteTrashold":
                    var absTrashold = interpreterNode.Element("trashold").Value.ToDecimal();
                    if (absTrashold == null)
                        throw new WrongConfigurationFileException();
                    result = this._interpretersFactory.AbsoluteTrashold(absTrashold.Value);
                    break;
                case "relativeTrashold":
                    var relTrashold = interpreterNode.Element("trashold").Value.ToDecimal();
                    if (relTrashold == null)
                        throw new WrongConfigurationFileException();
                    result = this._interpretersFactory.RelativeTrashold(relTrashold.Value);
                    break;
                case "relative":
                    var ratio = interpreterNode.Element("ratio").Value.ToDecimal();
                    if (ratio == null)
                        throw new WrongConfigurationFileException();
                    result = this._interpretersFactory.Relative(ratio.Value);
                    break;
                default:
                    throw new WrongConfigurationFileException();
            }
            return result;
        }

        private ClassificationSettings _makeDefault() {
            return new ClassificationSettings(
                this._interpretersFactory.Relative(2m),
                this._algorithmFactory.TopSuspiciousDirtyMinusOthers()
            );
        }
    }

    internal class ClassificationSettings {
        public IFuzzyClassificationInterpreter Interpreter { get; private set; }
        public FuzzyClassificationAlgorithm Algorithm { get; private set; }

        public ClassificationSettings(
            IFuzzyClassificationInterpreter interpreter,
            FuzzyClassificationAlgorithm algorithm
        ) {
            if (interpreter == null)
                throw new ArgumentNullException(nameof(interpreter));
            if (algorithm == null)
                throw new ArgumentNullException(nameof(algorithm));

            this.Interpreter = interpreter;
            this.Algorithm = algorithm;
        }
    }
}
