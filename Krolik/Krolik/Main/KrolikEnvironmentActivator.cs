﻿using Krolik.Classifiers.GpnClassifier;
using Krolik.Exceptions;
using Krolik.Gpns;
using Krolik.Mathematics;
using Krolik.TypeSystem;
using System;
using Krolik.Classifiers;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("KrolikConsole")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("KrolikNunit")]
// For Moq library.
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Krolik.Main {
    public class KrolikEnvironmentActivator {
        public IEnvironment CreateEnvironment() {
            var di = new Grace.DependencyInjection.DependencyInjectionContainer();
            this._configureDi(di);
            var environment = di.Locate<IEnvironment>();
            return environment;
        }

        private void _configureDi(Grace.DependencyInjection.DependencyInjectionContainer di) {
            di.Configure(c => c.Export<MyEnvironmnet>().As<IEnvironment>());

            di.Configure(c => c.Export<Cil.Design.FileSystemApi>().As<Cil.Design.IFileSystemApi>());

            //di.Configure(c => c
            //    .Export<StandardSequentialGpnFounderStrategy>()
            //    //.Export<ConcentratedPlanesSequentialGpnFounderStrategy>()
            //    //.Export<MultiConcentratedPlanesSequentialGpnFounderStrategy>()
            //    .As<ISequentialGpnFounderStrategy>()
            //);
            di.Configure(c => c.Export<GpnFoundersFactory>());
            di.Configure(c => c.Export<StandardSequentialGpnFounderStrategy>());
            di.Configure(c => c.Export<MultiConcentratedPlanesSequentialGpnFounderStrategy>());

            di.Configure(c => c.ExportFactory(() => LinearAlgebra.Get()));
            di.Configure(c => c.Export<CalculatorProvider>().As<ICalculatorProvider>());
            di.Configure(c => c.ExportInstance<IFuzzyLogic>(Fuzzy.Logic));

            di.Configure(c => c.Export<SequentialGpnFounder>());
            di.Configure(c => c.Export<CartesianGpnFounder>());

            di.Configure(c => c.Export<LeftInverseFuzzySet>());
            di.Configure(c => c.Export<LeftTriangleFuzzySet>());
            di.Configure(c => c.Export<LeftDiscreteFuzzySet>());

            di.Configure(c => c.Export<ClassificationSettingsFactory>());
            di.Configure(c => c.Export<ClassificationInterpretersFactory>());

            di.Configure(c => c.Export<ConcentratedPlanesFactory>());
            di.Configure(c => c.Export<MultiConcentratedPlanesFactory>());

            di.Configure(c => c.Export<FuzzySetFactory>().As<IFuzzySetFactory>());


            di.Configure(c => c
                .Export<CustomGpnClassifiersSerializer>()
                //.Export<StandardGpnClassifiersSerializer>()
                .As<IGpnClassifiersSerializer>()
            );

            di.Configure(c => c.ExportInstance(
                    (Func<IGpnFounder, IFuzzySet, IActivationStrategy, IActivationStrategy, GpnClassifier>)
                ((
                    gpnFounder,
                    fuzzySet,
                    activationStrategy1,
                    activationStrategy2
                ) => new GpnClassifier(
                    gpnFounder,
                    fuzzySet,
                    activationStrategy1,
                    activationStrategy2
                ))
            ));
            di.Configure(c => c.Export<GpnClassifierFactory>().As<IGpnClassifierFactory>());
        }

        private class GpnClassifierFactory : IGpnClassifierFactory {
            private readonly Func<IGpnFounder, IFuzzySet, IActivationStrategy, IActivationStrategy, GpnClassifier> _commonClassifierFactory;
            private readonly GpnFoundersFactory _foundersFactory;

            public GpnClassifierFactory(
                Func<IGpnFounder, IFuzzySet, IActivationStrategy, IActivationStrategy, GpnClassifier> commonClassifierFactory,
                GpnFoundersFactory foundersFactory
            ) {
                KrolikException.CheckArgumentNotNull(commonClassifierFactory, nameof(commonClassifierFactory));
                KrolikException.CheckArgumentNotNull(foundersFactory, nameof(foundersFactory));

                this._commonClassifierFactory = commonClassifierFactory;
                this._foundersFactory = foundersFactory;
            }

            public GpnClassifier Make(
                string founderType,
                string classificationActivationStrategy,
                IFuzzySet fuzzySet
            ) {
                var founder = this._foundersFactory.Get(founderType);
                Gpns.IActivationStrategy standardActivationStrategy = new Gpns.ClassicActivationStrategy(
                    new CustomPrecisionProvider((decimal)1e-9, 1)
                );
                Gpns.IActivationStrategy fuzzyActivationStrategy = new Gpns.FuzzyAndActivationStrategy(
                    new CustomPrecisionProvider((decimal)1e-9, 1),
                    fuzzySet
                );
                return this._commonClassifierFactory(
                    founder, 
                    fuzzySet, 
                    standardActivationStrategy,
                    classificationActivationStrategy == "fuzzyAnd"
                        ? fuzzyActivationStrategy
                        : standardActivationStrategy
                );
            }
        }

        private class FuzzySetFactory : IFuzzySetFactory {
            private readonly Func<decimal, LeftInverseFuzzySet> _invFuzzyFactory;
            private readonly Func<decimal, LeftTriangleFuzzySet> _triFuzzyFactory;

            public FuzzySetFactory(
                Func<decimal, LeftInverseFuzzySet> invFuzzyFactory,
                Func<decimal, LeftTriangleFuzzySet> triFuzzyFactory
            ) {
                KrolikException.CheckArgumentNotNull(invFuzzyFactory, nameof(invFuzzyFactory));
                KrolikException.CheckArgumentNotNull(triFuzzyFactory, nameof(triFuzzyFactory));

                this._invFuzzyFactory = invFuzzyFactory;
                this._triFuzzyFactory = triFuzzyFactory;
            }

            public IFuzzySet Make(string fuzzyType, decimal fuzzyParameter) {
                var fuzziness = fuzzyType == "simpleInverse"
                    ? (IFuzzySet)this._invFuzzyFactory(fuzzyParameter)
                    : this._triFuzzyFactory(fuzzyParameter);
                return fuzziness;
            }
        }
    }
}
