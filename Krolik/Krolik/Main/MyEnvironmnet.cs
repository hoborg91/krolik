﻿using Cil.Common;
using Krolik.CoreTypes;
using Krolik.TypeSystem;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik.Main {
    internal class MyEnvironmnet : IEnvironment, IKrolikEnvironment {
        private Dictionary<string, IObject> _objects = new Dictionary<string, IObject>();
        public IObject this[string name] {
            get {
                return this._objects.ContainsKey(name)
                    ? this._objects[name]
                    : null;
            }

            set {
                this._objects[name] = value;
            }
        }

        public IDictionary<string, IType> Types { get; set; }

        public MyEnvironmnet(CoreTypes.CoreTypes coreTypes) {
            this.Types = new Dictionary<string, IType>() {
                { "Gpn", coreTypes.Gpn },
                { "DataSource", coreTypes.DataSource },
            };
        }

        [Obsolete("This method is a workaround for instantiating primitive " +
            "types not presented in the type system. Corresponding primitive " +
            "type should be created instead of using this method.")]
        public IObject MakeObject<TContent>(IType type, TContent content) {
            return new MyObject<TContent>(type, content);
        }

        // TODO. Is this method necessary? Other ways?
        public ICallContext MakeContext(IObject t, IReadWrite<string> cmdLine, IObject[] pos, Dictionary<string, IObject> nam) {
            return new CallContext(t, cmdLine, pos, nam);
        }

        #region Implementation of IKrolikEnvironment

        public IGraphForVisualization GetGraphForVisualization(string variableName) {
            var graphVar = this[variableName] as MyObject<Krolik.Classifiers.GpnClassifier.GpnClassifier>;
            if (graphVar == null)
                return null;
            var gpn = graphVar.Content.Network;
            var vertices = gpn.Projection(
                x => {
                    var rwpc = x as IReceptorWithPlaneComponents;
                    return new VertexForVisualization {
                        Info = new Dictionary<string, string> {
                            { "ToString", x.ToString() },
                            { "K", x.K().ToString() },
                            { "M", string.Join(
                                ",",
                                x.Ms().Select(x1 => x1.Key.Index + "=" + x1.Value)
                                ) },
                            { "CtrlFor", string.Join(",", x.IsControlFor().Select(x1 => x1.Index)) },
                            { "Active", x.Active.ToString() },
                        },
                        Level = x.K(),
                        PlaneCoefficients = rwpc?.PlaneComponents,
                        IsControlForList = x.IsControlFor()
                            .Select(x1 => x1.ToString())
                            .ToList(),
                    };
                },
                (container, toBeAdded) => container.UpperList.Add(toBeAdded),
                (container, toBeAdded) => container.LowerList.Add(toBeAdded)
            );

            var result = new GraphForVisualization {
                Levels = vertices
                    .GroupBy(x => x.Level)
                    .OrderBy(x => x.Key)
                    .Select(x => (IEnumerable<IVertexForVisualization>)x.ToList())
                    .ToList(),
            };
            return result;
        }

        public IEnumerable<IVectorForVisualization> GetVectorsForVisualization(
            string variableName,
            bool isRealData
        ) {
            var dsObject = this[variableName] as MyObject<Krolik.VectorsProviders.IVectorsProvider>;
            if (dsObject == null)
                return null;
            var ds = dsObject.Content;
            var vectors = ds.GetAll();
            var result = vectors
                .Select(x => new VectorForVisualization(
                    x.ContinuumComponents,
                    x.DiscreteComponents
                        .Select(c => c.ToString())
                        .ToArray(),
                    x.Classes.Select(cls => cls.ToString()).ToArray(),
                    ClassificationResultsHolder.ClassesByVectors.ContainsKey(x)
                        ? ClassificationResultsHolder.ClassesByVectors[x].Select(c => c.ToString()).ToArray()
                        : new string[0],
                    isRealData)
                )
                .ToList();
            return result;
        }

        private class VectorForVisualization : IVectorForVisualization {
            public string[] TrueClasses { get; set; }
            public string[] SuggestedClasses { get; set; }
            public decimal[] ContinuousComponents { get; set; }
            public string[] DiscreteComponents { get; set; }
            public bool IsRealData { get; set; }

            public VectorForVisualization(
                decimal[] continuousComponents,
                string[] discreteComponents,
                string[] trueClasses,
                string[] suggestedClasses,
                bool isRealData
            ) {
                this.ContinuousComponents = continuousComponents;
                this.DiscreteComponents = discreteComponents;
                this.TrueClasses = trueClasses;
                this.SuggestedClasses = suggestedClasses;
                this.IsRealData = isRealData;
            }
        }

        private class VertexForVisualization : IVertexForVisualization {
            public IDictionary<string, string> Info { get; set; }
            public IEnumerable<IVertexForVisualization> Lower { get { return LowerList; } }
            public IEnumerable<IVertexForVisualization> Upper { get { return UpperList; } }
            public decimal[] PlaneCoefficients { get; internal set; }
            public IEnumerable<string> IsControlFor {
                get {
                    return IsControlForList;
                }
            }

            public int Level { get; set; }
            public List<IVertexForVisualization> LowerList { get; set; } = new List<IVertexForVisualization>();
            public List<IVertexForVisualization> UpperList { get; set; } = new List<IVertexForVisualization>();
            public List<string> IsControlForList { get; set; } = new List<string>();
        }

        private class GraphForVisualization : IGraphForVisualization {
            public List<IEnumerable<IVertexForVisualization>> Levels { get; set; }

            public override int GetHashCode() {
                if (this.Levels == null)
                    return 0;
                var result = 0;
                foreach (var level in this.Levels) {
                    var vc = 0;
                    foreach (var vertex in level) {
                        result ^=
                            vertex.Info.OrderBy(x => x.Key)
                                .Select(x => x.Value)
                                .Aggregate(0, (sum, add) => sum ^= add.GetHashCode())
                            + vertex.Lower.Count()
                            + vertex.Upper.Count();
                        vc++;
                    }
                    result ^= vc;
                }
                return result;
            }
        }

        #endregion

        #region Implementation of IDisposable
        
        private bool _isDisposed = false;
        
        public void Dispose() {
            if (this._isDisposed)
                return;
            foreach (var obj in this._objects.Values) {
                obj.Dispose();
            }
            this._isDisposed = true;
        }

        #endregion Implementation of IDisposable
    }
}
