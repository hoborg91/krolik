﻿using Krolik.Classifiers;
using Krolik.Mathematics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik {
    internal interface IVertexForBuilding {
        //bool IsActive { get; set; }
        IEnumerable<IVertex> GetZeroSuperset();
        void RemoveVromZeroSuperset(params IConceptor[] vertices);
        void AddToZeroSuperset(params IConceptor[] vertices);
        Certainty Active { get; set; }
    }

    internal interface IVertexForTeaching {
        int K();
        int M(Cls vectorCls); // TODO. Make indexer instead of two M() methods.
        void M(Cls vectorCls, int value);
        IDictionary<Cls, int> Ms();
        /// <summary>
        /// Marks the vertex as control for the given class.
        /// Returns true, if the vertex was NOT already marked as control.
        /// Otherwise, false.
        /// </summary>
        bool MarkAsControlFor(Cls vectorCls);
        IEnumerable<Cls> IsControlFor();
        IEnumerable<IVertex> GetSuperset();
        IEnumerable<IReceptor> GetUnderlyingReceptors();
    }

    internal interface IVertex 
        : IVertexForBuilding
        , IVertexForTeaching {
        
    }

    //public interface IFuzzyActiveVertex : IVertex {
    //    decimal FuzzyActive { get; set; }
    //}

    internal interface IReceptor : IVertex {
        // TODO. Refactor: it seems, that it is not a receptor's
        // responsibility to calculate certanty. It shoud calculate
        // just a distance to the given vector. But what about
        // discrete receptors? They have no distance...
        Certainty CorrespondsTo(IHybridVector vector, IPrecisionProvider precision, IFuzzySet fuzziness);
    }

    //public interface IFuzzyReceptor : IFuzzyActiveVertex {
    //    decimal FuzzyCorrespondsTo(IHybridVector vector, IPrecisionProvider precision, IFuzzySet fuzziness);
    //}

    internal interface IReceptorWithPlaneComponents {
        decimal[] PlaneComponents { get; }
    }

    internal interface IConceptor : IVertex {
        IEnumerable<IVertex> GetZeroSubset();
        void RemoveVromZeroSubset(params IVertex[] vertices);
        void AddToZeroSubset(params IVertex[] vertices);
    }

    internal static class IVertexExtensions {
        public static bool IsControlFor(this IVertex vertex, Cls cls) {
            return vertex.IsControlFor().Contains(cls);
        }

        public static IEnumerable<IVertex> GetSubset(this IConceptor conceptor) {
            var result = new List<IVertex>();
            foreach (var subVertex in conceptor.GetZeroSubset()) {
                result.Add(subVertex);
                var subConceptor = subVertex as IConceptor;
                if (subConceptor == null)
                    continue;
                result.AddRange(subConceptor.GetSubset());
            }
            return result.Distinct();
        }

        public static bool IsActive(this IVertexForBuilding vertex) {
            return vertex.Active == Certainty.Absolute;
        }
    }
}