﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Krolik {
    internal class KrolikGpnProjectOptions {
        public string Path { get; set; }
        public LoadMode Mode { get; set; }
        public int ClassColumnIndex { get; set; }
        public int VectorLength { get; set; }
        public string ValuesSeparator { get; set; }

        public IVectorSignature Signature { get; set; }

        public decimal Precision { get; set; }
        public decimal Divisor { get; set; }

        public static Dictionary<LoadMode, IList<string>> GetLoadModes() {
            var loadModeType = typeof(LoadMode);
            if (!loadModeType.IsEnum)
                throw new Exception("It seems that type 'LoadMode' is not an enum.");
            var modeNames = loadModeType.GetEnumNames();
            var fields = loadModeType.GetFields();
            var result = new Dictionary<LoadMode, IList<string>>();
            foreach (var modeName in modeNames) {
                var abbrAttr = fields
                    .Single(x => x.Name == modeName)
                    .GetCustomAttributes(true)
                    .OfType<LoadModeAbbriviationAttribute>()
                    .SingleOrDefault();
                if (abbrAttr == null)
                    continue;
                result[(LoadMode)Enum.Parse(loadModeType, modeName)] =
                    abbrAttr.Abbriviations;
            }
            if (result
                .SelectMany(x => x.Value)
                .Any(x => result.Count(p => p.Value.Contains(x)) != 1)
            )
                throw new Exception("Load mode abbriviations must be unique.");
            return result;
        }

        public enum LoadMode {
            [LoadModeAbbriviation("", "s", "simple")]
            Simple,
            [LoadModeAbbriviation("m")]
            MultipleFiles,
        }
    }

    /// <summary>
    /// Defines abbriviations corresponging the marked load mode.
    /// </summary>
    internal class LoadModeAbbriviationAttribute : Attribute {
        private string[] _abbriviations;

        /// <summary>
        /// Defines abbriviations corresponging the marked load mode.
        /// </summary>
        public LoadModeAbbriviationAttribute(params string[] abbriviations) {
            this.Abbriviations = abbriviations;
        }

        public string[] Abbriviations {
            get {
                return this._abbriviations;
            }
            set {
                this._abbriviations = value == null
                    ? new string[0]
                    : value.Distinct().ToArray();
            }
        }
    }
}