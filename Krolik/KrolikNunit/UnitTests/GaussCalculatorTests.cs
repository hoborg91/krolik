﻿using Krolik.Mathematics;
using NUnit.Framework;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class GaussCalculatorTests { //: CommonUnitTests<CalculatorProvider.GaussCalculator> {
        private CalculatorProvider.GaussCalculator _sut() {
            // TODO. Mock dependency?
            return new CalculatorProvider.GaussCalculator(new CalculatorProvider());
        }

        [Test]
        public void TestDeterminantCalculations() {
            // Arrange.
            var calculator = this._sut();
            var matrix = new decimal[] { -10, 30, -10, 0, };
            var dimension = 2;

            // Act.
            var determinant = calculator.CalculateDeterminant(
                matrix, 
                dimension,
                CustomPrecisionProvider.GetDefaultProvider()
            );

            // Assert.
            Assert.AreEqual(300, determinant);
        }
    }
}
