﻿using Cil.Common;
using Cil.Design;
using Krolik;
using Krolik.Classifiers;
using Krolik.Mathematics;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace KrolikNunit.UnitTests.Helpers {
    internal class HybridVectorsMocks {
        internal Lazy<VectorsComparer> Comparer = new Lazy<VectorsComparer>(() =>
            new VectorsComparer());

        internal IVectorSignature GetContinuousSignature(int continuousComponentsCount) {
            var result = new VectorSignature(Enumerable.Range(0, continuousComponentsCount)
                .Select(x => (IComponentType)(new ContinuousComponentType(x.ToString())))
                .ToArray()
            );
            return result;
        }

        internal IHybridVector GetContinuousVector(
            IVectorSignature sgn,
            long index, 
            Cls cls, 
            decimal[] components
        ) {
            if (components == null)
                throw new ArgumentNullException(nameof(components));

            var mock = new Mock<IHybridVector>();
            mock.Setup(x => x.Index).Returns((Idx)index);
            mock.Setup(x => x.Classes).Returns(new[] { cls, });
            mock.Setup(x => x.ContinuumComponents).Returns(components);
            mock.Setup(x => x.DiscreteComponents).Returns(new DiscreteValue[0]);

            mock.Setup(x => x.ToString()).Returns(_vectorToString(sgn, mock.Object));

            return mock.Object;
        }

        private string _vectorToString(IVectorSignature sgn, IHybridVector vector) {
            var result = new System.Text.StringBuilder();
            int takeDiscr = 3, takeCont = 5;
            int i = 0;
            var dcts = sgn.Components.OfType<DiscreteComponentType>().ToArray();
            var ccts = sgn.Components.OfType<ContinuousComponentType>().ToArray();
            foreach (var dv in vector.DiscreteComponents.Take(takeDiscr)) {
                result.Append(dcts[i++].Serialize(dv) + " ");
            }
            if (vector.DiscreteComponents.Length > takeDiscr)
                result.Append("... ");
            else
                result.Append("");
            i = 0;
            foreach (var cv in vector.ContinuumComponents.Take(takeCont)) {
                result.Append(ccts[i++].Serialize(cv) + " ");
            }
            if (vector.ContinuumComponents.Length > takeCont)
                result.Append("... ");
            else
                result.Append("");
            if (vector.Classes.Any())
                result.Append(" cls{" + string.Join(",", vector.Classes.Distinct()) + "}");
            else
                result.Append(" no cls");
            return result.ToString();
        }

        internal IPrecisionProvider GetPrecision(decimal? precision = null) {
            var precisionMock = new Mock<IPrecisionProvider>();
            precisionMock.SetupGet(x => x.Divisor)
                .Returns(1);
            if (precision.HasValue) {
                precisionMock.Setup(x => x.IsZero(It.IsAny<decimal>()))
                    .Returns((decimal value) => Math.Abs(value) < precision);
            };
            return precisionMock.Object;
        }

        internal Mock<IFileSystemApi> GetFileSystem(
            List<string> directories,
            List<string> files,
            Dictionary<string, string> filesContents
        ) {
            if (directories == null)
                directories = new List<string>();
            if (files == null)
                files = new List<string>();
            if (filesContents == null)
                filesContents = new Dictionary<string, string>();
            if (filesContents.Any(x => x.Value == null))
                throw new ArgumentException($"Do not supply null values in the '{filesContents}' parameter. Use string.Empty instead.");
            foreach (var fileContent in filesContents) {
                if (!files.Contains(fileContent.Key))
                    files.Add(fileContent.Key);
            }
            var intersection = directories.Intersect(files).ToList();
            if (intersection.Any())
                throw new Exception("The given lists of directories and files contain the same records: " +
                    string.Join(", ", intersection.Select(x => "\"" + x + "\"")));

            var fs = new Mock<IFileSystemApi>();

            // Setup DirectoryExists and FileExists for all given paths.
            fs.Setup(x => x.DirectoryExists(It.Is<string>(p => directories.Contains(p))))
                .Returns(true);
            fs.Setup(x => x.DirectoryExists(It.Is<string>(p => !directories.Contains(p))))
                .Returns(false);
            fs.Setup(x => x.FileExists(It.Is<string>(p => files.Contains(p))))
                .Returns(true);
            fs.Setup(x => x.FileExists(It.Is<string>(p => !files.Contains(p))))
                .Returns(false);

            // Setup GetFiles.
            fs.Setup(x => x.GetFiles(It.IsAny<string>()))
                .Returns((string path) => {
                    return files
                        .Where(x => Path.GetDirectoryName(x) == path)
                        .ToArray();
                });

            // Setup OpenForRead.
            foreach (var fileContent in filesContents) {
                var file = fileContent.Key;
                var lines = fileContent.Value.Split(
                    new[] { Environment.NewLine, },
                    StringSplitOptions.None
                );
                fs.Setup(x => x.OpenForRead(It.Is<string>(p => p == file)))
                    .Returns((string p) => {
                        int r = -1;
                        var reader = new Mock<IStreamReader>();
                        reader.Setup(x => x.ReadLine()).Returns(() => {
                            return r >= lines.Length - 1
                                ? null
                                : lines[++r];
                        });
                        return reader.Object;
                    });
            }

            return fs;
        }

        internal void AssertVectorsSetsAreEquivalent(
            IEnumerable<IHybridVector> vectorsA,
            IEnumerable<IHybridVector> vectorsB) {
            Assert.That(
                vectorsA,
                Is.EquivalentTo(vectorsB).Using((IComparer<IHybridVector>)this.Comparer.Value)
            );
        }

        internal class VectorsComparer : IComparer<IHybridVector>, IEqualityComparer<IHybridVector> {
            public int Compare(IHybridVector x, IHybridVector y) {
                if (object.ReferenceEquals(x, y))
                    return 0;
                if ((object)x == null || (object)y == null)
                    return 1;
                var eqComp = HybridVectorsFactory.DefaultFactory.EqualsComponents(x, y);
                var eqClss = x.Classes.ToList().EqualsAsMultiset(
                    y.Classes.ToList(),
                    (a, b) => a.Equals(b)
                );
                var result = true
                    && eqComp
                    && eqClss
                    ? 0
                    : 1;
                if (result != 0)
                    File.AppendAllText(
                        "Trace.txt", 
                        x.ToString() + " <> " + y.ToString() + " eqComp " + eqComp + " eqClss " + eqClss + Environment.NewLine);
                return result;
            }

            public bool Equals(IHybridVector x, IHybridVector y) {
                var result = Compare(x, y) == 0;
                return result;
            }

            public int GetHashCode(IHybridVector obj) {
                return obj.Index.GetHashCode();
            }
        }
    }
}
