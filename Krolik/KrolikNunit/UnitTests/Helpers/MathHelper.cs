﻿using Krolik.Mathematics;

namespace KrolikNunit.UnitTests.Helpers {
    internal class MathHelper {
        private ILinearAlgebra _linal;

        public ILinearAlgebra Linal {
            get {
                if(this._linal == null) {
                    this._linal = LinearAlgebra.Get();
                }
                return this._linal;
            }
        }
    }
}
