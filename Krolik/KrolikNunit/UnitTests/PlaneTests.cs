﻿using Krolik.Mathematics;
using KrolikNunit.UnitTests.Helpers;
using NUnit.Framework;
using System;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class PlaneTests {// : CommonUnitTests<IPlane> {
        private Lazy<MathHelper> _math = new Lazy<MathHelper>(() => new MathHelper());

        private IPlane _sut() {
            return new LinearAlgebra.Plane(1, -1, 0);
        }

        [Test]
        public void Test_PlaneCalculatesDistanceProperly() {
            // Arrange
            var plane = this._sut();
            var point = new decimal[] { 2, 0, };

            // Act
            var distance = plane.DistanceTo(point);

            // Assert
            Assert.LessOrEqual(0, distance);
            Assert.LessOrEqual(
                Math.Abs(distance - 2 / (decimal)Math.Sqrt(2)),
                1e-10m
            );
        }

        [Test]
        public void Test_PlaneCalculatesProjectionProperly_1() {
            // Arrange
            var plane = this._math.Value.Linal.MakePlane(3, 2, 1);
            var cases = new[] {
                new ProjectionTestCase(new[] { (decimal)0, 0, 0, }, new[] { (decimal)0, 0, 1, }),
                new ProjectionTestCase(new[] { (decimal)0, 0, 1, }, new[] { (decimal)0, 0, 1, }),
                new ProjectionTestCase(new[] { (decimal)0, 0, 2, }, new[] { (decimal)0, 0, 1, }),
                new ProjectionTestCase(new[] { (decimal)-1, -1, 0, }, new[] { (decimal)-1, -1, 1, }),
                new ProjectionTestCase(new[] { (decimal)-1, -1, 1, }, new[] { (decimal)-1, -1, 1, }),
                new ProjectionTestCase(new[] { (decimal)-1, -1, 2, }, new[] { (decimal)-1, -1, 1, }),
            };
            var precision = 1e-10;

            // Act
            foreach (var testCase in cases) {
                var projection = plane.ProjectionOf(testCase.Point);
                var diff = this._math.Value.Linal.Distance(projection, testCase.ExpectedProjection);
                Assert.Less(diff, precision);
            }
        }

        [Test]
        public void Test_PlaneCalculatesProjectionProperly_2() {
            // Arrange
            var plane = this._math.Value.Linal.MakePlane(
                new[] {
                    new[] { (decimal)0, 0, 1, },
                    new[] { (decimal)0, 1, 0, },
                    new[] { (decimal)1, 0, 0, },
                }, new HybridVectorsMocks().GetPrecision((decimal)1e-10)
            );
            var proj1 = (decimal)(1.0 / 3);
            var cases = new[] {
                new ProjectionTestCase(new[] { (decimal)0, 0, 0, }, new[] { proj1, proj1, proj1 }),
                new ProjectionTestCase(new[] { proj1, proj1, proj1, }, new[] { proj1, proj1, proj1, }),
                new ProjectionTestCase(new[] { (decimal)1, 1, 1, }, new[] { proj1, proj1, proj1, }),
                new ProjectionTestCase(new[] { (decimal)1, 2, 3, }, new[] { (decimal)(-2.0 / 3), (decimal)(1.0 / 3), (decimal)(4.0 / 3), }),
            };
            var precision = 1e-10;

            // Act
            foreach (var testCase in cases) {
                var projection = plane.ProjectionOf(testCase.Point);
                var diff = this._math.Value.Linal.Distance(projection, testCase.ExpectedProjection);
                Assert.Less(diff, precision);
            }
        }

        private class ProjectionTestCase {
            public decimal[] Point;
            public decimal[] ExpectedProjection;

            public ProjectionTestCase(decimal[] point, decimal[] expectedProjection) {
                this.Point = point;
                this.ExpectedProjection = expectedProjection;
            }
        }
    }
}
