﻿using Krolik.Mathematics;
using KrolikNunit.UnitTests.Helpers;
using NUnit.Framework;
using System.Linq;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class ConcentratedPlaneTests {
        /// <summary>
        /// Returns the concentrated plane based on points
        /// (0; 0; 1), (1; 0; 1) and (0; 1; 1).
        /// </summary>
        private LinearAlgebra.ConcentratedPlane _sut() {
            var basePoints = new[] {
                new decimal[] { 0, 0, 1, },
                new decimal[] { 1, 0, 1, },
                new decimal[] { 0, 1, 1, },
            };
            var precision = new Helpers.HybridVectorsMocks().GetPrecision((decimal)0.001);
            var truePlane = new MathHelper().Linal.MakePlane(
                basePoints,
                precision
            ) as IOpenPlane;
            Assert.IsNotNull(truePlane);
            var concentratedPlane = new LinearAlgebra.ConcentratedPlane(
                truePlane,
                basePoints[0],
                precision,
                new LeftInverseFuzzySet(1),
                new MathHelper().Linal
            );
            return concentratedPlane;
        }

        [Test]
        public void Test_ConcentratedPlane_Distance() {
            // Arrange
            var concentratedPlane = this._sut();
            var results = new[] {
                new TestConcentratedResult(
                    new decimal[] { 0, 0, 1, },
                    0
                ),
            };

            // Act
            foreach (var result in results) {
                result.CalculatedDistance = concentratedPlane
                    .DistanceTo(result.Point);
            }

            // Assert
            foreach (var result in results) {
                Assert.AreEqual(
                    result.ExpectedDistance,
                    result.CalculatedDistance
                );
            }
        }

        [Test]
        public void Test_ConcentratedPlane_DistanceRelative() {
            // Arrange
            var concentratedPlane = this._sut();
            var points = new[] {
                new decimal[] { 0, 0, 1, },
                new decimal[] { 0, 0, 2, },
                new decimal[] { 0, 0, 3, },
            };

            // Act
            var distances = points
                .Select(p => concentratedPlane.DistanceTo(p))
                .ToList();

            // Assert
            for (int i = 0; i < distances.Count - 1; i++) {
                Assert.Less(distances[i], distances[i + 1]);
            }
        }

        private class TestConcentratedResult {
            public decimal[] Point;
            public decimal ExpectedDistance;
            public decimal CalculatedDistance;

            public TestConcentratedResult(
                decimal[] point, 
                decimal distance
            ) {
                this.Point = point;
                this.ExpectedDistance = distance;
            }
        }
    }
}
