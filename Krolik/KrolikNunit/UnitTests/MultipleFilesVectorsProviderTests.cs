﻿using Krolik;
using Krolik.Classifiers;
using Krolik.VectorsProviders;
using KrolikNunit.UnitTests.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class MultipleFilesVectorsProviderTests {
        private readonly HybridVectorsMocks _mocks = new HybridVectorsMocks();
        
        [Test]
        public void Test_NextIterateOverAllVectorsOnce() {
            // Arrange
            string dir = "dir", f1 = dir + "\\f1", f2 = dir + "\\f2";
            var sgn = this._mocks.GetContinuousSignature(3);
            Cls cls1 = sgn.DeserializeClass("1"), cls2 = sgn.DeserializeClass("2");
            var vectors = new List<IHybridVector> {
                _mocks.GetContinuousVector(sgn, 1, cls1, new decimal[] { 1, 2, 3, }),
                _mocks.GetContinuousVector(sgn, 2, cls1, new decimal[] { 4, 5, 6, }),
                _mocks.GetContinuousVector(sgn, 3, cls2, new decimal[] { -1, -2, -3, }),
                _mocks.GetContinuousVector(sgn, 4, cls2, new decimal[] { -4, -5, -6, }),
                _mocks.GetContinuousVector(sgn, 5, cls2, new decimal[] { -7, -8, -9, }),
            };
            var f1content = cls1.ToString() + Environment.NewLine +
                string.Join(Environment.NewLine, vectors
                    .Where(v => v.Classes.Contains(cls1))
                    .Select(v => string.Join(" ", v.ContinuumComponents)));
            var f2content = cls2.ToString() + Environment.NewLine +
                string.Join(Environment.NewLine, vectors
                    .Where(v => v.Classes.Contains(cls2))
                    .Select(v => string.Join(" ", v.ContinuumComponents)));
            var provider = new MultipleFilesVectorsProvider(
                sgn,
                dir,
                this._mocks.GetPrecision(),
                this._mocks.GetFileSystem(
                    new List<string> { dir, }, 
                    new List<string> { f1, f2, },
                    new Dictionary<string, string> {
                        [f1] = f1content,
                        [f2] = f2content,
                    }
                ).Object
            );

            for (int i = 0; i < 2; i++) {
                // Act
                var vectorsFromNext = new List<IHybridVector>();
                Idx? idx = null;
                while (true) {
                    var vector = provider.Next();
                    if (idx == null)
                        idx = vector.Index;
                    else {
                        if (idx == vector.Index)
                            break;
                    }
                    vectorsFromNext.Add(vector);
                }

                // Assert
                Assert.AreEqual(vectorsFromNext.Count, vectorsFromNext.Distinct().Count());
                this._mocks.AssertVectorsSetsAreEquivalent(
                    vectors,
                    vectorsFromNext
                );
            }
        }
    }
}
