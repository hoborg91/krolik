﻿using Krolik.Mathematics;
using KrolikNunit.Exceptions;
using NUnit.Framework;
using System.Linq;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class GcdCalculatorTests {//: CommonUnitTests<IGcdCalculator> {
        private IGcdCalculator _sut() {
            return new StraightGcdCalculator();
        }

        private IPrecisionProvider _precision = CustomPrecisionProvider.GetDefaultProvider();

        [Test]
        public void Test_GcdCalculator_TreatsZeros() {
            // Arrange.
            var calculator = this._sut();
            var numbersAllZeros = new decimal[] {
                0, 0, 0,
            };
            var numbersWithAlmostZeros = new decimal[] {
                0, 0, 2,
            };
            var numbersWithoutZeros = new decimal[] {
                15, 25, 30,
            };
            var numbersComplemented = new decimal[numbersWithoutZeros.Length * 2];
            numbersWithoutZeros.CopyTo(numbersComplemented, 0);

            // Act.
            var gcdFromAllZeros = calculator.Gcd(this._precision, numbersAllZeros);
            var gcdFromWithOneZero = calculator.Gcd(this._precision, numbersWithAlmostZeros);
            var gcdFromWithoutZeros = calculator.Gcd(this._precision, numbersWithoutZeros);
            var gcdFromComplemented = calculator.Gcd(this._precision, numbersComplemented);

            // Assert.
            Assert.AreEqual((decimal)1, gcdFromAllZeros);
            Assert.AreEqual(numbersWithAlmostZeros.Single(x => x != 0), gcdFromWithOneZero);
            Assert.AreEqual(gcdFromWithoutZeros, gcdFromComplemented);
        }

        [Test]
        [TestCase(1, 6, -2, 3)]
        public void Test_GcdCalculator_TreatsNegativeNumbers(int gcd, params int[] numbers) {
            if (numbers == null || numbers.Length == 0)
                throw new BadTestCaseException("The given numbers array must not be empty.");

            // Arrange.
            var expectedGcd = (decimal)gcd;
            var fromNumbers = numbers.Select(x => (decimal)x).ToArray();
            var calculator = this._sut();

            // Act.
            var result = calculator.Gcd(this._precision, fromNumbers);

            // Assert.
            Assert.AreEqual(expectedGcd, result);
        }
    }
}
