﻿using Cil.Common;
using Cil.Design;
using Krolik;
using Krolik.Classifiers;
using Krolik.Mathematics;
using Krolik.VectorsProviders;
using KrolikNunit.UnitTests.Helpers;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class ColumnsMovingWindowProviderTests {
        private HybridVectorsMocks _mocks = new HybridVectorsMocks();

        [OneTimeSetUp]
        public void Setup() {
            VectorSignature.ClearCache();
        }

        [Test]
        public void Test_ProviderFailesOnNotExistingPaths() {
            // Arrange
            var path = "";
            var precision = Mock.Of<IPrecisionProvider>();
            var fileSystemMock = new Mock<IFileSystemApi>();

            fileSystemMock
                .Setup(x => x.FileExists(It.Is<string>(p => p == path)))
                .Returns(false);
            fileSystemMock
                .Setup(x => x.DirectoryExists(It.Is<string>(p => p == path)))
                .Returns(false);

            // Act and assert
            IVectorsProvider provider;
            Assert.Throws<Exception>(() => 
                provider = new ColumnsMovingWindowProvider(
                    path,
                    Mock.Of<IVectorSignature>(),
                    precision,
                    fileSystemMock.Object
                )
            );
        }

        [Test]
        public void Test_ProviderTreatsOneFileWithSingleColumn() {
            // Arrange
            var path = "";
            var precision = this._mocks.GetPrecision();
            var fileSystemMock = this._mocks.GetFileSystem(
                null, 
                new List<string> { path, },
                null
            );
            var predefinedVectors = new List<IHybridVector>();
            var sgn = this._mocks.GetContinuousSignature(3);
            predefinedVectors.AddRange(this._prepareSingleColumnFile(
                path,
                sgn,
                sgn.DeserializeClass("1"),
                10,
                fileSystemMock)
            );

            // Act
            IVectorsProvider provider;
            provider = new ColumnsMovingWindowProvider(
                path,
                sgn,
                precision,
                fileSystemMock.Object
            );
            var vectorsFromGetAll = provider.GetAll();
            provider.Reset();
            var vectorsFromNext = new List<IHybridVector>();
            Idx? vectorIndex = null;
            while (true) {
                var vector = provider.Next();
                if (vectorIndex == null)
                    vectorIndex = vector.Index;
                else if (vectorIndex == vector.Index)
                    break;
                vectorsFromNext.Add(vector);
            }
            var splitted = provider.Split(0.5f, 0.25f);
            var splittedVectors = splitted.Item1.GetAll()
                .Union(splitted.Item2.GetAll())
                .ToList();
            var sampleSize = 0.5f;
            var sample = ((Krolik.Classifiers.ISamplable<IHybridVector>)provider)
                .GetProbabilitySample(sampleSize).GetAll();

            // Assert
            this._mocks.AssertVectorsSetsAreEquivalent(predefinedVectors, vectorsFromGetAll);
            this._mocks.AssertVectorsSetsAreEquivalent(predefinedVectors, vectorsFromNext);
            this._mocks.AssertVectorsSetsAreEquivalent(predefinedVectors, splittedVectors);
            Assert.That(
                sample.All(x => predefinedVectors.Contains(x, (IEqualityComparer<IHybridVector>)this._mocks.Comparer.Value))
            );
            provider.Dispose();
        }

        [Test]
        public void Test_ProviderTreatsMultipleFilesWithSingleColumns() {
            // Arrange
            string path = "", f1 = "1", f2 = "2";
            var precision = this._mocks.GetPrecision();
            var fileSystemMock = this._mocks.GetFileSystem(
                new List<string> { path, },
                new List<string> { path + f1, path + f2, },
                null
            );
            var predefinedVectors = new List<IHybridVector>();
            var sgn = this._mocks.GetContinuousSignature(3);

            predefinedVectors.AddRange(this._prepareSingleColumnFile(
                path + f1,
                sgn,
                sgn.DeserializeClass("1"),
                10,
                fileSystemMock)
            );

            predefinedVectors.AddRange(this._prepareSingleColumnFile(
                path + f2,
                sgn,
                sgn.DeserializeClass("2"),
                20,
                fileSystemMock)
            );

            // Act
            IVectorsProvider provider;
            provider = new ColumnsMovingWindowProvider(
                path,
                sgn,
                precision,
                fileSystemMock.Object
            );
            var vectorsFromGetAll = provider.GetAll();
            provider.Reset();
            var vectorsFromNext = new List<IHybridVector>();
            Idx? vectorIndex = null;
            while (true) {
                var vector = provider.Next();
                if (vectorIndex == null)
                    vectorIndex = vector.Index;
                else if (vectorIndex == vector.Index)
                    break;
                vectorsFromNext.Add(vector);
            }
            var splitted = provider.Split(0.5f, 0.25f);
            var splittedVectors = splitted.Item1.GetAll()
                .Union(splitted.Item2.GetAll())
                .ToList();
            var sampleSize = 0.5f;
            var sample = ((Krolik.Classifiers.ISamplable<IHybridVector>)provider)
                .GetProbabilitySample(sampleSize).GetAll();

            // Assert
            this._mocks.AssertVectorsSetsAreEquivalent(predefinedVectors, vectorsFromGetAll);
            this._mocks.AssertVectorsSetsAreEquivalent(predefinedVectors, vectorsFromNext);
            this._mocks.AssertVectorsSetsAreEquivalent(predefinedVectors, splittedVectors);
            Assert.That(
                predefinedVectors,
                Is.SupersetOf(sample).Using((IEqualityComparer<IHybridVector>)this._mocks.Comparer.Value)
            );
            provider.Dispose();
        }

        private IEnumerable<IHybridVector> _prepareSingleColumnFile(
            string filePath,
            IVectorSignature sgn,
            Cls cls, 
            int rowsCount,
            Mock<IFileSystemApi> fileSystemMock
        ) {
            var predefinedVectors = new List<IHybridVector>();
            var vectorIdx = -1;

            var rawData = Enumerable.Range(1, rowsCount);
            IBlockQueue<decimal> go = new BlockQueue<decimal>(3, 1);
            go.Push(rawData.Select(x => (decimal)x));
            while (go.Ready())
                predefinedVectors.Add(this._mocks.GetContinuousVector(
                    sgn, ++vectorIdx, cls, go.Pop(IBlockQueueIfNotReady.Exception)));
                //predefinedVectors.Add(Mock.Of<IHybridVector>(x => true
                //    && x.Classes == new[] { cls, }
                //    && x.ContinuumComponents == go.Pop(IBlockQueueIfNotReady.Exception)
                //    && x.DiscreteComponents == new DiscreteValue[0]
                //));
            var forReader = new[] { cls.ToString(), }
                .Concat(
                    rawData.Select(x => x.ToString())
                )
                .ToArray();

            fileSystemMock
                .Setup(x => x.OpenForRead(It.Is<string>(p => p == filePath)))
                .Returns(() => {
                    var forReaderPointer1 = -1;
                    var reader = new Mock<IStreamReader>();
                    reader
                        .Setup(x => x.ReadLine())
                        .Returns(() => {
                            if (forReaderPointer1 >= forReader.Length - 1)
                                return null;
                            forReaderPointer1++;
                            return forReader[forReaderPointer1];
                        });
                    return reader.Object;
                });

            return predefinedVectors;
        }
    }
}
