﻿using Cil.Common;
using Krolik;
using Krolik.Classifiers;
using Krolik.Classifiers.GpnClassifier;
using Krolik.Gpns;
using Krolik.Mathematics;
using Krolik.VectorsProviders;
using KrolikNunit.UnitTests.Helpers;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace KrolikNunit.UnitTests {
    [TestFixture]
    public class GpnFounderTests {
        private decimal _precision = 1e10m;

        /// <summary>
        /// Tests that the precision is enough for constructing
        /// the planes with SequentialGpnFounder.
        /// </summary>
        [Test]
        public void CheckPrecision() {
            // Arrange.
            var linal = new MathHelper().Linal;
            var strategy = new StandardSequentialGpnFounderStrategy(linal);
            IGpnFounder founder = new SequentialGpnFounder(
                strategy,
                new CalculatorProvider(), // TODO. Mock dependency?
                linal
            );
            var gpn = new GpnFactory().Make(0);
            var vectorsStubs = new[] {
                new { Components = new decimal[] { 1, 4, }, Cls = 1, },
                new { Components = new decimal[] { 2, 3, }, Cls = 1, },
                new { Components = new decimal[] { 4, 1, }, Cls = 1, },
                new { Components = new decimal[] { 2, 6, }, Cls = 2, },
                new { Components = new decimal[] { 3, 5, }, Cls = 2, },
                new { Components = new decimal[] { 4, 4, }, Cls = 2, },
                new { Components = new decimal[] { 5, 3, }, Cls = 2, },
                new { Components = new decimal[] { 6, 2, }, Cls = 2, },
                new { Components = new decimal[] { 7, 1, }, Cls = 2, },
            }
                .Select(x => new {
                    Components = x.Components
                        .Select(x1 => x1 / this._precision)
                        .ToArray(),
                    Cls = x.Cls,
                })
                .ToList();
            var vectors = vectorsStubs
                .Select((x, i) => Mock.Of<IHybridVector>(v => v.Index == (Idx)i
                    && v.ContinuumComponents == x.Components
                    && v.Classes == new[] { (Cls)x.Cls, }))
                .ToList()
                .ToCycle();
            var providerMock = new Mock<IVectorsProvider>();
            providerMock
                .Setup(x => x.Next())
                .Returns(() => vectors.Next().Current);
            providerMock
                .Setup(x => x.PrecisionProvider)
                .Returns(CustomPrecisionProvider.GetDefaultProvider());

            // Act and assert.
            var steps = 0;
            while (founder.Found(gpn, providerMock.Object)) {
                steps++;
                Assert.Less(steps, 19, "Too many steps.");
            }
            var allVertices = gpn.GetVertices().ToList();
            Assert.AreEqual(2, allVertices.Count, "Check precision settings.");
        }
    }
}
