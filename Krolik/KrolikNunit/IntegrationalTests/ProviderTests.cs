﻿using Krolik;
using Krolik.Classifiers;
using Krolik.VectorsProviders;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Krolik.Infrastructure;
using Cil.Common;
using System;
using Moq;
using Krolik.Mathematics;

namespace KrolikNunit.IntegrationalTests {
    /// <summary>
    /// Contains tests for different vectors providers.
    /// </summary>
    [TestFixture]
    public class ProviderTests : TestsWithFiles {
        [Test]
        public void Test_SimpleVerctorsProvider() {
            TestHelpers.WriteLineToConsole("Test_SimpleVerctorsProvider");

            var predefinedVectors = GetPredefinedVectors();
            var sgn = predefinedVectors.Item2;
            var path = SlashMaster.CorrectPath(@"Data\ProviderTest\SimpleVectorsProviderTest.txt");
            using (var sw = new StreamWriter(path)) {
                foreach (var vector in predefinedVectors.Item1) {
                    var discrComponentTypes = sgn.Components.OfType<DiscreteComponentType>().ToArray();
                    for (int i = 0; i < discrComponentTypes.Length; i++) {
                        sw.Write(discrComponentTypes[i].Serialize(vector.DiscreteComponents[i]) + " ");
                    }
                    //foreach (var component in vector.DiscreteComponents) {
                    //    sw.Write(component + " ");
                    //}
                    var contComponentTypes = sgn.Components.OfType<ContinuousComponentType>().ToArray();
                    for (int i = 0; i < contComponentTypes.Length; i++) {
                        sw.Write(contComponentTypes[i].Serialize(vector.ContinuumComponents[i]) + " ");
                    }
                    //foreach (var component in vector.ContinuumComponents) {
                    //    sw.Write(component + " ");
                    //}
                    sw.WriteLine(sgn.SerializeClass(vector.Classes.Single()));
                }
            }
            IProvider<IHybridVector> p = new SimpleVectorsProvider(
                path,
                predefinedVectors.Item2,
                CustomPrecisionProvider.GetDefaultProvider()
            );
            ComparePredefinedAndProvidedVectors(predefinedVectors.Item1, p);
        }

        [Test]
        public void Test_MultipleFilesVectorsProvider() {
            TestHelpers.WriteLineToConsole("Test_MultipleFilesVerctorsProvider");

            var predefinedVectors = GetPredefinedVectors();
            var path = SlashMaster.CorrectPath(@"Data\ProviderTest\MultipleFilesVectorsProviderTest\");
            PrepareFilesForMultipleFilesVectorsProvider(predefinedVectors.Item1, predefinedVectors.Item2, path);
            IProvider<IHybridVector> p = new MultipleFilesVectorsProvider(
                predefinedVectors.Item2,
                path,
                CustomPrecisionProvider.GetDefaultProvider(),
                new Cil.Design.FileSystemApi()
            );
            ComparePredefinedAndProvidedVectors(predefinedVectors.Item1, p);
        }

        [Test]
        public void Test_InCodeVectorsProvider() {
            TestHelpers.WriteLineToConsole("Test_InCodeVectorsProvider");

            var predefinedVectors = GetPredefinedVectors();
            IProvider<IHybridVector> provider = new InCodeVectorsProvider(
                CustomPrecisionProvider.GetDefaultProvider(), 
                predefinedVectors.Item1.ToArray());
            ComparePredefinedAndProvidedVectors(predefinedVectors.Item1, provider);
        }

        /// <summary>
        /// Tests that MultipleFilesVectorsProvider 1) creates additional
        /// files when GetProbabilitySample method is successfully called
        /// and 2) cleans that files after disposing.
        /// </summary>
        [Test]
        public void Test_MultipleFilesVectorsProvider_CreatesAndCleansSampleFiles() {
            TestHelpers.WriteLineToConsole("Test_MultipleFilesVectorsProvider_CreatesAndCleansSampleFiles");

            var predefinedVectors = GetPredefinedVectors();
            var path = SlashMaster.CorrectPath(@"Data\ProviderTest\MultipleFilesVectorsProviderTest\");
            PrepareFilesForMultipleFilesVectorsProvider(predefinedVectors.Item1, predefinedVectors.Item2, path);
            using(var p = new MultipleFilesVectorsProvider(
                predefinedVectors.Item2,
                path,
                CustomPrecisionProvider.GetDefaultProvider(),
                new Cil.Design.FileSystemApi()
            )) {
                var filesWithoutSamples = Directory.GetFiles(path);
                using (p.GetProbabilitySample(1)) {
                    var filesWithSamples = Directory.GetFiles(path);
                    Assert.IsTrue(filesWithSamples.Length > filesWithoutSamples.Length);
                }
                var filesAfterDispose = Directory.GetFiles(path);
                Assert.AreEqual(filesWithoutSamples.Length, filesAfterDispose.Length);
            }
        }

        private void PrepareFilesForMultipleFilesVectorsProvider(
            IList<IHybridVector> predefinedVectors, 
            IVectorSignature sgn,
            string path
        ) {
            path = SlashMaster.CorrectPath(path);
            foreach (var group in predefinedVectors.GroupBy(v => v.Classes.Single())) {
                var fileIndex = 1;
                var chunks = group.SplitInChunks(2);
                foreach (var chunk in chunks) {
                    var cls = sgn.SerializeClass(group.Key);
                    using (var sw = new StreamWriter(path + cls + fileIndex + ".txt")) {
                        sw.Write(cls);
                        foreach (var vector in chunk) {
                            sw.WriteLine();
                            var discrComponentTypes = sgn.Components.OfType<DiscreteComponentType>().ToArray();
                            for (int i = 0; i < discrComponentTypes.Length; i++) {
                                sw.Write(discrComponentTypes[i].Serialize(vector.DiscreteComponents[i]) + " ");
                            }
                            //foreach (var component in vector.DiscreteComponents) {
                            //    sw.Write(component + " ");
                            //}
                            var contComponentTypes = sgn.Components.OfType<ContinuousComponentType>().ToArray();
                            for (int i = 0; i < contComponentTypes.Length; i++) {
                                sw.Write(contComponentTypes[i].Serialize(vector.ContinuumComponents[i]) + " ");
                            }
                            //foreach (var component in vector.ContinuumComponents) {
                            //    sw.Write(component + " ");
                            //}
                        }
                    }
                    fileIndex++;
                }
            }
        }

        /// <summary>
        /// Produces a sample set of vectors.
        /// </summary>
        private Tuple<IHybridVector[], IVectorSignature> GetPredefinedVectors() {
            var signature = VectorSignature.AllDiscrete(4);
            var cls1 = signature.DeserializeClass("A");
            var cls2 = signature.DeserializeClass("B");
            var precision = Mock.Of<IPrecisionProvider>(x =>
                x.Divisor == (decimal)1e9);
            var predefinedVectors = new[] {
                HybridVectorsFactory.DefaultFactory.Make(0, new object[] { 0, 1, 2, 3 }.Select(x => x.ToString()).ToArray(), cls1, signature, precision),
                HybridVectorsFactory.DefaultFactory.Make(1, new object[] { -1, 1.0, 2000, 0 }.Select(x => x.ToString()).ToArray(), cls2, signature, precision),
                HybridVectorsFactory.DefaultFactory.Make(2, new object[] { 0, -1, -2, -3 }.Select(x => x.ToString()).ToArray(), cls2, signature, precision),
                HybridVectorsFactory.DefaultFactory.Make(3, new object[] { 3, 2, 1, 0 }.Select(x => x.ToString()).ToArray(), cls1, signature, precision),
                HybridVectorsFactory.DefaultFactory.Make(4, new object[] { 0, 1, 1, 0 }.Select(x => x.ToString()).ToArray(), cls1, signature, precision),
            };
            return Tuple.Create(predefinedVectors, signature);
        }

        /// <summary>
        /// Checks that the given multiset of vectors equals
        /// the multiset of vectors given by the specified
        /// provider. Here two vectors are equal if their
        /// components are equal and they have equal classes.
        /// </summary>
        private void ComparePredefinedAndProvidedVectors(
            IList<IHybridVector> predefinedVectors,
            IProvider<IHybridVector> provider
        ) {
            var providedVectors = new List<IHybridVector>();
            IHybridVector nextVector;
            while (!providedVectors.Contains(nextVector = provider.Next())) {
                providedVectors.Add(nextVector);
            }
            var groupByIndices = providedVectors
                .GroupBy(x => x.Index)
                .Select(x => x.Count())
                .ToList();
            Assert.IsTrue(groupByIndices.All(x => x == 1));
            //var v1 = predefinedVectors.First();
            //foreach (var v2 in providedVectors) {
            //    var eq = v1.Equals(v2);
            //}
            Assert.IsTrue(predefinedVectors.EqualsAsMultiset(
                providedVectors, (a, b) => a.EqualsComponents(b)
                        && a.Classes.ToList().EqualsAsMultiset(b.Classes.ToList()))
            );
        }
    }
}
