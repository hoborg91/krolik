﻿using Krolik;
using Krolik.Classifiers;
using Krolik.Classifiers.GpnClassifier;
using Krolik.Infrastructure;
using Krolik.Mathematics;
using KrolikNunit.UnitTests.Helpers;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace KrolikNunit.IntegrationalTests {
    [TestFixture]
    public class GpnClassifierTests : TestsWithShell {
        private GpnClassifier _sut() {
            var linal = new MathHelper().Linal;
            var fuzzySet = new Krolik.Mathematics.LeftTriangleFuzzySet(1);
            return new GpnClassifier(
                new SequentialGpnFounder(
                    new StandardSequentialGpnFounderStrategy(linal),
                    new CalculatorProvider(), // TODO. Mock dependency?
                    linal
                ),
                fuzzySet,
                new Krolik.Gpns.ClassicActivationStrategy(
                    new CustomPrecisionProvider((decimal)1e-9, 1)
                ),
                new Krolik.Gpns.FuzzyAndActivationStrategy(
                    new CustomPrecisionProvider((decimal)1e-9, 1),
                    fuzzySet
                )
            );
        }

        private long mockVectorIndex = 0;

        private IHybridVector CreateMockVector(byte cls, params decimal[] components) {
            mockVectorIndex++;
            return Mock.Of<IHybridVector>(v => true
                && v.Index == (Idx)(mockVectorIndex)
                && v.Classes == new List<Cls> { (Cls)cls, }
                && v.ContinuumComponents == components
            );
        }

        [Test]
        public void Test_GpnClassifierClassifyFuzzy() {
            // Arrange
            var precision = new CustomPrecisionProvider(1e-1m, 1m);
            var classifier = this._sut();
            var trainingSet = new Krolik.VectorsProviders.InCodeVectorsProvider(precision,
                CreateMockVector(1, 1m, 1m),
                CreateMockVector(1, 1m, 2m),
                CreateMockVector(2, 2m, 1m),
                CreateMockVector(2, 2m, 2m)
            );
            var fuzzyLogic = Fuzzy.Logic;

            // Act
            while (classifier.Prepare(trainingSet)) { }
            var result = classifier.ClassifyFuzzy(
                CreateMockVector(1, 1.2m, 1.5m),
                precision,
                new FuzzyClassificationAlgorithmFactory(fuzzyLogic).TopSuspiciousDirtyMinusOthers()
            );

            // Assert
            Assert.IsNotNull(result);
            var classes = result.Classes;
            Assert.IsNotNull(classes);
            Assert.IsTrue(classes.ContainsKey((Cls)1));
            Assert.IsTrue(classes.ContainsKey((Cls)2));
            Assert.IsTrue(classes[(Cls)1].FinalCertainty > classes[(Cls)2].FinalCertainty);
        }

        [TestCase("Scripts\\TwoLinesWithHole.txt", "Gpns\\TwoLinesWithHole.gpn", 2)]
        public void Test_GpnClassifier_FoundsExactNumberOfReceptors(string script, string gpnFile, int receptorsCount) {
            script = SlashMaster.CorrectPath(script);
            var output = this.RunScript(script, true);
            GpnClassifier gpnClassifier;
            using (var s = new System.IO.FileStream(SlashMaster.CorrectPath(gpnFile), System.IO.FileMode.Open)) {
                gpnClassifier = new StandardGpnClassifiersSerializer().DeserializeFromStream(s);
            }
            Assert.IsNotNull(gpnClassifier);
            var receptors = gpnClassifier.Network.GetVertices()
                .OfType<IReceptor>()
                .ToList();
            Assert.AreEqual(receptorsCount, receptors.Count, "The number of created receptors does not equal the expected one.");
        }
    }
}
