﻿using NUnit.Framework;
using System;
using System.IO;

namespace KrolikNunit.IntegrationalTests {
    public class TestsWithFiles {
        [SetUp]
        public void SetupBaseDirectory() {
            try {
                Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
            } catch (NullReferenceException) {
                Console.WriteLine("Exception during call \"Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);\"");
                Console.WriteLine("It seems that some test runners do not provide property TestContext.CurrentContext.TestDirectory.");
            }
        }
    }

    public static class TestHelpers {
        public static void WriteLineToConsole(string line) {
            Console.WriteLine(line);
        }
    }
}
