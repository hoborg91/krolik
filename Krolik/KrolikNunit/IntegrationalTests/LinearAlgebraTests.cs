﻿using Krolik.Infrastructure;
using Krolik.Mathematics;
using KrolikNunit.Exceptions;
using KrolikNunit.UnitTests.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KrolikNunit.IntegrationalTests {
    [TestFixture]
    public class LinearAlgebraTests : TestsWithFiles {
        private ILinearAlgebra _sut = new MathHelper().Linal;

        /// <summary>
        /// Tests the MakePlane method. Provides a set of points,
        /// makes a plane which should contain these points and
        /// check this and some other points.
        /// </summary>
        [TestCaseSource("_getCodedExamples")]
        [Test]
        public void Test_MakePlane(MakePlaneTestCase testCase) {
            TestHelpers.WriteLineToConsole("Test_MakePlane");

            // Arrange
            var linAl = this._sut;
            var precisionProvider = CustomPrecisionProvider.GetDefaultProvider();
            
            // Act, assert
            _test_MakePlane(linAl, precisionProvider, testCase);
        }

        /// <summary>
        /// Tests the MakePlane method. Provides a set of points,
        /// makes a plane which should contain these points and
        /// check this and some other points.
        /// </summary>
        [Test]//[Ignore("Too many calculations.")]
        //[TestCase("PlaneCase1.txt", 1, 0, true)]
        //[TestCase("PlaneCase2.txt", 1, 0, true)]
        //[TestCase("PlaneCase3.txt", 1, 0, true)]
        //[TestCase("PlaneCase4.txt", 1, 0, true)]
        [TestCase("PlaneCase5.txt", 1, 0, false)]
        [TestCase("PlaneCase6.txt", 1, 0, false)]
        //[TestCase("PlaneCaseZeros1.txt", 0, 1, false)]
        public void Test_MakePlane_FromFile(string file, int toCheckPointsCount, int externalPointsCount, bool normalize) {
            TestHelpers.WriteLineToConsole("Test_MakePlane_FromFile");

            // Arrange
            var linAl = this._sut;
            var precisionProvider = CustomPrecisionProvider.GetDefaultProvider();
            var examples = this._getExamplesFromFiles(
                precisionProvider, 
                normalize, 
                toCheckPointsCount, 
                externalPointsCount, 
                file
            );

            // Act, assert
            foreach (var example in examples) {
                _test_MakePlane(linAl, precisionProvider, example);
            }
        }

        private void _test_MakePlane(ILinearAlgebra linAl, IPrecisionProvider precision, MakePlaneTestCase example) {
            //TestHelpers.WriteLineToConsole("Test_MakePlane: " + examples.Count + " examples.");
            //int exampleIndex = 0;

            //foreach (var example in examples) {
                var points = example.BasePoints;

                TestHelpers.WriteLineToConsole("Test_MakePlane: example #" +
                    //(++exampleIndex) + ", " +
                    example.BasePoints.Count + " base points.");

                // Act
                var plane = linAl.MakePlane(points, precision);

                TestHelpers.WriteLineToConsole("Test_MakePlane: plane = {" + plane.ToString() + "}.");

                // Assert
                foreach (var point in points.Union(example.AlsoTestThesePoints)) {
                    var contains = plane.Contains(point, precision);
                    Assert.IsTrue(contains, "Inconsistent result.");
                }
                foreach (var point in example.ButThisPointsAreExternal) {
                    var contains = plane.Contains(point, precision);
                    Assert.IsFalse(contains, "Inconsistent result.");
                }
            //}
        }

        private static IList<MakePlaneTestCase> _getCodedExamples() {
            var examples = new List<MakePlaneTestCase> {
                new MakePlaneTestCase {
                    BasePoints = new[] {
                        new decimal[] { 0, 1, },
                        new decimal[] { 1, 0, },
                    },
                    AlsoTestThesePoints = new[] {
                        new decimal[] { -1, 2, },
                        new decimal[] { 2, -1, },
                    },
                    ButThisPointsAreExternal = new[] {
                        new decimal[] { 0, 0, },
                    },
                },
                new MakePlaneTestCase {
                    BasePoints = new[] {
                        new decimal[] { 1, 1, },
                        new decimal[] { 2, 2, },
                    },
                    AlsoTestThesePoints = new[] {
                        new decimal[] { 0, 0, },
                        new decimal[] { -1, -1, },
                    },
                    ButThisPointsAreExternal = new[] {
                        new decimal[] { 0, 1, },
                    },
                },
                new MakePlaneTestCase {
                    BasePoints = new[] {
                        new decimal[] { 10, 0, 0, },
                        new decimal[] { 0, 30, 0, },
                        new decimal[] { 0, 0, 20, },
                    },
                    AlsoTestThesePoints = new[] {
                        new decimal[] { 5, 0, 10, },
                    },
                    ButThisPointsAreExternal = new[] {
                        new decimal[] { 0, 1, 2, },
                    },
                },
                new MakePlaneTestCase {
                    BasePoints = new[] {
                        new decimal[] { 0, 5, },
                        new decimal[] { 5, 0, },
                    },
                    AlsoTestThesePoints = new[] {
                        new decimal[] { 1, 4 },
                    },
                    ButThisPointsAreExternal = new[] {
                        new decimal[] { 0, 1, },
                    },
                },
                new MakePlaneTestCase {
                    BasePoints = new[] {
                        new decimal[] { 0, 0, 0, },
                        new decimal[] { 1, 0, 0, },
                        new decimal[] { 0, 0, 1, },
                    },
                    AlsoTestThesePoints = new[] {
                        new decimal[] { 1, 0, 1, },
                    },
                    ButThisPointsAreExternal = new[] {
                        new decimal[] { 0, 1, 0, },
                    },
                },
            };
            return examples;
        }

        /// <summary>
        /// Retrieves points for plane construction and checking.
        /// </summary>
        /// <param name="precision">The normalizing coefficient.</param>
        /// <param name="normalize">If true, use the normalizing coefficient.</param>
        /// <param name="toCheckPointsCount">Treat first lines as points for checking.</param>
        /// <param name="givenFiles">Files with points.</param>
        /// <returns></returns>
        private IList<MakePlaneTestCase> _getExamplesFromFiles(
            IPrecisionProvider precision, 
            bool normalize = true, 
            int toCheckPointsCount = 1, 
            int outerPointsCount = 0,
            params string[] givenFiles
        ) {
            if (toCheckPointsCount < 0)
                throw new ArgumentException("The 'toCheckPointsCount' must be >= 0.");
            var examples = new List<MakePlaneTestCase>();
            var makePoint = (Func<string, decimal[]>)(x => x
                .Split(' ')
                .Select(x1 => {
                    decimal r = 0;
                    if (new[] { typeof(System.Numerics.BigInteger), }.Contains(r.GetType())) {
                        var indexOfComma = x1.IndexOf(',');
                        r = decimal.Parse(indexOfComma < 0
                            ? x1
                            : x1.Substring(0, indexOfComma));
                    } else {
                        r = decimal.Parse(x1);
                        if (normalize)
                            r /= precision.Divisor;
                    }
                    return r;
                })
                .ToArray());
            var dir = SlashMaster.CorrectPath("TestCases\\PlaneCalculationFailure");
            if (!Directory.Exists(dir))
                throw new TestIsNotReadyException("The test needs directory \"" + dir + "\".");
            var files = Directory.GetFiles(dir);
            var needFiles = givenFiles
                .Select(x => SlashMaster.CorrectPath(dir + "\\" + x))
                .ToList();
            foreach (var needFile in needFiles) {
                if (!files.Contains(needFile))
                    throw new TestIsNotReadyException("The test needs data file \"" + needFile + "\".");
            }
            foreach (var file in needFiles) {
                TestHelpers.WriteLineToConsole("Test_MakePlane: get examples from file \"" + file + "\".");
                using (var sr = new StreamReader(file)) {
                    string line;
                    var toCheck = new List<decimal[]>();
                    for (int i = 0; i < toCheckPointsCount; i++) {
                        line = sr.ReadLine();
                        toCheck.Add(makePoint(line));
                    }
                    var outer = new List<decimal[]>();
                    for (int i = 0; i < outerPointsCount; i++) {
                        line = sr.ReadLine();
                        outer.Add(makePoint(line));
                    }
                    var points = new List<decimal[]>();
                    while ((line = sr.ReadLine()) != null) {
                        points.Add(makePoint(line));
                    }
                    examples.Add(new MakePlaneTestCase {
                        BasePoints = points,
                        AlsoTestThesePoints = toCheck,
                        ButThisPointsAreExternal = outer,
                    });
                }
            }
            return examples;
        }

        [Test]
        public void Test_MakePlain_Orthogonal() {
            TestHelpers.WriteLineToConsole("Test_MakePlain_Orthogonal");

            // Arrange
            var linAl = this._sut;
            var precisionProvider = CustomPrecisionProvider.GetDefaultProvider();

            var testSet = new[] {
                new {
                    TotalDims = 2,
                    DimIndex = 1,
                    Value = (decimal)1,
                    In = new[] {
                        new decimal[] { -1, 1, },
                        new decimal[] { 0, 1, },
                        new decimal[] { 1, 1, },
                    },
                    Out = new[] {
                        new decimal[] { 0, 0, },
                        new decimal[] { 0, 2, },
                    },
                },
                new {
                    TotalDims = 2,
                    DimIndex = 0,
                    Value = (decimal)1,
                    In = new[] {
                        new decimal[] { 1, 0, },
                        new decimal[] { 1, 1, },
                        new decimal[] { 1, -1, },
                    },
                    Out = new[] {
                        new decimal[] { 0, 0, },
                        new decimal[] { 2, 0, },
                    },
                },
                new {
                    TotalDims = 3,
                    DimIndex = 2,
                    Value = (decimal)1,
                    In = new[] {
                        new decimal[] { -1, 0, 1, },
                        new decimal[] { 0, 1, 1, },
                        new decimal[] { 1, -1, 1, },
                    },
                    Out = new[] {
                        new decimal[] { -1, 0, 0, },
                        new decimal[] { 0, 1, 2, },
                    },
                }
            };

            foreach (var testCase in testSet) {
                var plane = linAl.MakePlane(
                    testCase.TotalDims,
                    testCase.DimIndex,
                    testCase.Value);
                foreach (var pointIn in testCase.In) {
                    Assert.IsTrue(plane.Contains(pointIn, precisionProvider));
                }
                foreach (var pointOut in testCase.Out) {
                    Assert.IsFalse(plane.Contains(pointOut, precisionProvider));
                }
            }
        }

        [Test]
        public void Test_DistanceFromPointToPlane() {
            // Arrange
            var anchors = new[] {
                new decimal[] { 0, 0, },
                new decimal[] { 1, 0, },
            };
            var precision = CustomPrecisionProvider.GetDefaultProvider();

            // Act
            var plane = this._sut.MakePlane(anchors, precision);

            // Assert
            foreach (var anchor in anchors) {
                var distance = plane.DistanceTo(anchor);
                Assert.AreEqual((decimal)0, distance);
            }
            foreach (var example in new[] {
                new { Point = new decimal[] { 2, 0, }, Distance = (decimal)0, },
                new { Point = new decimal[] { -1, 0, }, Distance = (decimal)0, },
                new { Point = new decimal[] { 0, 1, }, Distance = (decimal)1, },
                new { Point = new decimal[] { 0, -2, }, Distance = (decimal)2, },
            }) {
                var distance = plane.DistanceTo(example.Point);
                Assert.AreEqual(example.Distance, distance);
            }
        }

        public class MakePlaneTestCase {
            public IList<decimal[]> BasePoints = new List<decimal[]>();
            public IList<decimal[]> AlsoTestThesePoints = new List<decimal[]>();
            public IList<decimal[]> ButThisPointsAreExternal = new List<decimal[]>();
        }
    }
}
