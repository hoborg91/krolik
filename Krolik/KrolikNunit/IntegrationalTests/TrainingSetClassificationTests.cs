﻿using Cil.Common;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KrolikNunit.IntegrationalTests {
    public class TestsWithShell : TestsWithFiles {
        protected List<string> RunScript(string scriptFile, bool strict = true) {
            var output = new List<string>();
            var shell = new KrolikShell.ShellBase();
            var cmds = new[] {
                "strictness " + (strict ? "strict" : "soft"),
                "load " + scriptFile,
                "exit",
            };
            var cmdIndex = 0;
            IReadWrite<string> rw = new CustomReadWrite<string>(
                (Func<string>)(() => {
                    var r = cmds[cmdIndex++];
                    Console.WriteLine(r);
                    return r;
                }),
                (Action<string>)((m) => {
                    output.Add(m);
                    Console.WriteLine(m);
                })
            );
            shell.Run(rw);
            return output;
        }
    }

    /// <summary>
    /// Contains tests for GPN.
    /// </summary>
    [TestFixture]
    public class TrainingSetClassificationTests : TestsWithShell {
        /// <summary>
        /// Checks that after training in strict mode the
        /// GPN classifies training set with 100% quality.
        /// </summary>
        [Test]
        [TestCase("Scripts\\Script-issue-1-cartesian.txt")]
        [TestCase("Scripts\\Script-issue-1-sequential.txt")]
        [TestCase("Scripts\\TwoLinesWithHole.txt")]
        public void Test_Gpn_ClassifiesTrainingSetWith100QualityInStrictMode(string scriptFile) {
            TestHelpers.WriteLineToConsole("Test_Gpn_ClassifiesTrainingSetWith100QualityInStrictMode(" + scriptFile + ")");
            this.Issue1Test(scriptFile);
        }

        /// <summary>
        /// Tests GPN serialization and deserialization (save/load).
        /// </summary>
        [Test]
        public void Test_Gpn_SerializationAndDeserialization() {
            TestHelpers.WriteLineToConsole("Test_Gpn_SerializationAndDeserialization");

            System.IO.File.Delete("SerializeAndDeserializeTest.gpn");
            foreach (var scriptFile in new string[] {
                "Scripts\\SerializeAndDeserialize.txt",
            }) {
                this.Issue1Test(scriptFile);
            }
        }

        private void Issue1Test(string scriptFile) {
            Console.WriteLine("Start Issue1Test(" + scriptFile + ").");
            var output = this.RunScript(scriptFile, true);
            var qualityToken = "Quality:";
            var qualitiesCount = 0;
            foreach (var line in output) {
                if (!line.Contains(qualityToken))
                    continue;
                qualitiesCount++;
                var localQualityString = new Func<string, string>[] {
                    (Func<string, string>)(x => x.Substring(x.IndexOf(qualityToken) + qualityToken.Length)),
                    (Func<string, string>)(x => x.Substring(0, x.IndexOf('.'))),
                    (Func<string, string>)(x => x.Trim()),
                }.Aggregate(
                    line,
                    (x, f) => f(x)
                );
                var quality = float.Parse(localQualityString);
                Assert.AreEqual(
                    1f,
                    quality,
                    "Classification quality for training or control set of \"" + scriptFile + "\" is less than 1."
                );
            }
            Assert.Greater(qualitiesCount, 0, "Treatment of \"" + scriptFile + "\" gave no information about classification quality.");
        }
    }
}
