﻿using KrolikTranslator;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace KrolikNunit.LibrariesAndPackagesTests {
    [TestFixture]
    public class TranslatorTests {
        /// <summary>
        /// Tests that the translator supports basic expression syntax and method calls.
        /// </summary>
        [Test]
        public void Test_SimpleMethod() {
            // Arrange
            string instance = "i";
            string method = "m";
            var translator = new ExpressionsTranslator<ExpressionStub>(new ExpressionsFactoryForTests());

            // Act
            var translated = translator.Translate($"{instance}.{method}()");

            // Assert
            var testMethod = translated as MethodStub;
            Assert.IsNotNull(testMethod);
            Assert.AreEqual(testMethod.MethodName, method);
        }

        /// <summary>
        /// Tests that the translator supports basic expression syntax,
        /// method calls and literals (string, number, boolean).
        /// </summary>
        [Test]
        [TestCase("true")]
        [TestCase("false")]
        public void Test_NamedParameters(string literalTextRepresentation) {
            // Arrange
            string instance = "i";
            string method = "m";
            string 
                paramName1 = "p1", 
                paramName2 = "p2", 
                paramName3 = "p3";
            string paramValue1 = "hello";
            string paramValue2Serialized = "1e-2";
            string paramValue3Serialized = literalTextRepresentation;

            var translator = new ExpressionsTranslator<ExpressionStub>(new ExpressionsFactoryForTests());

            // Act
            var translated = translator.Translate($"{instance}.{method}(" +
                $"{paramName1}: \"{paramValue1}\", " +
                $"{paramName2}: {paramValue2Serialized}, " +
                $"{paramName3}: {paramValue3Serialized}" +
                ")");

            // Assert
            var testMethod = translated as MethodStub;
            Assert.IsNotNull(testMethod);
            Assert.AreEqual(testMethod.MethodName, method);
            Assert.AreEqual(testMethod.Args.Count, 3);
            foreach (var paramName in new[] { paramName1, paramName2, paramName3, }) {
                Assert.IsTrue(testMethod.Args.ContainsKey(paramName));
            }

            var lit1 = testMethod.Args[paramName1] as LiteralStub;
            var lit2 = testMethod.Args[paramName2] as LiteralStub;
            var lit3 = testMethod.Args[paramName3] as LiteralStub;
            Assert.IsNotNull(lit1);
            Assert.IsNotNull(lit2);
            Assert.IsNotNull(lit3);
            Assert.AreEqual(paramValue1, lit1.Text);
            Assert.AreEqual(paramValue2Serialized, lit2.Text);
            Assert.AreEqual(paramValue3Serialized, lit3.Text);
        }

        private abstract class ExpressionStub : IKrolikExpression {

        }

        private class MethodStub : ExpressionStub {
            public string MethodName { get; private set; }
            public Dictionary<string, ExpressionStub> Args { get; private set; }

            public MethodStub(string methodName, Dictionary<string, ExpressionStub> args) {
                this.MethodName = methodName;
                this.Args = args;
            }
        }

        private class LiteralStub : ExpressionStub {
            public string Text { get; private set; }

            public LiteralStub(string textRepresentation) {
                this.Text = textRepresentation;
            }
        }

        private class VariableStub : ExpressionStub { }

        private class ExpressionsFactoryForTests : IKrolikExpressionsFactory<ExpressionStub> {
            public ExpressionStub MakeInstantiation(string cls, ExpressionStub[] positionParameters, Dictionary<string, ExpressionStub> namedParameters) {
                throw new NotImplementedException();
            }

            public ExpressionStub MakeLiteral(string textRepresentation) {
                if (textRepresentation.StartsWith("\"") && textRepresentation.EndsWith("\""))
                    textRepresentation = textRepresentation.Substring(1, textRepresentation.Length - 2);
                return new LiteralStub(textRepresentation);
            }

            public ExpressionStub MakeMethodCall(string instance, string method, ExpressionStub[] positionParameters, Dictionary<string, ExpressionStub> namedParameters) {
                return new MethodStub(method, namedParameters);
            }

            public ExpressionStub MakeVariable(string variableName) {
                return new VariableStub();
            }
        }
    }
}
