﻿namespace KrolikNunit.Exceptions {
    internal class BadTestCaseException : KrolikNunitException {
        public BadTestCaseException(string message) 
            : base(message)
        {

        }
    }
}
