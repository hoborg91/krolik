﻿using System;

namespace KrolikNunit.Exceptions {
    internal class KrolikNunitException : Exception {
        public KrolikNunitException(string message) : base(message) {

        }
    }
}
