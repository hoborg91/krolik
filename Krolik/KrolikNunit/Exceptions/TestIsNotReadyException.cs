﻿namespace KrolikNunit.Exceptions {
    internal class TestIsNotReadyException : KrolikNunitException {
        public TestIsNotReadyException(string message) :
            base(message)
        {

        }
    }
}
